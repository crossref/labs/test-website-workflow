# Dockerfile for serving static Hugo website content
FROM public.ecr.aws/nginx/nginx
COPY public/ /usr/share/nginx/html
COPY nginx/default.conf /etc/nginx/conf.d/
