+++
title = "Transferring responsibility for titles and DOIs"
date = "2021-10-21"
draft = false
author = "Isaac Farley"
aliases = [
    "/education/member-setup/metadata-manager/transferring-titles/",
    "/education/member-setup/metadata-manager/transferring-titles",
    "/education/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois",
    "/education/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/",
    "/help/transfers/",
    "/help/transfers",
    "/education/member-setup/metadata-manager/transferring-titles",
    "/education/member-setup/metadata-manager/transferring-titles/",
    "/documentation/member-setup/metadata-manager/transferring-titles",
    "/documentation/member-setup/metadata-manager/transferring-titles/",
    "/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/",
    "/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/"
]
type = "documentation"
layout = "documentation_single"
education_section = ["register-maintain-records", "creating-and-managing-dois", "transferring-responsibility-for-dois"]
identifier = "document/register-maintain-records/creating-and-managing-dois/transferring-responsibility-for-dois"
rank = 4
weight = 60802
+++

We enforce a concept of ownership for the titles you register through us.

We allow members to freely register records for titles that do not exist in our system. When the first submission for that title is processed, a title record is added to our database. This title record ties the title to the prefix belonging to the first registrant. The member who owns that prefix is then the only member allowed to create new DOIs for that title (or update the metadata on existing DOIs for that title).

If a title is acquired by a member with a different prefix, we have two options. The most common is that we update the title record to associate the title with the acquiring member's prefix going forward. But if the acquiring member has acquired *all* of the disposing members titles, we can also transfer the disposing members entire prefix over to the acquiring member.

On this page, find out more about:
* [Title transfers - updating a title record to a new prefix belonging to a different member](#00139)
* [Prefix transfers - moving an entire prefix and all titles to a different member](#00558)
* [Requesting a title transfer](#00559) (and [what to do next)](#00560)
* [Requesting a prefix transfer](#00140)
* [Title dispute resolutions](#00142)

## Title transfers <a id='00139' href='#00139'><i class='fas fa-link'></i></a>

In a standard title transfer, Member A acquires a single title from Member B. We transfer title ownership and all relevant reports over to Member A's prefix.

Member A must then register new content for that title on their own prefix. But they also inherit control of all the existing DOIs for this title, even though these DOIs are on Member B's prefix. Those existing DOIs will not change.

Member A can now update any metadata associated with the existing DOIs - for example, the resolution URL. They will also now show as the publisher in the metadata for these DOIs. Member A should continue to display and use the existing DOIs and they SHOULD NOT register a new DOI for content that already has a DOI. Once a DOI has been registered for an item, that DOI needs to remain the persistent identifier for that item - forever. Registering new DOIs for content that already has DOIs contravenes clause 2 h 3 of the [Crossref membership terms](/membership/terms), and causes confusion and inaccuracies for the organizations and individuals [using Crossref metadata](/services/metadata-retrieval/).

Here's an example of how this works. Let's say that DOI `10.1234/abcd` is for an article in a title that's acquired by a new member. The new members prefix is `10.5678`, and so ownership for that whole title is assigned to prefix `10.5678`.

This means that the existing DOI for that article will continue to be `10.1234/abcd`. The difference is that the member responsible for prefix `10.5678` is also able to update the metadata record for `10.1234/abcd`. For example, they may need to update the resolution URL to point at their website.

Backfile and current DOIs for that journal may, therefore, have different prefixes — and that’s OK!

Learn more about [what can often change, but always stays the same?](/blog/what-can-often-change-but-always-stays-the-same/)

### Transferring a title without taking responsibility for existing DOIs <a id='00138' href='#00138'><i class='fas fa-link'></i></a>

Typically, when a title is acquired by a member, all existing content is also acquired. We move the title itself, AND ownership of all existing records for that title to the acquiring member.

However, we can also assign ownership to individual records within a title. This is sometimes necessary when content ownership or hosting responsibility is assigned to different chunks of content for the same title.

For example, current issues of *Journal A* may be published by a member with prefix `10.1234`. Issues of *Journal A* published prior to 2010 are hosted and maintained by a member with prefix `10.5678`. *Journal A* is owned by prefix `10.1234`, but the member with prefix `10.5678` retains control of the back issue DOIs owned by prefix `10.5678`.

## Prefix transfers <a id='00558' href='#00558'><i class='fas fa-link'></i></a>

In a prefix transfer, Member C acquires Member D and all their titles. We move the entire prefix belonging to Member D (and all relevant reports) over to Member C. Member C can then continue to assign DOIs on Member D’s old prefix (the original prefix). If Member C uses a service provider to deposit metadata on their behalf, we will simply enable the service provider's account credentials to work with the newly acquired prefix.

## Requesting a title transfer<a id='00559' href='#00559'><i class='fas fa-link'></i></a>

There are several steps to a title transfer.

**1. Disposing and acquiring publisher confirm that all existing DOIs have definitely been registered with Crossref and agree financial arrangement for registration of DOIs.**

Prior to the transfer, it's important to make sure that any DOIs that have been displayed by the disposing publisher on their prefix have definitely been successfully registered with Crossref. If there are DOIs that are displayed on their prefix that haven't been registered, it will be very complicated to get them registered after the title transfer. However, this will have to be done, as once a DOI has been displayed, it has to be registered with Crossref to preserve the scholarly record.

In advance of sending us the title transfer request, make sure to get confirmation from the disposing publisher that they have definitely successfully registered all DOIs for this title that have already been displayed on their site.

**2. Disposing or acquiring publisher [contacts](/contact/) our support team to request a title transfer.**  

We need to receive a title transfer notification to confirm that the current owners are happy with the transfer. There are several different ways to do this:

* Option A (preferred): If a title transfer has been posted to the [Enhanced Transfer Alerting Service (TAS)](https://journaltransfer.issn.org/) let us know and we’ll proceed with the transfer without further confirmation.
* Option B: If you don't participate in ETAS, please [send us](mailto:member@crossref.org) confirmation that the disposing publisher is aware of and agrees with the ownership transfer. The confirmation may be a forwarded email from the disposing publisher to the acquiring publisher acknowledging the transfer. The forwarded email must contain the original sender details.
* Option C: Alternatively, if you have a letter on the letterhead of the disposing publisher or a press release announcement confirming the transfer that works, too.

Whichever option you use, please be specific about what is being transferred - include ISSNs, ISBNs, and when you need the transfer to occur (if applicable). Do be specific about which prefix the title is being transferred to, as some publishers have more than one prefix. Let us know if this is a transfer of the entire title and all associated DOIs, or just a transfer for future content.

(NB: We used to allow disposing publishers to transfer titles themselves through the Metadata Manager tool, but this service has been deprecated).

**3. We update the title record in our system and confirm when this is complete.**

We will update the title record to associate that title with the acquiring publisher's prefix going forward. This means that the acquiring publisher will be able to register new DOIs on their own prefix in the future.

After the transfer is complete, it's extremely important that the acquiring publisher doesn't register new DOIs for content that already has an existing DOI registered by the disposing publisher. Once a DOI has been registered for an item, that DOI needs to remain the persistent identifier for that item - forever. Registering new DOIs for content that already has DOIs contravenes clause 2 h 3 of the Crossref membership terms. The acquiring publisher should continue to display and use the existing DOIs, despite the fact that they aren't on their prefix.  However, the acquiring publisher will now be able to update the metadata associated with these existing DOIs, even though they aren't on their prefix.

We will provide the acquiring publisher with a link to all the DOIs that have been previously registered for this title.

**4. Acquiring publisher updates the metadata on existing DOIs as required**

At this point, the acquiring publisher will be able to update existing metadata records on the disposing publisher prefix, and create new records on their own prefix.

As the acquiring publisher, you should review the full metadata records provided by the disposing publisher, and [remove or update any member-specific metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-after-title-transfer/) such as text and data mining license and full-text URLs, Similarity Check full-text URLs, or Crossmark data. If the metadata supplied by the previous member is complete and accurate, you’ll only need to [update the resolution URLs](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00171) (the URLs associated with each DOI to point to your content).

Learn more about our [top tips for a pain-free title transfer](/blog/before-during-and-after-a-journey-through-title-transfers/).


## Requesting a prefix transfer<a id='00140' href='#00140'><i class='fas fa-link'></i></a>

DOI prefixes may be moved from one member to another with the consent of the current prefix owner. This may happen as part of a merger or acquisition. Prefixes may also be moved from one DOI registration agency to another. Please [contact us](/contact/) to start a prefix transfer.

### Prefix permissions<a id='00141' href='#00141'><i class='fas fa-link'></i></a>

If a prefix moves between members, note that the permissions associated with all DOIs currently owned by that prefix will transfer as well. This includes permissions related to Cited-by matches. You may transfer ownership of individual DOIs to a different prefix as needed.

## Title dispute resolution<a id='00142' href='#00142'><i class='fas fa-link'></i></a>

Title ownership may come into dispute when two members claim ownership of a single publication. This may occur when content is registered by members through an agreement with a society, and the society takes up an agreement with a new publisher. Or perhaps there is just a disagreement over who has the current rights to register the content - see term 2c of our [membership terms](/membership/terms/):

> **Rights to Content**. The Member will not deposit or register Metadata for any Content for which the Member does not have legal rights to do so.

As described above, the ‘owning’ member in our system is the member who is currently registering content for that publication. They have the ability to continue registering content for that title. The ‘disputing’ member is the member who wishes to register content for that journal going forward, but is unable to. Here's how this situation needs to be handled:

1. The disputing member will notify us of the title dispute - an [email to Support](/contact/) is sufficient
2. We’ll contact the owning member informing them of the title dispute.
3. If the owning member agrees that their ownership is incorrect or if they do not respond within 10 working days, we will re-assign title and record ownership to the disputing member, who then becomes the new owning member.
4. If the owning member challenges the claim, the two parties must resolve the issue together within 90 days. We will move title ownership under instruction from the owning member, or under direction from legal authority.
5. If the dispute is not resolved within 90 days, the disputing member can request that we remove the ability for any party to register further content for the publication under dispute until this is resolved.
6. This remains the case until we receive notice of a legal conclusion.
