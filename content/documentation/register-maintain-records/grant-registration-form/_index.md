+++
title = "Grant registration form"
date = "2022-12-07"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "grant-registration-form"]
identifier = "documentation/register-maintain-records/grant-registration-form"
aliases = [
]
rank = 4
weight = 50450
+++

The grant registration form can be used to deposit metadata for grant records. You do not need any knowledge of XML to use it. You can save your grant records to your local machine and upload to the form later to make edits. You can also save partial records to be used as templates in the future.

## How to use the grant registration form<a id='00100' href='#00100'><i class='fas fa-link'></i></a>

Start at the [grants registration form](https://beta.crossref.org/records) and choose to create a new record or load a record you’ve already created using this form. If this is the first time you’ve used this form, you’ll choose New Record.

<figure><img src='/images/documentation/grants-start-submission.png' alt='Start a new record submission page' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image117">Show image</button>
<div id="image117" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/grants-start-submission.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Create a new record<a id='00200' href='#00200'><i class='fas fa-link'></i></a>

Give your grant record a name. This is the name for the file you’ll download to your computer for future edits or use - so make it something descriptive and useful, like the grant number. This name doesn’t get deposited with Crossref or appear in any metadata.

Add the metadata associated with your grant.

Download your record to your local computer for future edits. The form will download as a .json file, with the name you gave it in the beginning.


### Submit your record <a id='00300' href='#00300'><i class='fas fa-link'></i></a>

Click submit at the bottom of the form, and enter your [Crossref account credentials](/documentation/member-setup/account-credentials/). The submission will be made immediately and a success message will appear on the screen. You can also download the record from this page.

<figure><img src='/images/documentation/grants-success.png' alt='Grant submission success page' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image117">Show image</button>
<div id="image117" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/grants-success.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


### Load a saved record <a id='00400' href='#00400'><i class='fas fa-link'></i></a>

If you’ve used this form before to create a grant record, you can load your saved copy to make edits and redeposit. Start at the [grants registration form](https://beta.crossref.org/records) and choose Load Record. Select the appropriate .json file from your computer and click Open. Note: the record you load must be a .json file previously downloaded from the grant registration form.

Once the form is loaded, you can make edits to it, download a new version to your local machine, and submit your record to update the metadata with us.


### Create a template <a id='00500' href='#00500'><i class='fas fa-link'></i></a>

You can partially complete a form and download it for use as a template in the future. As an example, your depositor information (name, email address) and funder information (funder name, funder ID) is likely to be the same across all submissions, so you might complete just those parts of the form, download the record, and load it each time you need to submit a grant record.
