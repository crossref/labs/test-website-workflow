+++
title = "Updating your metadata"
date = "2022-05-20"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "maintaining-your-metadata", "updating-your-metadata"]
identifier = "documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata"
rank = 4
weight = 140101
aliases = [
  "/education/metadata-stewardship/maintaining-your-metadata/updating-your-metadata",
  "/education/metadata-stewardship/maintaining-your-metadata/updating-your-metadata/",
  "/faqs/updating-metadata",
  "/faqs/updating-metadata/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/updating-your-metadata/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/updating-your-metadata",

]
+++

Because DOIs are designed to be persistent, a DOI string can’t be changed once registered, and [DOIs cannot be deleted](/documentation/content-registration/creating-and-managing-dois/changing-or-deleting-dois/).

However, you can update metadata associated with your registered DOIs, and we encourage you to do this as often as required. No fees are charged for updating existing metadata records.

To add, change, or remove metadata from your existing records, you generally just need to [resubmit your complete metadata record](#00168) with the changes included.

There are a few exceptions where you can't just resubmit your records using your chosen content registration method, or where there is an easier option, and you need to do something different:

## Exceptions to standard metadata updates<a id='00627' href='#00627'><i class='fas fa-link'></i></a>

* **Crossmark**: if you want to update or replace (not just delete) metadata for Crossmark and your deposit method is web deposit form or direct deposit of XML, you can't just resubmit the new information. Follow a two-step process to (1) actively delete the relevant metadata by redepositing the record but with the Crossmark field blank ([markup example](/documentation/register-maintain-records/maintaining-your-metadata/metadata-removal-markup-guide#00181) for users of direct deposit of XML), then (2) add the new Crossmark metadata in another redeposit.
* **Funding, license, and Similarity Check full-text URLs**: you can add this metadata to multiple DOIs at once by uploading a csv file to the web deposit form using the [supplemental metadata upload](/documentation/content-registration/web-deposit-form#00318).
* **References** can be added in a few different ways - learn more about [adding references to your metadata record](/documentation/register-maintain-records/maintaining-your-metadata/add-references/)
* **Relationships**: if you want to update or replace (not just delete) metadata for [relationships](/documentation/content-registration/structural-metadata/relationships/) and your deposit method is direct deposit of XML, you can't just resubmit the new information. Follow a two-step process to (1) actively delete the relevant metadata by sending us a [full redeposit but with the relationship field blank](/documentation/register-maintain-records/maintaining-your-metadata/metadata-removal-markup-guide#00185), then (2) add the new relationship metadata in another full redeposit.
* **Resolution URLs**: you may update resolution URLs in bulk using a tab-separated .txt file. See [full details below](#00171).
* **Titles**: to update the [title](#00167) for a book, journal, or other content associated with an ISSN or ISBN, you will need to [contact us](/contact/) as we may need to make some changes on our side.
* Some non-bibliographic metadata may be updated, added to, or removed from a metadata record independently using a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). This might make things easier for those of you who send us XML files directly.

## Updating bibliographic metadata by resubmitting your complete record<a id='00168' href='#00168'><i class='fas fa-link'></i></a>

Most metadata changes can be done by resubmitting your complete metadata record, but there are some exceptions - please check the [exceptions list](#00627) above.

* **OJS**: find the record you wish to update, leave the relevant field blank to delete it, or add in your new metadata to update it, and deposit it again using the Crossref import/export plugin. You must be running at least OJS 3.1.2 and have the [Crossref import/export plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual) enabled
* **[Web deposit form](/documentation/content-registration/web-deposit-form)**: re-enter *all the metadata including the changes* - leave the relevant field blank to delete it, or add in your new metadata to update it - and resubmit
* **Still using the deprecated Metadata Manager?**: search for the journal record you wish to update, leave the relevant field blank to delete it, or add in your new metadata to update it, and deposit it again. Learn more about [metadata corrections, updates, and additions in Metadata Manager](/blog/metadata-corrections-updates-and-additions-in-metadata-manager/)
* **Depositing XML files with Crossref**: make changes to your XML file and resubmit it to Crossref. When making an update, you must supply **all** the bibliographic metadata for the record being updated, not just the fields that need to be changed. During the update process we overwrite the existing metadata with the new information you submit, and insert null values for any fields not supplied in the update. This means, for example, that if you’ve supplied an online publication date in your initial deposit, you’ll need to include that date in subsequent deposits if you wish to retain it. Note that the value included in the `<timestamp>` element must be incremented each time a DOI is updated.

## Updating your resolution URLs<a id='00171' href='#00171'><i class='fas fa-link'></i></a>

If your content moves and you need to update all of your URLs, you can update them in bulk to make sure that all your DOIs resolve to your content persistently.

### Reasons for resource resolution URL updates<a id='00624' href='#00624'><i class='fas fa-link'></i></a>

* Platform migration: if you know that your URLs are going to need to be updated in the future as you’re planning a platform migration, use our [handy guide](/documentation/member-setup/working-with-a-service-provider/planning-a-platform-migration/) and [checklist](/documentation/member-setup/working-with-a-service-provider/checklist-for-platform-migration/) to help you manage this effectively.
* [Title transfers](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/): if you’ve acquired a title from another Crossref member, you’ll need to update your resolution URLs. Do make sure to also check the inherited metadata for other fields, as this may also need to be updated. Learn more about [updating inherited DOIs and metadata after a title transfer](/documentation/register-maintain-records/maintaining-your-metadata/updating-after-title-transfer/).
* No longer hosting the content: if the worst happens and you are no longer able to host your content, it’s invaluable to have an archive provider as a backup. We encourage all Crossref members to use best efforts to engage an archive provider, and to [include information about archiving arrangements in their metadata](/documentation/content-registration/administrative-metadata/archive-locations/).

### How to update your resolution URLs<a id='00172' href='#00172'><i class='fas fa-link'></i></a>

If you only have a few URLs to update, you can just [resubmit your record](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata).

If you have a long list of URLs that need updating (for example, you’ve just finished a platform migration, or you’ve acquired a new title), you can do a bulk resource URL update. Create a tab-separated list (formatted as a text (.txt) file) of DOIs and their new URLs, and apply the following header:

H: email=youremail@email.com;fromPrefix=10.xxxx

where youremail@email.com is your email address and 10.xxxx is the [owner prefix](https://www.crossref.org/blog/what-can-often-change-but-always-stays-the-same/) (this should be the prefix associated with the username you'll be processing this request with) for the DOIs you're updating.  

Only DOIs of the same owning prefix may be updated together using this header. For example, if you have DOIs against two owning prefixes, you'll need to separate your submissions and use the appropriate 10.xxxx prefix for each set of your DOIs.  

This is what your tab-separated list should look like:

H: email=youremail@email.com;fromPrefix=10.5555  
10.5555/doi1 &nbsp;	http://www.yourdomain.com/newurl1<br>
10.5555/doi2 &nbsp;	http://www.yourdomain.com/newurl2<br>
10.5555/doi3 &nbsp;	http://www.yourdomain.com/newurl3

You can [upload the file to the admin tool](/documentation/member-setup/direct-deposit-xml/admin-tool/) or use the [upload tool](/documentation/member-setup/direct-deposit-xml/https-post-using-java-program/). To use the admin tool, [login](https://doi.crossref.org/servlet/home) and navigate to Submissions>Upload. Upload your file, choose "Bulk URL Update" as the Type, and click Upload.

If you have more than 4,000 URLs to update, you should break them into smaller files. The file upload size limit for this operation is 400 KB.

We can provide a list of your existing DOIs and URLs if needed.

## Updating title records<a id='00167' href='#00167'><i class='fas fa-link'></i></a>

When a new journal, book, conference proceeding, standard, report, or dissertation is registered with Crossref, we create a title record in our database from the metadata provided in the submission. Titles associated with an ISSN or ISBN must be consistent from registration to registration (inconsistencies in title-level metadata submitted will lead to [deposit errors](/documentation/content-registration/verify-your-registration/troubleshooting-submissions/)). This means that if you need to change the title of a journal or a book, we will need to modify the title record in our system before you can update your metadata for that title.

Although members can't edit titles once they have been deposited, our support team can do the following when necessary - please [send us](/contact/) the details, and we’ll update the records:

* Add or adjust ISSNs - correct ISSN errors or add additional ISSNs.
* Add or adjust alternative spellings of titles - alternative title spellings and abbreviations are recorded for each title and used in query matching. They can be included in the `<abbrev>` elements, or we can add them for you.
* Correct title spelling - we'll need to correct these for you, please [open a support ticket](/contact/).
* Merge titles - if two title entries have been created in error, we'll merge the titles entries into one upon request.
* Delete titles - titles submitted by mistake can be deleted, once the DOIs assigned to the title have been migrated to another title.

The adjustments above affect only the title record - you'll also need to [update your metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata) for any existing content records related to this title to update the title within that metadata record.

### Journal title and ISSN changes<a id='00625' href='#00625'><i class='fas fa-link'></i></a>

The ISSN International Centre recommends applying for a new ISSN if the publication’s medium changes (for example, a print magazine becomes an online magazine), or if the publication’s title changes. A change of ISSN is not required for other changes such as change of publisher, publication location, frequency, editorial policy.

In the case of a significant title change, such as “The Journal of Things” to “International Journal of Important Stuff”, DOIs registered for the original title should stay associated with that title, and all new DOIs should be registered to the new title (and new ISSN and journal-title-level DOI).

For a minor title change, such as “The Journal of Things” to “Things Journal”, keep the existing ISSN and update the title record. [Ask us](/contact/) to update the journal title record in our system, then you update all DOIs previously registered for this title. Once all records associated with that title have been updated to include the new title, the journal title in your submissions will match the one in our records, and no longer trigger [deposit errors](/documentation/content-registration/verify-your-registration/troubleshooting-submissions/) (such as “This error indicates the ISSN(s), title, or publisher in your deposit do not match the data we have on record for that ISSN”).
