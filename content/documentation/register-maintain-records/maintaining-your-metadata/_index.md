+++
title = "Maintaining your metadata"
date = "2022-07-22"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "maintaining-your-metadata"]
identifier = "documentation/register-maintain-records/maintaining-your-metadata"
rank = 4
weight = 60900
aliases = [
  "/education/metadata-stewardship/maintaining-your-metadata",
  "/education/metadata-stewardship/maintaining-your-metadata/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/",
  "/documentation/metadata-stewardship",
  "/documentation/metadata-stewardship/"

]
+++
When you become a member of Crossref, you’re joining a community of organizations who have committed to link their content to each other persistently, and to share their metadata with each other and with the scholarly community.

You’re committing to:

* Stewarding your DOIs and their associated metadata for the long term;
* Making sure that the DOI always resolves to a live landing page;
* Keeping the scholarly community aware of any changes to your content (such as withdrawals or retractions);
* Adding to, updating, and perhaps even deleting some metadata to keep your metadata useful to the whole community and to make your content even more discoverable.

This means that the work doesn't stop after you first register your records - you should ensure that you continue to maintain this metadata record for the long term. There's no charge to update the metadata after a record has first been registered, and you should aim to keep your records **clean**, **complete** and **up-to-date**.

## Keep your records clean
Identify and correct any errors.
* Use our [reports](/documentation/reports/) to help you identify problems.
* If you have omitted or provided any incorrect metadata, just [update your metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/) to make corrections, remove incorrect metadata, or provide missing data.
* Pay particular attention to [the titles of journals, books, and conference proceedings](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00167) - our support team may need to help you with corrections here.
* Although you can remove metadata elements from a record, it's not possible to fully delete the records or DOIs, as they are designed to be persistent. Read more about [changing or deleting DOIs](/documentation/content-registration/creating-and-managing-dois/changing-or-deleting-dois), and [contact us](/contact/) with the details of your situation so we can help.

## Keep your records complete
Add information for additional fields, and don’t forget to do this for your backfiles too.
* Go beyond basic bibliographic metadata, and deposit as much rich metadata as possible.  Richer metadata includes information such as:
  * [References](/documentation/principles-practices/best-practices/references/)
  * [ORCID iDs](/documentation/principles-practices/best-practices/bibliographic/#contributors)
  * [Funding information](/documentation/principles-practices/best-practices/funding/), including funder registry IDs and funding award numbers
  * [Crossmark metadata](/documentation/crossmark/)
  * [Text and data mining URLs](/documentation/retrieve-metadata/rest-api/text-and-data-mining/)
  * [License information](/documentation/principles-practices/best-practices/license/)
  * [Similarity Check URLs](/documentation/similarity-check/participate#00040)
  * [Abstracts](/documentation/principles-practices/best-practices/abstracts/)
* Check your [participation report](https://www.crossref.org/members/prep/) to see what metadata is missing from your records.

## Keep your records up-to-date
Metadata may change over time, and ownership of records may change, so make sure your metadata is updated with these changes.
* [Update your resolution URLs](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00171) if the location of your landing pages or full-text content changes - for example: if your website domain changes; if a journal moves from one hosting platform to another; or if a journal ceases to publish and the content must be accessed through an archive.
* Keep the community up-to-date with updates, retractions, or withdrawals using the [Crossmark](/documentation/crossmark/) service.
* [Change the ownership of deposited DOIs](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/) when the ownership of the published content changes.
