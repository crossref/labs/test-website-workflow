+++
title = "Metadata removal markup guide"
date = "2020-10-06"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "maintaining-your-metadata", "metadata-removal-markup-guide"]
identifier = "documentation/register-maintain-records/maintaining-your-metadata/metadata-removal-markup-guide"
rank = 4
weight = 140104
aliases = [
  "/education/metadata-stewardship/maintaining-your-metadata/removing-metadata-from-a-record/",
  "/education/metadata-stewardship/maintaining-your-metadata/metadata-removal-markup-guide",
  "/education/metadata-stewardship/maintaining-your-metadata/metadata-removal-markup-guide/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/metadata-removal-markup-guide",
  "/documentation/metadata-stewardship/maintaining-your-metadata/metadata-removal-markup-guide"
]
+++

For most metadata elements, you can just [update the record](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/) to delete elements. However, if you are sending us XML, there are some non-bibliographic metadata elements where you have to go through a two-stage process - firstly send us a submission to delete this element, and then send us a further submission to add in the replacement data.

Metadata that needs to be explicitly deleted includes:

* [Crossmark data](#00181)
* [Funding, clinical trial, or license data from Crossmark](#00182)
* [Funding data](#00183)
* [License data](#00184)
* [Relationship data](#00185)
* [Text and data mining, Similarity Check, and multiple resolution URLs](#00186)
* [References](#00187)

You can also delete non-bibliographic metadata by supplying an empty parent element (see examples below), and include it in a metadata update or submit it as a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). Note that metadata submitted as part of a Crossmark update needs to be removed within Crossmark metadata (see examples below).

## Remove all Crossmark data<a id='00181' href='#00181'><i class='fas fa-link'></i></a>

Remove *all* [Crossmark](/documentation/crossmark/) data from a record by supplying an empty Crossmark element in a metadata deposit:

```
<first_page>1</first_page>
  <last_page>3</last_page>
</pages>
<crossmark/>
<doi_data>
   <doi>10.5555/12345678</doi>
   <resource>http://psychoceramics.labs.crossref.org/10.5555-12345678.html</resource>
</doi_data>
```

or as part of a resource-only deposit:

```
<crossmark_data>
  <doi>10.5555/12345678</doi>
  <crossmark/>
</crossmark_data>
```

## Remove funding, clinical trial, or license data from Crossmark<a id='00182' href='#00182'><i class='fas fa-link'></i></a>

Funding, license, and clinical trial data may all be supplied as part of a [Crossmark](/documentation/crossmark/) update. If you need to remove funding, license, or clinical trial metadata from your Crossmark metadata, you must submit the appropriate empty element within a Crossmark update. Note the other Crossmark metadata must be present as well to be retained.

In this example, funding data is removed from a Crossmark update:

```
<crossmark>
  <crossmark_version>1</crossmark_version>
  <crossmark_policy>10.5555/crossmark_policy</crossmark_policy>
  <crossmark_domains>
    <crossmark_domain>
      <domain>psychoceramics.labs.crossref.org</domain>
    </crossmark_domain>
  </crossmark_domains>
  <crossmark_domain_exclusive>true</crossmark_domain_exclusive>
  <custom_metadata>
    <assertion name="orcid" label="ORCID" group_name="identifiers" group_label="Identifiers" order="0" href="http\://orcid.org/0000-0002-1825-0097">http\://orcid.org/0000-0002-1825-0097</assertion>
    <assertion name="received" label="Received" group_name="publication_history" group_label="Publication History" order="0">2012-07-24</assertion>
    <assertion name="accepted" label="Accepted" group_name="publication_history" group_label="Publication History" order="1">2012-08-29</assertion>
    <assertion name="published" label="Published" group_name="publication_history" group_label="Publication History" order="2">2012-09-10</assertion>
    <program xmlns="https://www.crossref.org/fundref.xsd"/>
    <program xmlns="https://www.crossref.org/AccessIndicators.xsd"/>
    <program "https://www.crossref.org/clinicaltrials.xsd"/>
</custom_metadata>
</crossmark>
```

## Remove funding data<a id='00183' href='#00183'><i class='fas fa-link'></i></a>

Remove [funding data](/documentation/content-registration/administrative-metadata) from a non-Crossmark record by supplying an empty fundref program element in a metadata deposit:

```
<first_page>1</first_page>
  <last_page>3</last_page>
</pages>
<program xmlns="https://www.crossref.org/fundref.xsd"/>
<doi_data>
   <doi>10.5555/12345678</doi>
   <resource>http://psychoceramics.labs.crossref.org/10.5555-12345678.html</resource>
</doi_data>
```

 or as part of a resource-only deposit:

```
<fundref_data>
  <doi>10.5555/12345678</doi>
 <program xmlns="https://www.crossref.org/fundref.xsd"/>
</fundref_data>
```

## Remove license data<a id='00184' href='#00184'><i class='fas fa-link'></i></a>

Remove [license data](/documentation/content-registration/administrative-metadata) from a non-Crossmark record by supplying an empty *AccessIndicators* program element in a metadata deposit:

```
<first_page>1</first_page>
  <last_page>3</last_page>
</pages>
<program xmlns="https://www.crossref.org/AccessIndicators.xsd"/>
<doi_data>
   <doi>10.5555/12345678</doi>
   <resource>http://psychoceramics.labs.crossref.org/10.5555-12345678.html</resource>
</doi_data>
```

 or as part of a resource-only deposit:

```
<lic_ref_data>
  <doi>10.5555/12345678</doi>
 <program xmlns="https://www.crossref.org/AccessIndicators.xsd"/>
</lic_ref_data>
```

## Remove relationship data<a id='00185' href='#00185'><i class='fas fa-link'></i></a>

Remove [relationship](/documentation/content-registration/structural-metadata/relationships/) data by supplying an empty relationship program element in a metadata deposit:

```
<first_page>1</first_page>
  <last_page>3</last_page>
</pages>
<program xmlns="https://www.crossref.org/relations.xsd"/>
<doi_data>
   <doi>10.5555/12345678</doi>
   <resource>http://psychoceramics.labs.crossref.org/10.5555-12345678.html</resource>
</doi_data>
```

 or as part of a resource-only deposit:

```
<doi_relations>
  <doi>10.5555/12345678</doi>
 <program xmlns="https://www.crossref.org/relations.xsd"/>
</doi_relations>
```

## Remove text and data mining, Similarity Check, and multiple resolution URLs<a id='00186' href='#00186'><i class='fas fa-link'></i></a>

[Text and data mining](/documentation/retrieve-metadata/rest-api/text-and-data-mining/) and [multiple resolution](/documentation/content-registration/creating-and-managing-dois/multiple-resolution/) secondary URLs may be removed from a record by submitting an update containing an empty collection tag that includes the appropriate property:

* **Text and data mining** uses property `text-mining`
* **Similarity Check** URLs use the property `crawler-based`
* **Multiple resolution** secondary URLs use the property `list-based`
* The [**country-code resolution**](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00130) feature uses the property `country-based`

For example, to remove text and data mining URLs from a record submit the following as part of a metadata deposit:

```
<doi_data>
  <doi>10.5555/515151</doi>
  <resource>http://annalsofpsychoceramics.labs.crossref.org/abstract/515151/</resource>
  <collection property="text-mining"/>
</doi_data>
```

They may be included in a metadata deposit (above) or as part of a resource-only deposit:

```
<doi_resources>
  <doi>10.5555/515151</doi>
  <collection property="text-mining"/>
</doi_resources>
```

## Remove references<a id='00187' href='#00187'><i class='fas fa-link'></i></a>

Remove a [reference list](/documentation/content-registration/descriptive-metadata) from a record by supplying an empty `citation_list` element in a metadata deposit:

```
<first_page>1</first_page>
  <last_page>3</last_page>
</pages>
<doi_data>
   <doi>10.5555/12345678</doi>
   <resource>http://psychoceramics.labs.crossref.org/10.5555-12345678.html</resource>
</doi_data>
<citation_list/>
```

 or as part of a resource-only deposit:

```
<doi_citations>
  <doi>10.5555/12345678</doi>
<citation_list/>
</doi_citations>
```
