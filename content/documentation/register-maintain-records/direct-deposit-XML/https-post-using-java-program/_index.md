+++
title = "POSTing files using our upload tool"
date = "2021-05-10"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "direct-deposit-xml", "https-post"]
identifier = "documentation/register-maintain-records/direct-deposit-xml/https-post-using-java-program"
rank = 4
weight = 50604
aliases = [
	"/education/member-setup/direct-deposit-xml/https-post-using-java-program",
	"/education/member-setup/direct-deposit-xml/https-post-using-java-program/",
	"/documentation/member-setup/direct-deposit-xml/https-post-using-java-program",
	"/documentation/member-setup/direct-deposit-xml/https-post-using-java-program/",
	"/documentation/content-registration/direct-deposit-xml/https-post-using-java-program/",
	"/documentation/content-registration/direct-deposit-xml/https-post-using-java-program/",
]
+++

We provide a Java program that performs file uploads (via HTTPS POST) to Crossref. This program allows you to upload a single file, a list of files, or a whole directory of files.

To use, download [crossref-upload-tool.jar](/documentation-files/crossref-upload-tool.jar) and place it in /usr/local/lib.

### How to use our upload tool<a id='00496' href='#00496'><i class='fas fa-link'></i></a>

In the following examples:

* **user** is the username and password from your [Crossref account credentials](/documentation/member-setup/account-credentials/). If you are using organization-wide shared role credentials, the username is the role. If you're using personal user credentials, the username is your email address plus the role in the following format **email/role**.
* **file** is the name of the file you are uploading *or*
* **directory** is the name of the directory containing files to upload

#### To upload a metadata file<a id='00497' href='#00497'><i class='fas fa-link'></i></a>

```
java -jar crossref-upload-tool.jar --user myusername mypassword --metadata (filename | directory)
```

Using **role credentials** (note: in these examples, we have used the fictional role and password combination of mrcrossref and abc134):

```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --metadata crdeposit234.xml
```
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --metadata crdeposits
```

Using **user credentials** (note: in these examples, we have used the fictional user credential, role, and password combination of name@someplace.com, mrcrossref, and abc134)

```
java -jar crossref-upload-tool.jar --user name@someplace.com/mrcrossref abc134 --metadata crdeposit234.xml
```
```
java -jar crossref-upload-tool.jar --user name@someplace.com/mrcrossref abc134 --metadata crdeposits
```

#### To [upload a resource-only deposit file](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/)<a id='00498' href='#00498'><i class='fas fa-link'></i></a>
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --resources (file | directory)
```
for example:
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --metadata cr_refs.xml
```
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --metadata refdata
```
#### To upload conflict files<a id='00499' href='#00499'><i class='fas fa-link'></i></a>

Single file:
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --conflicts ticket1234.txt
```
Directory of files:
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --conflicts ALIAS_123
```
#### To upload bulk Resource URL updates<a id='00499' href='#00499'><i class='fas fa-link'></i></a>

Single file:
```
java -jar crossref-upload-tool.jar -user mrcrossref abc 134 -transfers ticket1234.text
```
Directory of files:
```
java -jar crossref-upload-tool.jar -user mrcrossref abc134 -transfer ALIAS_123
```

#### To direct upload(s) to the test system<a id='00500' href='#00500'><i class='fas fa-link'></i></a>
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --host test.crossref.org --metadata crdeposit234.xml
```
#### Dry run (test)<a id='00501' href='#00501'><i class='fas fa-link'></i></a>

Note that if the --metadata option is given a directory name instead of a filename then all files within the directory are uploaded. To ensure that you are uploading what you want use the --dry-run option and review the listing of files, eg:
```
java -jar crossref-upload-tool.jar --user mrcrossref abc134 --metadata mydeposits/ --dry-run
```
### Additional info<a id='00502' href='#00502'><i class='fas fa-link'></i></a>

If your upload is successful, you will see this message:

[...] INFO uploading to https\://doi.crossref.org:443/<br>
[...] INFO uploading submission: file=myfile.xml<br>
[...] INFO uploaded submission: file=myfile.xml<br>
[...] INFO done

If the username is wrong, you will see the message:

[...] INFO uploading to https\://doi.crossref.org:443/<br>
[...] INFO uploading submission: file=myfile.xml<br>
[...] INFO unauthorized: file=myfile.xml; user=mrcrossref<br>
[...] INFO done

### Upload options<a id='00503' href='#00503'><i class='fas fa-link'></i></a>
```
--user name password
--metadata ( file | directory )
--query ( file | directory )
--transfers ( file | directory )
--handles ( file | directory )
--resources ( file | directory )
--conflicts ( file | directory )
--address host port
--protocol ( http | https )
--dry-run
--help
```

Where:

* **user**: your Crossref system username (either **role**, for role credentials, or **email/role**, for user credentials) and password
* **metadata**: use for metadata deposits
* **query**: use for query deposits
* **transfers**: admin use only
* **handles**: admin use only
* **resources**: resource-only deposits
* **conflicts**: conflict resolution files
* **address**: direct to a different address (such as test.crossref.org)
* **protocol**: http or https
* **dry-run**: test uploader without uploading
* **help**: displays the above list of upload options

### Further examples<a id='00504' href='#00504'><i class='fas fa-link'></i></a>

Entry into terminal
```
java -jar /usr/local/crossref-upload-tool.jar --user mrcrossref abc134 --metadata /Users/mistercrossref/Uploader/September/19/134/
```
Key for entry into terminal:

* usr/local/crossref-upload-tool.jar = location of the upload program on your machine
* mrcrossref abc134 = Crossref role and password you are using for upload
* metadata = type of content being uploaded
* Users/mistercrossref/Uploader/September/19/134/ = location of XML files on my machine being uploaded
