+++
title = "Upload XML files using our admin tool"
date = "2022-06-30"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "direct-deposit-xml", "admin-tool"]
identifier = "documentation/register-maintain-records/direct-deposit-xml/admin-tool"
rank = 4
weight = 50602
aliases = [
  "/education/member-setup/direct-deposit-xml/admin-tool",
  "/education/member-setup/direct-deposit-xml/admin-tool/",
  "/education/member-setup/upload-xml-files-via-our-admin-tool/",
  "/education/member-setup/upload-xml-files-via-our-admin-tool",
  "/documentation/member-setup/direct-deposit-xml/admin-tool",
  "/documentation/member-setup/direct-deposit-xml/admin-tool/",
  "/documentation/content-registration/direct-deposit-xml/admin-tool/",
  "/documentation/content-registration/direct-deposit-xml/admin-tool/"
]
+++

If you generate your own XML files (or export them from OJS), you can deposit these using the deposit [admin tool](https://doi.crossref.org/servlet/useragent).

1. Log in with your [Crossref account credentials](/documentation/member-setup/account-credentials/)
2. Go to *Submissions*
3. Click *Upload*
4. Click *Browse* to locate the file you are uploading
5. Select the file type:
    * Metadata: content registration XML
    * Query: XML queries
    * DOI Query: XML-formatted DOI-to-metadata queries
    * DOI References/Resources: [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/) XML
    * Conflict Management: conflict management .txt file
    * Bulk URL Update: [URL updates .txt file](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/#00172)
6. Click *Upload*.

The uploaded file will be added to the system submission queue. We'll email submission reports and query results to the email address you specified in the uploaded file.
