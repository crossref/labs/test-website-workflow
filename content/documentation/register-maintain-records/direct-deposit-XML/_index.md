+++
title = "Direct deposit of XML"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "direct-deposit-xml"]
identifier = "documentation/register-maintain-records/direct-deposit-xml"
aliases =[
    "/education/member-setup/verifying-and-testing-your-xml/special-characters-in-your-xml/",
    "/education/member-setup/verifying-and-testing-your-xml/special-characters-in-your-xml",
    "/education/member-setup/direct-deposit-xml",
    "/education/member-setup/direct-deposit-xml/",
    "/documentation/member-setup/direct-deposit-xml",
    "/documentation/member-setup/direct-deposit-xml/",
    "/documentation/content-registration/direct-deposit-xml/",
    "/documentation/content-registration/direct-deposit-xml"
]
rank = 4
weight = 50600
+++

If you’re sending us XML directly, you'll need to review our [input schema](/documentation/content-registration/metadata-deposit-schema/), and check your XML files follow the schema's rules.

To deposit your XML files with Crossref, you have a choice of three methods:

* [Upload JATS XML using the web deposit form](/documentation/content-registration/web-deposit-form/)
* [Upload XML files using our admin tool](/documentation/member-setup/direct-deposit-xml/admin-tool/)
* [XML deposit using HTTPS POST](/documentation/member-setup/direct-deposit-xml/https-post/)

If you're making your deposits via the admin tool or HTTPS POST, you can use our [test system](/documentation/member-setup/direct-deposit-xml/testing-your-xml/).

## Special characters in your XML<a id='00662' href='#00662'><i class='fas fa-link'></i></a>

All XML submitted to our system must be UTF-8 encoded. There are two ways to include a special unicode character in a Crossref deposit XML file:

1. **Encode the special character using a numerical representation**. This is the preferred approach. Constructing an entity reference in the XML that is the numerical value of the character. For example, `<surname>&#352;umbera</surname>` includes the special character S with a háček (Š).
2. **Use a UTF-8 editor or tool** when creating the XML and insert characters directly into the file, which results in a one or more byte sequence per character in the file.

For example, an S with a háček (Š) has a decimal value of 352 which is 160hex. This value converts to the UTF-8 sequence C5,A0 in hex. You can create a small XML file in which you insert this two-byte sequence (shown here between the `<UTF_encoded>` tags).

 ```
 <?xml version="1.0" encoding="utf-8" ?>
<start>
<UTF_encoded>Š</UTF_encoded>
</start>
```

The character displays properly in a browser but if you save the XML source and try to view it in certain editors, it will not display correctly.

### Character entities<a id='00505' href='#00505'><i class='fas fa-link'></i></a>

XML based on schema does not support named character entities (sometimes referred to as html-encoded characters). For example, *&eacute;* or *&ndash;* are not allowed. To include these characters you must use their numerical representation, *\&#x0E9;* or *\&#x2013;* respectively. These are called *numerical entities*, shown by the *#* (hash or pound sign). The *x* following *#* indicates the value is in *hex* (rather than *decimal* if the *x* were omitted). All entities must end with the *;* character.

Character numerical values may be found in the [Unicode Character Code Charts](http://www.unicode.org/charts/). Learn more about [UTF-8 and unicode](https://www.cl.cam.ac.uk/~mgk25/unicode.html), and the [ISO 8859 series of standardized multilingual graphic character sets for writing in alphabetic languages](http://czyborra.com/charsets/iso8859.html).

### Using face markup<a id='00506' href='#00506'><i class='fas fa-link'></i></a>

Some style/face markup is supported by our schema but we recommend using it only when it is essential to the meaning of the text. Learn more about [face markup](/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-3-6/).
