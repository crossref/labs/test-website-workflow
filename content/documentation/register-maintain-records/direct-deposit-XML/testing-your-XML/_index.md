+++
title = "Testing your XML"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "direct-deposit-xml", "testing-your-xml"]
identifier = "documentation/register-maintain-records/direct-deposit-xml/testing-your-xml"
aliases =[
    "/education/member-setup/verifying-and-testing-your-xml/",
    "/education/member-setup/verifying-and-testing-your-xml",
    "/education/member-setup/direct-deposit-xml/testing-your-xml",
    "/education/member-setup/direct-deposit-xml/testing-your-xml/",
    "/documentation/member-setup/direct-deposit-xml/testing-your-xml",
    "/documentation/member-setup/direct-deposit-xml/testing-your-xml/",
    "/documentation/content-registration/direct-deposit-xml/testing-your-xml/",
    "/documentation/content-registration/direct-deposit-xml/testing-your-xml"
]
rank = 4
weight = 50605
+++

It’s a good idea to verify the format and structure of your XML file before trying to register your content. You can validate your XML locally using an XML editor such as [Oxygen](https://www.oxygenxml.com/) or [XMLSpy](https://www.altova.com/xmlspy-xml-editor), or command line tools such as xmllint. We provide an [XML parser](https://data.crossref.org/reports/parser.html) that supports single file uploads for validation only.

Our [test version of the admin tool](https://test.crossref.org/) allows members and service providers a sandbox to test their XML submissions, before depositing in the [production (live) system](https://doi.crossref.org). The test environment works in the same way as our production admin tool, but uses a test database and does not register DOIs with Handle. You can use the test system for deposits via the admin tool and HTTPS POST, but not for deposits via the web deposit form.

You can also use our [metadata quality check parser](https://www.crossref.org/02publishers/parser.html) to check your XML before submission. The parser quickly identifies errors in the XML you uploaded.

Any deposits you make in the test system have no effect on your resolution reports and conflict reports, which relate only to content you register in the production system. Learn more about [reports](/documentation/reports/).

### Differences between test and production systems<a id='00011' href='#00011'><i class='fas fa-link'></i></a>

[VoR/preprint match notifications](/documentation/content-registration/content-types-intro/posted-content-includes-preprints#00082): in the production system, a notification feature alerts preprint creators of any matches with journal articles, so they can link to future versions from the preprint. In the test system, you won’t be notified of matches.

### Accessing the test system<a id='00012' href='#00012'><i class='fas fa-link'></i></a>

We don’t automatically set up new accounts with access to the test system, but we are happy to give you this access at any time, whether during your membership application, or at any time after joining. Just [contact us](/contact/) to request access.

Log in to the test system with your [Crossref account credentials](/documentation/member-setup/account-credentials/). If your credentials do not work with the test system, please [contact us](/contact/) so we can enable access for you. If you have forgotten your password, you can reset it - learn more about [how to change your Crossref account credentials password](/documentation/member-setup/account-credentials#00620).

### Carrying out a platform migration?<a id='00013' href='#00013'><i class='fas fa-link'></i></a>

Please [let us know](/contact/) so we can update both your production and test accounts. Learn more about [planning a platform migration](/documentation/member-setup/working-with-a-service-provider/planning-a-platform-migration/).
