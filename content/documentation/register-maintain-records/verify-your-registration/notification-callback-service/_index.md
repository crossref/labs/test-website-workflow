+++
title = "Notification callback service"
date = "2023-02-23"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "verify-your-registration", "notification-callback-service"]
identifier = "documentation/register-maintain-records/verify-your-registration/notification-callback-service"
rank = 4
weight = 60704
aliases = [
    "/education/content-registration/verify/your-registration/notification-callback-service",
    "/education/content-registration/verify-your-registration/notification-callback-service/",
    "/documentation/content-registration/verify-your-registration/notification-callback-service/",
    "/documentation/content-registration/verify-your-registration/notification-callback-service"

]

+++


Notification callback is a service you can use to notify you when a [submission log](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log), either in the [test](https://test.crossref.org) or [production admin tool](https://doi.crossref.org), is available for a metadata, [batch query](/documentation/retrieve-metadata/openurl), or [Cited-by](/documentation/cited-by/retrieve-citations/) query submission. Notification is provided in the form of a HTTP(S) URL where the log can be retrieved. If the notification callback service is enabled, you will no longer receive submission log emails.

### How the notification callback service works<a id='00161' href='#00161'><i class='fas fa-link'></i></a>

The callback will be an HTTP(S) request to a URL (notify-url) provided by the member with all data relayed via HTTPS headers. The notification specifies the availability of the result, some context of the request, and an HTTP(S) URL from which to get the result. ​The submission log may then be retrieved using the HTTP(S) URL.

The headers use the simple name and value structure; that is, the value has no additional structure that divides it into parts. To ensure that all Unicode values can be accommodated all header values will be UTF-8 encoded.

When the notify-url is used the following HTTPS headers are provided:

* CROSSREF-NOTIFY-ENDPOINT: the `notify-endpoint` (required) is just the name used to identify the specific notification (more on this below)
* CROSSREF-EXTERNAL-ID: the id given by the member with regards to the request. For metadata deposits, for example, it is the value of the `doi_batch_id element` (Optional)
* CROSSREF-INTERNAL-ID: the id given by us with regards to the request (Optional)
* CROSSREF-RETRIEVE-URL: the URL for the member to use to retrieve the request's result. Since the HTTPS header value is UTF-8 encoded, the URL will contain no URI encodings. For example, an _Á_ will not be encoded as `%C3%81`
* CROSSREF-SERVICE-DATE: the date and time stamp of the service request. Learn more about format specification in [RFC 2616](https://tools.ietf.org/html/rfc2616).
* CROSSREF-RETRIEVE-URL-EXPIRATION-DATE: the timestamp after which service result is no longer available at the given retrieve-url.

### Setting up an endpoint<a id='00162' href='#00162'><i class='fas fa-link'></i></a>

You'll need to set up and register an endpoint to receive callbacks.

1. Create an endpoint using cURL:
```
curl -s -D - "https://doi.crossref.org/notification-callback/exec/setNotifyEndpoint\
?usr=ROLE\
&pwd=PASSWORD\
&endpoint=com.foo.1\
&url=http://foo.com/crossref/callback
```
2. Test your endpoint:

```
curl -s -D - "https://doi.crossref.org/notification-callback/exec/createNotificationCallback\
?usr=USERNAME\
&pwd=PASSWORD\
&notifyEndpoint=com.foo.1\
&notifyPayloadContentType=text/plain\
&notifyPayloadContent=this+is+test+1\
&externalTrackingId=test-1"
```
After a few minutes your end-point will receive a callback with your test payload message.

This is an example of the test payload message that will be delivered to your end-point: [https://doi.crossref.org/retrieve/f41557cf-f2f2-4f33-9d4c-3848fcc42187](https://doi.crossref.org/retrieve/f41557cf-f2f2-4f33-9d4c-3848fcc42187).

```
{
  "id" : 918323,
  "status" : "N",
  "completed" : null,
  "serviced" : "2022-05-03 15:13:54.0",
  "notifyEndpoint" : "org.jonmstark.submission",
  "notifyPayloadId" : "f41557cf-f2f2-4f33-9d4c-3848fcc42187",
  "notifyPayloadExpiration" : "2022-05-10 15:13:54.0",
  "internalTrackingId" : "jms-test-1",
  "externalTrackingId" : "jms-test-1",
  "recordCreated" : "2022-05-03 15:13:54.0",
  "recordUpdated" : null
}
```

3. [Contact us](/contact/) to activate the service - we’ll need:
* your endpoint info (`notify-endpoint` and `notify-url`) -- the `notify-endpoint` is just a name to identify the specific notification. The `notify-endpoint` should be something you can recognize so when you receive responses that include the endpoint name, it is easy to know which of the callback feeds it is coming from. The`notify-url` has to be the actual URL of your callback receiver, as that is where the notification callback transmits to via http/https.
* the services you’re activating the service for (metadata submissions, batch querying, Cited-by alerts)
* the username and/or DOI prefix you’ll be using.

Make sure you inform us of any changes to your endpoint: if a message fails to send we will retry for up to a week after which you will no longer be able to receive it.

### Example of a notification<a id='00163' href='#00163'><i class='fas fa-link'></i></a>

For the submission `1368966558` the notification would be as follows (new lines have been added between header name and header value to improve readability):

<figure><img src='/images/documentation/Notification-callback-service-submission-1368966558-example.png' alt='notification callback submission example' title='' width='75%'></figure>

```
CROSSREF-NOTIFY-ENDPOINT:
F8DD070C-89A9-4B82-B77E-1CADCD989DAE
CROSSREF-EXTERNAL-ID:
apsxref:7ca42f54-093f-11e4-b65b-005056b31eb6
CROSSREF-INTERNAL-ID:
1368966558
CROSSREF-SERVICE-DATE:
Fri, 11 Jul 2014 21:08:24 GMT
CROSSREF-RETRIEVE-URL-EXPIRATION-DATE:
Fri, 18 Jul 2014 21:08:24 GMT
CROSSREF-RETRIEVE-URL:
https://doi.crossref.org/notification/retrieve/67BCBED2-7AE2-4FD7-B90E-514E19B1DE49
```

### Querying for past callbacks<a id='00164' href='#00164'><i class='fas fa-link'></i></a>

The notification callback service can be queried for past callbacks. The query is implemented as an HTTPSservice.(Access control and limits to end-points and time frames TBD).

The query takes 3 criteria, the notify-endpoints, an inclusive *from* timestamp, and an exclusive *until* timestamp. All timestamps use the [ISO 8061](https://www.iso.org/iso-8601-date-and-time-format.html) format YYYY-MM-DD’T’hh:mm:ss’Z, for example, *2014-07-23T14:43:01Z*.

The query results in a JSON array of callbacks. For example, querying for the single endpoint "1CFA094C-4876-497E-976B-6A6404652FC2" returns:

```
[
 {
   "notify-endpoint": "1CFA094C-4876-497E-976B-6A6404652FC2",
   "external-id": "apsxref:7ca42f54-093f-11e4-b65b-005056b31eb6",
   "service-date": "2014-07-14T21:08:24Z",
   "retrieve-url": "https://doi.crossref.org/.../67...49",
   "retrieve-url-expiration-date": "2014-07-11T21:08:24Z",
   "audit": [
     {
       "notify-url" : "http://abc.org/crossref/callbacks",
       "date" : "2014-07-14T21:09:00Z",
       "explanation": "http status 200"
     }
   ]
 },
 {
   "notify-endpoint": "1CFA094C-4876-497E-976B-6A6404652FC2",
   "external-id": ...
 },
...
]
```

A flat structure is used to aid processing the result as a stream. There is no order defined.

The *audit* item is a record of attempted callbacks. It details the notify-endpoint's notify-url used at the time of the callback, the timestamp of the callback, and the HTTPS status of the callback. If more than one attempt has been tried then the audit array will contain multiple elements; there is no order defined.

The query service is currently available at:

```
https://doi.crossref.org/notification-callback/exec/findNotificationCallbackAttempts?usr=USER&pwd=PASSWORD&notifyEndpoints=ENDPOINT&from=2014-01-01&until=2014-12-31
```

The `usr` and `pwd` are your Crossref username and password. The ENDPOINT value is a notify-endpoint or a space separated set of notify-endpoints.

### Glossary of notification callback service terms<a id='00165' href='#00165'><i class='fas fa-link'></i></a>

* notify-url: the URL that the member provides and is used to notify them of the availability of a service request's result. How the URL is provided to us will depend on the service.
* notify-endpoint: an opaque token used to select a `notify-url`. The token will be anonymous and difficult to guess. The `notify-endpoint` is provided by the member. The `notify-endpoint` is associated with one `notify-url` (many `notify-endpoints` can be associated with the same `notify-url`).
* retrieve-url: the URL that we provides that is used by the member to get the service request result.
* notify-payload: the data that specifies what service request this notification is for. This payload will use HTTPS headers so as to be HTTPS method-neutral (such as POST, PUT).
* retrieve-payload: the service result. Each service will define its own result content-type (that is very much like what would be sent in email today).
* notification-authentication: This is the method of authentication we will use with the `notify-url`. Credentials are provided by the member.
* retrieval-authentication: This is the method of authentication the member will use with the `retrieve-url`. The [account credentials](/documentation/member-setup/account-credentials/) are provided by us.
