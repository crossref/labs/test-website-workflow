+++
title = "Interpret submission logs"
date = "2022-07-22"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "verify-your-registration", "interpret-submission-logs"]
identifier = "documentation/register-maintain-records/verify-your-registration/interpret-submission-logs"
rank = 4
weight = 60702
aliases = [
    "/education/content-registration/verify-your-registration/interpret-submission-logs",
    "/education/content-registration/verify-your-registration/interpret-submission-logs/",
    "/documentation/content-registration/verify-your-registration/interpret-submission-logs",
    "/documentation/content-registration/verify-your-registration/interpret-submission-logs/"
]
+++

Submission logs contain information about the DOIs and metadata you have submitted to our system. They let you know if your content is registered successfully, and if not, what issues need to be addressed.

Your logs are by default emailed to the address provided in your registration XML or entered in our web form. You may also use the admin tool to [search for past deposits](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00143) or [retrieve them by polling](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00145).

Examples of a log:
* [for a successful deposit](#00147)
* [with deposit errors](#00148)
* [with XML validation error](#00149)
* [with warnings](#00150)
* [containing references](#00151)

### Example of a log for a successful deposit<a id='00147' href='#00147'><i class='fas fa-link'></i></a>
Note that the `<failure_count>` = 0 and that the `<record_count>` = `<success_count>`.

```
<?xml version="1.0" encoding="UTF-8"?>
 <doi_batch_diagnostic>
  <submission_id>9349810</submission_id>
  <batch_id>FINAL_001</batch_id>
  <record_diagnostic status="Success">
   <doi>10.5555/second_conflict_003</doi>
   <msg>Successfully added</msg>
  </record_diagnostic>
  <batch_data>
   <record_count>1</record_count>
   <success_count>1</success_count>
   <warning_count>0</warning_count>
   <failure_count>0</failure_count>
  </batch_data>
 </doi_batch_diagnostic>
```

### Example of a log with deposit errors<a id='00148' href='#00148'><i class='fas fa-link'></i></a>

This is an example of a deposit containing errors. In the example, note that the `<success_count>` and `<record_count>` do not match. A status of "Failure" indicates the record was rejected and the DOI was not registered or updated. The `<record_diagnostic>` for each registration failure contains an error message. Each error within a deposit should be corrected and the deposit resubmitted. Learn more about [error and warning messages](/documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions/).

```
<?xml version="1.0" encoding="UTF-8"?>
 <doi_batch_diagnostic status="completed" sp="cr5.crossref.org">
  <submission_id>394260418</submission_id>
  <batch_id>314668373.xml</batch_id>
  <record_diagnostic status="Failure">
   <doi>10.5555/11111</doi>
   <msg>Record not processed because submitted version: 20070904093839 is less or equal to	previously submitted version (DOI match)</msg>
  </record_diagnostic>
  <record_diagnostic status="Failure" msg_id="4">
   <doi>10.5555/44444</doi>
   <msg>Record not processed because submitted version: 20070904093839 is less or equal to	previously submitted version (DOI match)</msg>
  </record_diagnostic>
  <record_diagnostic status="Success">
   <doi>10.5555/55555</doi>
   <msg>Successfully added</msg>
  </record_diagnostic>
  <batch_data>
   <record_count>3</record_count>
   <success_count>1</success_count>
   <warning_count>0</warning_count>
   <failure_count>2</failure_count>
  </batch_data>
</doi_batch_diagnostic>
```

### Example of a log with XML validation error<a id='00149' href='#00149'><i class='fas fa-link'></i></a>

This is an example of a submission log for a deposit with an error that prevented all DOIs from being processed. This happens when there are XML formatting issues, or if the uploaded item is not XML. Note that `<record_count>` and `<failure_count>` both equal *1*. This will be true no matter how many DOIs were actually included in the submission. Learn more about [error and warning messages](/documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions/).

```
<?xml version="1.0" encoding="UTF-8"?>
 <doi_batch_diagnostic status="completed" sp="ds3.crossref.org">
  <submission_id>394260418</submission_id>
  <batch_id>314668373.xml</batch_id>
  <record_diagnostic status="Failure" msg_id="29">
   <doi />
   <msg>Deposited XML is not well-formed or does not validate: Error on line 1: Content is not allowed in prolog.</msg>
  </record_diagnostic>
  <batch_data>
   <record_count>1</record_count>
   <success_count>0</success_count>
   <warning_count>0</warning_count>
   <failure_count>1</failure_count>
  </batch_data>
 </doi_batch_diagnostic>
```

### Example of a log with warnings<a id='00150' href='#00150'><i class='fas fa-link'></i></a>

This is an example of a submission log with **warnings**. Warnings almost always indicate that DOIs have been successfully deposited and were flagged as a [conflict](/documentation/reports/conflict-report) with a previously deposited DOI.

```
<?xml version="1.0" encoding="UTF-8"?>
 <doi_batch_diagnostic status="completed" sp="ds4.crossref.org">
  <submission_id>394260418</submission_id>
  <batch_id>314668373.xml</batch_id>
  <record_diagnostic status="Success">
   <doi>10.5555/11112</doi>
   <msg>Successfully added</msg>
  </record_diagnostic>
  <record_diagnostic status="Warning">
   <doi>10.5555/11113</doi>
   <msg>Added with conflict</msg>
   <conflict_id>5166446</conflict_id>
	<dois_in_conflict>
 	<doi>10.5555/22223</doi>
	</dois_in_conflict>
  </record_diagnostic>
  <record_diagnostic status="Success">
   <doi>10.5555/11114</doi>
   <msg>Successfully added</msg>
  </record_diagnostic>
  <record_diagnostic status="Warning">
   <doi>10.5555/11115</doi>
   <msg>Added with conflict</msg>
   <conflict_id>5166447</conflict_id>
	<dois_in_conflict>
 	<doi>10.5555/22225</doi>
	</dois_in_conflict>
  </record_diagnostic>
  <batch_data>
   <record_count>4</record_count>
   <success_count>2</success_count>
   <warning_count>2</warning_count>
   <failure_count>0</failure_count>
  </batch_data>
</doi_batch_diagnostic>
```

### Example of a log containing references<a id='00151' href='#00151'><i class='fas fa-link'></i></a>

This is an example of a submission log from a deposit containing references. Each reference in the deposit will be included in the log, identified by the citation key included in the deposit.

```
<?xml version="1.0" encoding="UTF-8"?>
 <doi_batch_diagnostic status="completed" sp="ds5.crossref.org">
  <submission_id>03480197</submission_id>
  <batch_id>XYZ00000000</batch_id>
  <record_diagnostic status="Success">
   <doi>10.5555/example</doi>
   <msg>Successfully updated</msg>
   <citations_diagnostic>
 	<citation key="10.5555/example_bb0030" status="error">Either ISSN or Journal title or Proceedings title must be supplied.</citation>
 	<citation key="10.5555/example_bb0005" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0010" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0015" status="resolved_reference">10.1590/S0006-87051960000100077</citation>
 	<citation key="10.5555/example_bb0045" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0050" status="resolved_reference">10.1007/BF01916741</citation>
 	<citation key="10.5555/example_bb0075" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0080" status="resolved_reference">10.1093/jxb/4.3.403</citation>
 	<citation key="10.5555/example_bb0085" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0090" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0095" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0100" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0105" status="resolved_reference">10.1038/181424b0</citation>
 	<citation key="10.5555/example_bb0110" status="resolved_reference">10.1038/1831600a0</citation>
 	<citation key="10.5555/example_bb0115" status="resolved_reference">10.1007/BF01912405</citation>
 	<citation key="10.5555/example_bb0120" status="resolved_reference">10.1038/185699a0</citation>
 	<citation key="10.5555/example_bb0125" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0150" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0155" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0160" status="resolved_reference">10.1038/1781359a0</citation>
 	<citation key="10.5555/example_bb0165" status="resolved_reference">10.1093/jxb/13.1.75</citation>
 	<citation key="10.5555/example_bb0170" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0175" status="resolved_reference">10.2134/agronj1960.00021962005200080014x</citation>
 	<citation key="10.5555/example_bb0180" status="resolved_reference">10.2134/agronj1960.00021962005200080015x</citation>
 	<citation key="10.5555/example_bb0185" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0190" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0195" status="resolved_reference">10.1007/BF00622243</citation>
 	<citation key="10.5555/example_bb0200" status="resolved_reference">10.4141/cjps58-055</citation>
 	<citation key="10.5555/example_bb0205" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0210" status="stored_query"></citation>
 	<citation key="10.5555/example_bb0215" status="resolved_reference">10.1038/178601a0</citation>
 	<citation key="10.5555/example_bb0220" status="stored_query"></citation>
 	</citations_diagnostic>
   </record_diagnostic>
  <batch_data>
 	<record_count>1</record_count>
 	<success_count>1</success_count>
 	<warning_count>0</warning_count>
 	<failure_count>0</failure_count>
  </batch_data>
</doi_batch_diagnostic>
```
