+++
title = "References"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section =  ["schema-library", "markup-guide-metadata-segment",  "references"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/references"
rank = 4
weight = 60626
aliases = [
    "/education/content-registration/descriptive-metadata/references",
    "/education/content-registration/descriptive-metadata/references/",
    "/education/retrieve-metadata/opening-your-references-for-distribution/",
    "/education/retrieve-metadata/opening-your-references-for-distribution",
    "/reference-distribution/",
    "/reference-distribution",
    "/documentation/content-registration/descriptive-metadata/references/",
    "/documentation/content-registration/descriptive-metadata/references"
]
+++

Registering references means submitting them as part of your metadata deposit. It is optional, but strongly encouraged. Registering references is a prerequisite for participating in [Cited-by](/documentation/cited-by/).

Note that registering references is not the same as [reference linking](/documentation/reference-linking/) - learn more about the [differences](/blog/linking-references-is-different-from-depositing-references/).

The benefits of registering references as part of your metadata include:

* making your content more discoverable
* enabling evaluation of research, and
* helping with citation counts.

Whenever you register content with us, make sure you include your references in the submission.

{{< snippet "/_snippet-sources/references-include.md" >}}

## Detailed information for users of direct deposit of XML<a id='00177' href='#00177'><i class='fas fa-link'></i></a>

In this section, learn more about:

* [Current elements for citation tagging](#00178)
* [Metadata deposit example](#00179)
* [Resource-only deposit example](#00180)

When depositing references as part of your content registration XML, mark up individual citations according to our deposit schema section. For example:

``` XML
<citation key="ref1">
  <journal_title>Current Opinion in Oncology</journal_title>
  <author>Chauncey</author>
 <volume>13</volume>
 <first_page>21</first_page>
 <cYear>2001</cYear>
</citation>
```

Marking up each element allows us to be very precise when identifying potential matches with registered DOIs.

If you know the DOIs of individual citations, include them. We'll use the metadata deposited for the DOI when generating Cited-by matches:

``` XML
<citation key="ref2">
<doi>10.5555/small_md_0001</doi>
</citation>
```

References may also be included as an unstructured citation. This option is not as precise as including an already-matched DOI or marking up a citation into individual elements.

``` XML
<citation key="ref=3">
<unstructured_citation>Clow GD, McKay CP, Simmons Jr. GM, and Wharton RA, Jr. 1988. Climatological observations and predicted sublimation rates at Lake Hoare, Antarctica. Journal of Climate 1:715-728.</unstructured_citation>
</citation>
```

As well as for depositing conventional references, data and software citations can also be deposited by inserting them into an item’s references metadata. To do so, follow the general process for depositing references as described above. Members can deposit the full data or software citation as an unstructured reference, or they can employ any number of reference tags currently accepted by us. It’s always best to include the DOI (either DataCite or Crossref) for the dataset if possible.

### Current elements for citation tagging<a id='00178' href='#00178'><i class='fas fa-link'></i></a>

* `<issn>`: ISSN of a series (print or electronic)
* `<journal_title>`
* `<author>`: first author of an article or other publication
* `<volume>`: volume number (journal or book set)
* `<issue>`: journal issue
* `<first_page>`
* `<cYear>`: year of publication
* `<article_title>`: journal article, conference paper, or book chapter title
* `<isbn>`
* `<series_title>`: title of a book or conference series
* `<volume_title>`: book or conference proceeding title
* `<edition_number>`
* `<article_title>`
* `<std_designator>`
* `<standards_body_name>`
* `<standards_body_acronym>`
* `<component_number>`: the chapter, section, part number for a content item in a book
* `<unstructured_citation>`: citations for which no structured data is available. Our ability to process unstructured citations is limited (learn more about [querying with formatted citations](/documentation/retrieve-metadata/xml-api/querying-with-formatted-citations))
* `<doi>`: include the DOI wherever possible

Learn more about the `<citation>` element in the [schema documentation](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html).

All citation elements are optional, but please submit as much information as you can to help us match your citations to DOIs.

Journal citations should include either `<issn>` or `<journal_title>` or both. `<journal_title>` only is preferred over `<issn>` only. In addition the first author (`<author>`) and `<first_page number>` should be submitted. `<first_page>` number is preferred, but for those citations that are "in press", the author should be submitted.

When submitting a book or conference citation, you should include an `<isbn>`, `<series_title>`, `<volume_title>`, or any combination of these three elements as may be available.

A citation for a standard must include a standard designator (`<std_designator>`) as well as the name and acronym of a standards body (`<standards_body_name>`, `<standards_body_acronym>`). These elements are required for identifying a citation of a standard.

### Metadata deposit example<a id='00179' href='#00179'><i class='fas fa-link'></i></a>

References may be included with a metadata deposit. The references are included within the `<citation_list>` element. Review the example below or [download an XML file](/xml-samples/article_with_references.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schema/deposit/crossref4.3.7.xsd">
 <head>
  <doi_batch_id>1dbb27d1030c6c9d9d-7ff0</doi_batch_id>
  <timestamp>200504260247</timestamp>
  <depositor>
   <depositor_name>your name</depositor_name>
   <email_address>your@email.com</email_address>
  </depositor>
  <registrant>WEB-FORM</registrant>
 </head>
 <body>
  <journal>
   <journal_metadata>
	<full_title>Test Publication</full_title>
	<abbrev_title>TP</abbrev_title>
	<issn media_type="print">12345678</issn>
   </journal_metadata>
   <journal_issue>
	<publication_date media_type="print">
 	<month>12</month>
 	<day>1</day>
 	<year>2005</year>
	</publication_date>
	<journal_volume>
 	<volume>12</volume>
	</journal_volume>
	<issue>1</issue>
   </journal_issue>
<!--This is the article's metadata-->
   <journal_article publication_type="full_text">
	<titles>
 	<title>Article 12292005 9:32</title>
	</titles>
	<contributors>
 	<person_name sequence="first" contributor_role="author">
  	<given_name>Bob</given_name>
  	<surname>Surname</surname>
  	<ORCID>http\://orcid.org/0000-0002-4011-3590</ORCID>
 	</person_name>
	</contributors>
	<publication_date media_type="print">
 	<month>12</month>
 	<day>1</day>
 	<year>2004</year>
	</publication_date>
	<pages>
 	<first_page>100</first_page>
 	<last_page>200</last_page>
	</pages>
	<doi_data>
 	<doi>10.50505/test_20051229930</doi>
 	<resource>http://www.crossref.org/</resource>
	</doi_data>
<!--the list of references cited in the above article-->
	<citation_list>
 	<citation key="ref1">
  	<journal_title>Current Opinion in Oncology</journal_title>
  	<author>Chauncey</author>
  	<volume>13</volume>
  	<first_page>21</first_page>
  	<cYear>2001</cYear>
 	</citation>
 	<citation key="ref2">
  	<doi>10.5555/small_md_0001</doi>
 	</citation>
 	<citation key="ref=3">
  	<unstructured_citation>Clow GD, McKay CP, Simmons Jr. GM, and Wharton RA, Jr. 1988. Climatological observations and predicted sublimation rates at Lake Hoare, Antarctica. Journal of Climate 1:715-728.</unstructured_citation>
 	</citation>
	</citation_list>
   </journal_article>
  </journal>
 </body>
</doi_batch>
```

### Resource-only deposit example<a id='00180' href='#00180'><i class='fas fa-link'></i></a>

References may be added to an existing metadata record using a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). Review the example below or [download an XML file](/xml-samples/article_with_references.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/doi_resources_schema/4.3.6" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.6" xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.3.6 http://www.crossref.org/schema/deposit/doi_resources4.3.6.xsd">
 <head>
  <doi_batch_id>123456</doi_batch_id>
  <depositor>
   <depositor_name>your name</depositor_name>
   <email_address>support@crossref.org</email_address>
  </depositor>
 </head>
 <body>
  <doi_citations>
<!--The DOI of the article that contains the citations  -->
   <doi>10.5555/small_md_0001</doi>
<!--The list of references cited in the above article -->
   <citation_list>
	<citation key="ref1">
 	<journal_title>Current Opinion in Oncology</journal_title>
 	<author>Chauncey</author>
 	<volume>13</volume>
 	<first_page>21</first_page>
 	<cYear>2001</cYear>
	</citation>
	<citation key="ref2">
 	<doi>10.5555/small_md_0001</doi>
	</citation>
	<citation key="ref=3">
 	<unstructured_citation>Clow GD, McKay CP, Simmons Jr. GM, and Wharton RA, Jr. 1988. Climatological observations and predicted sublimation rates at Lake Hoare, Antarctica. Journal of Climate 1:715-728.</unstructured_citation>
	</citation>
   </citation_list>
  </doi_citations>
 </body>
</doi_batch>
```
