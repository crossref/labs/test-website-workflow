+++
title = "Resource and full-text URLs"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide", "urls"]
identifier = "documentation/schema-library/markup-guide/urls"
rank = 4
weight = 60614
aliases = [
    "/education/content-registration/administrative-metadata/full-text-URLs",
    "/education/content-registration/administrative-metadata/full-text-URLs/",
    "/documentation/content-registration/administrative-metadata/full-text-URLs/",
    "/documentation/content-registration/administrative-metadata/full-text-URLs",
    "/education/content-registration/administrative-metadata/resource-resolution-URL",
    "/education/content-registration/administrative-metadata/resource-resolution-URL/",
    "/documentation/content-registration/administrative-metadata/resource-resolution-url/",
    "/documentation/content-registration/administrative-metadata/resource-resolution-url"
]
+++
The resolution URL is a link to the web page you want users to see when they click on your DOI. This [landing page](/documentation/member-setup/creating-a-landing-page/) needs to contain specific information including how to access your full-text content. For online works, this is often a link to content in HTML or PDF format, and for physical works, a catalog record including location details.

As well as the resolution URL, there are other URLs that you may include in the metadata for your content:

* Full-text URLs for Similarity Check - these URLs allow Turnitin to index your content and include it in the iThenticate database. You’ll include these URLs if you want to participate in the Similarity Check service, or if you want to make sure your content is included when other users check submitted manuscripts for similarity to content that’s already been published. Learn more about [Similarity Check](/documentation/similarity-check/).
* Full-text URL for text and data mining (TDM) - these URLs are provided specifically for researchers carrying out text and data mining. Learn more about [text and data mining](/documentation/retrieve-metadata/rest-api/text-and-data-mining-for-members/).
