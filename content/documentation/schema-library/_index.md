+++
title = "Schema library"
date = "2021-10-11"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library"]
aliases = [
    "/education/content-registration/metadata-deposit-schema",
    "/education/content-registration/metadata-deposit-schema/",
    "/education/content-registration/crossrefs-metadata-deposit-schema",
    "/education/content-registration/crossrefs-metadata-deposit-schema/",
    "/documentation/content-registration/metadata-deposit-schema",
    "/documentation/content-registration/metadata-deposit-schema/",
    "/schema"

]
[menu.main]
parent = "Documentation"
weight = 160000
[menu.documentation]
identifier = "documentation/schema-library"
parent = "Documentation"
rank = 4
weight = 160000
+++

The metadata that our members register with us is stored as XML, and our XML schema provides a structure and set of rules to keep everything consistent and interoperable.

If you're using one of our [helper tools](/documentation/member-setup/choose-content-registration-method#00479), you don't need to worry too much about the schema, as the tool you use will transform your information into XML for you. This means that if you are using one of our helper tools, you don't need to read this section of the documentation.

However, if you're [sending us XML directly](/documentation/content-registration/direct-deposit-xml/), it's important that you understand the schema, so this section of the documentation is for you.

As a registration agency of the International DOI Foundation, we follow the ISO/IEC 11179 Metadata Registry (MDR) standard, which specifies a schema for recording both the meaning and technical structure of the data for unambiguous usage by humans and computers.

We have a single deposit schema, which supports a range of different content types [(see full list of content types)](/documentation/register-maintain-records/).

We support [several versions](/documentation/schema-library/schema-versions/) of our schema but we recommend using the [latest version (v5.3.1)](/documentation/schema-library/metadata-deposit-schema-5-3-1/). For certain types of updates, we also offer the [resource-only section ](/documentation/schema-library/resource-only-deposit-schema-4-4-2/) of the schema. (Here's [more information](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/) on what you can update with a resource-only deposit).
