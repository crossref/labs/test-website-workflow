+++
title = "XSD schema quick reference"
date = "2021-10-05"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "xsd-schema-quick-reference"]
identifier = "documentation/schema-library/xsd-schema-quick-reference"
rank = 4
weight = 60403
aliases = [
    "/education/content-registration/metadata-deposit-schema/xsd-schema-quick-reference",
    "/education/content-registration/metadata-deposit-schema/xsd-schema-quick-reference/",
    "/documentation/content-registration/metadata-deposit-schema/xsd-schema-quick-reference/",
    "/education/content-registration/crossrefs-metadata-deposit-schema/crossref-xsd-schema-quick-reference/",
    "/documentation/content-registration/metadata-deposit-schema/xsd-schema-quick-reference"
]
+++

We support additional schema (not listed here) for legacy purposes.

## Deposit schema<a id='00004' href='#00004'><i class='fas fa-link'></i></a>

Used for registering and updating DOI metadata records:

|Schema|Purpose|Status|Further info|
|--- |--- |--- |--- |
|[grant_id0.1.1.xsd](http://www.crossref.org/schemas/grant_id0.1.1.xsd)|grant metadata | recommended | [documentation](/documentation/schema-library/markup-guide-record-types/grants/)|
|[crossref5.3.1.xsd](https://data.crossref.org/schemas/crossref5.3.1.xsd)|full metadata deposits|recommended|[documentation](https://data.crossref.org/reports/help/schema_doc/5.3.1/index.html)|
|[crossref4.8.1.xsd](https://data.crossref.org/schemas/crossref4.8.1.xsd)|full metadata deposits|recommended|[documentation](https://data.crossref.org/reports/help/schema_doc/4.8.1/index.html)|
|[crossref4.4.2.xsd](https://data.crossref.org/schemas/crossref4.4.2.xsd)|full metadata deposits|available|[documentation](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html)|
|[AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)|license data|imported|
|[relations.xsd](https://data.crossref.org/schemas/relations.xsd)|relationships between DOIs and other identifiers|imported|
|[clinicaltrials.xsd](https://data.crossref.org/schemas/clinicaltrials.xsd)|relationships between publications that report on a clinical trial|imported|
|[doi_resources4.4.2.xsd](https://data.crossref.org/schemas/doi_resources4.4.2.xsd)|used to append or update specific sets of metadata to an existing record|recommended|[documentation](https://data.crossref.org/reports/help/schema_doc/doi_resources4.4.2/index.html)|

## Query schema<a id='00005' href='#00005'><i class='fas fa-link'></i></a>

Used for formatting XML queries:

|Schema|Purpose|Further info|
|--- |--- |--- |
|[Crossref_query_input2.0.xsd](https://data.crossref.org/schemas/crossref_query_input2.0.xsd)|used to input XML queries to the system|[documentation](https://data.crossref.org/reports/help/schema_doc/query_input2.0/query_input.html)|

## Metadata retrieval schema<a id='00006' href='#00006'><i class='fas fa-link'></i></a>

Used for retrieving our metadata:

|Schema|Purpose|Further info|
|--- |--- |--- |
|[crossref_query_output2.0.xsd](https://data.crossref.org/schemas/crossref_query_output2.0.xsd)|returns query results in xsd_xml format, used for Cited-by results|[documentation](https://data.crossref.org/reports/help/schema_doc/crossref_query_output2.0/query_output2.0.html)|
|[crossref_query_output3.0.xsd](https://data.crossref.org/schemas/crossref_query_output3.0.xsd)|returns query results in UNIXSD format|[documentation](https://data.crossref.org/reports/help/schema_doc/crossref_query_output3.0/query_output3.0.html)|
|[unixref1.1.xsd](https://data.crossref.org/reports/help/schema_doc/unixref1.1/)|returns query results in the UNIXML format, also used to support the data delivered by our OAI-PMH service|[documentation](https://data.crossref.org/reports/help/schema_doc/unixref1.1/unixref1.1.html)|
|[unixref1.0.xsd](https://data.crossref.org/schemas/unixref1.0.xsd)|returns query results in the UNIXML format for some older content, also used to support the data delivered by our OAI-PMH service|[documentation](https://data.crossref.org/reports/help/schema_doc/unixref1.0/unixref.html)|
|[crossref_output3.0.1.xsd](https://data.crossref.org/schemas/crossref_output3.0.1.xsd)|used to support data files generated for bulk data distribution|[documentation](https://data.crossref.org/reports/help/schema_doc/crossref_output3.0.1/output.html)|
|[OAI-PMH.cr.xsd](https://data.crossref.org/schemas/OAI-PMH.cr.xsd)|accommodates differences between Crossref's OAI-PMH implementation and the published [Open Archives](http://www.openarchives.org/) OAI 2.0 schema | [documentation](https://data.crossref.org/reports/help/schema_doc/OAI-PMH/OAI-PMH.cr.html)|
