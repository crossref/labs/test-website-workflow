+++
title = "Books and chapters markup guide"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "books-and-chapters"]
identifier = "documentation/schema-library/markup-guide-record-types/books-and-chapters"
rank = 4
weight = 60602
aliases = [
    "/education/content-registration/content-type-markup-guide/books-and-chapters",
    "/education/content-registration/content-type-markup-guide/books-and-chapters/",
    "/education/content-registration/recommendations-by-content-type/books-and-book-chapters/",
    "/education/content-registration/recommendations-by-content-type/books-and-book-chapters",
    "/documentation/content-registration/content-type-markup-guide/books-and-book-chapters/",
    "/documentation/content-registration/content-type-markup-guide/books-and-book-chapters"
]
+++

This guide gives markup examples for members registering books and chapters by direct deposit of XML. You can also register the books and chapters content type using one of our helper tools: [web deposit form](/documentation/content-registration/web-deposit-form/).

On this page, learn more about:

* [Book structures](#00058)
* [Example of a book deposit containing a single book with chapters](#00059)
* [Example of a book series deposit](#00060)
* [Example of a book set deposit](#00061)

## Book structures<a id='00058' href='#00058'><i class='fas fa-link'></i></a>

`<book>` is the container for all information about a single book volume and (optionally) the book chapters. Books may be deposited as a single book, a series, or a set, and the metadata requirements differ slightly for each type.

* Book: a *book* is single book (monograph) that is not part of a series or a set. The title-level metadata for the book is captured in `<book_metadata>`.
* Book series: books that are part of an ongoing series and have an ISSN assigned should be deposited as a book series. Book metadata is captured in `<book_series_metadata>`, with series-specific details such as ISSN and series title captured in `<series_metadata>`. A series-level and volume-level title must be supplied for each book submitted as part of a series. A series-level ISBN and/or DOI may optionally be assigned. Examples of books in series:
    * Series: a sequence of books with certain characteristics in common that are formally identified together as a group. They may be released in successive parts once a year, or less often. For example, [Loeb Classical Library](http://www.hup.harvard.edu/collection.php?cpk=1031) or [Oxford World’s Classics](https://global.oup.com/academic/content/series/o/oxford-worlds-classics-owc/?type=listing&lang=en&cc=gb)
    * Monographs in series: if volumes can stand alone as separate books (if not, they are a *book set*). For example, [Advances in Experimental Medicine and Biology](https://www.springer.com/series/5584)
* Book set: book volumes that cannot stand alone as separate books must be deposited as a *book set*. A book set has a set-level title but does not require an ISSN. An ISBN and/or DOI may optionally be assigned at the set level. Example of a book set: *Le Deuxième Sexe* by Simone de Beauvoir, in two volumes: *Les faits et les mythes* and *L'expérience vécue*.

## Book metadata

* Book titles: Books with subtitles should use the `<title>` and `<subtitle>` tags to capture the appropriate segment of the title.
* Book title DOIs: A DOI is required for each book that you submit. It is not possible to submit DOI information for individual chapters without assigning a DOI to the entire work.
* ISBNs: ISBNs should be supplied when available. Both a print and electronic ISSN may be supplied for a book. If a book does not have an ISBN, the `<noisbn>` element is required.
* Contributors: If a book as a whole has a single set of authors, the author(s) should be included in the `<contributors>` section for the book itself. Editors may also be deposited. If each chapter has distinct authors and/or editors, the authors should also be included at the chapter (`<content_item>`) level.
* Editions: `<edition_number>`, when given, should include only a number and not additional text such as "edition". For example, you should submit "3", not "third edition".
* Citations: `<citation_list>` should only be used in `<book_metadata>` instead of `<content_item>` when the reference list is a separate section of the book and chapters are not included in the deposit.
* Book language: The language of the book should be specified in the book_metadata language attribute using the [ISO639 language code](https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#language.atts). If a book contains items in multiple languages this attribute should be set for the predominant language of the book. Individual items may have their language specified in content_item. If all content items are the same language, it is only necessary to specify the language at the book level.
* Book chapters: Book chapters are captured in the container element `<content_item>`. Book chapter DOIs are optional but strongly recommended if the book content will be commonly cited at the chapter level.

Metadata supplied in the top-level book section (`<book_metadata>`, `<book_set_metadata>`, or `<book_series_metadata>`) will be applied to the chapters as well unless distinct information is supplied for a chapter. This includes contributors, citation lists, and publication dates.

## Example of a book deposit containing a single book with chapters<a id='00059' href='#00059'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/monograph.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>2004-10-19-10-04-31-1016001</doi_batch_id>
<timestamp>20082117100522</timestamp>
<depositor>
<depositor_name>Sample Master</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>CrossRef</registrant>
</head>
<body>
<book book_type="edited_book">
<book_metadata language="en">
<titles>
<title>Use of Recycled Materials</title>
</titles>
<publication_date>
<year>2005</year>
</publication_date>
<isbn media_type="electronic">2912143691</isbn>
<publisher>
<publisher_name>RILEM Publications SARL</publisher_name>
</publisher>
<doi_data>
<doi>10.50505/book</doi>
<resource>http://www.crossref.org/hello</resource>
</doi_data>
</book_metadata>
<content_item component_type="chapter" publication_type="full_text">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>E.</given_name>
<surname>Vázquez</surname>
</person_name>
</contributors>
<titles>
<title>Miscellaneous</title>
</titles>
<pages>
<first_page>53</first_page>
<last_page>55</last_page>
</pages>
<publisher_item>
<item_number>rep030-006</item_number>
</publisher_item>
<doi_data>
<doi>10.50505/m4</doi>
<resource>
https://www.rilem.net/boutique/fiche.php?cat=book&reference=rep030-006
</resource>
</doi_data>
</content_item>
<content_item component_type="chapter" publication_type="full_text">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>L.</given_name>
<surname>De Bock</surname>
</person_name>
</contributors>
<titles>
<title>Recycled asphalt pavement</title>
</titles>
<pages>
<first_page>45</first_page>
<last_page>51</last_page>
</pages>
<publisher_item>
<item_number>rep030-005</item_number>
</publisher_item>
<doi_data>
<doi>10.50505/m5</doi>
<resource>
https://www.rilem.net/boutique/fiche.php?cat=book&reference=rep030-005
</resource>
</doi_data>
</content_item>
</book>
</body>
</doi_batch>
```

## Example of a book series deposit<a id='00060' href='#00060'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/book_series.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>123456</doi_batch_id>
<timestamp>20082003110604</timestamp>
<depositor>
<depositor_name>test data</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>test data</registrant>
</head>
<body>
<book book_type="edited_book">
<book_series_metadata language="en">
<series_metadata>
<titles>
<title>Electrochemistry</title>
</titles>
<issn>0305-9979</issn>
</series_metadata>
<contributors>
<person_name sequence="first" contributor_role="editor">
<given_name>D</given_name>
<surname>Pletcher</surname>
</person_name>
</contributors>
<titles>
<title>Electrochemistry</title>
</titles>
<volume>8</volume>
<publication_date media_type="print">
<year>1983</year>
</publication_date>
<isbn>978-0-85186-067-1</isbn>
<publisher>
<publisher_name>Royal Society of Chemistry</publisher_name>
<publisher_place>Cambridge</publisher_place>
</publisher>
<doi_data>
<doi>10.50505/testdoi13</doi>
<resource>http://ebook.rsc.org/?DOI=10.1039/9781847557179</resource>
</doi_data>
</book_series_metadata>
<content_item component_type="other" publication_type="full_text">
<contributors>
<person_name sequence="first" contributor_role="editor">
<given_name>D.</given_name>
<surname>Pletcher</surname>
</person_name>
</contributors>
<titles>
<title>Front cover</title>
</titles>
<pages>
<first_page>X001</first_page>
<last_page>X002</last_page>
</pages>
<doi_data>
<doi>10.50505/testdoi14</doi>
<resource>
http://ebook.rsc.org/?DOI=10.1039/9781847557179-FX001
</resource>
</doi_data>
</content_item>
</book>
</body>
</doi_batch>
```

## Example of a book set deposit<a id='00061' href='#00061'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/book_set.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>20080305081200</doi_batch_id>
<timestamp>200815071200</timestamp>
<depositor>
<depositor_name>Sample Master</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>Sample Data</registrant>
</head>
<body>
<book book_type="edited_book">
<book_set_metadata language="en">
<set_metadata>
<titles>
<title>Sample Set Title</title>
</titles>
<isbn media_type="print">0 571 08989 5</isbn>
</set_metadata>
<titles>
<title>Sample Volume Title</title>
</titles>
<volume>1</volume>
<publication_date media_type="print">
<year>2007</year>
</publication_date>
<isbn media_type="print">0064410145</isbn>
<publisher>
<publisher_name>Sample Publisher</publisher_name>
</publisher>
</book_set_metadata>
<content_item component_type="chapter" level_sequence_number="1" publication_type="full_text" language="en">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Patricia</given_name>
<surname>Feeney</surname>
</person_name>
</contributors>
<titles>
<title>Sample Chapter Title</title>
</titles>
<publication_date media_type="print">
<year>2007</year>
</publication_date>
<pages>
<first_page>1</first_page>
<last_page>200</last_page>
</pages>
<publisher_item>
<item_number item_number_type="sequence-number">S0091679X0861064X</item_number>
</publisher_item>
<doi_data>
<doi>10.50505/testset1</doi>
<resource>http://www.crossref.org/</resource>
</doi_data>
</content_item>
</book>
</body>
</doi_batch>
```
