+++
title = "Posted content (includes preprints) markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "posted-content-includes-preprints"]
identifier = "documentation/schema-library/markup-guide-record-types/posted-content-includes-preprints"
rank = 4
weight = 60624
aliases = [
    "/education/content-registration/content-type-markup-guide/posted-content-includes-preprints",
    "/education/content-registration/content-type-markup-guide/posted-content-includes-preprints/",
    "/education/content-registration/recommendations-by-content-type/posted-content-includes-preprints",
    "/documentation/content-registration/content-type-markup-guide/posted-content-includes-preprints",
    "/documentation/content-registration/content-type-markup-guide/posted-content-includes-preprints/"
]
+++
This guide gives markup examples for members registering posted content (includes preprints) by direct deposit of XML. It is not currently possible to register the posted content (includes preprints) content type using one of our helper tools.

## Depositing and updating posted content<a id='00084' href='#00084'><i class='fas fa-link'></i></a>

Posted content is a Crossref content type available starting with [schema](/documentation/content-registration/metadata-deposit-schema/) version 4.4.0. The schema updates include a number of posted content-specific elements. The top level element is called `posted_content` and has an attribute called `type`. This attribute is given a value from an enumerated list that defines the nature of the posted content. The current set of enumerations are:

* preprint
* working_paper
* letter
* dissertation
* report
* other

The default value is *preprint*. Please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) if you want to deposit metadata for a posted content type which is not on this list.

## Metadata elements<a id='00085' href='#00085'><i class='fas fa-link'></i></a>

The posted content type contains the following elements (\* = required):

|Element|Description|
|--- |--- |
|<code>group_title</code> |The hosting platform may organize its posted content into categories or subject areas. This field is used to name the container for the posted item|
|<code>contributors</code> |Container for author information|
|<code>titles</code>\* |The titles (title and subtitle) of the posted content|
|<code>posted_date</code>\* |The date when the posted content became available online on the hosting platform|
|<code>acceptance_date</code>|The date the content item was submitted to and accepted by the hosting platform|
|<code>institution</code>|Container for information about an organization that sponsored or hosted an item but is not the publisher|
|<code>program: funding</code>|Source of funding applicable to research related to the posted content|
|<code>program: access indicators</code>|License terms|
|<code>program: relations</code>|Relationships (other than bibliographic citation) to other works, such as found in acknowledgments or list of supplemental material|
|<code>doi_data</code>\* |Container for persistent identifier and URL|
|<code>citation_list</code>|Posted content bibliography listing of citations to other works|

## Updating metadata with relationship to AAM/VoR<a id='00086' href='#00086'><i class='fas fa-link'></i></a>

Once a posted content item has been published, the posted content publisher must update their publication metadata with the AAM/VoR DOI using the `isPreprintOf` relation type.

``` XML
<program xmlns="https://www.crossref.org/relations.xsd">
    <related_item>
         <!--DOI of the AM / VOR-->
         <intra_work_relation relationship-type="isPreprintOf" identifier-type="doi">10.5555/preprint_sample_doi_vor</intra_work_relation>
    </related_item>
</program>
```

The relationship metadata may be [updated](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata) with either a full metadata deposit (existing metadata plus the ‘relationship’ metadata) or as a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). Metadata deposited using the resource schema will be appended to the existing metadata.

## Posted content and conflicts<a id='00087' href='#00087'><i class='fas fa-link'></i></a>

When a posted content item is submitted for a published item that has a registered metadata record, a conflict is created. The conflict is resolved when the posted content item is updated with the relationship metadata for the published item's DOI. Learn more about the [conflict report](/documentation/reports/conflict-report).

## Posted content and Cited-by matches<a id='00088' href='#00088'><i class='fas fa-link'></i></a>

When you query for [Cited-by](/documentation/cited-by/) matches, you can choose to include posted content matches in your results. By default, posted content is not included. To retrieve matches including posted content:

### HTTPS queries<a id='00089' href='#00089'><i class='fas fa-link'></i></a>

Add the `include_postedcontent=true` parameter to your query, for example:

```
https://doi.crossref.org/servlet/getForwardLinks?usr=XXXX&pwd=XXXX&doi=10.5555/12345678&include_postedcontent=true
```

### XML queries<a id='00090' href='#00090'><i class='fas fa-link'></i></a>

Add the `include_postedcontent="true"` attribute to your `fl_query` element, for example:

``` XML
<?xml version = "1.0" encoding="UTF-8"?>
  <query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <head><doi_batch_id>eXtyles Request AMP.dodge0724.doc__76</doi_batch_id>
    </head>
<body>
    <fl_query include_postedcontent="true"><doi>10.5555/12345678</doi></fl_query>
</body>
</query_batch>
```

## Example of a posted content deposit<a id='00091' href='#00091'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/posted-content.xml).

``` XML
<posted_content>
       	<!--group_title: Prepublication content items may be organized into groupings within a given publisher. This element provides for naming the group. It is expected that publishers will have a small number of groups each of which reflect a topic or subject area. (required)--> 	 
       	<group_title>Metadata Quality</group_title>
       	<contributors>
           	<person_name sequence="first" contributor_role="author">
               	<given_name>Dorothy</given_name>
               	<surname>Depositor</surname>
               	<ORCID>http\://orcid.org/0000-0002-4011-3590</ORCID>
           	</person_name>
       	</contributors>
       	<titles>
           	<title>Mind your &lt; and &gt;: why XML needs to be valid</title>
       	</titles>
       	<!--posted_date: The date when the posted content became available online on the hosting platform (required)-->
       	<posted_date>
           	<month>01</month>
           	<day>15</day>
           	<year>1971</year>
       	</posted_date>
       	<!--acceptance_date: date the content item was submitted to and accepted by the hosting platform. (optional) -->
       	<acceptance_date>
          	<month>01</month>
           	<day>01</day>
           	<year>1971</year>
       	</acceptance_date>
       	<!--optional funding and license data-->
       	<program name="fundref" xmlns="http://www.crossref.org/fundref.xsd">
           	<assertion name="fundgroup">
               	<assertion name="funder_name"> U.S. Department of Energy <assertion
                       	name="funder_identifier">100000015</assertion>
               	</assertion>
               	<assertion name="award_number">DE-FG03-03SF22691</assertion>
           	</assertion>
           	<assertion name="fundgroup">
               	<assertion name="funder_name"> U.S. Department of Energy <assertion
                       	name="funder_identifier">100000015</assertion>
               	</assertion>
               	<assertion name="award_number">DE-AC52-06NA27279</assertion>
           	</assertion>
       	</program>
       	<program xmlns="http://www.crossref.org/AccessIndicators.xsd">
           	<free_to_read start_date="2016-01-01"/>
           	<license_ref>http://some.co.org/license_page.html</license_ref>
       	</program>
       	<!--DOI and URL (required)-->
       	<doi_data>
           	<doi>10.50505/preprint_sample_doi_1</doi>
           	<resource>http://www.crossref.org/index.html</resource>
       	</doi_data>
       	<!--citation list for the item (optional)-->
       	<citation_list>
           	<citation key="pp1">
               	<doi>10.5555/12345678</doi>
           	</citation>
       	</citation_list>
   	</posted_content>
```

## Example of a posted content deposit containing a relationship to a VOR<a id='00092' href='#00092'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/posted-content.updated.xml).

``` XML
<!-- relationship established with VOR DOI (required when VOR is identified)-->
    <program xmlns="https://www.crossref.org/relations.xsd">
       <related_item>
          <intra_work_relation relationship-type="isPreprintOf" identifier-type="doi">10.5555/preprint_sample_doi_vor</intra_work_relation>
       </related_item>
     </program>
           <!--DOI and URL (required)-->
           <doi_data>
               <doi>10.50505/preprint_sample_doi_1</doi>
               <resource>https://www.crossref.org/index.html</resource>
           </doi_data>
```

## Example of a journal article deposit that includes a relationship to posted content<a id='00093' href='#00093'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/journal.pc.xml).

``` XML
<publication_date>
   <year>2016</year>
</publication_date>
<!--relationship established with posted content (preprint) DOI-->
<program xmlns="https://www.crossref.org/relations.xsd">
   <related_item>
     <intra_work_relation relationship-type="hasPreprint" identifier-type="doi">10.50505/preprint_sample_doi_1</intra_work_relation>
   </related_item>
</program>
               <doi_data>
                   <doi>10.5555/preprint_sample_doi_vor</doi>
                   <resource>https://www.crossref.org/index.html</resource>
               </doi_data>
```

## Example of email notification of posted content match<a id='00094' href='#00094'><i class='fas fa-link'></i></a>

>Member Example Publishing has deposited DOI 10.5555/preprint_sample_doi_vor (http://doi.org/10.5555/preprint_sample_doi_vor) claiming it is the VoR for your posted content DOI 10.50505/preprint_sample_doi_1.
Please display a link to the Version of Record from your posted content online. Linking posted content to the published record is critical to enabling the full history of scholarly results, and ensuring that the citation record is clear and up-to-date.
If you have questions please contact support@crossref.org and one of our colleagues (in the EST timezone) will get back to you.
Many thanks,
Crossref
