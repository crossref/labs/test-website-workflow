+++
title = "Peer reviews markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "peer-reviews"]
identifier = "documentation/schema-library/markup-guide-record-types/peer-reviews"
rank = 4
weight = 60618
aliases = [
    "/education/content-registration/content-type-markup-guide/peer-reviews",
    "/education/content-registration/content-type-markup-guide/peer-reviews/",
    "/education/content-registration/recommendations-by-content-type/peer-reviews/",
    "/education/content-registration/recommendations-by-content-type/peer-reviews",
    "/documentation/content-registration/content-type-markup-guide/peer-reviews/",
    "/documentation/content-registration/content-type-markup-guide/peer-reviews"
]
+++

This guide gives markup examples for members registering peer reviews by direct deposit of XML. It is not currently possible to register the peer reviews content type using one of our helper tools.

## Getting started with registering peer reviews<a id='00075' href='#00075'><i class='fas fa-link'></i></a>

Registration of peer reviews is supported as of [schema](/documentation/content-registration/metadata-deposit-schema/) version 4.4.1. Peer reviews include referee reports, decision letters, and author responses. You may also register post-publication reviews using our peer review content type.

Peer review metadata includes a number of review-specific elements. Many are optional to accommodate differences in review practices, but please include all elements relevant to your reviews when submitting your metadata records.

Our schema includes support for the following fields:

* `contributor`, to capture reviewer name and role, choose from:
    * reviewer
    * review-assistant
    * stats-reviewer
    * reviewer-external
    * reader
    * translator
    * anonymous
* `title`
* `review_date`
* `institution`
* `competing_interest_statement`
* `running_number`
* `license data`
* `relations`
* `stage`
* `type`
* `recommendation`
* `revision-round`
* `language`

Note that all reviews must include relationship metadata linking the review with the item being reviewed. Learn more about [obligations and limitations for peer review registration](/documentation/content-registration/content-types-intro/peer-reviews#00073).

| Element | Description | Limits |
|---|---|---|
| [contributor](/documentation/content-registration/descriptive-metadata/contributors/), includes person_name or anonymous | Captures reviewer name and role. If anonymous, must capture as \<anonymous/\>.<br>Peer review roles are: reviewer, review-assistant, stats-reviewer, reviewer-external, reader, translator, author, editor | optional |
| title | Title of review. If you don’t have a review-specific title convention, we recommend that you include _Review_ (or member’s own term for review) in your peer review registration, as well as a revision and review number.<br>For example, a review pattern of _Review: title of article (Revision number/Review number)_ will be:<br> _Review: Analysis of the effects of bad metadata on discoverability (R2/RC3)_ | required |
| review_date | Date of review, including month, day, year | _year_ is required |
| institution | Organization (member or other) submitting the peer review, strongly advised if submitter differs from publisher of item being reviewed | optional, may include up to 5 |
| competing_interest_statement | Competing interest statement provided by review author during review process | optional |
| running_number | Internal number/identifier used to identify specific review | optional |
| [license data](/documentation/content-registration/administrative-metadata/license-information/) | Text or data mining license info | optional |
| [relations](/documentation/content-registration/structural-metadata/relationships/) | Relate review to item being reviewed through relationships - must supply the DOI of item being reviewed as an inter-work relation with review type <code>isReviewOf</code> | required |

Some metadata is captured as attributes with specific enumerated values:

|Attributes|Description|Limits|
|--- |--- |--- |
|stage|Options are <code>pre-publication</code> and <code>post-publication</code> |optional|
|type|Types of report include: <code>referee-report</code>, <code>editor-report</code>, <code>author-comment</code>, <code>community-comment</code>, <code>aggregate</code>, <code>recommendation</code>|optional|
|recommendation|Values are: <code>major-revision</code>, <code>minor-revision</code>, <code>reject</code>, <code>reject-with-resubmit</code>, <code>accept</code>|optional|
|revision-round|Revision round number, first submission is defined as revision round 0|optional|
|language|Language of review|optional|

The types are refer to the following categories of peer reviews:

* **referee-report**: also commonly known as a reviewer report - comments provided by someone who has been verified as an expert in the field of the work, usually invited by an editor. Note that we treat the terms reviewer and referee as interchangeable.
* **editor-report**: comments by an editor representing the journal or platform where the work is submitted. It might contain feedback on other peer reviews or a decision on whether to accept the work for publication.
* **author-comment**: a response by one or more authors to peer review reports on their work.
* **community-comment**: comments made on a work from the community at large. These are usually part of a public call for reviews, rather than personally invited.
* **aggregate**: a summary of a review process, for example collating the responses of several referees and/or editors.
* **recommendation**: Reporting an editorial decision on a peer review with one of the values: `major-revision`, `minor-revision`, `reject`, `reject-with-resubmit`, `accept`.

## Example of connecting a review to the reviewed item through relations<a id='00076' href='#00076'><i class='fas fa-link'></i></a>

``` XML
<program xmlns="https://www.crossref.org/relations.xsd">
   <related_item>
      <description>Referee report of Treatment of plaque psoriasis with an ointment
formulation of the Janus kinase inhibitor, tofacitinib: a Phase 2b randomized
clinical trial</description>
      <inter_work_relation relationship-type="isReviewOf" identifier-type="doi"
>10.1186/s12895-016-0051-4</inter_work_relation>
   </related_item>
</program>
```

## Example of a complete review<a id='00077' href='#00077'><i class='fas fa-link'></i></a>

``` XML
<?xml version="1.0" encoding="UTF-8"?>
   <doi_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 http://www.crossref.org/schema/crossref4.4.2.xsd"
xmlns="http://www.crossref.org/schema/4.4.2" version="4.4.2">
  	<head>
     	<doi_batch_id>20170807</doi_batch_id>
     	<timestamp>2017080715731</timestamp>
     	<depositor>
       	<depositor_name>Crossref</depositor_name>
       	<email_address>support@crossref.org</email_address>
     	</depositor>
     	<registrant>Crossref</registrant>
  	</head>
  	<body>
     	<peer_review stage="pre-publication" revision-round="1" recommendation="accept">
         	<contributors>
           	<person_name contributor_role="reviewer" sequence="first">
              	<given_name>Wilson</given_name>
              	<surname>Liao</surname>
           	</person_name>
         	</contributors>
         	<titles>
            	<title>Review: Treatment of plaque psoriasis with an ointment formulation of the Januskinase inhibitor, tofacitinib: a Phase 2b randomized clinical trial. V1</title>
         	</titles>
         	<review_date>
            	<month>08</month>
            	<day>19</day>
            	<year>2016</year>
         	</review_date>
         	<competing_interest_statement> There were no competing interests</competing_interest_statement>
         	<running_number>RC1 </running_number>
         	<program xmlns="http://www.crossref.org/relations.xsd">
            	<related_item>
              	<description>Referee report of Treatment of plaque psoriasis with an ointment formulation of the Janus kinase inhibitor, tofacitinib: a Phase 2b randomized clinical trial</description>
              	<inter_work_relation relationship-type="isReviewOf" identifier-type="doi"
>10.1186/s12895-016-0051-4 </inter_work_relation>
            	</related_item>
         	</program>
         	<doi_data>
            	<doi>10.5555/abc123</doi>
            	<resource>https://www.example.org/openpeerreview/art%3A10.1186%2Fs12895-016-0051-4/12895_2016_51_ReviewerReport_V2_R1.pdf</resource>
         	</doi_data>
     	</peer_review>
   	</body>
   </doi_batch>
```
