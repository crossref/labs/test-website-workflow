+++
title = "Metadata deposit schema 4.4.2"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "metadata-deposit-schema-4-4-2"]
identifier = "documentation/schema-library/metadata-deposit-schema-4-4-2"
rank = 4
weight = 60409
aliases = [
    "/education/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-2",
    "/education/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-2/",
    "/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-2/",
    "/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-2"
]
+++

Beginning with deposit schema version `4.4.2`, all Crossref schema releases are available in our GitLab schema repository as a bundle. [Bundle 0.1.0](https://gitlab.com/crossref/schema/-/releases) contains schema version `4.4.2` and associated files.

Schema: [crossref4.4.2.xsd](https://data.crossref.org/schemas/crossref4.4.2.xsd)
Full documentation: [4.4.2](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html)

Crossref included schema:

* [common4.4.2.xsd](https://data.crossref.org/schemas/common4.4.2.xsd)
* [fundref.xsd](https://data.crossref.org/schemas/fundref.xsd)
* [AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)
* [clinicaltrials.xsd](https://data.crossref.org/schemas/clinicaltrials.xsd)
* [relations.xsd](https://data.crossref.org/schemas/relations.xsd)

External imported schema:

* [MathML](http://www.w3.org/Math/XMLSchema/mathml3/mathml3.xsd)
* [JATS](https://jats.nlm.nih.gov/publishing/1.2/xsd/JATS-journalpublishing1.xsd)

Changes from `4.4.1`

* support for [pending publication](/documentation/schema-library/markup-guide-record-types/pending-publications/)
* support for [distributed usage logging (DUL)](/working-groups/distributed-usage-logging/)
* support for JATS 1.2 abstracts
* add abstract support to dissertations, reports, and allow multiple abstracts wherever available
* add support for multiple dissertation authors
* add `acceptance_date` element to journal article, books, book chapter, conference paper
