+++
title = "How do I create reference links?"
date = "2020-04-08"
draft = false
author = "Anna Tolwinska"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reference-linking", "how-do-i-create-reference-links"]
identifier = "documentation/reference-linking/how-do-i-create-reference-links/"
rank = 4
weight = 70100
aliases = [
  "/education/reference-linking/how-do-I-create-reference-links",
  "/education/reference-linking/how-do-I-create-reference-links/"
]
+++

**Step 1**: Find DOIs for as many of your references as possible. There are two ways to find DOIs for your references:

  * [Simple Text Query](https://apps.crossref.org/SimpleTextQuery) - paste your reference lists into this web form, and it will return matches. This is a manual interface, and is suitable for low-volume querying.
  * [XML API](/documentation/retrieve-metadata/xml-api/) - submit XML formatted according to the query schema section to our system as individual requests or as a batch upload. This method requires API skills, and allows you significant control over your query execution and results.

**Step 2**: Display the DOIs in your references: once you have retrieved the relevant DOIs, you must display them as URLs in your references (following our [DOI display guidelines](/display-guidelines)).

{{< figure-linked
	src="/images/documentation/DOI-display.png"
	large="/images/documentation/DOI-display.png"
	alt="DOI displayed according to display guidelines"
	title="DOI displayed according to display guidelines"
	id="image109"
>}}
