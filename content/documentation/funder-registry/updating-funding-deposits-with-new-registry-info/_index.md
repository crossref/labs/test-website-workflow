+++
title = "Updating funding deposits with new registry info"
date = "2020-04-08"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = ["funder-registry", "updating-funding-deposits-with-new-registry-info"]
identifier = "documentation/funder-registry/updating-funding-deposits-with-new-registry-info"
rank = 4
weight = 90400
aliases = [
	"/education/funder-registry/updating-funding-deposits-with-new-registry-info",
	"/education/funder-registry/updating-funding-deposits-with-new-registry-info/"
]
+++

The [Funder Registry](https://doi.org/10.13039/fundref_registry) is growing rapidly. New funders are continually being evaluated for inclusion, and the Registry is updated almost every month. Once added, a funder name is given a DOI, making it an official Registry entry. Check regularly and add funder IDs to existing records where no funder ID was available at the time of first deposit.

## Updating deposited data to reflect registry changes<a id='00300' href='#00300'><i class='fas fa-link'></i></a>

If an appropriate identifier did not exist in the registry (or a match could not be made) at the time of deposit, metadata will need to be updated to include new identifiers and better registry metadata.

### getFunders API<a id='00301' href='#00301'><i class='fas fa-link'></i></a>

We have a simple getFunders service to help you identify Funder Registry changes that affect your existing deposits. The getFunders service displays the funder information for a DOI (including Registry changes).

For example, DOI [https://doi.org/10.1037/0735-7036.121.3.306](https://doi.org/10.1037/0735-7036.121.3.306) was registered on 18 May 2017 with the following funding data. This deposit identifies five sources of funding for the article, only one of which was identified with a registry DOI.

```
<xref:publisher_item>
<xref:identifier id_type="other">2007-11961-009</xref:identifier>
<xref:identifier id_type="pmid">17696657</xref:identifier>
</xref:publisher_item>
<fr:program xmlns:fr="http://www.crossref.org/fundref.xsd">
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">
American Psychological Association
<fr:assertion name="funder_identifier">http://dx.doi.org/10.13039/100006324</fr:assertion>
</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">
American Association of University Women
<fr:assertion name="funder_identifier">http://dx.doi.org/10.13039/100005280</fr:assertion>
</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">Soroptimist International</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">SEASPACE</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">
Walt Disney Company
<fr:assertion name="funder_identifier">http://dx.doi.org/10.13039/100004795</fr:assertion>
</fr:assertion>
</fr:assertion>
</fr:program>
<xref:doi_data>
<xref:doi>10.1037/0735-7036.121.3.306</xref:doi>
<xref:resource>
http://doi.apa.org/getdoi.cfm?doi=10.1037/0735-7036.121.3.306
</xref:resource>
```

The API call [https://doi.crossref.org/getFunders?q=10.1037/0735-7036.121.3.306](https://doi.crossref.org/getFunders?q=10.1037/0735-7036.121.3.306) shows these same five funders with additional Registry information (in JSON format)

```
{
"fundedItemDOI": "10.1037/0735-7036.121.3.306",
"funders": [
{
"asDeposited": "American Psychological Association",
"depositedAsIdentifier": "false",
"suggested": [
{
"isRegistryName": "true",
"identifier": "10.13039/100006324",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "American Psychological Association"
},
{
"isRegistryName": "true",
"identifier": "10.13039/100006324",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "American Psychological Association"
},
{
"isRegistryName": "true",
"identifier": "10.13039/100006324",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "American Psychological Association"
}
]
},
{
"asDeposited": "American Association of University Women",
"depositedAsIdentifier": "false",
"suggested": [
{
"isRegistryName": "true",
"identifier": "10.13039/100005280",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "American Association of University Women"
},
{
"isRegistryName": "true",
"identifier": "10.13039/100005280",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "American Association of University Women"
},
{
"isRegistryName": "true",
"identifier": "10.13039/100005280",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "American Association of University Women"
}
]
},
{
"asDeposited": "Soroptimist International",
"depositedAsIdentifier": "false",
"suggested": []
},
{
"asDeposited": "SEASPACE",
"depositedAsIdentifier": "false",
"suggested": []
},
{
"asDeposited": "Walt Disney Company",
"depositedAsIdentifier": "false",
"suggested": [
{
"isRegistryName": "true",
"identifier": "10.13039/100004795",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "Walt Disney Company"
},
{
"isRegistryName": "true",
"identifier": "10.13039/100004795",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "Walt Disney Company"
},
{
"isRegistryName": "true",
"identifier": "10.13039/100004795",
"country": "http://sws.geonames.org/6252001/",
"prefLabel": "Walt Disney Company"
}
]
}
]
}
```

This tells you that:

* *SEASPACE* was deposited without an identifier, and nothing in the registry has changed
* *Walt Disney Company* was deposited without an identifier, but there is a registry entry for *The Walt Disney Company* which has a registry DOI
* *Soroptimist International* was deposited without an identifier, and nothing in the registry has changed
* *American Psychological Association* was deposited without an identifier, but it does have a registry DOI
* *American Association of University Women* was deposited with a registry DOI.

## Depositing funding and license metadata using a .csv file<a id='00302' href='#00302'><i class='fas fa-link'></i></a>

We support the deposit of funding and [text and data mining license](/documentation/retrieve-metadata/rest-api) metadata in .csv format through our web deposit form - learn how to do a [supplemental metadata upload using a .csv file](/documentation/content-registration/web-deposit-form#00318).
