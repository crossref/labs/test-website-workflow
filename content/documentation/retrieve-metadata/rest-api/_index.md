+++
title = "REST API"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "rest-api"]
identifier = "documentation/retrieve-metadata/rest-api"
rank = 4
weight = 30100
aliases = [
  "/education/retrieve-metadata/rest-api",
  "/education/retrieve-metadata/rest-api/",
  "/services/metadata-delivery/rest-api/"
]
+++

Our publicly available REST API exposes the metadata that members deposit with Crossref when they register their content with us. And it’s not just the bibliographic metadata either: funding data, license information, full-text links, ORCID iDs, abstracts, and Crossmark updates are in members’ metadata too. You can search, facet, filter, or sample metadata from thousands of members, and the results are returned in JSON. Learn more in our [REST API documentation](https://api.crossref.org).

Numerous tools and services rely on our metadata, be they for search, annotation, sharing, or analysis. Some common uses for our REST API include:

* [Text and data mining](/documentation/retrieve-metadata/rest-api/text-and-data-mining)
* Helping with auditing funder mandates
* Identifying author publications

Many familiar organizations use our metadata through this modern machine interface. Check out the [API case studies](/categories/api-case-study) from organizations like Authorea and SHARE.

No sign-up is required to use the REST API, and the data can be treated as facts from members. The data is not subject to copyright, and you may use it for any purpose.  

Crossref generally provides metadata without restriction; however, some abstracts contained in the metadata may be subject to copyright by publishers or authors.  
