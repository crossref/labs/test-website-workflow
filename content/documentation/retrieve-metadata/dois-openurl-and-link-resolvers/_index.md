+++
title = "DOIs, OpenURL, and link resolvers"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "dois-openurl-and-link-resolvers"]
identifier = "documentation/retrieve-metadata/dois-openurl-and-link-resolvers"
aliases =[
    "/02publishers/16openurl.html",
    "/education/retrieve-metadata/dois-openurl-and-link-resolvers",
    "/education/retrieve-metadata/dois-openurl-and-link-resolvers/"
]
rank = 4
weight = 309000
+++

DOIs point to the authoritative version of content on the publisher's web site and to publisher-designated resources. In an institutional context, it is often useful to direct users to other resources via their institution's link resolver.

Crossref acts as a source of metadata to enhance OpenURL-based local link resolvers and supports DOI redirection for localized linking within library holdings.

## How DOIs and OpenURL work together<a id='00413' href='#00413'><i class='fas fa-link'></i></a>

DOIs and OpenURL work together in several ways. First, the DOI resolver itself---where link resolution occurs---is OpenURL-enabled. This means that it can recognize a user with access to a local resolver. When a user clicks on a DOI link, it is used as a key to pull the metadata needed to create the OpenURL targeting the local link resolver out of our metadata, and redirects that DOI back to the user's local resolver. The institutional user is then directed to appropriate resources. 

It works this way:

1. A library user clicks a DOI link within a link resolver-enabled resource.
2. A cookie on the user’s machine alerts the DOI proxy server to redirect this DOI to the local linking server.
3. The local linking server receives the metadata needed for local resolution, either from the source of the link or from Crossref via OpenURL.
