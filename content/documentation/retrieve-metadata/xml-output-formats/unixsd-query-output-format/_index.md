+++
title = "UNIXSD query output format"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-output-formats", "unixsd-query-output-format"]
identifier = "documentation/retrieve-metadata/xml-output-formats/unixsd-query-output-format"
rank = 4
weight = 31101
aliases = [
  "/education/retrieve-metadata/xml-output-formats/unixsd-query-output-format",
  "/education/retrieve-metadata/xml-output-formats/unixsd-query-output-format/"
]
+++

UNIXSD query results include member metadata as deposited (as with the UNIXREF format) as well as some Crossref-generated information about the metadata record. UNIXSD is the most comprehensive metadata output format available for our metadata records.

UNIXSD format will return deposited references for other members. References will also be returned to members querying for their own deposited data.

## UNIXSD metadata<a id='00452' href='#00452'><i class='fas fa-link'></i></a>

UNIXSD results contain a sequence of Crossref produced meta-metadata including:

* `book-id`: a Crossref internal identifier assigned to a non-journal title (book, conference proceeding, database, standard, dissertation, or report/working paper)
* `citation-id`: a Crossref internal identifier assigned to a DOI record
* `citedby-count`: number of Cited-by matches identified by Crossref
* `created`: the date the record was created
* `deposit-timestamp`: timestamp provided in most recent submission
* `journal-id`: a Crossref internal identifier assigned to a journal title
* `last-update`: the date the record was last updated
* `member-id`: a Crossref internal identifier assigned to a member
* `owner-prefix`: the prefix that ‘owns’ (has permissions to update) the DOI record
* `prefix-name`: name associated with the prefix
* `prime`: the DOI a record is aliased to (if the record is aliased)
* `publisher-name`: member account name
* `relation`: related item, includes ‘type’ attribute to identify the identifier, and ‘claim’ to identify type of relationship
* `series-id`: a Crossref internal identifier assigned to a series title (applies to book and conference proceeding series)

This meta-metadata is contained in a `<crm-item>` element, for example:

```
<crm-item name="citedby-count" type="number">0</crm-item>
<crm-item name="owner-prefix" type="string">10.1353</crm-item>
<crm-item name="citation-id" type="number">25715607</crm-item>
<crm-item name="journal-id" type="number">48965</crm-item>
<crm-item name="book-id" type="number" />
<crm-item name="series-id" type="number" />
<crm-item name="last-update" type="date">2007-08-07 15:31:43.0</crm-item>
```

The full metadata record as deposited by members is available as well. The member as-deposited XML will begin with the `<crossref>` element.

## UNIXSD schema<a id='00453' href='#00453'><i class='fas fa-link'></i></a>

UNIXSD results are generated using `crossref_query_output3.0.xsd` ([schema](https://data.crossref.org/schemas/crossref_query_output3.0.xsd) | [documentation](https://data.crossref.org/reports/help/schema_doc/crossref_query_output3.0/output.html))

### Example UNIXSD result<a id='00454' href='#00454'><i class='fas fa-link'></i></a>

```
<crossref_result xmlns="http://www.crossref.org/qrschema/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" xsi:schemaLocation="https://www.crossref.org/qrschema/3.0 https://data.crossref.org/schemas/crossref_query_output3.0.xsd">
 <query_result>
  <head>
   <doi_batch_id>none</doi_batch_id>
  </head>
  <body>
   <query status="resolved">
 	<doi type="journal_article">10.1353/aq.1998.0005</doi>
 	<crm-item name="publisher-name" type="string">Johns Hopkins University Press</crm-item>
 	<crm-item name="prefix-name" type="string">Muse - Johns Hopkins University Press</crm-item>
 	<crm-item name="member-id" type="number">147</crm-item>
 	<crm-item name="citation-id" type="number">25715607</crm-item>
 	<crm-item name="journal-id" type="number">48965</crm-item>
 	<crm-item name="deposit-timestamp" type="number">20070206205234</crm-item>
 	<crm-item name="owner-prefix" type="string">10.1353</crm-item>
 	<crm-item name="last-update" type="date">2007-02-13T20:56:13Z</crm-item>
 	<crm-item name="created" type="date">2007-02-07T02:04:57Z</crm-item>
 	<crm-item name="citedby-count" type="number">1</crm-item>
 	<doi_record>
  	<crossref xmlns="https://www.crossref.org/xschema/1.0" xsi:schemaLocation="https://www.crossref.org/xschema/1.0 https://doi.crossref.org/schemas/unixref1.0.xsd">
   	<journal>
    	<journal_metadata language="en">
     	<full_title>American Quarterly</full_title>
     	<abbrev_title>American Quarterly</abbrev_title>
     	<issn media_type="electronic">1080-6490</issn>
    	</journal_metadata>
    	<journal_issue>
     	<publication_date media_type="print">
      	<year>1998</year>
     	</publication_date>
     	<journal_volume>
      	<volume>50</volume>
     	</journal_volume>
     	<issue>1</issue>
    	</journal_issue>
    	<journal_article publication_type="full_text">
     	<titles>
      	<title>&quot;Disturbing the Peace: What Happens to American Studies If You Put African American Studies at the Center?&quot;: Presidential Address to the American Studies Association, October 29, 1997</title>
     	</titles>
     	<contributors>
      	<person_name sequence="first" contributor_role="author">
       	<given_name>Mary Helen.</given_name>
       	<surname>Washington</surname>
      	</person_name>
     	</contributors>
     	<publication_date media_type="print">
      	<year>1998</year>
     	</publication_date>
     	<pages>
      	<first_page>1</first_page>
      	<last_page>23</last_page>
     	</pages>
     	<doi_data>
      	<doi>10.1353/aq.1998.0005</doi>
       	<timestamp>20070206205234</timestamp>
       	<resource>
http://muse.jhu.edu/content/crossref/journals/american_quarterly/v050/50.1washington.html</resource>
     	</doi_data>
    	</journal_article>
   	</journal>
  	</crossref>
 	</doi_record>
   </query>
  </body>
 </query_result>
</crossref_result>
```
