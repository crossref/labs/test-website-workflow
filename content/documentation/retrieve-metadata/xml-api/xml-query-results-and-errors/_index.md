+++
title = "XML query results and errors"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "xml-query-results-and-errors"]
identifier = "documentation/retrieve-metadata/xml-api/xml-query-results-and-errors"
rank = 4
weight = 31004
aliases = [
  "/education/retrieve-metadata/xml-api/xml-query-results-and-errors",
  "/education/retrieve-metadata/xml-api/xml-query-results-and-errors/"
]
+++

Query results may be requested in three [different formats: XSD_XML, UNIXREF, and UNIXSD](/documentation/retrieve-metadata/xml-output-formats/).

UNIXSD is the recommended and most robust format, but XSD_XML is the default for many services for legacy reasons.

Possible errors returned by malformed or insufficient queries include:

* an invalid XML query will return no result (`<body/>`)
* either `first page` or `author` must be supplied
* either `ISSN` or `journal title` must be supplied
* unreasonable DOI found (an *unreasonable DOI* does not follow the expected DOI format (`10.XXXX/yyy...`) and/or exceeds 200 characters)
* piped query must contain either 9 pipes (for journals) or 11 pipes (for books/conference proceedings)
* either `ISSN/ISBN` or `series/volume title` must be supplied.
