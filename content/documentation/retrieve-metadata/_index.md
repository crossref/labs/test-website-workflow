+++
title = "Metadata Retrieval"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata"]
aliases = [
  "/education/retrieve-metadata",
  "/education/retrieve-metadata/",
  "/04intermediaries/60affiliate_rules.html",
]
[menu.main]
parent = "Documentation"
identifier = "documentation/metadata-retrieval"
weight = 450000
[menu.documentation]
parent = "Documentation"
identifier = "documentation/retrieve-metadata"
rank = 4
weight = 450000
+++

{{< snippet "/_snippet-sources/metadata-retrieval-intro.md" >}}
{{< snippet "/_snippet-sources/metadata-retrieval-detail.md" >}}
