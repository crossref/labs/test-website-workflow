+++
title = "How to participate in Cited-by"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["cited-by", "cited-by-participation"]
identifier = "documentation/cited-by/cited-by-participation"
rank = 4
weight = 80100
aliases = [
    "/education/cited-by/cited-by-participation/",
    "/education/cited-by/cited-by-participation"
]
+++

Members can participate in Cited-by by completing the following steps:

1. [Deposit references](/documentation/schema-library/markup-guide-metadata-segments/references/) for one or more prefixes as part of your [content registration](/services/content-registration/) process. Use your [Participation Report](https://www.crossref.org/members/prep/) to see your progress with depositing references.
2. [Contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) to ask for the Cited-by service to be enabled for your prefix(es).
3. We match the metadata in the references to DOIs to establish Cited-by links in the database. As new content is registered, we automatically update the citations and, for those members with [Cited-by alerts enabled](/documentation/cited-by/retrieve-citations/#00272), we notify you of the new links.
4. [Retrieve citations](/documentation/cited-by/retrieve-citations/) through a URL query or the admin tool.
5. Display the links on your website. We recommend displaying citations you retrieve on DOI landing pages, for example:

<figure><a href='https://doi.org/10.1177%2F0163443715620931'><img src='/images/documentation/Cited-by-display-example-landing-page.png' alt='Example landing page with Cited-by display' title='' width='75%'></a></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image22">Show image</button>
<div id="image22" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Cited-by-display-example-landing-page.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

If you are a member through a Sponsor, you may have access to Cited-by through your sponsor – please contact them for more details.

## Citation matching<a id='00269' href='#00269'><i class='fas fa-link'></i></a>

Members sometimes submit references without including a DOI tag for the cited work. When this happens, we look for a match based on the metadata provided. If we find one, the reference metadata is updated with the DOI and we add the `"doi-asserted-by": "crossref"` tag. If we don’t find a match immediately, we will try again at a later date.

There are some references for which we won’t find matches, for example where a DOI has been registered with an agency other than Crossref (such as [DataCite](https://datacite.org/)) or if the reference refers to an object without a DOI, including conferences, manuals, blog posts, and some journals’ articles.

To perform matching, we first check if a DOI tag is included in the reference metadata. If so, we assume it is correct and link the corresponding work. If there isn’t a DOI tag, we perform a search using the metadata supplied and select candidate results by thresholding. The best match is found through a further validation process. Learn more about how we [match references](/blog/reference-matching-for-real-this-time/). The same process is used for the results shown on our [Simple Text Query](https://apps.crossref.org/SimpleTextQuery) tool.

All citation to a work are returned in the corresponding Cited-by query.
