+++
title = "Crossmark button guidelines"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "crossmark-button-guidelines"]
identifier = "documentation/crossmark/crossmark-button-guidelines"
rank = 4
weight = 110400
aliases = [
	"/education/crossmark/crossmark-button-guidelines",
	"/education/crossmark/crossmark-button-guidelines/"
]
+++

It’s important that all members use the Crossmark button consistently, to make sure it is familiar to readers and can be easily recognised.

The Crossmark button is available in color and monochrome versions, and can be resized to suit your website or PDFs. It must not otherwise be altered or adapted. We recommend using the color button on HTML pages, and either the color or monochrome button on PDFs for maximum user recognition.

The [Crossmark button](https://crossmark.crossref.org/widget/v2.0/readme.html) should be used in two contexts: on HTML abstract pages and PDF files.

* [CROSSMARK_BW_horizontal.eps](https://assets.crossref.org/logo/CROSSMARK_BW_horizontal.eps)
* [CROSSMARK_BW_square.eps](https://assets.crossref.org/logo/CROSSMARK_BW_square.eps)
* [CROSSMARK_BW_square_no_text.eps](https://assets.crossref.org/logo/CROSSMARK_BW_square_no_text.eps)
* [CROSSMARK_Color_horizontal.eps](https://assets.crossref.org/logo/CROSSMARK_Color_horizontal.eps)
* [CROSSMARK_Color_square.eps](https://assets.crossref.org/logo/CROSSMARK_Color_square.eps)
* [CROSSMARK_Color_square_no_text.eps](https://assets.crossref.org/logo/CROSSMARK_Color_square_no_text.eps)

## Do<a id='00334' href='#00334'><i class='fas fa-link'></i></a>

* Place the Crossmark button close to the title of the article, preferably next to or above the title.
* Use [our button](https://crossmark.crossref.org/widget/v2.0/readme.html) (*View Source* of that page to see the embed code) and do not modify or adapt it.

For web and PDF there are three versions of the button. We recommend you use one of the two buttons with the *Check for updates* text. Please be careful not to make them so small they become illegible. If you need a smaller button, use the square one with no text.

## Don’t<a id='00335' href='#00335'><i class='fas fa-link'></i></a>

* Modify the colors or text of the button
* Create your own version of the Crossmark button
* Download JavaScript or image assets and serve them from your own site. Doing this will prevent us from providing software updates and could cause the dialog to stop working properly
* Adjust the ratios of the buttons
