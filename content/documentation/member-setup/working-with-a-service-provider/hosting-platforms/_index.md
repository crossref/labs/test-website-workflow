+++
title = "Hosting platforms"
date = "2020-04-08"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "working-with-a-service-provider", "hosting-platforms"]
identifier = "documentation/member-setup/working-with-a-service-provider/hosting-platforms"
rank = 4
weight = 50901
aliases = [
	"/education/member-setup/working-with-a-service-provider/hosting-platforms",
	"/education/member-setup/working-with-a-service-provider/hosting-platforms/",
	"/service-providers/hosting-platforms/",
	"/service-providers/hosting-platforms"
]
+++

Hosting platforms are organizations that host publisher content. They give visibility to Crossref metadata in two key ways:

1. Platforms [register content](/services/content-registration/) on behalf of publishers. This means that they are responsible for a significant proportion of the metadata records in Crossref.
2. Platforms show published research to the world. They support authors, readers and search engines through the display of bibliographic metadata, links to citing articles and Cited-by counts, and [updates and retractions through Crossmark](/documentation/crossmark/).

Hosting platforms deliver metadata to Crossref as well as [consuming it as output](/documentation/retrieve-metadata/). Citation matching is a good example of metadata retrieval - try this out using our [Metadata Search](https://search.crossref.org/references), and learn more about [looking up metadata and identifiers](/documentation/retrieve-metadata#00043).

Hosting platforms work with our members and help deliver Crossref services to our shared community. When we plan a change or new service, we help hosting platforms to prepare both for the change and for questions from members.

Content sometimes moves from one member or platform to another. We aim to make these transitions as smooth as possible, so that deposits and other key activities continue uninterrupted. Learn more about [transferring responsibility for DOIs](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois).
