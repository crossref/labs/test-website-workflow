+++
title = "Understanding your Similarity Report"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "similarity-report-understand"]
identifier = "documentation/similarity-check/ithenticate-account-use/similarity-report-understand"
rank = 4
weight = 100400
aliases = [
  "/education/similarity-check/ithenticate-account-use/similarity-report-understand",
  "/education/similarity-check/ithenticate-account-use/similarity-report-understand/",
  "/documentation/similarity-check/ithenticate-account-use/similarity-report-understand",
  "/documentation/similarity-check/ithenticate-account-use/similarity-report-understand/"
]
+++

## How is the Similarity Score calculated?<a id='00609' href='#00609'><i class='fas fa-link'></i></a>

The below information will  help you understand how to interpret your Similarity Report, whether you're using iThenticate v1 or v2.

To calculate the Similarity Score, iThenticate scans your submitted document’s text, and checks it against each of the repositories you’ve chosen. The system takes the number of matching words found within the document and divides it by the document’s total word count to produce the Similarity Score percentage for the report.

If you apply exclusion options to the document, the system removes all matches for the exclusion option logic and recalculates the Similarity Score percentage.

Learn more about exclusion settings when [setting up a new folder (v1 only)](/documentation/similarity-check/ithenticate-account-use/folders#00586), [editing filters and exclusions in existing folders (v1 only)](/documentation/similarity-check/ithenticate-account-use/folders#00586), filters and exclusions within the Similarity Report ([v1](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00604) or [v2](https://help.turnitin.com/crossref-similarity-check/user/managing-files-and-folders/editing-similarity-report-settings.htm)), and URL filters ([v1](/documentation/similarity-check/ithenticate-account-setup/account-info#00572) or [v2](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/website-exclusions.htm)) for account administrators.

## How to interpret the Similarity Report<a id='00608' href='#00608'><i class='fas fa-link'></i></a>

iThenticate does not check for plagiarism; it checks for similarity. Where a section of the submission’s content is similar or identical to one or more sources, it will be flagged for review. This doesn’t automatically mean plagiarism, however - just similarity.

It’s perfectly natural for a submission to match against some sources in the database. A high degree of overlap may indicate a well-researched document with many references to existing work, and as long as these sources are quoted and referenced correctly, this is perfectly acceptable. A high degree of overlap may also be present where an author has already shared their work on a preprint repository. If the author(s) are the same, this is not a problem.

It’s important that you don’t set a Similarity Score over which you automatically reject manuscripts - where there’s a high degree of overlap, your editors and reviewers should decide if the match is acceptable or not, as part of their general review process.

### Similarity Reports and preprints<a id='00664' href='#00664'><i class='fas fa-link'></i></a>

It is entirely possible (and acceptable) for an author to submit an article to a journal even though they’ve previously made the article available as a preprint. In this case, we expect a high degree of similarity between the [preprint and author’s submitted manuscript](/documentation/crossmark/version-control-corrections-and-retractions/).

Therefore, if you find a high degree of similarity between a manuscript you’re checking in iThenticate and a preprint by the same author(s), this is likely to be because the manuscript is a match with its own preprint. However, if the manuscript and preprint do not have the same author(s), this may indicate a problem, and you should investigate further.

Some preprints can be found in iThenticate’s [Crossref Posted Content repository](/documentation/similarity-check/ithenticate-account-use/folders#00587), so take this into account if you are checking against this repository.
But even if you have excluded the Crossref Posted Content repository in your settings ([v1](/documentation/similarity-check/ithenticate-account-use/folders/#00586)or [v2](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/excluding-search-repositories.htm)), it is still possible for preprints to appear as matches to a submission, because iThenticate also crawls preprint repositories on the web.

We recommend including preprints in your results to ensure you are checking that preprints haven’t been plagiarised by a different author, but if you see a pre-print match for the same author, this isn't plagiarism.
