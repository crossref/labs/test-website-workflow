+++
title = "Setting up your iThenticate v2 account for use directly in the browser (admins only)"
date = "2022-07-15"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticatev2-account-setup"]
identifier = "documentation/similarity-check/ithenticatev2-account-setup/"
rank = 4
weight = 100250
+++

This section is for Similarity Check account administrators only. It explains how administrators need to set up the iThenticate v2 account for their organizations if they are planning to use iThenticate in the browser. You need to follow the steps in this section before you start to set up your users and share the account with your colleagues.

* If you are using iThenticate v1 rather than iThenticate v2, take a look at the section for [v1 account administrators](/documentation/similarity-check/ithenticate-account-setup/).
* If you intend to use iThenticate v2 through an integration with your Manuscript Submission System (MTS) instead, go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup).

## Your personal administrator account in iThenticate v2

Once Turnitin has enabled iThenticate v2 for your organization, the main editorial contact provided on your application form will become the iThenticate account administrator.

You will receive an email from Turnitin with a link to set your credentials. The email will look like this:
<figure><img src='/images/documentation/SC-email-login.png' alt='email login' title='' width='50%'></figure>

Click on the blue ‘Set up my account’ button at the bottom of the email. This will bring you to a page which looks something like this:
<figure><img src='/images/documentation/SC-v2-initial-login-screen.png' alt='v2 initial login screen' title='' width='50%'></figure>

Fill out your username and password, and don’t forget to tick to agree to the terms and conditions. You will then arrive at your new iThenticate v2 account.
<figure><img src='/images/documentation/SC-v2-welcome-screen.png' alt='v2 welcome screen' title='' width='75%'></figure>

## How do you know if you’re an account administrator?

When you are logged in to iThenticate, what tabs can you see?

If you're using iThenticate v2, you will only be able to see *Users* on the menu if you're an account administrator.
<figure><img src='/images/documentation/SC-admins-tabs-v2.png' alt='Admins tabs view v2' title='' width='20%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image106">Show image</button>
<div id="image106" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-tabs-v2.png" class="img-responsive" style="width: 50%;">
            </div>
        </div>
    </div>
</div>

So if you can't see *Manage Users* or *Users*, you’re not an account administrator, and you can just read the [user instructions for iThenticate v2](https://help.turnitin.com/crossref-similarity-check/user.htm) on the Turnitin website.

## Updating your email address, username or password in the future
If you need to change your personal email address, username or password in the future, you can find instructions on the Turnitin website.
* [Updating your email address or username](https://help.turnitin.com/crossref-similarity-check/user/account-basics/editing-your-profile.htm)
* [Changing your password](https://help.turnitin.com/crossref-similarity-check/user/account-basics/changing-your-password.htm)
