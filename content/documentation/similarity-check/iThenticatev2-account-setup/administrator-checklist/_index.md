+++
title = "Administrator checklist"
date = "2022-07-15"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticatev2-account-setup", "similarity-check-administrator-checklist"]
identifier = "documentation/similarity-check/ithenticatev2-account-setup/administrator-checklist"
rank = 4
weight = 100251

+++

As an administrator, you create and manage the users on your account, and you decide how your organization uses the iThenticate tool. You’ll find the system easier to use if you set it up correctly to start with, so do read through the checklist below carefully and make sure you've set up your account how you want it before inviting any users to your account.

This section is for Similarity Check account administrators using iThenticate v2 through the browser.

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup/administrator-checklist).
* Integrating iThenticate v2 with your Manuscript Submission System (MTS) instead? Go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup/administrator-checklist).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

As an administrator, you decide how your organization uses the iThenticate tool. You’ll find the system easier to use if you set it up correctly to start with, so do read through the checklist below carefully and make sure you've set up your account how you want it before inviting any users to your account.

1. [Decide how to manage your users and folders](#06614)
2. [Decide on your exclusions](#06615)
3. [Decide if you want to use the Submitted Works repository (or Private Repository)](#06616)
4. [Decide how you'll budget for your document checking fees](#06617)
5. [Make sure you stay eligible for the Similarity Check service](#06618).

### 1. How do you want to manage your users and folders?<a id='06614' href='#06614'><i class='fas fa-link'></i></a>

* How many users do you need to set up?
* Do you need them to have different permissions or access to different folders? Do you want users to be able to see each other’s folders?
* Do you want to be the only account administrator, or do you want to add other administrators?
* If you set up different folders in iThenticate to manage the manuscripts you’re checking, you’ll be able to assign different users to each folder. For example, you may choose to set up different folders for different titles or years of publication.

Learn more about [how to manage users and folders](https://help.turnitin.com/crossref-similarity-check/administrator/users.htm) on the Turnitin website.

### 2. Decide on your exclusions<a id='06615' href='#06615'><i class='fas fa-link'></i></a>
You can decide to exclude preprints, certain websites, or even specific sections of text. We recommend starting without any exclusions to avoid excluding anything important. Once your users are experienced enough to identify words and phrases that appear frequently but are not potentially problematic matches (and can therefore be ignored) in a Similarity Report, you can start carefully making use of this feature.

Find out more in the [Exclusions](/documentation/similarity-check/ithenticatev2-account-setup/exclusions/) section.

### 3. Decide if you want to use the Submitted Works repository (or Private Repository)<a id='06616' href='#00616'><i class='fas fa-link'></i></a>
The Submitted Works repository (or Private Repository) allows users to find similarity not just across Turnitin’s extensive Content Database but also across all previous manuscripts submitted to your iThenticate account for all the journals you work on. This would allow you to find collusion between authors or potential cases of duplicate submissions but it also means you could share sensitive data between users, so you need to think very carefully about how you will use this feature. [Find out more](/documentation/similarity-check/ithenticatev2-account-setup/private-repository/).

### 4. Decide how you'll budget for your document checking fees<a id='06617' href='#06617'><i class='fas fa-link'></i></a>

There’s a [charge for each document checked](/fees#similarity-check-per-document-fees), and you’ll receive an invoice in January each year for the documents you’ve checked in the previous year. If you're a member of Crossref through a Sponsor, your Sponsor will receive this invoice.

As well as setting a Similarity Check document fees budget for your account each year, it’s useful to monitor document checking and see if you’re on track. You can monitor your usage in your [Statistics section](https://help.turnitin.com/crossref-similarity-check/administrator/statistics/viewing-submission-details.htm). Ask yourself:

* How many documents do you plan to check?
* How often do you want to monitor usage? Set yourself a reminder to check your Statistics periodically.

It’s a good idea to come back to these questions periodically, consider how your use of the tool is evolving, and make changes accordingly.

### 5. Make sure you can stay eligible for the Similarity Check service<a id='06618' href='#06618'><i class='fas fa-link'></i></a>
Your organization gets reduced rate access to the iThenticate tool through the Similarity Check service because you make your own published content available to be indexed into the iThenticate database. You do this by providing full text URLs specifically for this service in the metadata that you register with Crossref. Talk to your colleagues who are responsible for registering your DOIs with Crossref, and make sure that they continue to include full text URLs for Similarity Check in the metadata they register with us.
