+++
title = "Manage users and folders"
date = "2022-07-18"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticatev2-MTS-account-setup", "manage-users"]
identifier = "documentation/similarity-check/ithenticatev2-mts-account-setup/manage-users"
rank = 4
weight = 100281

+++

This section is for Similarity Check account administrators who are integrating iThenticate v2 with their Manuscript Submission System (MTS)

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup/manage-users).
* Using iThenticate v2 through a browser instead? Go to [setting up iThenticate v2 for browser users](/documentation/similarity-check/ithenticatev2-account-setup/manage-users).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

## No need to create users with an integration with an MTS
If you are using iThenticate through an integration with an MTS, then you do not need to set up any other users on your new iThenticate account. This is because all the submissions from your MTS will be made by the API key you’ve set, rather than individual users. The only person who will need credentials for the iThenticate account is the administrator.

## No need to create folders with an integration with an MTS
If you previously used iThenticate v1, you might be used to creating folders in iThenticate to integrate with your MTS. However, you no longer need to create folders. Everything will be handled through the integration panel in your MTS.
