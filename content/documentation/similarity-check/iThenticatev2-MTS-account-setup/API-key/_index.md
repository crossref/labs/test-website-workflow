+++
title = "Setting up your MTS integration with an API key"
date = "2022-07-18"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticatev2-MTS-account-setup", "API-key"]
identifier = "documentation/similarity-check/ithenticatev2-mts-account-setup/api-key"
rank = 4
weight = 100278
+++

This section of our documentation is for Similarity Check account administrators who are integrating iThenticate v2 with their Manuscript Submission System (MTS).

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup).
* Using iThenticate v2 through a browser instead? Go to [setting up iThenticate v2 for browser users](/documentation/similarity-check/ithenticatev2-account-setup).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

To set up your integration, you need to create an API key by logging into iThenticate through the browser. You will then share this API key and the URL of your iThenticate v2 account with your MTS.

### Step One: Decide how many API scopes and API keys you need

Within iThenticate, you can set up different API Scopes, and within that, different API keys. Most members will just need one API Scope and one API key. However, some members may need more than one.

* If you need to integrate with more than one Manuscript Tracking System (MTS), you will need a different API Scope for each MTS.
* If you publish on behalf of societies or work with other organizations who want to keep their activities separate from each other, you will need a different API Scope and API key for each society.
* If at some point in the future, you need to change your API key for an existing MTS integration, you must generate a new API key under the same scope that you originally used for this integration.

### Step Two: Create your API Scope and API key(s)

Click on “Integrations” in the menu.

<figure><img src='/images/documentation/SC-integrations-v2-menu.png' alt='integrations v2 menu' title='' width='20%'></figure>

This will bring you to the Integrations section. Click on the “Generate API Scope” key.

<p>
<figure><img src='/images/documentation/SC-integrations-v2.png' alt='integrations v2' title='' width='75%'></figure>
</p>

You will then give your API Scope a name.
<p>
<figure><img src='/images/documentation/SC-generate-API-scope.png' alt='v2 generate API scope' title='' width='50%'></figure>
</p>

For example, this may be the name of a particular MTS, or of a particular society.

Under your new API Scope, you can then set up your first API key.

<figure><img src='/images/documentation/SC-v2-new-API-key.png' alt='v2 create new API key' title='' width='50%'></figure>


Once you add the key name, you will be able to click on the “Create and view” button. The system will then generate your key.

<figure><img src='/images/documentation/SC-API-key-confirmation.png' alt='v2 API key confirmation' title='' width='50%'></figure>

### Step three: Add your API key into your Manuscript Tracking System (MTS)

In order to integrate your new iThenticate v2 account and your Manuscript Tracking system(s), your MTS will require from you:

* At least one API key
* Your unique iThenticate URL containing your Crossref membership number using the following format: https://crossref-xxx.turnitin.com. (For example, if your Crossref Membership number is 1234, your URL will be: https://crossref-1234.turnitin.com. If you are not sure what your Crossref Membership number is, [please ask us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691).

Follow the instructions below for the relevant MTS:

**Editorial Manager**
* Enter your iThenticate API key(s) and your iThenticate v2 account URL into the iThenticate configuration page in Editorial Manager. There are [instructions available from Aries Systems here](https://www.ariessys.com/wp-content/uploads/iThenticate-v2.0-Configuration-Guide.pdf).

**eJournal Press**
* Email your API key(s) and your iThenticate v2 account URL to support@ejpress.com and the team at eJournal Press will set up the integration for you.

**ScholarOne**
* If you are already using iThenticate with ScholarOne and are upgrading from iThenticate v1 to iThenticate v2, please email your API key(s) and your iThenticate v2 account URL to s1help@clarivate.com, and the team at ScholarOne will make the change for you. Please put “Product Management” in the subject line of your email.
* If you are a new subscriber to Similarity Check and you haven’t used iThenticate before, you don't need to email the team at ScholarOne. Just enter your iThenticate API key(s) and your iThenticate v2 account URL into the iThenticate configuration page in ScholarOne.

**Scholastica**
* The team at Scholastica will set up the integration for you. Give them your API key(s) and your iThenticate v2 account URL by filling out [this form](https://docs.google.com/forms/d/e/1FAIpQLScVBTDQD_KAX96IVGNz_9AbyTt6_Y_XFxEEQUnqCLuTcLCXog/viewform).
* The team at Scholastica will also set up any exclusions for you, so in the form they'll ask you which sort of content you want to exclude from displaying as a match.
