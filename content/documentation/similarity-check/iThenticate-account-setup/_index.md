+++
title = "Setting up your iThenticate v1 account (admins only)"
date = "2022-07-15"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-setup"]
identifier = "documentation/similarity-check/ithenticate-account-setup"
rank = 4
weight = 100200
aliases = [
  "/education/similarity-check/ithenticate-account-setup",
  "/education/similarity-check/ithenticate-account-setup/",
    "/education/similarity-check/ithenticate-account-setup/admin-account",
    "/education/similarity-check/ithenticate-account-setup/admin-account/",
    "/documentation/similarity-check/ithenticate-account-setup/admin-account",
    "/documentation/similarity-check/ithenticate-account-setup/admin-account/"
  ]
+++
This section is for Similarity Check account administrators using iThenticate v1. You need to follow the steps in this section before you start to set up your users and share the account with your colleagues.

If you are using iThenticate v2 rather than iThenticate v1, there are separate instructions for you.
* Using iThenticate v2 directly in the browser - go to [setting up iThenticate v2](/documentation/similarity-check/ithenticatev2-account-setup/).
* Integrating iThenticate v2 with your MTS - go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup).

Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).

## Your personal administrator account in iThenticate v1

Once Turnitin has enabled iThenticate v1 for your organization, the main editorial contact provided on your application form will become the iThenticate account administrator. As an administrator, you create and manage the users on your account, and you decide how your organization uses the iThenticate tool.

To start with, you need to login to iThenticate and set your password.

### Log in to your administrator account (v1)<a id='00039' href='#00039'><i class='fas fa-link'></i></a>

1. Start from the link in the invitation email from noreply@ithenticate.com with the subject line “Account Created” and click *Login*
2. Enter your username and single-use password
3. Click to agree to the terms of the end-user license agreement. These terms govern your personal use of the service. They’re separate from the central Similarity Check service agreement that your organization has agreed to.
4. You will be prompted to choose a new password
5. Click ​*Change Password​* to save.

## How do you know if you’re an account administrator?

Once you've logged in, you will only be able to see the *Manage Users* tab if you're an account administrator.

<figure><img src='/images/documentation/SC-admins-tabs.png' alt='Admins tabs view' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image105">Show image</button>
<div id="image105" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-tabs.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

So if you can't see *Manage Users* or *Users*, you’re not an account administrator, and you can skip ahead to the [user instructions for iThenticate v1](/documentation/similarity-check/ithenticate-account-use/).

## Updating your personal email address or password
Changing your email address or updating your password is the same for admins and other users. There's more information in the [user instructions for iThenticate v1](/documentation/similarity-check/ithenticate-account-use/).
