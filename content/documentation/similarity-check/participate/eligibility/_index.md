+++
title = "Checking your eligibility and applying"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "participate", "eligibility"]
identifier = "documentation/similarity-check/participate/eligibility"
rank = 4
weight = 100101
aliases = [
  "/education/similarity-check/participate/eligibility",
  "/education/similarity-check/participate/eligibility/"
]
+++

Similarity Check is only available to Crossref members who have full-text URLs for Similarity Check present in the metadata of at least 90% of their registered articles (across all journal prefixes). These URLs must point directly to the full-text PDF, HTML, or plain text content and if your content is behind authentication, you need to whitelist the Turnitin IP address. Learn more about [how to participate](/documentation/similarity-check/participate/).

You can check the percentage of Similarity Check URLs already included in your metadata using the widget below - just start typing your account name in the box, select the correct one from the list, and your result will be automatically calculated. Don’t worry, you don’t need to know your Member ID.

If you do meet the threshold, the widget will link you to a form where you can apply for Similarity Check and click to accept the service terms. If you don’t meet the threshold, the widget will provide a .csv file to download which shows all your DOIs that don’t have Similarity Check full-text URLs. It will also provide instructions for how to add the missing full-text URLs.


{{< simcheckjs >}}

{{% simcheck-div above %}}
{{% divwrap blue-highlight %}}Good news - you're eligible to apply for our Similarity Check service.{{% /divwrap %}}

You can now [apply here](/_apply/simcheck) and accept the service terms.

Once we receive your application, we’ll work with the team at Turnitin to confirm that they can access your content. If they can't access your content, we won't be able to continue with your application until this problem is solved, but we'll work with you to fix any issues. Don’t forget, if your full-text content is protected by authentication, then you'll need to ask your hosting provider to <a href="https://www.crossref.org/education/similarity-check/participate#00037" target="_blank">whitelist Turnitin’s IP range</a> to ensure your content is accessible for indexing purposes. Do make sure that this is done before you apply.

We’ll also send you a pro-rated invoice for your first year subscription to the service.

{{% /simcheck-div %}}


{{% simcheck-div below %}}
{{% divwrap yellow-highlight %}}We're sorry, but you are not eligible to register for our Similarity Check service just yet. To be eligible you need to register full-text urls for Similarity Check pointing to the full text article for more than 90% of your content. {{% /divwrap %}}
Find out <a href="https://www.crossref.org/documentation/similarity-check/participate/urls-for-existing-deposits//" target="_blank">how to add these full-text urls for Similarity Check to your existing content</a>. To see exactly which content items are missing the full text article, simply click the “Generate CSV” button above. Please note, this CSV can only show the first 10k content items. If you have more than 10k content items missing DOIs, please <a href="https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691">contact us</a> and we'll be able to provide you with the full list.

{{% /simcheck-div %}}

{{% simcheck-div help %}}
{{% divwrap yellow-highlight %}}We're sorry, but you are not eligible to register for our Similarity Check service just yet. It looks as though you haven’t registered any content with us yet.{{% /divwrap %}}  
<span style="color:red"> To be eligible for Similarity Check you need to be registering content with us and including URLs for Similarity Check that point to the full text article for at least 90% of your content. Find out more about <a href="https://www.crossref.org/education/content-registration/" target="_blank">how to register content</a> and learn more about <a href="https://www.crossref.org/education/similarity-check/participate#00040/" target="_blank">full-text URLs for Similarity Check</a>.

<br> Please note - if you aren't registering ANY journal articles at all and are only registering non-journal content (eg conference papers, books) then the tool may not give accurate results. Please <a href="https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691" target="_top">contact our support team</a> for more help.</span>
{{% /simcheck-div %}}

{{% simcheck-div above10k %}}
{{% divwrap yellow-highlight %}}Unfortunately, you aren't currently eligible for the Similarity Check service as you don't have Similarity Check URLs in the metadata of over 90% of your content. As you have so many DOIs registered, we can't provide a CSV file showing which DOIs are missing the Similarity Check URLs. Please <a href="https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691">contact our support team</a> with our team and they'll be able to provide you with the CSV file. {{% /divwrap %}}  
{{% /simcheck-div %}}
