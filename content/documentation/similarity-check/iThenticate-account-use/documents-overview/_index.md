+++
title = "Documents overview"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "documents-overview"]
identifier = "documentation/similarity-check/ithenticate-account-use/documents-overview"
rank = 4
weight = 100302
aliases = [
  "/education/similarity-check/ithenticate-account-use/documents-overview",
  "/education/similarity-check/ithenticate-account-use/documents-overview/"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 Documents overview](https://help.turnitin.com/crossref-similarity-check/user.htm#Fileuploads)**

**v1 Documents overview, keep reading:**


Within a folder, the *Documents* tab shows all the submitted documents for that folder.

<figure><img src='/images/documentation/SC-folders-documents.png' alt='View of all documents in one folder' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image107">Show image</button>
<div id="image107" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-documents.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Each document submitted generates a Similarity Report after the document has been through the Similarity Check. If more documents are present than can be displayed at once, the pages feature will appear beneath the documents - click the page number to display, or click *Next* to move to the next page of documents.

You can [submit documents](/documentation/similarity-check/ithenticate-account-use/documents-submit/) in three ways:

* [upload a file](/documentation/similarity-check/ithenticate-account-use/documents-submit#00597) - to submit a single file
* [zip file upload](/documentation/similarity-check/ithenticate-account-use/documents-submit#00598) - to submit a zip file containing multiple documents, up to a maximum of 100MB or 1,000 files. Larger files may take longer to upload
* [cut & paste](/documentation/similarity-check/ithenticate-account-use/documents-submit#00599) - to submit text directly into the submission box. Use this to copy and paste a submission from a file format that is not supported. This method supports plain text only (no images or non-text information)

iThenticate currently accepts the following file types for document upload:

* Microsoft Word® (.doc and .docx)
* Word XML
* plain text (.txt)
* Adobe PostScript®
* Portable Document Format (.pdf)
* HTML
* Corel WordPerfect® (.wpd)
* Rich Text Format (.rtf)

Each file may not exceed 400 pages, and each file size may not exceed 100 MB. Reduce the size of larger files by removing non-text content. You can’t upload or submit to iThenticate files that are password-protected, encrypted, hidden, system files, or read-only.

.pdf documents must contain text - if they contain only images of text, they will be rejected during the upload attempt. To check, copy and paste a section of the .pdf into a plain-text editor such as Microsoft Notepad® or Apple TextEdit®. If no text is copied over, the selection does not contain text.

To convert scanned images of a document, or an image saved as a .pdf, use Optical Character Recognition (OCR) software to convert the image to text. The conversion software can introduce errors, so manually check and correct the converted document.

Some document formats can contain multiple data types, such as text, images, embedded information from another file, and formatting. Non-text information that is not saved directly within the document will not be included in a file upload, for example, references to a Microsoft Excel® spreadsheet included within a Microsoft Office Word® document.

Use a word-processing program to save your file as one of the accepted types listed above, such as .rtf or .txt. Neither file type supports images or non-text data within the file. Plain text format does not support any formatting, and rich text format allows only limited formatting.

When converting a file to a new format, save it with a different name from the original, to avoid accidentally overwriting the original file. This is especially important when converting to plain text or rich text formats, to prevent permanent loss of the original formatting or image content of the file.
