+++
title = "Submit a document"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "documents-submit"]
identifier = "documentation/similarity-check/ithenticate-account-use/documents-submit"
rank = 4
weight = 100303
aliases = [
  "/education/similarity-check/ithenticate-account-use/documents-submit",
  "/education/similarity-check/ithenticate-account-use/documents-submit/"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 Submit a document](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/uploading-a-file.htm)**

**v1 Submit a document, keep reading:**


You can submit a document by [simple upload](#00597), [zip file upload](#00598), or [cut and paste](#00599). Once uploaded, you can [edit the document information](#00600).

## Uploading your file (v1)<a id='00597' href='#00597'><i class='fas fa-link'></i></a>

*Upload a File* allows you to submit a single document from a variety of document types.  From the *Submit a document* menu, click *Upload a File*, and the *Upload a file* form opens.

<figure><img src='/images/documentation/SC-documents-upload.png' alt='Upload a file' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image82">Show image</button>
<div id="image82" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-documents-upload.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

1. Under *Destination Folder*, choose the folder to which you wish to upload the file. Its Similarity Report will be added to the same folder.
2. Complete *Author First Name*, *Author Last Name*, and *Document Title* fields. If *Document Title* is left blank, the document’s filename will be used.
3. Click *Choose File*, and locate the file to upload. Use *Add another file* to add more files, up to a total of ten.
4. Click *Upload* to proceed with with uploading the selected document(s), or click *Cancel* to cancel the upload.

### Zip file upload (v1)<a id='00598' href='#00598'><i class='fas fa-link'></i></a>

iThenticate allows you to submit multiple documents from a variety of document types in a compressed zip file. The zip file may be up to approximately 100MB in size and contain up to 1,000 individual files. If the zip file exceeds either limit, it will be rejected. Check that your zip file contains only accepted file types, and no duplicate copies of the same file.

<figure><img src='/images/documentation/SC-documents-upload-zip.png' alt='Zip file upload' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image83">Show image</button>
<div id="image83" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-documents-upload-zip.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

1. Click *Zip File Upload* from the *Submit a document* menu. Choose your *Destination Folder* from the drop-down. The Similarity Report for the file will also be found here.
2. The information you enter in the *Author First Name* and *Author Last Name* fields will be applied to all the documents in the zip file. You can manually change these once the document is uploaded to the folder.
3. Click *Choose file*, locate the zip file on your device, and click *Upload*.

The title of the each document in the zip files will be the default title of each submission.

### Cut and paste (v1)<a id='00599' href='#00599'><i class='fas fa-link'></i></a>

Use the *cut and paste submission* option to submit information from non-supported file types, or to submit only specific parts or areas of a document.

Only text can be submitted using this method - any graphics, graphs, images, and formatting are lost when pasting into the text submission box.

<figure><img src='/images/documentation/SC-documents-upload-cut-paste.png' alt='Cut and paste' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image84">Show image</button>
<div id="image84" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-documents-upload-cut-paste.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

1. Click *Cut & Paste* from the *Submit a document* menu.
2. Choose your *Destination Folder* from the drop-down. The Similarity Report for the file will also be found here.
3. Complete the *Author First Name*, *Author Last Name*, and *Document Title* fields. If no title is given, the default title “Pasted Document” will be used.
4. Copy your desired text for checking, paste it into the *Paste your document in the area below* text box, and click *Upload*.

To view recent uploads, go to the *Submit a document* menu, click *Recent Uploads*, and you will see recent uploads listed in reverse chronological order (most recent first). Click the *Date & Time* header to see the uploads in chronological order (oldest first).

## Edit document information (v1)<a id='00600' href='#00600'><i class='fas fa-link'></i></a>

To edit a document’s information (title and author name), click the *edit* icon to the right of a document in a folder. You will see the *Document Properties* page. Edit the fields, and click *Update* to save your changes.
