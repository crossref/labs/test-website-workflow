+++
title = "Settings"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "settings"]
identifier = "documentation/similarity-check/ithenticate-account-use/settings"
rank = 4
weight = 100309
aliases = [
  "/education/similarity-check/ithenticate-account-use/settings",
  "/education/similarity-check/ithenticate-account-use/settings/"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 User Settings](https://help.turnitin.com/crossref-similarity-check/user/managing-files-and-folders/editing-similarity-report-settings.htm)**

**v1 User Settings, keep reading:**


{{< snippet "/_snippet-sources/similarity-check-settings.md" >}}
