+++
title = "Reports and working papers"
date = "2022-09-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "reports"]
identifier = "documentation/principles-practices/reports"
rank = 4
weight = 10114
+++
Our reports model supports registration of reports and working papers within or outside of a series.

When registering reports, DO:
* include a report title and publication date
* include all relevant [funding metadata](/documentation/principles-practices/best-practices/funding/), [license information](/documentation/schema-library/markup-guide-metadata-segments/license-information/), and [relationships](/documentation/schema-library/markup-guide-metadata-segments/relationships/)
* include all [contributors](/documentation/principles-practices/best-practices/#contributors) info, publisher and institution details, abstracts, approval dates, as well any edition numbers, contract numbers, and ISBN applied to your report

3. Report registration files may include a publisher name (within `publisher`) and/or institution name (within <institution> depending on the organization issuing the report.
4. Reports/working papers may be deposited as a series.


* register DOIs for each section or chapter of a report or working paper if you want to make each section citeable, or if each section has dates, contributors, or other metadata that differs from the report itself

Review our [Reports and Working Papers Markup Guide](/documentation/schema-library/markup-guide-record-types/reports-and-working-papers/) for XML and metadata help.
