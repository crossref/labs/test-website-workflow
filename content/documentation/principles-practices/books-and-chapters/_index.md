+++
title = "Books and chapters"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "books-and-chapters"]
identifier = "documentation/principles-practices/books-and-chapters"
rank = 4
weight = 10102
aliases = [
    "/education/content-registration/content-types-intro/books-and-chapters",
    "/education/content-registration/content-types-intro/books-and-chapters/",
    "/help/books-best-practice/",
    "/help/books-best-practice",
    "/documentation/content-registration/content-types-intro/books-and-chapters",
    "/documentation/content-registration/content-types-intro/books-and-chapters/"
]
+++

Our [Books Advisory Group](/working-groups/books) advises on books best practices; its goals are to:

* Maximize reference linking between books and other content types including journals and conference proceedings
* Ensure that we collect and distributes persistent identifiers and authoritative metadata for online books
* Ensure that book content is part of all our services
* Enhance the discovery, visibility, and usage of book content.

On this page, learn more about:

* [Best practices for depositing metadata, linking, and DOI use for books](#00053)
* [Best practices for updates and versions](#00054)
* [Best practices for citation matching for book title queries](#00055)
* [Best practices for citation matching for book chapters or reference entry queries](#00056)
* [Best practices for DOIs in citations](#00057)
* [Best practices for books on multiple platforms](#00638)

Review our [Books Markup Guide](/documentation/schema-library/markup-guide-record-types/books-and-chapters/) for XML and metadata help.

## Depositing metadata, linking, and DOI use for books<a id='00053' href='#00053'><i class='fas fa-link'></i></a>

You should:

1. Register the content by depositing metadata at the time of online publication and assign DOIs at the title and chapter/entry level. Learn more about the [benefits of registering book chapters](/blog/crossing-the-rubicon-the-case-for-making-chapters-visible/).
2. Add DOI links to references in books. Learn more about our [DOI display guidelines](/display-guidelines)
3. Deposit references from books to allow participation in our [Cited-by](/documentation/cited-by/) service
4. Instruct authors to cite specific chapters and entries using page numbers, chapter/entry titles and DOIs
5. Update your editorial guidelines - ask copyeditors to look for page numbers and chapter titles in book citations. Use our tools to check references as part of the production process so that references can be corrected and missing information added. Learn more about [creating reference links](/documentation/reference-linking/how-do-i-create-reference-links/).

## Updates and versions<a id='00054' href='#00054'><i class='fas fa-link'></i></a>

There are two types of updates:

* **Major content changes** that may affect the interpretation of a work may mean a new edition with new ISBNs. Major version changes imply that the publisher will formally notify readers that content has changed (through errata, corrigenda, or new editions (which would also get a new ISBN)
* **Minor content changes** are unlikely to affect a reader’s interpretation of the work, and the publisher will not generally draw attention to the changes with a new version.

Just as publishers decide when a new print edition or version is warranted, it is publishers’ responsibility to distinguish between major and minor versions in online content.

Since a Crossref DOI is a citation identifier, a new DOI should only be issued if the new version will be cited differently. The same logic applies to differing formats, for example, the file types or *containers* used to present content: a distinct DOI should not be registered for different formats unless the format will be cited in a different way. This means, for example, that you should not assign one DOI to an EPUB version of a book and another DOI to the PDF version of a book if the format doesn’t affect how the book is cited. You may register a single DOI for all versions of a translated book. Distinct DOIs may also be registered for translated versions of content.

The recommended best practice is:

1. Assign new DOIs to new major versions or editions of books, chapters and entries. This practice will preserve the scholarly citation record. Older versions should remain available online with links to the latest version. In use, a reader follows a link to the version cited and then has the option to follow a link to the current version.
2. Do not assign new DOIs to minor new versions of books, chapters and entries.
3. Where book content is hosted on multiple platforms (such as NetLibrary, ebrary) and publishers can enable enable linking from a single DOI to those platforms, they should use multiple resolution, which allows multiple URLs to be associated with one DOI. Learn more about [multiple resolution](/documentation/content-registration/creating-and-managing-dois/multiple-resolution/).

If multiple resolution doesn’t work for your circumstances, or content on your platform does not already have DOIs, please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) to find a solution.

## Citation matching for book title queries<a id='00055' href='#00055'><i class='fas fa-link'></i></a>

To enable citation matching at the title level, the minimum query must include the following elements:

* book title
* book author
* book copyright year

To increase the accuracy of matching, members should also include as many of the following elements as possible in the query:

* editor (where appropriate)
* ISBN
* ISSN / DOI
* publisher

## Citation matching for book chapters or reference entry queries<a id='00056' href='#00056'><i class='fas fa-link'></i></a>

The metadata provided for a book title is used to identify book chapters during querying. This means that a book chapter query should include title metadata as well. The minimum query for a book chapter must include the following elements:

* book title
* title and subtitle should be separated with a colon (:)
* book year
* chapter author
* first page

To increase the accuracy of matching, publishers should also include as many of the following elements as possible in the query:

* editor (where appropriate)
* publisher
* chapter title

Combining *chapter title* and *chapter author* returns the best matches.

## DOIs in citations<a id='00057' href='#00057'><i class='fas fa-link'></i></a>

Following a review in 2017 of common citation style guides and publishers’ instructions to authors, this is what we recommend for the use of DOIs in citations, of any style or format:

* Include DOIs whenever they are available (use [Metadata Search](https://search.crossref.org/) to find DOIs for registered content)
* Display DOIs as links and follow our [DOI display guidelines](/display-guidelines)
* In your in author guidelines, describe the use of DOIs in general, and for different content types (such as journals, conference proceedings)
* Books that have DOIs at the title and chapter levels should be cited accordingly
* Print materials (of any content type) may display DOIs but should not have their own print-only DOIs
* Providing information to us on update cycles (when possible), and contact information for keeping in touch with questions or future developments (such as new content types).

You can register books and chapters using one of our helper tools: [web deposit form](/documentation/member-setup/web-deposit-form/), and by [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/) - learn more about [markup examples for books and chapters](/documentation/schema-library/markup-guide-record-types/books-and-chapters/).

## Books on multiple platforms<a id='00638' href='#00638'><i class='fas fa-link'></i></a>

Multiple Resolution and Co-access are options for addressing books distributed across multiple platforms. Multiple Resolution ties together all locations where content might be hosted under a single DOI and represents the most comprehensive solution to ensure that metadata and citations are maintained and provisioned together. In the event that those who host book content on  behalf of other publishers cannot adopt the single DOI to content distributed across a number of different platforms, Co-access provides a last resort for these parties to independently assign DOIs and deposit metadata for such books.
