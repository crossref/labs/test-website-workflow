+++
title = "Conference proceedings and papers"
date = "2021-11-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "conference-proceedings"]
identifier = "documentation/principles-practices/conference-proceedings"
rank = 4
weight = 10103
aliases = [

]
+++

Our conference proceedings model supports registration of conference series, proceedings, and papers.

### Conference series

You can register series-level metadata for proceedings that are part of an ongoing series with an ISSN.  A DOI is not required for the series information, but is recommended.

When registering a **conference series** DO:
* include the series name and ISSN in your metadata
* register each proceedings within the series as a separate volume
* include series-level [contributors](/documentation/principles-practices/best-practices/#contributors)
* register a DOI for the conference series - this makes the series cite-able and easy to identify
* include a series number (if you have one)

### Conference proceedings

When registering a single **conference proceeding** DO:
* include a conference title, publisher, and publication date in the proceedings metadata
* include a conference name in the event metadata
* include proceedings-level [contributors](/documentation/principles-practices/best-practices/#contributors) like editors
* register a DOI for the conference volume
* include ISBN assigned to the proceedings
* include a conference acronym within the even metadata
* supply relevant event information like date, location, number, acronym, theme, and sponsor

Do not:
* include more than one proceeding within a single conference element

### Conference papers

When registering **conference papers**, DO:
* include a paper title
* include a publication date
* include all relevant [funding](/documentation/principles-practices/best-practices/funding/), [license](/documentation/principles-practices/best-practices/license/), and [relationship](/documentation/principles-practices/best-practices/funding/) metadata
* include a language (using the language attribute on the conference_paper element)
* include all [contributors](/documentation/principles-practices/best-practices/#contributors)
* include abstracts (recommended for all types of content, but particularly useful for conference papers)
* include [references](/documentation/principles-practices/best-practices/#references)
* identify the publication type (full text or abstract)
* include article / elocation IDs

Do not:
* register separate records for abstract and full text

Review our [Conference Proceeding Markup Guide](/documentation/schema-library/markup-guide-record-types/conference-proceedings/) for XML and metadata help.
