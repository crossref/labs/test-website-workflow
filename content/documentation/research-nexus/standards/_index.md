+++
title = "Introduction to standards"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "standards"]
identifier = "documentation/content-registration/content-types-intro/standards"
rank = 4
weight = 60512
+++

### Why register standards

Standards are often cited in research and registering these with Crossref means that these connections remain part of the scholarly record.

### Obligations and limitations

1. Follow the standards [metadata best practices.](/documentation/principles-practices/standards)
2. You can only register standards by direct deposit of XML using schema version 4.3.6 and above. Our helper tools do not currently support this content type.

### Fees

In additional to annual membership dues, all records attract a one-time registration fee. Fees for standards are different depending on whether the content registered is current or backfile. [Read about the fees.](/fees/#content-registration-fees)

### History

Crossref began accepting metadata deposits for standards in 2005. Our input schema was modified significantly for standards with help from the Standards Technical Working Group. Significant changes to the deposit and indexing of designators were made with schema version 4.3.6, as a result standards may only be deposited using schema versions 4.3.6 and above.

### Key links

* [Standards metadata best practices](/documentation/principles-practices/standards)
* How to register content using [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/)
* [Standards markup guide](/documentation/schema-library/markup-guide-record-types/standards/)
