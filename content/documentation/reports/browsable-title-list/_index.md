+++
title = "Browsable title list"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports", "browsable-title-list"]
identifier = "documentation/reports/browsable-title-list"
rank = 4
weight = 140201
aliases = [
	"/education/metadata-stewardship/reports/browsable-title-list",
	"/education/metadata-stewardship/reports/browsable-title-list/",
	"/education/reports/browsable-title-list/",
	"/education/reports/browsable-title-list"
]
+++

The [browsable title list](https://www.crossref.org/titleList/) provides an alphabetical list of journals, books, and conference proceedings for which Crossref has metadata, and is updated weekly. Browsing and searching may be limited by genre (all, journals, books, or conference proceedings) or search type (title, ISSN/ISBN, subject, or publisher). To search for a specific title, enclose the title in quotes, or search by ISSN.

Search results will include the following (when available):

* *Title* (Journal/Book/Conf Proc): Title name. Journal titles are gray, book titles are green, and conference proceedings titles are purple.
* *Publisher*: Publisher of the title as registered with us.
* *Print ISSN/ISBN*: ISSN or ISBN (indicated by color) of the print version of the title.
* *Electronic ISSN/ISBN*: ISSN or ISBN (indicated by color) of the electronic version of the title.
* *DOI*: DOI assigned at the title level.

To review the results:

* Click the <img src="/images/documentation/Icon-pink-arrow-down.png" alt="Pink arrow down icon" height="23" > icon to view the year(s), volume(s), and issue(s) deposited with Crossref for a title
* Click the <img src="/images/documentation/Icon-green-arrow-right.png" alt="Green arrow down icon" height="23" > icon to view alternative title information, abbreviated titles (if any), other ISSNs or ISBNs, subjects covered, and any coverage notes for this content item. This information is obtained from a third party and may not match data deposited with Crossref
* To request a [missed conflict report](/documentation/reports/missed-conflict-report) for a title, click the <img src="/images/documentation/Icon-blue-chevrons-right.png" alt="Blue chevrons right icon" height="23" > icon at the far right of the row

You can also [download a comma-separated journal coverage list](http://ftp.crossref.org/titlelist/titleFile.csv) (warning: large file).
