+++
title = "Resolution report"
date = "2021-10-18"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports", "resolution-report"]
identifier = "documentation/reports/resolution-report"
rank = 4
weight = 140209
aliases = [
  "/education/metadata-stewardship/reports/resolution-report",
  "/education/metadata-stewardship/reports/resolution-report/",
  "/education/reports/resolution-report",
  "/education/reports/resolution-report/",
  "/support.crossref.org/hc/en-us/articles/213197246",
  "/documentation/metadata-stewardship/reports/resolution-report/"
]
+++

> #### The monthly resolution report shows the number of successful and failed DOI resolutions for the previous month.

## What is a resolution?
When a researcher clicks on a DOI link for an article and the link resolves to the article, that counts as one DOI resolution. For example, clicking on [https://doi.org/10.1038/nature02426](https://doi.org/10.1038/nature02426) counts as one resolution to Nature. No information is captured about the user, and these numbers are not a precise measure of traffic to a member's website, but they provide a measure of the effectiveness of a member's participation in Crossref.

If a researcher clicks on a DOI link for an article and it doesn't resolve, that counts as a failed resolution.

## What's in your Resolution report?

Resolution reports are sent out to members by email at the beginning of each month, and include statistics about all DOI resolutions from the preceding month. By default, resolution reports are sent to the Primary contact for your organization (previously known as the business contact), but we can add or change the recipient(s) as needed. We’ll send you a separate report for each DOI prefix you’re responsible for.

The report includes:

* **Resolution failure rate**: the percentage of DOI resolution attempts that failed. The prefix failure rate and the overall failure rate (for all Crossref members) are also included so you can see how you compare to others.
* **Resolutions by month**: total number of resolutions per month for the past 12 months, by prefix (count) and overall (all members).
* **Resolution stats**: resolution counts for the report prefix.
* **Top ten DOIs**: list of the ten DOIs with the highest number of successful resolutions for the month, and the number of times each DOI was successfully resolved.
* **Failed DOIs**: a list of DOI resolution attempts that failed (i.e. resolved to a Handle error page). This list is presented as a .csv file attached to the report email and contains both the failed DOI and number of failures.
* **Resolution counts by publication title**: the number of total DOI resolutions per title

## What should I do with my resolution report? <a id='00229' href='#00229'><i class='fas fa-link'></i></a>

The resolution report gives you an overview of DOI resolution traffic, and can help identify problems with your DOI links or your DOI registration process. The *failed DOI.csv* linked to your resolution report email contains a list of all DOIs with failed resolution attempts - if a user clicks on a DOI with your DOI prefix and the DOI is not registered, it will be included on this report.

There’s a certain amount of noise with these reports (resolutions from crawlers or automated processes) but do check any DOIs with a high number of failures, and look out for significant changes in your resolution failure rate.

### Troubleshooting: possible reasons for DOI failures and how to fix them <a id='00643' href='#00643'><i class='fas fa-link'></i></a>

* **The DOI has never been registered with Crossref** - if your DOI has never been successfully registered with us, then you will see failed resolutions. Make sure you (or your suppliers) are definitely successfully registering your DOIs. Learn more about how to [verify your registration](/documentation/content-registration/verify-your-registration/).
* **DOI publication and registration are out of sync** - if you publish your DOIs on your website before you register them with us, this will lead to DOI resolution failures. DOIs should be registered and distributed simultaneously. If this is not possible, the gap should be hours or a day, not days or weeks.
* **The DOIs are displayed incorrectly on others websites** - sometimes others may copy your DOIs and display them on their own website, but they make a mistake with the display. They may add a period to the end, or cut off the final digit. If others then try to resolve that incorrect DOI, you will see resolution failures. You can find this out by googling any DOIs with problems from your Resolution Report and seeing where you find them online. You can then decide if you wish to contact the website owners to ask them to update the DOI to the correct one.
* **Linking issues** - the DOI resolution link may be incorrect. You can update your DOI resolution URLs at any time by [resubmitting your record](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/#00168).
* **User error** - users sometimes make mistakes when typing or copying DOIs. If you often see failures that you think are caused by user error, review how your DOIs are displayed. Some common user errors are:
    * Confusing O and 0, or l and 1 - this is more common for DOIs on print publications, because have to type them, rather than clicking on a link. If your DOI failures often contain DOIs with O being confused with 0, or l with 1, consider changing your DOI suffix. Long strings of letters and numbers can cause problems as well
    * DOIs ending with '.' - if a viable DOI has a '.' appended, it will fail to resolve. This is often caused by the DOI being linked from references that end with a '.'
    * DOIs with special characters instead of *‘-’* - this commonly happens when a user pastes a DOI from a PDF
    * DOIs with special characters such as \<, \>, \#, \* and \+ (Crossref no longer accepts deposits with special characters).
* **Problems with URL-encoded DOIs** - the handle resolver supports URL-encoded DOIs. The resolution logs sometimes misrepresent the encoded characters. As a result, some badly-encoded DOIs will appear in your resolution log as correctly-encoded DOIs. This typically happens when an already-encoded DOI is mistakenly encoded again. For example, DOI `10.5555/example` would be correctly encoded as `10.5555%2Fexample` (the / is encoded as %2F). If the DOI is encoded again, the % in the DOI becomes %25, making the DOI `10.5555%252Fexample`. This DOI will not resolve but will appear in the failed DOI report as `10.5555%2Fexample` (a valid DOI).


## Resolution Report FAQs <a id='00235' href='#00235'><i class='fas fa-link'></i></a>

{{% accordion %}}
{{% accordion-section "Where do the resolution report statistics come from?" %}}

Resolution statistics are based on the number of DOI resolutions made through the [DOI proxy server](https://doi.org/) on a month-by-month basis. These statistics give an indication of the traffic generated by users clicking DOIs. CNRI (the organization that manages the DOI resolver) sends us resolution logs at the end of every month.
{{% /accordion-section %}}

{{% accordion-section "When can I expect to receive my monthly resolution report?" %}}

Each month we deliver resolution reports to the primary contact on your account (or, anyone else affiliated with your account who has been added as a recipient). In the past, you could expect these reports somewhere between the 5th and 10th day of each month. We'd receive logs from the [Corporation for National Research Initiatives](https://www.cnri.reston.va.us/) (CNRI) and then use those logs to generate your reports. As more and more content is registered with us and resolutions logged with CNRI, that means both CNRI and Crossref have more and more data to process.

The volume of resolutions for DOIs registered with us continues to grow, which means you can and should expect that reports will arrive to you later than they may have arrived in the past. Once we begin the report, it takes our system three to five days to distribute those reports to the contacts on each account.

Thus, we think it's realistic that distribution of resolution reports (to be completed to all of our members) will be the second week of each month.
{{% /accordion-section %}}

{{% accordion-section "What do you mean by 'unique DOI?'" %}}
The *unique DOI* number is the number of distinct DOIs that have been resolved. If your report lists 20 resolutions and 1 unique DOI, this means 1 DOI was resolved 20 times.
{{% /accordion-section %}}

{{% accordion-section "My resolution report tells me I have a high failure rate - what should I do?" %}}
The ideal failure rate is 0%, but 2-3% is the norm. If you are new to Crossref, or have only deposited metadata for a small number of content items, you may have a high failure percentage (for example, 1 failure and 9 successes = 10% failure rate).

If your overall resolution failure rate is higher than around 2-4%, look closely at the .csv file of failed DOIs for your account, and make sure the DOIs listed have definitely been registered. Our [troubleshooting section](/documentation/reports/resolution-report/#00643) will help with other possible reasons for a high failure rate.
{{% /accordion-section %}}

{{% accordion-section "This DOI is registered - why is it on my failed DOI list?" %}}
We send you all failed resolutions from the preceding month, even if the content has subsequently been registered. If an unregistered DOI is clicked on December 5 but not registered until December 6, we count that as a resolution failure. If you find a lot of registered DOIs in your failed DOI list, you should make sure you aren’t distributing your DOIs before they have been registered.
{{% /accordion-section %}}

{{% accordion-section "Will a high failure rate affect my membership?" %}}
It won’t affect your membership status (unless you’re truly negligent and regularly distribute DOIs without registering them) but a Crossref membership is of limited value if you don’t register your DOIs and provide quality metadata.
{{% /accordion-section %}}
{{% /accordion %}}

Learn more about [the history and current state of our popular monthly resolution reports](/blog/resolution-reports-a-look-inside-and-ahead/).
