In order to get working DOIs for your content and share your metadata with the scholarly ecosystem, you need to register your content with Crossref.

Your metadata is stored with us as XML. Some members send us XML files directly, but if you’re not familiar with writing XML files, you can use a helper tool instead. There are two helper tools available - these are online forms with different fields for you to complete, and this information is converted to XML and deposited with Crossref for you.
A big decision to make as a new member is which of our content registration methods to use.
