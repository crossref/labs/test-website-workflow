Many types of organizations register their research objects with us. You could be a research institution, a publisher, a government agency, a research funder, or a museum! In order to become a member, you need to meet the criteria set out in our [membership terms](/membership/terms) approved by our governing board:

> Membership in Crossref is open to organizations that produce professional and scholarly materials and content.

Essentially, if your content is likely to be cited in the research ecosystem and you consider it part of the evidence trail, then you’re eligible to join.

You should take out Crossref membership in the name of the parent organization that is responsible for publishing your content. This means, for example, that if you wish to register a journal with us, and the journal is actually published by your university, then your university would need to join Crossref rather than just your journal or department. If you wish to register a journal and the journal itself is the largest legal entity, then you are welcome to join us in the name of your journal.  

NB Due to international sanctions, we are currently unable to accept membership applications from organizations based in Cuba, Iran, North Korea, Syria, or the Donbas or Crimea regions of Ukraine. There's more information on our [sanctions page](/operations-and-sustainability/membership-operations/sanctions/). But do still get in touch if you are based in one of these countries so we can make a note of your interest and let you know if the situation changes.
