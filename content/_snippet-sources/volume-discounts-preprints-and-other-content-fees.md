|Total number of registered DOIs per quarter|Registration fee per record (current)| Registration fee per record (backfile)|
| :------| :-----------:|:-----------:|
|0-1000|USD 0.25|USD 0.15
|1,001 - 10,000 |USD 0.25 |USD 0.12
|10,001 - 100,000 |USD 0.25|USD 0.06
|100,001 and up |USD 0.25|USD 0.02
