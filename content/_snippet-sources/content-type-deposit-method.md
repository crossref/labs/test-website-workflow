## Which registration tools support which metadata?<a id='00000' href='#00000'><i class='fas fa-link'></i></a>

| Record type / deposit method | OJS-to-Crossref plugin | Web deposit form | Direct deposit of XML|
|--- |--- |--- |--- |
|Books and chapters | No (OJS is a journal platform) | Yes |   Yes |
|Conference proceedings | No (OJS is a journal platform) | Yes | Yes |
|Datasets | No (OJS is a journal platform) | No |   Yes |
|Dissertations | No (OJS is a journal platform) | Yes |   Yes |
|Journals and articles | Yes | Yes |  Yes |
|Peer reviews | No (OJS is a journal platform) | No |   Yes |
|Posted content (including preprints) | No (OJS is a journal platform) | No|  Yes |
|Reports and working papers | No (OJS is a journal platform) | Yes |   Yes |
|Standards | No (OJS is a journal platform) | No |  Yes |
|Documentation for this deposit method | [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/) | [Web deposit form](/documentation/member-setup/web-deposit-form/) | [ Direct deposit of XML](/documentation/member-setup/direct-deposit-xml/) |
