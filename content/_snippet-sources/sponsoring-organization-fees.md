If you’re an organization who works on behalf of groups of smaller organizations that want to register content with Crossref, you’ll be set up as a [Sponsor](/community/sponsors). Sponsors work directly with us in order to provide administrative, technical and---if applicable---language support to the communities they work with.

The annual membership fee you pay as a Sponsor is based on the total annual publications revenue, income or funding of all the organizations you support, whichever is higher.

|Total annual revenue or income or funding|Annual fee|
| :------| :-----------:|
|<USD 1 million USD|USD 275|
|USD 1 million - USD 5 million|USD 550|
|USD 5 million - USD 10 million|USD 1,650|
|USD 10 million - USD 25 million|USD 3,900|
|USD 25 million - USD 50 million|USD 8,300|
|USD 50 million - USD 100 million|USD 14,000|
|USD 100 million - USD 200 million|USD 22,000|
|USD 200 million - USD 500 million|USD 33,000|
|>USD 500 million|USD 50,000|

Each quarter you will also be sent an invoice for any [content items](/fees/#content-registration-fees) that have been registered by the organizations that you sponsor. The charges are listed by each organization’s prefix on the invoice. There are different fees for different content types and the fees are also different depending on the publication date of the content. You will be asked to recategorize your annual fee at the end of every year, as the number of sponsoring members you represent grows.
