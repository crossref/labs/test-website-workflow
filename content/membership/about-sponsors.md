+++
title = "Working with a sponsor"
date = "2022-11-23"
author = "Susan Collins"
draft = false
image = "/images/banner-images/get-started-new-grass.jpg"
maskcolor = "crossref-blue"
aliases = [
    "/membership/small-publishers",
    "/membership/small-publishers/"
]
[menu.main]
parent = "Become a member"
rank = 2
weight = 2
+++

We have thousands of small members from all over the world. Being small-scale doesn’t limit your ability to connect your content with the global network of online scholarly research. Each Crossref member also gets to cast their vote to create a board that represents all types of organizations.

## Why join via a Sponsor?

If you publish one journal or thousands, you’re welcome to join our growing community.

We know that cost and technical capabilities can be barriers to participation. Joining through a Sponsor can help. Members who take this option have the same [obligations and benefits](/membership#member-obligations-and-benefits) as any other member, but they have someone representing them for Crossref services and they don't pay fees to Crossref directly.

Instead, the sponsor pays one membership fee to Crossref for all the members that they work with, and sponsors also pay the content registration fees for any content registered by their members. Many sponsors then pass on these fees to members and/or charge the members for their services, so it’s important for members to discuss the agreement carefully with a sponsor before starting to work with them.

Sponsors have to fulfill [strict criteria](/community/sponsors#sponsor-criteria) to be accepted. Different sponsors offer different services, but most:

* Facilitate content registration with Crossref on behalf of the members they work with
* Provide administrative, technical, and (if applicable) local language support
* Handle Crossref billing (which is in US$) on behalf of these members
* Are able to receive payment for their services in local currency.

## How to join via a Sponsor

Some Sponsors only work with organizations that are connected to them in some way - for example, they may be part of the same university consortium or the organization may be using the Sponsor's publishing platform. You can find a list of Sponsors below. If they're based on your region and have contact details below then they will be happy to discuss acting on your behalf for Crossref membership.

Do contact them directly if you'd like to explore working with them; we cannot broker that arrangement for you. If you agree to work together, your Sponsor will send you a dedicated link to apply for Crossref membership under their sponsorship. Please don't use the standard link to apply for membership that you can find on this website.

Upon receipt of your application, we usually provide your Sponsor with your prefix and [Crossref account credentials](/education/member-setup/account-credentials/) within four working days and they will be in touch with you from there.

## Find a Sponsor

The following organizations are currently acting as Crossref Sponsors. Jump to your region:

#### [Asia Pacific](#asia-pacific)
#### [Central Asia](#central-asia)
#### [Central/Eastern Europe](#central-and-eastern-europe)
#### [Latin America/Caribbean](#latin-america-and-caribbean)
#### [North Africa/Middle East](#north-africa-and-middle-east)
#### [Sub-Saharan Africa](#sub-saharan-africa)
#### [US/Canada](#us-and-canada)
#### [Western Europe](#western-europe)

---

### Asia Pacific
{{% accordion %}}
{{% accordion-section "APUB (South Korea)" %}}  

APUB was established in 2018 and provides IT services such as online publishing, online paper submission review system and journal homepages for many academic societies.

### Contact  

Homepage:[http://www.apub.kr/](http://www.apub.kr/)  
Email: [master@apub.kr](mailto:master@apub.kr)  

{{% /accordion-section %}}
{{% accordion-section "DocuHut Co., Ltd. (South Korea)" %}}

DocuHut is a professional publisher in Korea. We provide total solutions from first publication, online journal, manuscript submission & review system, manuscript editing, copyediting, proofreading, printing, JATS XML, to DB indexing.

### Contact  

Homepage: [http://www.docuhut.com](http://www.docuhut.com)
Email: [support@docuhut.com](mailto:support@docuhut.com)
Phone: +82-2-2274-6771  

{{% /accordion-section %}}

{{% accordion-section "Guhmok Publishing Company (South Korea)" %}}

Guhmok is an academic publishing company located in Korea. We provide one-stop solutions for journals—Manuscript submission system, copyediting, proofreading, printing, JATS XML, journal site, etc.— and are currently working only with Korean organizations.

### Contact

[guhmok@guhmok.com](mailto:guhmok@guhmok.com)
Phone: +82-2-2277-3324  
Homepage: [http://www.guhmok.com](http://www.guhmok.com)

{{% /accordion-section %}}

{{% accordion-section "Neliti (Indonesia)" %}}

[Neliti](https://www.neliti.com/) is a web-based software platform for creating, hosting, managing and indexing institutional repositories, academic journals and conference proceedings.

Neliti offers DOI registration as an auxiliary service to help institutions assign DOIs and utilize other Crossref services.

Neliti runs a helpdesk for Crossref-related enquiries which is available for all institutions, including those not directly working with Neliti. They can be contacted via email at [hello@neliti.com](mailto:hello@neliti.com). For more information, please visit [https://www.neliti.com/](https://www.neliti.com/).

### Contact

[hello@neliti.com](mailto:hello@neliti.com)

{{% /accordion-section %}}

{{% accordion-section "Publishing House for Science and Technology, Vietnam Academy of Science and Technology (Vietnam)" %}}

Providing support services for journal publishing, Publishing House for Science and Technology is a not-for-profit publisher of open access scientific research journals. We have over 10 years of scientific publication experience. We are a group of highly-motivated educationists, researchers and technology enthusiasts who are committed to promoting open access publications, thus enabling speedy propagation of quality research information. We are eager to share our experience and help any journal willing to register their content with Crossref.

Our services include:
- preparation and validation of article metadata     
- design and allocation of DOIs to articles     
- depositing validated metadata to Crossref as per their XML format
- training and support.

### Contact

To learn more about our full suite of editorial services, please reach out to us at [cip@vjs.ac.vn](mailto:cip@vjs.ac.vn).

{{% /accordion-section %}}

{{% accordion-section "Relawan Jurnal Indonesia (Indonesia)" %}}

[Relawan Jurnal Indonesia (RJI)](https://relawanjurnal.id/)/ Indonesian Journal Volunteers were established after a meeting of the journal managers in Indonesia.  

Represented parties agreed upon a voluntary spirit and contributions of thoughts, energy and materials related to electronic journal management with other journal managers in other universities, research institutions, and other institutions publishing journals throughout Indonesia without any differentiation. To realize its vision, Relawan Jurnal Indonesia registered its organization with the Ministry of Law and Human Rights of Indonesia numbered AHU-0005712.AH.01.07.YEAR 2017, on Certification of Legitimately Established Organization of the Relawan Jurnal Indonesia/Indonesian Journal Volunteers. The vision of Relawan Jurnal Indonesia/Indonesian Journal Volunteers, is to assist journal managers' publication processes into at least a national-level of high quality, and well-respected electronic journal management.   

Type of organization we can work with:
- Publishers or Universities using Open Journal System for their journals or Open Monograph Press for their Books, or Eprints for their Repositories. However, any similar platforms are welcome.
- Come from Indonesia or Southeast Asia. Other regions in Asia are welcome if they are not a profit oriented organization.
- Languages: English, Bahasa (Indonesia).
- Organization types: Universities, Governments, or other non profit organizations are welcome.
- Fee category: We welcome publisher or organization with total publishing revenue or expenses less than USD 1 million USD/Year
- Content types: Journals, books, conference proceedings, conference papers, theses, dissertations.  

For more information see: [http://doi.relawanjurnal.id/](http://doi.relawanjurnal.id/)

### Contact

Email: [contact@relawanjurnal.id](mailto:contact@relawanjurnal.id)  
{{% /accordion-section %}}

{{% accordion-section "Additional Sponsors (Asia Pacific)" %}}  

- Airiti, Inc. (Taiwan)  
- Aliansi Jurnal Ekonomi dan Bisnis Indonesia (Indonesia)  
- Asosiasi Pengelola Jurnal Indonesia (Indonesia)  
- Conferences.id (Indonesia)     
- EArticle (South Korea)  
- Forum Pengelola Jurnal Manajemen (Indonesia)  
- Inforang (South Korea)  
- Isway (South Korea)  
- Japan Science and Technology Agency (JST)   (Japan)  
- Korea Scholar (South Korea)  
- Korean Association of Medical Journal Editors (KAMJE)   (South Korea)  
- Korean Studies Information (KSI)   (South Korea)  
- Kyobobook Center (South Korea)  
- M2PI (South Korea)  
- Nurimedia Co., Ltd. (South Korea)
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)   
- Public Knowledge Project (PKP) (based in Canada but work with organizations globally)  
- Research and Social Study Institute (ReSSI)   (Indonesia)  
- Sin-Chn Scientific Press Pte. Ltd (Singapore)
- UniveID (Indonesia)  
- Vietnam Citation Gateway (Vietnam)  

{{% /accordion-section %}}
{{% /accordion %}}

---

### Central Asia
{{% accordion %}}

{{% accordion-section "Academic Research Publishing Group (Pakistan)" %}}  

Academic Research Publishing Group (ARPG) is a publisher of peer-reviewed international journals. ARPG was established in 20015. We have over 6 years of scientific publication experience. ARPG promote research the World over in numerous disciplines including science, engineering, management, technology, social sciences, economics, education, language and literature. ARPG is an open-access publisher because all journals of international repute provide free access to the complete text of articles just on single click. ARPG publishes articles after they are peer-reviewed and edited by some World’s leading researchers, authors and scholars. ARPG allows anyone in the World to adapt copy and use the work printed. The original work and sources are cited properly. We are eager to share our experience and help any journal willing to get DOI number for their content. Our DOI services include:
 - Preparation and validation of the article’s metadata.
 - Design and allocation of DOI numbers to articles.


### Contact
Website: [https://www.arpgweb.com/](https://www.arpgweb.com/)
Email: [info@arpgweb.com](mailto:info@arpgweb.com)

{{% /accordion-section %}}


{{% accordion-section "Applied and Natural Science Foundation (India)" %}}  

Applied and Natural Science Foundation is a not-for-profit publisher of open access scientific research journals. We have over 10 years of scientific publication experience. We are a group of highly motivated educationists, researchers, and technology enthusiasts who are committed to promoting open access publications, thus enabling speedy propagation of quality research information.

We are eager to share our experience and help any journal willing to get DOI number for their content.  Our DOI services include:
- Preparation and validation of the article’s metadata.
- Design and allocation of DOI numbers to articles.
- Deposit validated metadata to Crossref as per their XML format.
- Training and support.

### Contact
To learn more about our full suite of editorial services, please reach out to us at [info@ansfoundation.org](mailto:info@ansfoundation.org)  

{{% /accordion-section %}}

{{% accordion-section "Erudite Data Science & Analytics (India)" %}}  

Purpose: Rendering web technologies support & research datasets evaluation services to publishers. We work with both academic and corporate publications. Our services include web development, web security, web metadata development, web publishing workflow design, SEO, video & text based media content creation & promotion, academic publishing support services, data creation & evaluation, data refinement & applications.

Read more about Publishers Evaluation Criteria at https://eruditedata.com/publishers.html.

### Contact

Address: Erudite Data Science & Analytics, #1/1, Tagore Garden, Yamuna Nagar (HR) India PIN 135001  
Email: [service.desk@eruditedata.com](mailto:service.desk@eruditedata.com)  

{{% /accordion-section %}}

{{% accordion-section "EScience Press (Pakistan)" %}}  


[EScience Press](https://esciencepress.net/) is a not-for-profit publisher of scientific research journals and books. Our team is committed to achieving the highest quality science to drive progress in research and innovation.

For over 10 years we have been helping students, researchers, scientific societies, and academic institutions to achieve their goals in an ever-changing world. By partnering with leading academic institutions and learned scientific societies, we support researchers to communicate discoveries that make a difference.

EScience Press provides cloud hosting and professional website solutions for institutions/organizations publishing scientific journals. We offer fine-tuned, blazing fast and managed editorial workflow system, Open Journal System (OJS) hosting with a professionally run server environment that includes an up-to-date, secure version of the software plus training and support. We help our member organizations collaborate internationally on research programs that we coordinate in almost every scientific domain.

EScience Press is partnering with [DOI Pakistan](https://doi.org.pk/), and works as a Crossref sponsor to offer all of its core services to sponsored members globally.

### Contact  

Website: https://esciencepress.net/
Email: [info@esciencepress.net](mailto:info@esciencepress.net)
Phone: +17249900670

DOI Support

Website: https://doi.org.pk
Email: [info@doi.org.pk](mailto:info@doi.org.pk)
Phone: +923006812118

{{% /accordion-section %}}

{{% accordion-section "Informatics (India)" %}}  


[Informatics](https://www.informaticsglobal.com) was promoted three and a half decades ago with a vision to be a leading global player in the electronic information Business. Focusing on both domestic and global markets, we serve global publishing clients by providing editorial and compilation services that includes – Online Journal Management System (OJMS), digitization of backfiles, DOI, Plagiarism Check Service, Layout Designing, online & print publishing.

### Contact  

Phone: 9900236751
Email: [publishing@informaticsglobal.com](mailto:publishing@informaticsglobal.com)  

{{% /accordion-section %}}

{{% accordion-section "KVR Scientific Services (KVRSS Group) (India)" %}}

KVR Scientific Services (KVRSS Group) is offering its services to the national and international
clients with its unique support channel availability for 24/7/365 days.  We provide ready-to-use applications / solutions, tech infrastructure, and resources that can
make the publisher’s work easier. Our services are as follows:    

**General Services**
* Journal management
* Book publishing house management
* Conference management
* Web development &amp; hosting
* Application development
* Typesetting (designing of papers for the perfect layout)
* Editing / proofreading of manuscripts
* Support for the Open Journal System (OJS)
* Indexing of the published papers in best possible indexing platforms
* Digital repository for the publishers
* Plagiarism check service
* SEO
* Google workspace for the professional emails with your domain and its related services
* Microsoft 365 for the professional emails with your domai.
* Zoho for the professional emails with your domain and its related services
* SEO support.  

**Crossref Services**  
* DOI registration for the journals, books, conference proceedings, conference papers,
theses and dissertations.
* Preparation and validation of the article’s metadata
* Design and allocation of DOI numbers to the articles
* Deposit validated metadata to the Crossref as per their format
* Content registration &amp; updation
* Reference linking
* Crossmark
* Similarity Check
* Cited-by (DOI based citations) registration &amp; integration

### Contact

Website: [https://kvrssgroup.com/kvrph](https://kvrssgroup.com/kvrph)  
Email: [mou@kvrssgroup.com](mailto:mou@kvrssgroup.com)  
Phone / Whatsapp: +91-6281333383

{{% /accordion-section %}}

{{% accordion-section "MRI Publication Pvt. Ltd. (OPC) (India)" %}}

At MRI we provide end-to-end publishing services to authors and
publishers. Our service portfolio consists of online submission on a
manuscript review system with user-friendly access for the authors,
reviewers, and editors; online publishing; DOI submission; XML-based
process; digital preservation; working closely with the indexing bodies; and content promotion.

### Contact

For more information, please visit our website [https://mripub.com](https://mripub.com) or contact Pradeep Tiwari at [pradeep@mripub.com](mailto:pradeep@mripub.com).

{{% /accordion-section %}}

{{% accordion-section "QTanalytics India (India)" %}}

QTanalytics was established in 2016 with a leading data analytics, research, and training provider firm based in Delhi, India. We operate globally, serving both national and international clients, and we provide complete journal management services to publishers, offering DOI, Plagiarism Check Service, Layout Designing, online & print publishing to scholars, academic institutions, societies, associations, and corporations.

We have an experienced team to serve publishers joining Crossref. We offer content registration, reference linking, Crossmark, and Similarity Check.

### Contact

Send us a message: [https://qtanalytics.in/contact](https://qtanalytics.in/contact)  
URL: [https://qtanalytics.in](https://qtanalytics.in)  
Email:[qtanalyticsindia@gmail.com](mailto:qtanalyticsindia@gmail.com)  Phone: +91-9458270556

{{% /accordion-section %}}

{{% accordion-section "Research and Innovation Center LLC (Uzbekistan)" %}}

Our goal is to support science and innovation projects, improve the quality of scientific research, and support the integration of science and industry.

### Contact

For more information, please visit our website [https://iric.uz](https://iric.uz) or contact us at [support@iric.uz](mailto:support@iric.uz) / [uziric@gmail.com](mailto:uziric@gmail.com)

{{% /accordion-section %}}

{{% accordion-section "Sequence Research & Development Private Limited (India)" %}}
Innovation through Research
Company is aiming to empower the research community around the world with the help of technology & innovation. Provide ready-to-use solutions, community platforms, the technology infrastructure, and resources that can make the researcher’s life easier.
Providing services to:
* Publisher, University, Research Scholar, Open Journal System (OJS), Open Monograph Press (OMP), Open Preprint Systems, Dspace or their Repositories
* Organization types: Universities, Governments, or other non-profit organizations are welcome.
* Content types: Journals, books, conference proceedings, conference papers, theses, dissertations.

Website: [https://sequencernd.com]([https://sequencernd.com/)
Email: [sequencernd@gmail.com](mailto:sequencernd@gmail.com)

{{% /accordion-section %}}

{{% accordion-section "Scientific Research Solution Private Limited (SCIRESOL) (India)" %}}

Scientific Research Solution Private Limited (SCIRESOL) offers a wide range of journal maintenance and management services to Public Universities, Deemed to be Universities, Societies, and individual journal owners.  

We provide a dedicated platform to support and maintain scientific journals, helping in journal allied services like DOI, XML, ePUB, PDF, and metadata support to the journal owners.  


### Contact

Website: [www.sciresol.co](http://www.sciresol.com), [www.manuscriptcommunicator.com](http://www.manuscriptcommunicator.com)  
Email: [info@sciresol.com](mailto:info@sciresol.com)  
Phone: -91+9845883696  

{{% /accordion-section %}}

{{% accordion-section "Ubitech Solutions Pvt Ltd (India)" %}}  

Ubitech Solutions Private Limited was established on 17 August 2006. They provide an end-to-end Journal Management System for publishers, academic institutions, societies, and associations. They also provide services like journal website designing, custom XML creation and typesetting along with CrossRef services: DOI (content registration), reference linking, Crossmark, Cited-by and Similarity Check.  

### Contact

Website: [https://ubijournal.com](https://ubijournal.com)   
Email: [sales@ubitechsolutions.com](mailto:sales@ubitechsolutions.com)   
Phone: +91 98262 74403, +91 7773000234    

{{% /accordion-section %}}


{{% accordion-section "VS Infosolution (India)" %}}

We at VS Infosolution offer web technologies support & services to publishers. We work with both academic and corporate publications. Our services include web development; web security; web metadata development; web publishing workflow design; SEO; video and text-based media content creation and promotion; academic publishing support services; data creation and evaluation; data refinement and applications. We are eager to share our experience and help any journal/publisher from South East Asia willing to get DOI number for their content. Our
DOI services include:

* Preparation and validation of the article’s metadata.
* Design and allocation of DOI numbers to articles.
* Deposit validated metadata to Crossref as per their XML format.
* Provide other services of Crossref like Crossmark, Reference linking and Funder Registry etc.
* Training and support.

### Contact

Email: [info@vsinfosolution.com](mailto:info@vsinfosolution.com)  
Address: VS Infosolution, G-20, Phase II, Heramb Paradise, Wayale Nagar, Kalyan (West), Thane (MS) India. Pin: 421301

{{% /accordion-section %}}

{{% accordion-section "Wanfang Data (China)" %}}

Wanfang Data focuses on science & technology, academic contents over 25 years as the leading information service provider in China. It was founded by Institute of Scientific and Technical Information of China (ISTIC) in 1993, which is subsidiary of Ministry of Science and Technology of P. R. China, became the first state-owned shareholding high technology enterprise in information service area in 2000.  

In 2007，Wanfang Data & ISTIC established the first DOI registration service in Asia - "Chinese DOI" jointly. By the end of 2017, we’ve registered more than 27 million DOIs for contents published in China, including 25 million journal papers from 7400 journal titles, 2 million dissertations, science data sets, books, conference proceedings, etc.

In 2013, Wanfang Data became a Sponsoring Affiliate of Crossref, to help academic publishers in China to register DOIs for their English contents with Crossref service. Our agent service for Crossref combined with Chinese DOI service can help publishers promote the influence for their English contents quickly and widely, both in China and abroad.

### Contact

[cuixl@wanfangdata.com.cn](mailto:cuixl@wanfangdata.com.cn), [doi@istic.ac.cn](mailto:doi@istic.ac.cn)  
Phone: +861058882665

Address: Room 216, 15 Fuxing Rd., Beijing China, 100038  

QQ Group: 277120936 (please note the name of your journal or organization when you join the group)  
Website: [http://www.chinadoi.cn](http://www.chinadoi.cn), [http://www.doi.org.cn](http://www.doi.org.cn), [http://www.wanfangdata.com.cn](http://www.wanfangdata.com.cn)  

<a href="http://www.chinadoi.cn" target="_blank"><img src="/images/community-images/china-doi.png" alt="china doi logo" width="250"></a> <a href="http://www.wanfangdata.com.cn" target="_blank"><img src="/images/community-images/wangfang-data.png" alt="wangfang data logo" width="250"></a> <br>  

{{% /accordion-section %}}


{{% accordion-section "Additional Sponsors (Central Asia)" %}}

- Academic Journal Incorporated (Uzbekistan)  
- Advance Educational Institute and Research Center (Pakistan)  
- Clever Consult (Kazakhstan)
- EScience Press (Pakistan)
- I Edu Group (Uzbekistan)  
- Institute of Metallurgy and Ore Benefication (Kazakhstan)  
- Marwah Infotech (India)
- Mongolian Digital Knowledge Solutions LLC	(Mongolia)
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)
- Public Knowledge Project (PKP) (based in Canada but work with organizations globally)  
- Tadqiqot (Uzbekistan)  
- TechWheels.net (India)    

{{% /accordion-section %}}
{{% /accordion %}}

---

### Central and Eastern Europe

{{% accordion %}}

{{% accordion-section "ASOS Publishing (Turkey)" %}}
Dergi Platformu for academic journals published anywhere in the world, in Turkey or in electronic media hosting and management services offering editorial process. All processes are executed automatically by the system. It has an easy to use interface. Users and referees are informed about the broadcast status via regular e-mail and sms. Advanced statistics such as individual and regional (city-based) downloads and readings of articles are provided by the system. You can use the system from ready-made designs or with an tailored interface. Articles uploaded to the system can be submitted to the similarity test automatically via intihal.net. Articles published for journals scanned in SOBİAD can be indexed directly through the SOBIAD - Citation Index . There is a messaging module between authors and editors within the system.

### Contact
Website: [http://dergiplatformu.com](http://dergiplatformu.com)
For more information: [info@dergiplatformu.com](mailto:info@dergiplatformu.com)

{{% /accordion-section %}}

{{% accordion-section "Editorum (Russia)" %}}

Universal publishing platform EDITORUM (www.editorum.io) for publishers/universities and authors/scientists. Full automation from science paper creation to editorial work-flow processes for educational and scientific publishers, university publishers and other publishing organizations and a wide range of content such as: (journals, monographs, textbooks, conference proceedings etc.) and CMS system.

Универсальная издательская платформа EDITORUM (www.editorum.ru) — программное онлайн решение для научных издателей, организаций и ученых. Полная автоматизация от момента создания научных публикаций автором до редакционных процессов и управления сайтом. Система позволяет организовать и автоматизировать работу издательства со всеми типами научного контента (журналы, монографии, учебники, материалы конференций и т.п.)

### Contact
Phone: +7 (499) 350-54-81
support@editorum.ru
https://editorum.ru

### Контакты
Phone: +7 (499) 350-54-81
support@editorum.ru
https://editorum.ru

{{% /accordion-section %}}

{{% accordion-section "Han Yazılım Co (Turkey)" %}}

As Han Yazılım Co. we are serving our customers with our software solutions since 2001. Based on the requirements of daily improvements, we did update our abilities as well and jumped to .NET technology in 2006 additional to traditional .asp and .html software.
We have our own server in İzmir, and we keep all our projects in there. After being rewarded by the Ministry of Education as the second best project education software holder in 2006, we fired up our operations and turned into a national service provider rather than a localized one. With our customers located in the local market and even international market – from Istanbul to Paris – we are adopting internet technology more and more for our business. Our team consists of academic professionals, software engineers, and diligent young people. We always believe that a successful project needs imagination, defining and accurate planning and correct strategy, considering and foreseeing all possible circumstances, getting the benefit of technology, and a perspective of modern IT approach.

We have been acting as a Sponsor for Crossref since 2012 and currently work with hundreds of publishers. The Crossref services we support include Content Registration, Similarity Check, Crossmark and Cited-by. We offer our customers a custom web app (http://doi.doidestek.com) which they can access and easily submit or edit DOIs and metadata with our simple, user-friendly interface. Demo accounts are available for testing. We also provide our Sponsored Members tutorial videos in order to let them start registering content right away. We are ready to work with any publisher who is willing to get benefits of using CrossRef services.

### Contact

Any potential Sponsored Member (international) may get in touch with us by sending an email to [info@hanyazilim.com](mailto:info@hanyazilim.com) or they can reach us via Whatsapp (+90 537 727 8803). For those local customers - within Turkey - our call center number is +90 850 303 1955.  

{{% /accordion-section %}}

{{% accordion-section "METADATA, LLC (Republic of Moldova)" %}}

METADATA aims to promote and provide permanent access to various digital research resources, thus putting into practice Open Science practices among different stakeholders. We assign DOIs to research outputs, improving their discoverability, accessibility, citation and reuse.

We welcome collaboration with research organisations, universities,
government and not-for-profits.

Feel free to contact us for more details.

### Contact

Email: [info@metadata.md](mailto:info@metadata.md)

{{% /accordion-section %}}

{{% accordion-section "LLC Scientific Publications (Ukraine)" %}}

The company Scientific Publications has been a market leader in scientific and publication consulting in Ukraine, Kazakhstan, Poland, and the CIS countries for more than 6 years. The main values of the organisation are professionalism; individual approach in interaction with clients and partners; mutual assistance; and innovation in decision-making.

The credibility of the company Scientific Publications is confirmed by cooperation with the most influential Ukrainian, European, and Asian researchers, and the existence of a declared partnership, by signing official documents and memorandums, with more than 50 scientific organisations and higher education institutions.

Many years of experience in the field of scientometrics and scientific consulting helps the specialists of the company Scientific Publications to provide comprehensive support for publication projects of any level of complexity. The activity is aimed at providing: information and communication support for the publication of articles in journals of the most influential international databases; editing and structuring of scientific material; professional translation and proofreading services by a native speaker of a foreign language; communication with representatives of publishing houses and members of the editorial board of scientific journals; and support for indexing a scientific article.

Cooperation of the company Scientific Publications with Crossref contributes to:

- Better check the uniqueness of the text using the anti-plagiarism system, conduct an audit of scientific works.  
- Ensuring the possibility of assigning a digital object identifier (DOI) for scientific organizations and publishing houses.  
- Implementation of comprehensive consulting services in the field of writing and publication of scientific research.  

We are always glad to cooperate with you!

### Contact

Email: [info-ua@publ.science](mailto:info-ua@publ.science)
Phone: +38 044 33 44 099

{{% /accordion-section %}}

{{% accordion-section "National and University Library in Zagreb (Croatia)" %}}

National and University Library in Zagreb is the host institution of the Croatian DOI Office. The role of the Office is to act as an intermediary between Croatian publishers of scientific journals and the Crossref DOI registration agency, manage DOI-related administrative operations (e.g. DOI membership, payment of DOI services), provide technical support and organise the promotion of the DOI system.

It intermediates in the assignment of DOIs and Content Registration for titles, volumes, issues and articles of Croatian current online scientific and professional journals whose publishers are interested in membership in the DOI system, or those that are already members of the system but wish to use the advantages available as part of services provided by the Office. The standards required for DOI assignment for journals are that they regularly publish full-text articles in the Croatian online environment, that their bibliographic data and other related details are available according to Crossref recommendations and that they have an ISSN.

Membership through the Croatian DOI Office enables publishers to use a unique prefix, or, in the case of publishers who were part of the DOI system before registering with the Office, to keep using the prefix that they were assigned at their first independent DOI registration request. The Office provides its technical support through a newly developed unique national DOI system (DOI-HR). The National and University Library in Zagreb, as an authorised archive, is responsible for the permanent storage of metadata and objects assigned DOIs. Thus, in the case that an object disappears from the internet, its DOI is redirected to its archived copy in order to ensure the permanency of its link to the object. The expenses for all DOI services and membership during one year for publishers applying for DOIs through the Croatian DOI Office are covered by subsidies provided by the Croatian Ministry of Science and Education.

### Contact

Homepage: [http://www.nsk.hr/doi/](https://doi.nsk.hr/)
E-mail: [doi@nsk.hr](mailto:doi@nsk.hr)  

{{% /accordion-section %}}

{{% accordion-section "NEICON (Russia)" %}}

Not-for-profit Partnership National Electronic Information Consortium (NEICON) was established in 2002 as an independent union of Russian scholar and educational organizations which  produces and use science information. The membership is free. Currently NEICON joins over 1000  scientific and educational organizations from Russia. Today NEICON works on various directions  

- Information support on over 400 international resources and databases
- Education Center on legal and practical matters
- Conferences, seminars, master-classes, etc.
- Represents ORCID, COUNTER, DOAJ, Crossref
- Elpub editorial platform
- Publishing House
- National Open Access Repository Project (NORA)

NECON runs a Russian helpdesk for Crossref queries - this is available even to members not cooperating directly with NEICON. They can be contacted by phone on: +7 (499) 754-99-93, or via email: [crossref@neicon.ru](mailto:crossref@neicon.ru). See: [https://elpub.ru/crossref](https://elpub.ru/crossref)

NEICON cooperates with all types of organizations.

Некоммерческое партнерство Национальный электронно-информационный консорциум (НЭИКОН) был образован в 2002 г. как независимое объединение российских научных и образовательных организаций-потребителей и генераторов научной информации. Членство в Консорциуме для всех типов организаций – бесплатное. На настоящий момент НЭИКОН в своем составе имеет более 1000 научно-исследовательских и образовательных учреждений России. Сегодня НЭИКОН – это:

- Информационное обеспечение организаций – более 400 ресурсов  
- Учебный центр способствует повышению информационной и правовой грамотности  
- Проведение семинаров, мастер классов, конференций по актуальным вопросам издательского и библиотечного сообществ  
- Партнерство с ORCID, COUNTER, DOAJ, Crossref  
- Система комплексной поддержки научного журнала – Elpub  
- Издательство  
- Проект «Национальный агрегатор открытых депозитариев российских университетов» (НОРА)  
- НЭИКОН сотрудничает с организациями любого типа  

НЭИКОН оказывает техническую и методическую поддержку по работе с DOI и другими сервисами Crossref на русском языке. Сервис доступен для всех, а не только для членов Консорциума и пользователей Платформы Elpub. Связаться со Службой можно по телефону +7 (499) 754-99-93, или почтой crossref@neicon.ru.   

### Contact

[Maxim Mitrofanov/Максим Митрофанов](mailto:mmi@neicon.ru)  
Phone: +7(499)754-99-93
[https://elpub.ru/crossref](https://elpub.ru/crossref)  

{{% /accordion-section %}}


{{% accordion-section "Open Science in Ukraine (Ukraine)" %}}  

[Open Science in Ukraine](https://openscience.in.ua) (OSU) is a project for the comprehensive support of scientific journals on the Internet.

What do we usually do:
- Installing, Configuring, and Running Open Journal Systems (OJS)
- Registering content with DOIs
- Advising, providing useful information to authors and editors

Feel free to contact! We are always happy to help!

### Contact

Contact person: Usenko Pavel
Phone: +380667791427
Email: [mail@openscience.in.ua](mailto:mail@openscience.in.ua)  

{{% /accordion-section %}}


{{% accordion-section "OpusJournal (Bosnia & Herzegovina)" %}}
Our team at Opus Journal comprises more than 30 members. The team possesses a formidable depth of experience and an array of skillsets suited ideally to the enrichment of your technological infrastructure of peer review management and article production in order to allow you to engage in data-driven transformative processes of open access scholarly publishing and to fulfill all open access publishing mandates.

Our aim is to solve problems and deliver solutions and we think systems should be as convenient as possible for your users. That is why we tailored OpusJournal specifically for the scholarly publishing industry offering a genuinely innovative way to publish.

Contact person: Milan Vukic
Phone: +38765616339
Email: [info@opusjournal.com](mailto:info@opusjournal.com)
Website: [www.opusjournal.com](https://www.opusjournal.com)

{{% /accordion-section %}}

{{% accordion-section "Publishing House “Helvetica” (Ukraine)" %}}

Publishing House “Helvetica” is a team of highly qualified professionals with many years’ experience in preparing and publishing books, study guides, monographs, scientifical periodicals, etc. Its shared sense of purpose is to provide high-quality services. The team gives individual attention to every author and client, and all orders are carried out at a high professional level.

Publishing House “Helvetica” is a reliable partner for more than 70 state and private universities and specialized institutions of higher education. Besides Ukrainian universities, Publishing House “Helvetica” actively cooperates with 30 universities in neighbouring countries including Hungary, Romania, Slovakia, Poland, Lithuania, Latvia and Estonia.

Today Publishing House “Helvetica” manages more than 100 scientific journals on various subject areas. Services include: working with authors of scientific articles; coordination of scientific review by researchers; proofreading with the involvement of philologists; professional page layout; printing (both monochrome and colour); registration of journals in international scientometric databases; and administration of journal websites; as well as assistance for journal founders in cooperating with the authorities on issues affecting scientific periodicals.

Don’t hesitate to contact us. We are glad to be of service to you!

### Contact

Email: [mailbox@helvetica.com.ua](mailto:mailbox@helvetica.com.ua)
Telephone: +38 097 713 35 50  

{{% /accordion-section %}}

{{% accordion-section "Russian Agency for Digital Standardization (RADS) (Russia)" %}}

Russian Agency for Digital Standardization (RADS) is an organization that aims to standardize a huge number of scientific publications in Russia and the CIS. RADS is a sponsoring member of the Crossref registration agency and a direct member of DataCite agency. RADS is authorized to assign a DOI unique prefix to sponsored organizations, which is an integral part of DOI system.

RADS contributes to the development of the DOI (Digital Object Identifier) ​​standard in Russia and the CIS countries. RADS assigns DOIs to academic digital data to improve its recognition and subsequent citation. The data is also placed in the RADS repository and in a number of world databases supporting the OAI open-access technology.

DOI registration with RADS complies with the Decree of the Government of the Russian Federation of November 16, 2015 No. 1236 (number in the State Register of Computer Programs [2019663477](https://new.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2019663477&TypeFile=html).

Русское агентство цифровой стандартизации (РАЦС) это организация, преследующая цель стандартизировать огромное количество научных публикаций в России и СНГ. РАЦС является членом-спонсором регистрационного агентства Crossref и членом консорциума и регистрационного агентства DataCite. РАЦС уполномочено закреплять за организациями уникальный префикс, неотъемлемую часть DOI, для того, чтобы те могли регистрировать идентификаторы цифрового объекта (DOI).

РАЦС способствует появлению в России и СНГ стандарта DOI (Digital Object Identifier) — идентификатора цифрового объекта. Наша организация присваивает DOI академическим цифровым данным для улучшения их распознания и последующего цитирования. Данные размещаются в [репозитории РАЦС](http://search.rads-doi.org/) и ряде мировых баз данных, поддерживающих технологию открытого доступа OAI.

Регистрация DOI с РАЦС соответствует Постановлению Правительства Российской Федерации от 16 ноября 2015 г. № 1236 «Об установлении запрета на допуск программного обеспечения, происходящего из иностранных государств, для целей осуществления закупок для обеспечения государственных и муниципальных нужд». РАЦС использует собственное, и зарегистрированное в Российской Федерации программное обеспечение (номер в Государственном реестре программ ЭВМ [2019663477](https://new.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2019663477&TypeFile=html).

РАЦС оказывает техническую и методическую поддержку по работе с DOI и другими сервисами Crossref на русском языке. Сервис доступен для всех.

### Contact

Website: http://rads-doi.org/pricing/  
Email: [info@rads-doi.org](mailto:info@rads-doi.org)  
Phone: +7 (343) 286-83-22

{{% /accordion-section %}}

{{% accordion-section "Shapovalov Scientific Publishing OU (Estonia)" %}}

Shapovalov Scientific Publishing OU is an Estonian Scholarly Publisher available internationally. We publish peer-reviewed indexed journals, books and thesis. Our main goal is to spread Open Science through the publication in open access scientific articles. We focus, as publishers, on the following scientific fields: medicine, pharmacy, jurisprudence, engineering, social science, and information technologies.  We also provide Crossref sponsorship for all services, with our gentle assistance to help reach your goals.

We have multilingual team of native speakers of various languages to provide you with the best support possible. We're based in Estonia, but provide our services in Europe and the Caspian region in the following countries: Estonia, Finland, Latvia, Lithuania, Moldova, Ukraine, Kazakhstan, Uzbekistan, Azerbaijan, Turkey, Turkmenistan, Georgia, Tajikistan, and Armenia.

Our lectures and webinars are freely accessible to everyone, where we spread our knowledge of how Crossref can help you in your work.

Feel free to contact us.

### Contact  

Website: [https://crossref.ssp.ee](https://crossref.ssp.ee)  
Email: [crossref@ssp.ee](mailto:crossref@ssp.ee)

{{% /accordion-section %}}

{{% accordion-section "URAN Publishing Service (Ukraine)" %}}

URAN Publishing Service is a Ukrainian technology company, working for sector of universities and research institutions of the Eastern European region. The main activity of the company is development of software for scientific information analytics and dissemination. Acting as a Sponsor of PILA for Ukrainian scholarly publishers, URAN Publishing Service supports DOI regional registry (Ukrainan registry of research outputs) and acts as an authorized intermediary between Ukrainian publishers and the Crossref DOI registry.  

More information about URAN Publishing Service can be found at [http://www.uran.ua/~eng/ps-ltd.htm](http://www.uran.ua/~eng/ps-ltd.htm) (English) or [http://www.uran.ua/~ukr/ps-ltd.htm](http://www.uran.ua/~ukr/ps-ltd.htm) (Ukrainian).  

Our services for publishers include PILA/Crossref membership benefits as a part of membership in the project "Scientific Periodicals of Ukraine" (http://journals.uran.ua/). This project is a national technology platform of Ukrainian scholarly periodicals (journals and proceedings). The resource is being developed on the basis of voluntary mutually beneficial partnership of publishers, academic libraries and information centers of Ukraine.  

URAN Publishing Service provides publishers with full technical assistance and consultation support. Fee categories (tariff packs) depends on amount of published content.

under 250 articles per year: 10 800 UAH  

251-500 articles per year: 18 960 UAH

501-1000 articles per year: 37 020 UAH

1001-2000 articles per year: 72 000 UAH

2001-5000 articles per year: 175 020 UAH

### Contact  

Local support service: [support@journals.uran.ua](mailto:support@journals.uran.ua)

{{% /accordion-section %}}

{{% accordion-section "Yazilim Parki Bilisim Teknolojileri D.O.R.P. Ltd. Sti. (Turkey)" %}}

Yazılım Parkı is a software development company established in 2013, located in Turkey. We provide online scholarly journal publishing and peer-review (manuscript submission) software fully integrated with Crossref services, technical consultation for inclusion scientific indexes, JATS XML preparation and online congress abstract submission and review software solutions. Our clients are including, but not limited to non-profit organizations, academic societies, research institutes and universities. We can provide specific solutions considering modern technologies, user friendly design and accessibility with superior knowledge and experience.

### Contact  

To learn more about our services, please visit [https://yazilimparki.com.tr](https://yazilimparki.com.tr) or contact us at [bilgi@yazilimparki.com.tr](mailto:https://yazilimparki.com.tr).    

{{% /accordion-section %}}

{{% accordion-section "Additional Sponsors (Central/Eastern Europe)" %}}  

- Akdema Bilisim Yayincilik ve Dan. Tic. Ltd. Sti.  (Turkey)  
- Albanian Business Partner  (Albania)
- Association For Science  (Georgia)  
- Association of Lithuanian Serials  (Lithuania)   
- BAYT Bilimsel Arastirmalar Basin Yayin Ltd.  (Turkey)   
- Digital Publishing Service LLC  (Ukraine)   
- ECO-Vector LLC  (Russia)  
- Electronic Scientific Library  (Russia)  
- European Scientific Platform  (Ukraine)  
- Institute of Knowledge Management (North Macedonia)  
- Institute of Metallurgy and Ore Benefication  (Kazakhstan)  
- Kama Research Centre  (Russia)  
- Kvantor Plus S.R.L.  (Republic of Moldova)  
- Laboratory of Intellect, Ltd  (Belarus)  
- LIBCOM Piotr Karwasinski  (Poland)  
- Library and Information Centre, Hungarian Academy of Sciences  (Hungary)  
- LLC Integration Education and Science  (Russia)  
- LLC Logic+  (Russia)  
- Lucian Blaga Central University Library of Cluj  (Romania)   
- National Academy of Sciences of Ukraine  (Co. LTD Ukrinformnauka) (Ukraine)
- National and University Library - St. Klement of Ohrid - Skopje  (North - Macedonia)  
- National and University Library of Bosnia and Herzegovina  (Bosnia and Herzegovina)  
- National Library of Armenia  (Armenia)  
- Online Bilgi  (Turkey)  
- Open Access Scholarly Publishers Association (OASPA)  (based in The Netherlands but work with organizations globally)
- Public Knowledge Project (PKP)  (based in Canada but work with organizations globally)  
- Scientific World /SWorld/, Ltd.  (Russia)  
- Slovak Centre of Scientific and Technical Information - SCSTI  (Slovakia)  
- State Scientific Institution - Ukrainian Institute of Scientific and - Technical (Expertise and Info)  (Ukraine)   
- Tubitak Ulakbim DergiPark  (Turkey)  
- Turkish Primary Education Association  (Turkey)  
- Ulyanovsk State Pedagogical University  (Russia)  
- University of Belgrade Faculty of Law  (Serbia)  


{{% /accordion-section %}}
{{% /accordion %}}

---

### Latin America and Caribbean

{{% accordion %}}

{{% accordion-section "Acesso Academico (Brazil)" %}}
ACESSO ACADÊMICO serves associations, foundations, publishers, universities and public administrations, with or without profit purposes, that use or want to use a software platform to manage their publications or magazines, such as Open Journal Systems (OJS). We offer all the services necessary for the setup, hosting, maintenance and dissemination of scientific journal content.

As Crossref sponsors we support publishers for the acquisition of the DOI prefix, content registration, and all other Crossref-related services (Similarity Check, Cited-by, etc.).

Moreover, we host online scientific events including conferences, symposiums, seminars and workshops to facilitate the evaluation of new ideas and new research in an innovative, resourceful and creative setting.

### Contact
Contact: [contato@acessoacademico.com.br](mailto:contato@acessoacademico.com.br)

{{% /accordion-section %}}

{{% accordion-section "Biteca SAS (Columbia)" %}}
We are PKP and Crossref sponsors. We offer editorial services and technical support in Spanish for Open Journal Systems, Open Monograph Press, Open Conference Systems and Open Preprints Server. We have more than 15 years of experience providing services to scientific journals. We are experts in XML-JATS markup (For Scielo and PubMed Central), Crossref technical support for the management of DOI (Digital Object Identifier), Reference Linking, Cited by and Crossmark. We also develop custom plugins for OJS and mobile APPs for iOS and Android for journals.   

Somos patrocinadores en PKP y Crossref. Ofrecemos servicio técnico especializado en español para Open Journal Systems, Open Monograph Press, Open Conference Systems y Open Preprints Server. Contamos con más de 15 años de experiencia prestando servicios a revistas científicas. Somos expertos en marcación XML-JATS (Para Scielo y PubMed Central) , soporte técnico Crossref para la gestión de DOI (Digital Object Identifier), Reference Linking, Cited by y Crossmark. También desarrollamos plugins personalizados para OJS y aplicaciones móviles (APPs) para iOS y Android para revistas.  

Contact:  [Carlos Bermúdez](mailto:info@biteca.com)  
https://www.biteca.com  
Avenida Caracas # 34 – 86 oficinas 401-402  
Bogotá – Colombia – Sur América  
Colombia  

{{% /accordion-section %}}

{{% accordion-section "GeniusDesign Marketing Digital e Editora (Brazil)" %}}

GeniusDesign provides hosting, OJS installation/configuration, technical support, and publishing services to independent publishers, universities, non-profit and for-profit institutions. We also provide digital marketing services to increase traffic and visibility to scientific journals.

Contact: [contato@geniusdesign.com.br](mailto:contato@geniusdesign.com.br)  
[telegram.me/geniusdesignbrasil](https://telegram.me/geniusdesignbrasil)

{{% /accordion-section %}}

{{% accordion-section "Journals & Authors SAS (Colombia)" %}}

Somos una empresa que quiere ayudar al aumento de la calidad y mejoramiento de la difusión de la literatura científica. Nuestros esfuerzos se concentran en brindar a los editores de revistas y autores de manuscritos, asesorías y servicios calificados para la redacción, edición, publicación, difusión y posicionamiento de los textos científicos. Nos distinguimos por la calidad y puntualidad de nuestros servicios, el profesionalismo, experiencia y buen trato. Nuestro equipo está conformado por profesionales de las áreas de la ingeniería de sistemas, traducción, salud pública, ciencias sociales y humanas, publicidad, bibliotecología, diseño gráfico y web y la administración financiera.

Propendemos por una difusión del conocimiento en el marco de buenas prácticas editoriales que eviten las violaciones éticas y favorezcan el crecimiento razonable de la literatura especializada.

Estamos siempre atentos para conocer las necesidades de nuestros clientes y dispuestos a construir relaciones duraderas basadas en la confianza que nos permitan poner en práctica las estrategias más eficaces e innovadoras para su revista o artículo científico.
Nuestros mayores clientes son las Universidades por trabajar con publicaciones científicas, sin embargo, no tenemos exclusiones en el momento.

Our company aims to contribute to enhancing the quality and dissemination of scientific literature. We are committed to provide researchers and journal editors with qualified services and consulting for drafting, editing, publishing, disseminating and positioning their scientific publications. We stand by the quality and timeliness of our services, professionalism, expertise and friendly treatment. Our team is comprised of professionals in the fields of systems engineering, translation, public health, social and human sciences, advertising, library and information science, graphic and web design and financial administration.

We promote access to knowledge within the framework of good editorial practices that prevent ethical violations and encourage the reasonable growth of scientific literature.
We are always responsive to the needs of our customers and are willing to build lasting relationships with them based on trust, which allow us to implement the most effective and innovative strategies for their journal or papers.

Our biggest clients are Universities for working with scientific publications, however, we do not have exclusions at the moment.

### Contact

Operamos desde Medellín, Colombia. Si usted desea conocer más nuestros servicios por favor contáctenos. Escríbanos a [info@jasolutions.com.co](mailto:info@jasolutions.com.co) y con gusto le daremos toda la información que necesite o visítenos en [www.jasolutions.com.co](http://www.jasolutions.com.co).

We are headquartered in Medellin, Colombia. If you wish to quote our services, please write to us to [info@jasolutions.com.co](mailto:info@jasolutions.com.co) or visit [www.jasolutions.com.co](http://www.jasolutions.com.co).  

{{% /accordion-section %}}

{{% accordion-section "High Rate Consulting" %}}

High Rate Consulting is a company committed to the dissemination of scientific knowledge. Our services focus on consulting for the creation and maintenance of journal on the OJS platform, edition of individual and collective books, as well as training associated with these processes. We also provide complementary services in terms of web design and social networks. We distinguish ourselves by having highly qualified personnel in each of these tasks. We support good editorial practices in favor of the growth of open access scientific publications, compliance with quality standards for indexing and the promotion of the digital image of both organizations and individuals.  

We work to develop close processes with our clients in order to provide them with the best services and permanent support in the processes. We are currently focused on supporting emerging scientific journals initiatives that require registration with Crossref to promote open access dissemination, especially those located in Latin America and Africa.  

The journals that are part of Crossref through us receive ongoing information and advice on improving processes to achieve the objectives of indexing and international positioning, as well as advice on the use of tools such as Similarity Check, Crossmark, Cited-by and Reference Linking, to name a few.  

High Rate Consulting es una empresa comprometida con la difusión del conocimiento científico. Nuestros servicios se centran en asesorías para la creación y mantenimiento de revistas en la plataforma OJS, edición de libros individuales y colectivos, así como, adiestramiento asociado a dichos procesos. También prestamos servicios complementarios en cuanto a diseño web y redes sociales. Nos distinguimos por contar con personal altamente calificado en cada una de estas labores. Apoyamos las buenas prácticas editoriales en favor del crecimiento de las publicaciones científicas en acceso abierto, cumplimiento de los estándares de calidad para la indexación y la promoción de la imagen digital tanto de organizaciones como de personas.  

Trabajamos por desarrollar procesos de cercanía con nuestros clientes en función de brindarle los mejores servicios y acompañamiento permanente en los procesos. Actualmente nos enfocamos en apoyar iniciativas de revistas emergentes que requieren la inscripción ante Crossref para impulsar la difusión en acceso abierto, sobre todo aquellas ubicadas en Latinoamérica y África.  

Las revistas que forman parte de Crossref a través de nosotros reciben información y asesoría permanente sobre el mejoramiento de los procesos para el logro de los objetivos de indexación y posicionamiento internacional, además de asesoría en el uso de herramientas como Similarity Check, Crossmark, Cited-by y Reference Linking, por nombrar algunas.  

### Contact  

Contact/contacto: [https://www.highrateco.com/](https://www.highrateco.com/)  
Contact person/persona contacto: Wileidys Artigas  
Email/correo: [wile@highrateco.com](mailto:wile@highrateco.com)    

{{% /accordion-section %}}

{{% accordion-section "Hipertexto-Netizen" %}}

We are Hipertexto – Netizen, a specialized company that deploys tech solutions, media, platforms, and technologies that help our publishers to strengthen the processes related to the generation, transformation, distribution, and delivery of content. We aim to spread knowledge through technology. We have different operations, including specialized metadata systems and platforms, focusing on academic, universities, and STM publishers. We serve more than 12 countries in LatAm, Spain, Brazil, and Portugal.

Somos Hipertexto - Netizen, una empresa especializada que despliega soluciones tecnológicas, medios, plataformas y tecnologías que ayudan a nuestros editores a fortalecer los procesos relacionados con la generación, transformación, distribución y entrega de contenidos. Nuestro objetivo es difundir el conocimiento a través de la tecnología. Contamos con diferentes operaciones, incluyendo plataformas y sistemas de metadatos especializados, con foco en editoriales académicas, universitarias y STM. Atendemos a más de 12 países en LatAm, España, Brasil y Portugal.

### Contact  

Headquarters:  

COLOMBIA
CIO - Centro Internacional de Operaciones  
Calle 32 A No. 19 - 24 Barrio Teusaquillo  
Bogotá - Colombia
Tel.: +57 601 643 4389  

MÉXICO
Ciudad de México - México  
Tlacotalpan #13 Int. 2  
Col. Roma Sur, Del. Cuauhtémoc, 06760  
Tel.: +52 (55) 7827 7068  
Celular: + 52 1 (55) 3668 3358  

Website: [https://www.hipertexto.com.co/](https://www.hipertexto.com.co)  
Email: Dirección Científica SIMEH [sac@hipertexto.com.co](mailto:sac@hipertexto.com.co)

{{% /accordion-section %}}

{{% accordion-section "InfoEduTec" %}}

Infoedutec is focusing on supporting small publishers, journal editors, research institutions and universities to improve the research and publishing management, to increase the quality and performance of scholarly journals, and monitoring the visibility or scientific impact through use of open source software and best practices in information and knowledge management.

Infoedutec is a legal company with operations in Peru under the trade name INFOEDUTEC.COM and registered name INFOEDUTEC - INFORMATION EDUCATION TECHNOLOGIES AND PUBLICATION SERVICES E.I.R.L. The main economic activities of the company include - but are not limited to - the following:
- Information technology and computer service activities
- Professional, scientific and technical activities
- Publishing of newspapers, magazines and other periodical

We offer services and products related to IT TECHNOLOGIES AND SOLUTIONS: digital repositories, e-journals, websites, virtual campus; library systems, and CRIS systems; SCIENTIFIC PUBLICATION: thesis revision, research advisoring and article copy-editing; EDUCATION AND TRAINING: virtual courses, on-demand workshops and customized in-house programs; INFORMATION MANAGEMENT: journal indexing, XML JATS formatting; bibliometric reports and Crossref services (membership, DOI registration, etc.).

Infoedutec apoya a pequeños editores, editores de revistas, instituciones de investigación y universidades públicas o privadas. Se centra en contribuir a mejorar la gestión de la investigación y la publicación, aumentar la calidad y el rendimiento de las revistas académicas y monitorear la visibilidad e impacto científico mediante el uso de software de código abierto y las mejores prácticas de gestión de la información y del conocimiento.

Infoedutec es un empresa legalmente constituida con operaciones en todo el territorio peruano bajo la denominación comercial INFOEDUTEC.COM y denominación registral INFOEDUTEC – INFORMACIÓN EDUCACIÓN TECNOLOGÍAS Y SERVICIOS DE PUBLICACIÓN E.I.R.L. Entre las principales actividades económicas de la empresa, pero no limitadas a, se encuentran las siguientes:
- Actividades de tecnología de la información y de servicios informáticos
- Actividades profesionales, científicas y técnicas
- Edición de periódicos, revistas y otras publicaciones periódicas

Ofrecemos servicios y productos relacionados con TECNOLOGÍAS Y SOLUCIONES TI: repositorios digitales, revistas electrónicas, páginas web, campus virtuales; gestión de bibliotecas y sistemas CRIS; PUBLICACIÓN CIENTÍFICA: asesoría de tesis, investigación y revisión de artículos; EDUCACIÓN Y CAPACITACIÓN: cursos virtuales, talleres a demanda y programas in-house a medida; GESTIÓN DE INFORMACIÓN: indización de revistas, marcación XML JATS; informes bibliométricos y trámites Crossref (membresía, registro DOI, etc.).

### Contact  

Website: [https://www.infoedutec.com/](https://www.infoedutec.com)   

Please email us at the following addresses:  

Contact information: [contacto@infoedutec.com](mailto:contacto@infoedutec.com)  
Administration and payments: [administracion@infoedutec.com](mailto:administracion@infoedutec.com)  
Support and technical assistance: [sistemas@infoedutec.com](mailto:sistemas@infoedutec.com)  

{{% /accordion-section %}}

{{% accordion-section "OJSBR (Brazil)" %}}

### OJSBR - Brazil

OJSBR serves independent publishers, non-profit and for-profit institutions. We are specialists in the OJS platform and perform all related technological services. We also provide training for editors and institutions. As Crossref sponsors, we serve for-profit publishers for the acquisition of the DOI prefix, content registration, and all other Crossref-related services.

### Contact

[support@ojsbr.com](mailto:support@ojsbr.com)

{{% /accordion-section %}}

{{% accordion-section "Open Journal Systems Chile (Chile)" %}}

### Open Journal Systems Chile - Chile  

Open Journal Systems Chile es una empresa creada para brindar apoyo al trabajo editorial de los editores en Chile.

Open Journal Systems Chile trabaja exclusivamente con Instituciones y Universidades que utilizan una plataforma Online para la difusión de sus contenidos científicos.
Open Journal Systems ofrece todos los servicios que son necesarios para la instalación, mantenimiento y difusión del contenido de las revistas científicas: hosting, Crossref (DOI, Similarity Check, Reference Linking, Cited-by, etc.), indexación, instalación y personalización del sistema OJS, etc.

Con qué tipo de organización acepta / trabaja como Miembro Patrocinado: Región geográfica específica: Chile

### Contact  

Dirección de contacto para consultas: [contacto@openjournalsystems.cl](mailto:contact@openjournalsystems.cl)

{{% /accordion-section %}}

{{% accordion-section "Open Journal Solutions" %}}

### Open Journal Solutions - Brazil  

Open Journal Solutions provides hosting services, technical support, editorial support, and training for journals in OJS.

### Contact  

Telephone/Telegram/WhatsApp: +55 11 98959-9988  
E-mail: [contato@openjournalsolutions.com.br](mailto:contato@openjournalsolutions.com.br)

{{% /accordion-section %}}

{{% accordion-section "Universidad Mayor de San Andres" %}}

### Universidad Mayor de San Andres - Bolivia

Universidad Mayor de San Andrés (Bolivia), brinda servicio a instituciones públicas y privadas, con o sin fines de lucro. Ofrecemos todos los servicios necesarios para la instalación, alojamiento, mantenimiento y difusión de contenidos de revistas científicas. Apoyamos a los editores para la adquisición de DOI, registro de contenido y todos los demás servicios relacionados con Crossref. Asimismo, ofrecemos apoyo en buenas prácticas de publicación.

### Contact

Sitio web: [https://iiaren.agro.umsa.bo/](https://iiaren.agro.umsa.bo)  
Correo electrónico: [iiaren.agronomia@umsa.bo](mailto:iiaren.agronomia@umsa.bo)

{{% /accordion-section %}}

{{% accordion-section "Additional Sponsors (Latin America/Caribbean)" %}}  

- Galoa (Brazil)  
- Asociacion Uruguaya de Revistas Academicas (AURA) (Uruguay)  
- Associacao Brasileira de Editores Cientificos do Brasil (ABEC) (Brazil)  
- Dossier Soluciones S.A.S (Colombia)  
- Grupo Anltyk S.A. de C.V. (Mexico  
- Infotegra S.A.S. (Colombia)  
- Lepidus Tecnologia	(Brazil)  
- Meta-datos (Mexico)  
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)
- OpenCiencia (Guatemala)  
- Paideia Studio (Argentina)
- Public Knowledge Project (PKP) (based in Canada but work with organizations globally)   
- Scientificomm LLC (Mexico)  
- Telecomexpert (Ecuador)
- Terceiro Andar International (Brazil)  

{{% /accordion-section %}}
{{% /accordion %}}

---

### North Africa and Middle East

{{% accordion %}}
{{% accordion-section "AJPS Publications (Algeria)" %}}

AJPS Publications offers the following services:

- facilitate content registration with Crossref on behalf of sponsored members
- ojs,omp web hosting for journals and books
- indexing services and scientific IDs for researchers and journals

We work with publishers from Algeria and all African and Arabic states who are using OJS for their journals, OMP for their books, or Eprints for their repositories. However, any similar platforms are also welcome.

We support the following content types:

- journals and journal articles
- conference papers and proceedings, theses and dissertations;
- books, book chapters and reference works;
- datasets;
- grants;
- peer reviews, preprints, reports and working papers.

We can provide support in English, French and Arabic.

### Contact  
Website: [https://www.polimpact.com/](https://www.polimpact.com/)
You can contact us by email at [editor@maspolitiques.com](mailto:editor@maspolitiques.com) and by telephone at +21 3794222878.

{{% /accordion-section %}}
{{% accordion-section "Additional Sponsors (North Africa/Middle East)" %}}  

- Dar Almandumah Inc. (Saudi Arabia)  
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)
- Public Knowledge Project (PKP) (based in Canada but work with organizations globally)  

{{% /accordion-section %}}
{{% /accordion %}}

---

### Sub-Saharan Africa

{{% accordion %}}
{{% accordion-section "Additional Sponsors (Sub-Saharan Africa)" %}}  

- Sabinet Online Ltd (South Africa)
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)
- Public Knowledge Project (PKP) (based in Canada but work with organizations globally)   

{{% /accordion-section %}}
{{% /accordion %}}


### US and Canada
{{% accordion %}}
{{% accordion-section "Advanced Multiple Inc. (Canada)" %}}  

Advanced Multiple Incorporation is a dynamic and versatile organization having multiple programs and activities. We help to obtain digital identifiers and copyright confirmation, confirming
ownership of scientific entities, intellectual property and innovative materials.
Moreover, we initiate scientific, academic professional programs and events including conferences, symposia, seminars and workshops to facilitate the evaluation of new ideas and new research in an
innovative, resourceful and creative setting. This helps to promote vigorous dialogue between industry and professionals. Our vision is to profess knowledge and disseminate research information for the scientific community by providing an advanced platform. We receive skilled opinions from researchers, key leaders and influential individuals able to guide others.
Thus, we are at the frontline of distribution of knowledge through enlightening, informative and high-quality events, specializing in providing comprehensive information and updates through conferences, journals and exhibitions. We cover all major areas including health, medicine, humanities, business, engineering, physics, geology and social sciences.

### Contact
Telephone: +1(647)526-0885
Email: [info@advancedmultiple.ca](mailto:info@advancedmultiple.ca); [office@advancedmultiple.ca](mailto:office@advancedmultiple.ca)
Website: [http://www.advancedmultiple.ca](http://www.advancedmultiple.ca); [http://www.advancedmultiple.com](http://www.advancedmultiple.com)

{{% /accordion-section %}}

{{% accordion-section "Additional Sponsors (US/Canada)" %}}  


- Altum (USA)  
- Erudit/Coalition Publica (Canada)  
- Longleaf Services Inc (USA)  
- Niva Inc (Canada)  
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)
- Public Knowledge Project (PKP) (Canada)  
- University of Toronto (Canada)  

{{% /accordion-section %}}
{{% /accordion %}}

---

### Western Europe

{{% accordion %}}

{{% accordion-section "Associations of Lithuanian Serials (Lithuania)" %}}  

The mission of the Association is to use its best endeavours to unite Lithuanian research journal publishers and to promote the development of innovative technologies in publishing to speed up and facilitate scholarly research.

The purposes and tasks of the Association’s activities are to:
- Promote innovations in scholarly journal publishing in Lithuania.
- Collaborate with international organisations and initiatives to promote Lithuanian journals in the academic community worldwide.
- Develop and establish ethical principles in scholarly journal publishing.
- Take care of consultancy and training for the members of the Association.

Members: [https://serials.lt/lithuanian-journal-publishers/](https://serials.lt/lithuanian-journal-publishers/)  

### Contact
Website: [https://serials.lt/](https://serials.lt/)  
For more information: [info@serials.lt](mailto:info@serials.lt)  

{{% /accordion-section %}}

{{% accordion-section "Ciencia Avui (Spain)" %}}

Ciència Avui es una empresa catalana que trabaja para facilitar el registro de metadatos mediante el OJS o la interfaz estándar Crossref. El registro de metadatos permite aumentar la visibilidad del artículo y el número de referencias, obtener subvenciones y aumentar el prestigio de la publicación. Colaboramos con los principales institutos de investigación, universidades y editoriales en España y Europa. También, ofrecemos los servicios relacionados con la plataforma OJS y bases de datos científicas (WoS, Scopus).

### Contact

Para saber más, por favor visite nuestra página web [https://ciencia-avui.org/contactes/](https://ciencia-avui.org/) o escriba directamente al correo electrónico [hola@ciencia-avui.org](mailto:hola@ciencia-avui.org).  

{{% /accordion-section %}}

{{% accordion-section "Cultural Hosting (Spain)" %}}

Cultural Hosting offers web hosting services for cultural heritage organizations, including scientific and academic rights. To offer a valuable service, we have become a Crossref Sponsor to help small organizations with DOI management.

The company works with associations, foundations, publishers, universities and public administrations that use or want to use a software platform to manage their publications or magazines, such as Open Journal Systems (OJS).

As a Sponsor, we only provide services to entities based in the European Union or in any country in the Americas, in English or Spanish (Portuguese available, but less fluent).

Cultural Hosting ofrece servicios de alojamiento web para instituciones culturales, incluyendo organizaciones académicas y científicas. Para ofrecer un servicio más completo, hemos alcanzado un acuerdo para ser una Organización Patrocinadora de Crossref y así poder ayudar a las organizaciones más pequeñas con la gestión DOI.  

La empresa trabaja con asociaciones, fundaciones, editoriales, universidades y administraciones públicas que utilizan o desean utilizar una plataforma de software para administrar sus publicaciones o revistas, como OJS (OpenJournal Systems).

Como Organización Patrocinadora, solo brindamos los servicios a entidades con sede en la Unión Europea o en cualquier país de América, en inglés o español (portugués disponible, pero con menos fluidez).

### Contact

If you want more information about our services, please write to [info@culturalhosting.com](mailto:info@culturalhosting.com) or visit [https://culturalhosting.com/](https://culturalhosting.com/)   

Si desea información sobre los servicios, escriba a [info@culturalhosting.com](mailto:info@culturalhosting.com) o visite [https://culturalhosting.com](https://culturalhosting.com)  

{{% /accordion-section %}}

{{% accordion-section "Open Academia (Sweden)" %}}

Open Academia is partnering with Public Knowledge Project (PKP) to provide a complete production and publication service that includes processing manuscripts through copyediting, typesetting, proofreading (liaising with authors), online publication, indexing and archiving. The Open Academia staff also serves as the peer review support contact for all system users: editors, section editors, authors, reviewers and readers.

Basically, we offer support on all core elements of the publishing process. We have clients from all around the globe across all academic disciplines, and support English and Scandinavian languages.

Open Academia also offers individual services ranging from editorial support, production, OJS 3 website design and maintenance. For OJS journals that have recently upgraded to OJS 3, Open Academia can provide training sessions for editorial staff as well as website makeovers.

We form partnerships with academic societies and we also support smaller presses looking to publish open access journals.

Read more at [https://openacademia.net](https://openacademia.net).  
Must members use a specific platform? Open Journal Systems (all versions)

Organization type (gov’t, university, not for profit) – all of the above!
We typically work with academic societies and universities but welcome work with gov’t and non-profit organizations as well.

Content type – Open Access scholarly journals spanning all sizes and academic disciplines.

### Contact

[info@openacademia.net](mailto:info@openacademia.net)  

{{% /accordion-section %}}

{{% accordion-section "Stichting OpenAccess (Netherlands)" %}}

Stichting OpenAccess (SOAP) offers affordable and fast solutions for the dissemination of research content in the arts, architecture, built environment and design domain. We host open access e-print, pre-print, conference proceedings, book and journal archives.

The definition of 'research content' includes, ‘art’, 'design', 'engineering' or 'planning' as long as that design, engineering or planning contributes to the body of knowledge in their respective fields.

The open access label we use for all submitted, accepted and published publications that we handle is CC-BY-4.0. We don't deal with delayed or limited open access content.

SOAP is also a proud Crossref sponsor. Sponsors provide a solution for small organizations who want to register metadata for their research and participate in Crossref but that are not able to join directly due to financial, administrative, or technical barriers.

SOAP is based in The Netherlands but works internationally. For more information go to [https://www.openaccess.ac](https://www.openaccess.ac).

{{% /accordion-section %}}


{{% accordion-section "Additional Sponsors (Western Europe)" %}}

- Factoria Nemesys Dospuntocero SL (Spain)  
- Gudinfo SL (Spain)  
- INASP (United Kingdom)  
- Lund University Library (Sweden)  
- National Library of Sweden (Sweden)  
- Open Access Scholarly Publishers Association (OASPA) (based in The Netherlands but work with organizations globally)  
- OpenJournals (The Netherlands)  
- Public Knowledge Project (PKP) (based in Canada but work with organizations globally)  
- Prepress Projects, Ltd. (United Kingdom)   
- The Federation of Finnish Learned Societies (Finland)   
- University of Zurich (Switzerland)  
- Xercode Media Software, S.L. (Spain)  

{{% /accordion-section %}}

{{% /accordion %}}


---
For more information please contact our [membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152).
