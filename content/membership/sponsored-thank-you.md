+++
title = "Thank you for your application"
date = "2019-03-28"
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "sponsored-thank-you"
+++

## Thanks for applying - you're on your way!

### Here's what happens next:

Thanks for submitting your application to become a Crossref Member via a Sponsor. Our team will now check out your details, set you up and send your credentials over to your Sponsor. If you have any questions in the meantime, do contact your Sponsor.

Please note, if you’re already a member of Crossref moving to a Sponsor, we’ll send you any outstanding invoices you may have. These will need to be paid before we can transfer you to your new sponsor. And if you’re already a Sponsored Member of Crossref with another Sponsor, there may be a delay while we ask for permission from your previous Sponsor to move you.


### Please make sure you have read and understood:

* The [member terms](/membership/terms) you're agreeing to.


We're looking forward to your participation in our community!

---
