---
title: 'Hello, meet Event Data Version 1, and new Product Manager'
author: Christine Buske
draft: false
authors:
  - Christine Buske
date: 2018-03-29
categories:
  - Citation
  - Collaboration
  - Data
  - Event Data
  - Identifiers
archives:
  - 2018

---

I joined Crossref only a few weeks ago, and have happily thrown myself into the world of Event Data as the service’s new product manager. In my first week, a lot of time was spent discussing the ins and outs of Event Data. This learning process made me very much feel like you might when you’ve just bought a house, and you’re studying the blueprints while also planning the house-warming party.

<!--more-->

If Event Data is like a house, it’s been built and we’ve recently been putting on a last coat of paint. We’re very happy to announce version 1 of the API today. This is bringing us closer to the launch (house warming party), which will officially present Event Data to the world. Further to that analogy, while I bought into the house, I wasn’t around to see it being built. That’s both incredibly exciting and a little daunting.

Version 1 contains fixes for some challenges we came up against. Like scalability, data modeling for Wikipedia, and polishing. Version 1 is a new release of the data, but it is the same data set you already know and love. It should solve some of the recent stability issues, for which we apologize.

Moving forward, we expect the data model in V1 to persist and are not planning to make further large scale, fundamental changes to the Event Data API. As such, the version 1 release of the API is exceptional and a big step forward. It is important that we address these fixes before we go into production as it affects everyone who uses the service.

## Same Event Data, new address


In setting up for the upcoming production service rollout, we have updated the Event Data API domain so that it is in line with Crossref’s suite of APIs. The Query API can now be found at a new URL. Here is an example query: https://api.eventdata.crossref.org/v1/events?rows=1

We have also simplified the standard query parameters in favor of a cleaner filter syntax.

Lastly, we have added a new “Mailto” parameter, [just like in our REST API](https://github.com/CrossRef/rest-api-doc#etiquette). It is encouraged but optional, so you are not obliged to supply it. We'll only use it to contact you if there's a problem.

## Changes to the Wikipedia data structure


We’ve done a lot of work to use the [canonical URLs](https://www.eventdata.crossref.org/guide/data/ids-and-urls/) for web pages to represent content as consistently as possible. This has entailed updating previously collected Events across data sources. As such, we’ve updated our Wikipedia data model to align with this. Because this update has impacted every Wikipedia Event in the system, we recommend those who have used or saved existing data from the deprecated Query API version to pull a new copy of the data. Read more about [the rationale for changing the Wikipedia data model](https://groups.google.com/forum/#!topic/crossref-event-data-beta-testers/-RAzhr7SIHY).

## Updated data


This then brings me to how we now handle updated data. Sometimes we edit Events to add new features, or we may edit Events if there is an issue processing and/or representing the data when we provision it to the community. And sometimes we must remove Events  to comply with a particular data source’s terms and conditions (ex: deleted Tweets). You can read about how updates work in [the user guide](https://www.eventdata.crossref.org/guide/data/updates/).

To make life easier moving forward, we’ve split updated Events into two API endpoints.
If you are already using Event Data, you will need to make some small updates to your client(s) to align with this. The new endpoints are further described [in the documentation](https://www.eventdata.crossref.org/guide/service/query-api/).

## Event Data beta group


With the version 1 release we are making solid progress towards an official launch (the house-warming party!), we are quite excited to [hear how you are using Event Data](mailto:eventdata@crossref.org). Please consider [joining our beta group] (https://groups.google.com/forum/#!forum/crossref-event-data-beta-testers), if you are using the Event Data API or want to hear about updates.

This is also where you can [read about these updates in more detail](https://groups.google.com/forum/#!topic/crossref-event-data-beta-testers/2fak5d1UMag).

For more information and to get started with Crossref Event Data, please refer to [the user guide](https://www.eventdata.crossref.org/guide/index.html).

I am looking forward to seeing how Event Data is being used, and working with the community to continuously improve what we can offer through this service. Feedback is always welcome, feel free to get in touch with me at eventdata@crossref.org.
