---
title: 'Meet the members, Part 3 (with INASP)'
author: Christine Cormack Wood
draft: false
authors:
  - Christine Cormack Wood
  - Sioux Cumming
date: 2018-06-20
categories:
  - Sponsors
  - Meet The Members
  - Community
  - Perspectives
archives:
  - 2018

---
Next in our [Meet the members](/categories/meet-the-members/) blog series is INASP, who isn’t a direct member, but acts as a [Sponsor](/community/sponsors) for hundreds of members. Sioux Cumming, Programme Specialist at [INASP](https://www.inasp.info/home) tells us a bit about the work they’re doing, how they use Crossref and what the future plans for INASP are.

<!--more-->

{{% imagewrap right %}} <img src="/images/blog/INASP.jpg" alt=“INASP logo" height="150px" width="250px" class="img-responsive" /> {{% /imagewrap %}}

### Can you tell us a little bit about INASP?

[INASP](https://www.inasp.info/home) is an international development organization working with a global network of partners in Africa, Latin America and Asia. We have a vision of research and knowledge at the heart of development, so are working to support individuals and institutions to produce, share and use research and knowledge, which can transform lives.

Our work includes strengthening research communication, which we do via AuthorAID (supporting researchers, especially early-career researchers, in getting their research published); improving information access (supporting library consortia with access to international journals and other online resources); supporting evidence use in policy making; working with higher-education institutions to improve critical thinking skills; improving gender equity in research systems; and my area, which I’ll talk more about below, supporting academic publishing in the Global South.

INASP’s approaches are based on the core pillars of capacity development, convening, influencing and working in partnership. INASP promotes equity by actively addressing the needs of both men and women across all our work and addressing issues of power within the research and knowledge system.

### What’s your role within INASP?

I’m a Programme Specialist and since I started at INASP 15 years ago I’ve been responsible for our [academic publishing work](https://web.archive.org/web/20220629190912/https://www.inasp.info/theme/academic-publishing). This work supports increased visibility, accessibility and quality of peer-reviewed journals published in developing countries so that the research outputs that are produced in these countries can be found, shared and used more effectively.

We recognize two big challenges for Southern journals in playing their part in global research systems. The first is awareness of Southern journals, many of which were until recently only available in print. Supporting editors and national organizations to put their journals online on central platforms (the Journals Online platforms) has helped increase their visibility.

We have also provided support to the local management teams in communicating about the platforms, the journals and the research they publish, and we recently published a Handbook for Journal Editors - [www.inasp.info/editorshandbook](https://www.inasp.info/editorshandbook). This is intended to be a free resource for editors worldwide that can be used as a stand-alone handbook or as an accompaniment to the journal quality online course that we are currently developing.

The second challenge is supporting publishing quality and enabling Southern journals to demonstrate their quality so they will be regarded as credible. In the early days, this, for me, was largely about providing training and mentoring for journal editors and Journals Online platform managers about standard publishing practices.

More recently, as local handover progressed, our role shifted towards helping journals to demonstrate their credibility. Last September INASP and African Journals Online launched our Journal Publishing Practices and Standards (JPPS) [framework](https://www.journalquality.info/en/) for assessing the quality of Southern publishing processes. This has been really well received by the international publishing sector and by the journal editors we work with.

### Tell us a bit about who you support, and how you support them

We support others communicating their research and finding out about the research of others.

> The five Journals Online platforms that were handed over to local management at the end of March collectively host 397 journals from Bangladesh, El Salvador, Honduras, Mongolia, Nepal, Nicaragua and Sri Lanka.

These platforms help the research from these countries to become even more integrated in the global research community. Some fascinating and valuable research is published in the journals on these platforms. You can see some [examples](https://www.inasp.info/publications/helping-southern-research-reach-global-audience) of this research in this article about a small piece of work we did with these platforms to commission and disseminate press releases of some of the research.

### What’s your participation level with Crossref?

INASP has been a Crossref Sponsor for the Journals Online platforms since 2008 and all articles on the sites have DOIs assigned to them (approximately 50,000 articles). All the in-country training sessions for journal editors publishing via the JOL platforms have included sessions to explain how DOIs work and why they are important. We have also trained editors on how to find and include the DOIs for the references of their articles. More recently, in 2015, we provided access to the Crossref Similarity Check service to editors, which enabled them to improve the quality of their submissions by identifying instances of plagiarism before the articles were published.

### What trends are you seeing in your part of the scholarly communications community?

Demonstrating credibility of journals is an important part of journal publishing today. There are so many journals worldwide and it is a tough challenge for authors and readers to navigate this sector – a challenge that we often see through the discussions in our AuthorAID network. But it is important that researchers don’t simply turn to the handful of well-known publishers in the Global North that have dominated scholarly discourse to date.

To really tackle global issues and increase equality in global research we need to work towards levelling the playing field and including all voices – and this challenge needs to be embraced across the global research and knowledge system. We have seen encouraging signs over the past couple of years of magazines, blogs, conference organizers and industry groups in the Global North approaching us to help bring in more global perspectives to scholarly discussions. However, there is plenty more to be done and we are particularly focusing on equity in our new areas of work.

### How would you describe the value of being a Crossref Sponsor?

> Collaboration with Crossref over the past few years has been one of a number of ways that we have been able to connect small, scholar-led titles in the Global South with the latest global standards and approaches in scholarly publishing. This is important as it all helps to level the playing field.

Including DOIs in papers is one of the criteria for being awarded a JPPS star and thus the journals are incentivized to understand and use them more.

### What are INASP’s plans for the future?

INASP has recently completed a major five-year programme of work with a significant focus on strengthening organizations in the countries we have been working in and handing over responsibility for managing things like the Journals Online platforms. We are now in a new phase of work, building on what has gone before but with a particular emphasis on improving equity both within and between research systems.

Many challenges remain – the global research system still tends to be biased towards the Global North. From an academic publishing perspective this is apparent both in terms of awareness of journals and also in terms of impressions of credibility. JPPS is intended to tackle the latter challenge but it is still early days – we only announced the first badges awarded a few months ago. Over the next few years we will be building on and strengthening this work and ensuring that it is an important part of the processes for journal editors and for authors and readers.

Thank you Sioux for your participation in our [Meet the members](/categories/meet-the-members/) series. If your organization would like to feature in this series, [please get in touch](mailto:feedback@crossref.org).
<br>
