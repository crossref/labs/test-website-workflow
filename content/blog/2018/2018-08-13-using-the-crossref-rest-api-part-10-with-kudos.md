---
title: 'Using the Crossref REST API. Part 10 (with Kudos)'
author: Christine Cormack Wood
draft: false
authors:
  - Christine Cormack Wood
  - David Sommer
date: 2018-08-13
categories:
  - APIs
  - Identifiers
  - Interoperability
  - API Case Study
archives:
  - 2018
---

Continuing our blog series highlighting [the uses of Crossref metadata](/categories/api-case-study), we talked to David Sommer, co-founder and Product Director at the research dissemination management service, [Kudos](http://www.growkudos.com/). David tells us how Kudos is collaborating with Crossref, and how they use the REST API as part of our [Metadata Plus](/services/metadata-retrieval/metadata-plus/) service.

<!--more-->

### Introducing Kudos

{{% imagewrap right %}} <img src="/images/blog/kudos-logo.png" alt=“Kudos logo" height="150px" width="250px" class="img-responsive" /> {{% /imagewrap %}}
At Kudos we know that effective dissemination is the starting point for impact. Kudos is a platform that allows researchers and research groups to plan, manage, measure, and report on dissemination activities to help maximize the visibility and impact of their work.

We launched the service in 2015 and now work with almost 100 publishers and institutions around the world, and have nearly 250,000 researchers using the platform.

We provide guidance to researchers on writing a plain language summary about their work so it can be found and understood by a broad range of audiences, and then we support researchers in disseminating across multiple channels and measuring which dissemination activities are most effective for them.

As part of this, we developed the [Sharable-PDF](https://blog.growkudos.com/2017/11/15/kudos-solution-illegal-sharing-copyright-content/) to allow researchers to legitimately share publication profiles across a range of sites and networks, and track the impact of their work centrally. This also allows publishers to prevent copyright infringement, and reclaim lost usage from sharing of research articles on scholarly collaboration networks.

<center>{{< figure src="/images/blog/kudos-page.png" title="An example of a Kudos publication page showing the plain language summary" link="https://www.growkudos.com/publications/10.12688%25252Ff1000research.8013.1/reader" alt="Kudos publication page" width="75%">}}</center>


### How is Crossref metadata used in Kudos?

Since our launch, Crossref has been our metadata foundation. When we receive notification from our publishing partners that an article, book or book chapter has been published, we query using the Crossref REST API to retrieve the metadata for that publication. That data allows us to populate the Kudos publication page.

We also integrate earlier in the researcher workflow, interfacing with all of the major [Manuscript Submission Systems](https://blog.growkudos.com/2018/03/28/extended-integrations-with-manuscript-submission-systems/) to support authors who want to build impact from the point of submission.

More recently, we started using the Crossref REST API to retrieve citation counts for a DOI. This enables us to include the number of times content is cited as part of the ‘basket of metrics’ we provide to our researchers. They can then understand the performance of their publications in context, and see the correlation between actions and results.

<p align="center"><img src="/images/blog/kudos-metrics.png" alt="Kudos metrics page" width="75%" />
</p>
<p align="center">A Kudos metrics page, showing the basket of metrics and the correlation between actions and results</p>


### What are the future plans for Kudos?

We have exciting plans for the future! We are developing Kudos for Research Groups to support the planning, managing, measuring and reporting of dissemination activities for research groups, labs and departments. We are adding a range of new features and dissemination channels to support this, and to help researchers to better understand how their research is being used, and by whom.


### What else would Kudos like to see in Crossref metadata?

We have always found Crossref to be very responsive and open to new ideas, so we look forward to continuing to work together. We are keen to see an industry standard article-level subject classification system developed, and it would seem that Crossref is the natural home for this.

We are also continuing to monitor [Crossref Event Data](/services/event-data/) which has the potential to provide a rich source of events that could be used to help demonstrate dissemination and impact.

Finally, we are pleased to see the work Crossref are doing to help improve the quality of the metadata and supporting publishers in auditing their data. If we could have anything we wanted, our dream would be to prevent “funny characters” in DOIs that cause us all kinds of escape character headaches!

---

Thank you David. If you would like to contribute a case study on the uses of Crossref Metadata APIs please contact the [Community team](mailto:feedback@crossref.org).
