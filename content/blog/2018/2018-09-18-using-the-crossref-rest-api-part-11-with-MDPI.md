---
DOI: https://doi.org/10.5555/6jg66ekhkq
aliases: /blog/using-the-crossref-rest-api.-part-11-with-mdpi/
archives:
- 2018
author: Christine Cormack Wood
authors:
- Christine Cormack Wood
- Martyn Rittman
- Bastien Latard
categories:
- APIs
- Identifiers
- Interoperability
- API Case Study
date: 2018-09-18
draft: false
title: Using the Crossref REST API. Part 11 (with MDPI/Scilit)
x-version: 0.0.0
---

Continuing our blog series highlighting [the uses of Crossref metadata](/categories/api-case-study ), we talked to Martyn Rittman and Bastien Latard who tell us about themselves, MDPI and Scilit, and how they use Crossref metadata.

<!--more-->

## Can you give us a brief introduction yourselves, and to MDPI/Scilit

Martyn is Publishing Services Manager at MDPI. He joined five years ago as an editor and has worked on editorial, production, and software projects. Prior to joining MDPI, he completed a PhD and worked as a postdoc. His research covered physical chemistry, biochemistry and instrument development.
Bastien Latard is the project leader of Scilit. He created Scilit as part of his Master’s degree in 2013. He is now completing a PhD on the subject of semantically linking research articles, using data from Scilit.

Scilit was developed in 2014 by open access (OA) publisher MDPI with the goal of having a backup of metadata for all OA articles. Soon, Scilit became more general and embraced all articles with a digital object identifier (DOI) from Crossref and those with a Pubmed ID (PMID). After seeing the potential of the database and how it could be used in a number of different contexts, we decided to make it public. Recently, other article types, including preprints have been integrated. Our main goal now is to provide useful services to the research and academic publishing communities.

## What problem is your service trying to solve?

Other indexing databases offer paid access, are highly selective, or host documents apart from research articles. We want to offer a comprehensive database, but also one that clearly identifies open access material. The last part is still a work in progress, but we have made good progress recently.

To make the access as direct as possible, we have recently integrated several OA aggregators that pick up or host free versions of full-text articles, including CORE, Unpaywall, and PubMed Central.


## Can you tell us how you are using the Crossref Metadata API at MDPI/Scilit?

Scilit queries Crossref’s API in order to index metadata for single articles. DOIs are a key part of the system; because they are standards, we can use them to merge new sources into Scilit while avoiding duplicates. We cross-check the data from Crossref against other sources and update it as necessary. Citation data is also really appreciated and opens doors to further developments.

As a publisher, MDPI makes daily deposits to Crossref, to register journal articles on [mdpi.com](http://www.mdpi.com/), conference papers from [sciforum.net](https://sciforum.net), and preprints from [Preprints.org](https://www.preprints.org/). We also use the data collected at Scilit to find suitable reviewers and let authors know when their work has been cited.

## What metadata values do you pull from the API?

As much as we can! Scilit crawls the latest indexed articles every few hours to ensure it is as up-to-date as possible. This is the most important function of our system because it provides metadata for the very latest published articles, including a link to the publisher version. Scilit parses Crossref metadata and saves them. They are then indexed into our solr search engine for fast, real-time usage.

## Have you built your own interface to extract this data?

We wrote our own code to get the data, but the API interface made this very straightforward. Scilit has been developed completely in-house by MDPI and the lead developer, Bastien Latard, is currently completing a PhD looking at how to make the most of the data using semantic data extraction.

## What are the future plans for MDPI/Scilit?

Scilit is and will be highly used in MDPI current and future projects. We have a few ideas about how to improve Scilit. We are, for example, implementing a scientific profile networking service, which will allow scholars to build their own (scientific) network with lots of functionalities. We think that it will be a really good place to search, comment, exchange around articles… maybe even more!

## What else would you like to see the REST API offer?

Crossref is already doing a great job, especially with its integrated citation data. Maybe further analysis and mapping of data about organizations and institutions would be an improvement.

---

Thank you Martin and Bastien. If you'd like to share how you use the Crossref Metadata APIs please contact the [Community team](mailto:feedback@crossref.org).