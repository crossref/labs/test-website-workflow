---
title: Is FRBR the OSI for Web Architecture?
author: thammond
authors:
  - thammond
date: 2010-02-13

categories:
  - Linked Data
archives:
  - 2010

---
(This post is just a repost of a comment to Geoff’s [last entry][1] made because it’s already rather long, because it contains one original thought - FRBR as OSI - and because, well, it didn’t really want to wait for moderation.)

Hi Geoff:

First off, there is no question but that Crossref was established to take on the reference linking challenge for scholarly literature. (Hell, it’s there, as you point out, in the organization name - PILA - as well as in the application name - Crossref.)

But one should also remember that DOI as it was sold at the time was promising so much more. I disagree with you that the participants back then were as wholly innocent of the FRBR terms as you might suggest. Certainly there were ample presentations on DOI that sought to elucidate those relationships.

No matter. FRBR is a useful reference model to clarify some of these concepts. But not one that we are overly concerned with at this time. Nor even whether DOI maps one to one onto a given FRBR layer. What we are more concerned with on a pragmatic level is how DOI maps onto the Web architecture and especially how it plays along with Linked Data concepts.

(Aside: A propos FRBR we might be in danger of repeating the OSI mistake for standardizing the network layer model. Ultimately that was maintained as a reference model but dropped as a concrete model in favour of the TCP/IP stack. Could be that FRBR is our OSI and Linked Data is our TCP/IP stack? That is, we might have to settle on the coarser data model in order to get a coherent story out the door where all can agree.)

You say:

> _&#8220;we need a mechanism to distinguish between when we are getting the thing pointed to by the Crossref DOI (the PDF , HTML, etc.) as opposed to &#8220;something about the thing&#8221; (e.g. the landing page, metadata record, etc.)&#8221;_

But that is exactly what we were chasing up in the earlier posts (both my [DOI: What Do We Got?][2] and John Erickson’s [DOIs, URIs and Cool Resolution][3]). You want to distinguish between a thing and a description about a thing. And Web architecture does just that: it distinguishes between Information Resources (i.e. the things) and Non-Information Resources (i.e. descriptions of the things).

Now is this something that Crossref can truly distinguish and make apparent in its service architecture? If we retain the notion of landing page we are already essentially saying that a Crossref HTTP URI identifies a decsription of the resource, i.e. a Non-Information Resource, or Other Resource, and that is properly indicated within the architecture by returning a &#8220;303 See Other status&#8221; code.

I think that’s all we’re saying at the moment as a first step.

Web architecture wants to know if the DOI HTTP URI is a thing or description of a thing. I say the latter. You seem to suggest in your comment the latter too. I wonder if we could get a vote on that.

And btw, I am not suggesting that Crossref needs to dive into the business of _&#8220;tracking compoend documents in their entirety&#8221;_. Far from it. Lets just get a common resource architecture agreed publicly and then we can build on that.

This observation I received in a private email is something I fully support:

> _&#8220;The real problem is what doi http uri identify on the web. Everything flows from the answer to that Q.&#8221;_

Tony

 [1]: /blog/does-a-crossref-doi-identify-a-work
 [2]: /blog/doi-what-do-we-got/
 [3]: http://bitwacker.wordpress.com/2010/02/04/dois-uris-and-cool-resolution/
