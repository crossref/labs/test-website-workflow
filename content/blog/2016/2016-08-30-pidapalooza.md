---
title: 'Announcing PIDapalooza - a festival of identifiers'
author: Ginny Hendricks
authors:
  - Ginny Hendricks
date: 2016-08-30
categories:
  - Meetings
  - DataCite
  - ORCID
  - CDL
  - Collaboration
  - PIDapalooza
  - Persistence
  - Identifiers
archives:
  - 2016

---
{{% imagewrap left %}}<img src="/wp/blog/uploads/2016/08/sideA-300x213.jpg" alt="sideA" width="300" height="213" />{{% /imagewrap %}}

The buzz is building around PIDapalooza - the first open festival of scholarly research persistent identifiers (PID), to be held at the [Radisson Blu Saga Hotel Reykjavik](https://www.radissonblu.com/en/sagahotel-reykjavik)on November 9-10, 2016.

<span >PIDapalooza will bring together creators and users of PIDs from around the world to shape the future PID landscape through the development of tools and services for the research community. PIDs support proper attribution and credit, promote collaboration and reuse, enable reproducibility of findings, foster faster and more efficient progress, and facilitate effective sharing, dissemination, and linking of scholarly works.</span>

<span >We believe that by bringing together everyone who’s working with PIDs for two days of discussions, demos, workshops, brainstorming, updates on the state of the art, and more, we can make this happen faster. And you can help by giving us your input on which sessions would be most valuable. Please send us your ideas, using this </span>[<span >form</span>][1] <span >by September 18. We will send session proposal notifications the first week of October with the festival lineup.</span>

##### **Register to attend**

[**Registration is now open**][2] **&#8212; c**<span >ome join the festival with a crowd of like-minded innovators. And please help us spread the word about PIDapalooza in your community! </span>

<span >Stay updated with the latest news on on the </span>[<span >PIDapalooza website</span>][3] <span >and on Twitter (</span>[<span >@PIDapalooza</span>][4]<span >) in the coming weeks.</span>

<span >Looking forward to seeing you in November! </span>

&nbsp;

 [1]: https://docs.google.com/forms/d/e/1FAIpQLSej7YKQVCPTTCo8zeIS-ODjtsb5SIS299uZZBo8ZN6yD0WI5Q/viewform?c=0&w=1&usp=send_form
 [2]: http://pidapalooza.eventbrite.com
 [3]: http://pidapalooza.org/
 [4]: http://twitter.com/pidapalooza
