---
title: Get ready for Crossmark 2.0!
author: Kirsty Meddings
authors:
  - Kirsty Meddings
date: 2016-08-17

categories:
  - Crossmark
  - Metadata
  - News Release
tags:
  - Crossmark
archives:
  - 2016

---
<span ><span ><span >TL;DR&#8230; In a few weeks, publishers can upgrade to the new and improved Crossmark 2.0 including a mobile-friendly pop-up box and new button. We will provide a new snippet of code for your landing pages, and we’ll support version v1.5 until March 2017.</span></span></span>

<span ><span >We recently revealed a new look for the Crossmark box, bringing it up-to-date in design and offering extra space for more metadata. The new box pulls all of a publication’s Crossmark metadata into the same space, so readers no longer have to click between tabs. </span><a href="/blog/linked-clinical-trials-are-here/"><span >Linked Clinical Trials</span></a><span > and author names (including ORCID iDs) now have their own sections alongside funding information and licenses. Feedback so far tells us that the new box is a vast improvement.</span></span>   

<span >However, this was only phase one of the Crossmark makeover. We will soon complete the upgrade to display a fully responsive, mobile-friendly box. The Crossmark button has been given a facelift too, and we are excited to offer the first public preview today:</span>

> <span ><img class="wp-image-1955 size-medium alignnone" src="/wp/blog/uploads/2016/08/CROSSMARK_LOGO-300x65.png" alt="CROSSMARK_LOGO" width="300" height="65" srcset="/wp/blog/uploads/2016/08/CROSSMARK_LOGO-300x65.png 300w, /wp/blog/uploads/2016/08/CROSSMARK_LOGO.png 355w" sizes="(max-width: 300px) 85vw, 300px" /></span>
>
> <span >The new button brings the Crossmark icon up to date and is designed to be more &#8220;clickable&#8221; than the current button. It will be available in several different ratios and also in greyscale.</span>

<span >The first phase of the new design was rolled out in the existing Crossmark pop up window (Crossmark v1.5) without the need for changes within publisher systems. For the Crossmark v2.0 upgrade, publishers will need to update their landing pages with a new snippet of code, to ‘unlock’ the new button and functional enhancements.</span>

<span >Crossmark 2.0 will be available to adopt in a few weeks, and each publi</span><span ><span >sher can decide when to switch over. We encourage members to upgrade sooner rather than later to get the benefits of the new box, but we also understand there are planned development schedules and the need for a testing period so <strong>w</strong></span><span ><strong>e will continue to support Crossmark v1.5 until March 2017</strong>.</span></span>

<span >Many thanks to all of those who completed our surveys to help us shape the new button. And congratulations to <strong>Elizabeth Ramsey</strong>, a researcher from <strong>Trent University in Canada</strong>, who will be receiving a limited edition Crossref Moleskine notebook from the survey prize draw.</span>

<span >Our User Experience Designer, Rakesh Masih, will be blogging soon with details about the research and testing for this project, as well as more about our new approach to user experience at Crossref.</span>
