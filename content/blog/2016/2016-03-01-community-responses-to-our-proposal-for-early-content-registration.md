---
title: Community responses to our proposal for early content registration
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2016-03-01

categories:
  - Accepted Manuscripts
  - Content Registration
  - DOIs
  - Research Funders
  - Request for Community Comment
archives:
  - 2016

---
## <span >TL;DR:</span>

<span >We will proceed with implementing the <a href="/blog/rfc-registering-content-before-online/" target="_blank">proposed support for registering content before online availability</a>. Adopting the workflow will be optional and will involve no extra fees.</span>

## <span >Background</span>

<span >At the end of January, Crossref issued a “request for community comment” on a proposed new process to support the registration of content including DOIs before online availability. We promised that we would summarize the results of the survey once we had received and analyzed all the responses.</span>

<span >Support for Crossref implementing the proposed new workflow was overwhelming. Of the 104 responses, 90 were positive, 7 were neutral and 7 were negative. As such we will proceed to make the necessary changes to better support registering content before online availability. We aim to enable this functionality in the second half of 2016.</span>

<span >We received survey responses varying in length from one or two sentences to multiple pages. A lot of the responses also interspersed questions and observations about entirely different issues that were of interest to respondents. As such, it has taken a while for us to analyze the results. We also found it was pretty much impossible for us to tabulate a summary of the responses to the direct questions. Instead we’ll summarize the responses at a high level and then drill down into some of the nuances in the answers and issues that were raised from the responses.</span>

## <span >The positive responses</span>

<span >By “positive” we mean the respondent understood the issues we were trying to address and thought what we were proposing was a reasonable way to address the problems. Here are a few (anonymized) excerpts from the responses:</span>

> <span >“[This] is very timely as we have been made aware of changes to manuscript deposit requirements for UK authors. Authors who partake in the REF system will have to deposit articles at their manuscript stage before publication. We need to set an embargo on the articles so that they only become discoverable at some point after the publication date. Ideally we would like this to happen with all articles regardless of where they are from as authors will put their own work up on open access sites.”</span>

> <span >“Your proposal and the associated workflow look good to us and will help with our media embargo timelines, as well as our authors’ institutional requirements.”</span>

> <span >“The workflows and solutions seem reasonable &#8230; The temporary landing page seems like a sustainable technical solution. Hosting by Crossref is key to this – there is no way that all publishers would otherwise take on maintaining temporary pages. And having a standard display for metadata consistency is crucial too.”</span>

> <span >“Early assignment and recording of DOIs from the point of acceptance forms a key step in [the university’s] proposed ‘Submit-accept-deposit’ workflow. We welcome the proposal by Crossref to enable early assignment of DOIs for publications.”</span>

<span >Note that a positive response did not mean the respondents thought the problems necessarily applied to them or that they would necessarily be implementing the changes - just that what we were proposing seemed sound for those who needed to address the issues.</span>

> <span >“While not directly relevant to our business the proposal seems aimed to protect the integrity of DOIs and Crossref’s role and that is not a bad thing.”</span>

> <span >“I would consider it an irresponsible use of the system on the part of a publisher to circulate dois that don’t (yet) work. This is bound to lead to frustration with users encountering errors. However I appreciate that this situation may arise in some workflows and therefore your proposals to implement temporary landing pages make sense.”</span>

> <span >“I was not aware of these issues, but think that your solutions seem feasible. We are a small journal and generally don’t add doi’s or publish until the article is complete (i.e., we don’t post anything that’s just accepted - only finalized). So we would be unlikely to update our workflow.”</span>

<span >Also, though respondents might have been generally positive about the proposal - that didn’t always mean they were also sanguine about it. For example, several shared concerns about the potential costs of changing their workflows.</span>

> <span >“[we] would consider implementing this change into our workflow. Limiting factors would include the effort and additional cost to enable our paper management system vendor&#8230;”</span>

> <span >“My only comment is that the process needs to be streamlined as much as possible so small publishers without great technical capacity will not be burdened with twice the work or with additional expense. After reading through Crossref’s proposal, I believe you have taken such things into account and will implement an efficient and worthwhile system.”</span>

> <span >“The workflow makes sense as a solution to the problem you describe [&#8230;] but will require extensive workflow changes on our end in order to implement. Speaking for a small publishing house I’m not sure it’s reasonable to expect this from us on any short term.”</span>

<span >Several of the positive respondents also wondered about how we would handle particular edge cases (e.g. rescinding acceptances) and/or offered suggestions to improve the proposal. We will discuss these further at the end of this post.</span>

## <span >The neutral responses</span>

<span >The responses we categorised as “neutral” were generally too short to conclude much about. They consisted of one or two sentences that said something like &#8220;this doesn’t apply to me.&#8221; It wasn’t clear whether it didn’t apply to them because they didn’t have the problems we described or because they’d already solved the problems we described (e.g. by providing their own interim landing pages). They also didn’t comment on the applicability to other members or whether they thought the issues might eventually affect them.</span>

## <span >The negative responses</span>

<span >We categorised responses as “negative” when the member rejected that the issues we outlined were actual problems or they rejected the mechanisms we were proposing to address the problems.</span>

> <span >“&#8230;a formal letter of acceptance on a letter in PDF will be OK for </span><span >authors. Why a DOI is better?”</span>

> <span >“&#8230;I am aware of funder and institutional requirements for authors to take action on acceptance of manuscripts for publication in journals but don’t think the time pressure is so high that it has to happen in short time between acceptance and published ahead of print online…”</span>

> <span >“Of all the accepted-but-not-yet-published papers in existence at any time, the number whose existence must be demonstrated to promotion and tenure review boards must be awfully small.”</span>

<span >There are a few common themes here. The first is that, historically, the industry has been content with acceptance letters as proof of publication and that it was relatively rare for authors to have to produce such proof.</span>

<span >The problem that has led us to propose support for a modified workflow is that now we have situations where all the researchers in a country require such letters on a regular basis - not just when they are up for promotion or tenure. This is the new reality faced by researchers and institutions who are subject to regular national evaluation schemes like the <a href="http://www.ref.ac.uk/" target="_blank">REF</a> and <a href="https://web.archive.org/web/20160229001254/http://www.arc.gov.au/era-faqs" target="_blank">ERA</a>.</span>

<span >One of the negative respondents added:</span>

> <span >“This is very familiar territory. It’s definitely coming out of STEM.”</span>

<span >Indeed, the initial pressure to support the earlier registration of DOIs is certainly falling on our members who focus on STEM publishing. Researchers in the STEM fields are generally under more pressure to publish articles frequently and they are primarily affected by emerging funder mandates. The relatively high research output in STEM fields combined with the need for regular compliance checks and regular evaluation schemes is creating an environment that requires more automated mechanisms to keep track of publications. Asking for and processing letters of acceptance in these situations just doesn’t scale.</span>

<span >Some of the negative responses also questioned our assertions about the hazards of promulgating unregistered DOI-like strings and/or the problems associated with the delay between when content is made available online and when the content is registered.</span>

> <span >“I don’t buy the argument that people lose trust in DOIs in general because they once tried to resolve one and it didn’t lead to an article. By the same argument, URLs in general are similarly undermined.”</span>

> <span >“where authors ask me for their DOIs so they can accurately cite the paper in another publication or use it for grants and applications. I explain that it won’t work until the issue as a whole posts and I have never heard back about confusion or distrust of the system.”</span>

<span >To this all we can say is, we have the data. Next to typos, unregistered DOIs account for the second greatest category of failed resolutions on the Crossref system. Our help desk has to explain them to researchers constantly. We have promoted DOIs as being more robust, persistent identifiers than ordinary URLs. People are not surprised when URLs don’t work. They are surprised when DOIs don’t work. We’d like to keep it that way.</span>

<span >What seems to be at the the root of the few negative responses - is that most assumed that Crossref was mandating that publishers change their workflow - even if they didn’t face any of the issues outlined in the proposal. There is very little that Crossref *mandates* to participate. This is by design. Our membership is just too diverse for us to have mandates that can be sensibly applied to all. Still - we should have made it clearer in the proposal that the proposed changes would not be mandated. We will certainly need to make this clearer when we roll out support for the new processes.</span>

<span >Oh yeah - one respondent called us out for using the phrase “advanced publication” instead of “advance publication”. For this we are truly sorry. The employee who made this mistake has been dragged out and shotted.</span>

## <span >Issues raised and questions asked</span>

<span >Both the positive and negative respondents raised issues, asked questions and provided suggestions regarding the proposal. We will make sure that, when the proposal is implemented, we address all of these issues more clearly, but in the meantime, we thought it would be helpful if we answered some of them briefly here.</span>

<span ><strong>Q:</strong> Would Crossref charge extra for the new workflow?</span>

<span ><strong>A:</strong> No. We should have made this clear in the proposal. We should have also mentioned that, in the “Crossref-facilitated Early Registration” scenario members will only be charged once they have replaced the “registered_content” metadata with metadata for the published item using one of the existing content schemas (e.g. article, book, confproc).</span>

<span ><strong>Q:</strong> Would Crossref require that publishers adopt the new proposed workflows?</span>

<span ><strong>A:</strong> No. But we will recommend them to members who need to address the issues outlined in the proposal. And in general, we will recommend that our members register DOIs as early in the process as practicable.</span>

<span ><strong>Q:</strong> What does “acceptance” mean? It was pointed out that there were lots of variations of “acceptance” including “acceptance pending revisions”, etc.</span>

<span ><strong>A:</strong> We would expect that “contingent acceptance” does not constitute final acceptance and that in this case “acceptance” should mean that the publisher has a copy of the manuscript in which the author has made all of the changes asked of them.</span>

<span ><strong>Q:</strong> Doesn’t “acceptance” works both ways? A researcher has to grant permission to publish to the publisher.</span>

<span ><strong>A:</strong> This is a vital point - the publisher should only register content for which they have already secured the rights to publish.</span>

<span ><strong>Q:</strong> Collecting and verifying the metadata associated with a paper is expensive and time consuming. As such, some publishers only produce complete and robust metadata after a paper has been accepted. We face a Catch-22. if we deposit metadata immediately after acceptance, it will be sparse and unreliable. If we wait to collect and verify the metadata, then we risk violating some of the emerging mandates. How do we resolve this dilemma?</span>

<span ><strong>A:</strong> This is clearly beyond our control, but we expect that those issuing the mandates will have to make some reasonable accommodations if they expect publishers to register content both early and with reasonably useful metadata.</span>

<span ><strong>Q:</strong> How would publishers handle rescinded acceptances?</span>

<span ><strong>A:</strong> Publishers can handle this the same way they handle retractions or withdrawals. Additionally, the registered content type and the “intent to publish” landing page will both support Crossmark for those members who use Crossmark to promulgate corrections to the literature. We will explore adding a new “acceptance rescinded” update type to the Crossmark schema.</span>

<span ><strong>Q:</strong> The Crossref DOIs we generate contain embedded publication information such as volume and issue. We don’t know these details at acceptance so how can we register DOIs early? </span>

<span ><strong>A:</strong> Many of our members generate Crossref DOIs with embedded semantic information in them such as volume/issue, publication date or even author initials and title. After 16 years of experience, we have found that this tends to be a bad idea. Publication schedules slip. Metadata changes. We will soon be revising our guidelines on DOI best practice in Crossref DOI generation to recommend against embedding such information into the DOI itself. Clearly, if you decide to assign Crossref DOIs at acceptance, you will need to adopt a DOI structure that accommodates this.</span>

<span ><strong>Q:</strong> Our hosting provider manages DOI registrations for us. If we have to register DOIs earlier in the process, can we have one party (e.g. a manuscript tracking system vendor) register the initial “registered_content” metadata and then have different party (e.g. hosting provider or typesetter) replace that record with the final metadata?</span>

<span ><strong>A:</strong> Yes.</span>

<span ><strong>Q:</strong> Will you be working with industry vendors to help them support this new workflow?</span>

<span ><strong>A:</strong> Yes.</span>

<span ><strong>Q:</strong> Will we support the pre-registration of DOIs in the the deposit forms on the Crossref site?</span>

<span ><strong>A:</strong> Yes.</span>

<span ><strong>Q:</strong> If Crossref hosts the “intent to publish” landing page, how will publishers be able to account for visits to the page and incorporate that information into their metrics?</span>

<span ><strong>A:</strong> While visitors to the Crossref hosted page will not show up in the publisher’s own hosting platform logs, publishers will be able to easily see how many times their “intent to publish” landing page was accessed by looking at their standard Crossref DOI resolution logs.</span>

<span ><strong>Q:</strong> Could the Crossref-hosted landing page also include the URL that the DOI will eventually be associated with?</span>

<span ><strong>A:</strong> This is an interesting idea and was suggested by two separate respondents. The challenge will be in explaining to the user that the URL might or might not work. We are also concerned that this would reduce the incentive for publishers to replace the holding page in a timely manner. We’ll explore this option as we continue to work on implementation.</span>

<span ><strong>Q:</strong> Would the Crossref-hosted landing page be open to indexing by Google and others? If so, wouldn’t this undermine articles under press embargoes?</span>

<span ><strong>A:</strong> The idea behind the limited metadata required for registering content is that it allows the publisher to control the balance between discovery (needed to meet funder requirements) and discretion (needed to manage publicity). So yes, the Crossref-hosted landing pages would be open to indexing, but publishers can still control what gets indexed by withholding metadata as needed.</span>

<span ><strong>Q:</strong> The table of required metadata elements for the “registered content” type does not include the author. How are such records supposed to be used as proof of acceptance if they do not include the author name?</span>

<span ><strong>A:</strong><strike> We made a mistake. The table should have included the contributor in the required element column.</strike><strong> Update:</strong> We retract our retraction! We are trying to accommodate several different use cases for 'early content registration'  and these different use cases often have contradictory metadata implications. So, for example, including the author is certainly important for monitoring mandate compliance. However, including the author might be problematic when the publisher is trying to manage publicity around an upcoming publication. Again, Crossref is not in a position to resolve this dilemma and we expect that those issuing the mandates will make some reasonable accommodations with publishers who need to manage publicity around publications. In short, &#8220;authors&#8221; will remain optional metadata.</span>

## <span >Summary and conclusions</span>

<span >We were delighted with the response rate on the proposal. It is clear to us that a lot of the respondents really appreciated both being alerted to a set of issues that they were not yet aware of and that they valued the chance to comment on our proposed mechanisms for addressing said issues. We also learned some lessons on how to better structure any such future surveys in order to make them easier for us to summarise and respond to. The wide variety of responses and detailed descriptions of different workflows reconfirmed our sense that Crossref members vary widely in their working practices. We need to continue to work directly with members and understand these different working practices so that we can provide appropriately flexible services to our membership and to the scholarly community in general.</span>

<span >Finally, the feedback we received will be used by our product team and our communications & outreach teams to refine our rollout plans for registering content before online availability. We expect that we will rollout this functionality in the second half of 2016.</span>

<span >Thanks to those who responded to our RFC. Some of those responses included questions about other matters relating to Crossref. We have attempted to extract these and answer them directly- but if we have not yet answered one of your questions, please follow-up with us at <a href="mailto:feedback@crossref.org">feedback@crossref.org</a></span>
