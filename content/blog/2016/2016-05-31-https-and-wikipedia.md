---
title: HTTPS and Wikipedia
author: Joe Wass
authors:
  - Joe Wass
date: 2016-05-31

categories:
  - Crossref Labs
  - Data
  - DOIs
  - Event Data
  - Wikipedia
archives:
  - 2016

---
<span ><em>This is a joint blog post with Dario Taraborelli, coming from <a href="https://meta.wikimedia.org/wiki/WikiCite_2016">WikiCite 2016</a>.</em></span>

<span >In 2014 we were taking our first steps along the path that would lead us to <a href="http://eventdata.crossref.org">Crossref Event Data</a>. At this time I started looking into the DOI resolution logs to see if we could get any interesting information out of them. This project, which became <a href="/blog/introducing-chronograph/">Chronograph</a>, showed which domains were driving traffic to Crossref DOIs.</span>

<span >You can read about the latest results from this analysis in the <a href="/blog/where-do-doi-clicks-come-from/">&#8220;Where do DOI Clicks Come From&#8221;</a> blog post.</span>

<span >Having this data tells us, amongst other things:</span>

  * <span >where people are using DOIs in unexpected places</span>
  * <span >where people are using DOIs in unexpected ways</span>
  * <span >where we knew people were using DOIs but the links are more popular than we realised</span>

<!--more-->

<span >By the time the <a href="http://www.lagotto.io/workshop_2014/">ALM Workshop 2014</a> rolled around there was some preliminary data and we realised that Wikipedia came into the third category. There are lots of DOIs in Wikipedia and people click them!</span>

<span >I met with Dario Taraborelli, head of research at the Wikimedia Foundation, and shared the data. Dario — who co-authored in 2010 the Altmetrics Manifesto — has been interested in understanding how scholarly citations are used in Wikipedia. Over the years, Wikipedia contributors have made extensive use of references to the scientific literature using DOIs, and by doing so they have created a resource that represents today in many ways the <a href="https://meta.wikimedia.org/wiki/Wikipedia_as_the_front_matter_to_all_research">&#8220;front matter to all research&#8221;</a>. There is growing interest in the community in understanding how DOIs are being used in Wikipedia and in non traditional scholarship.</span>

<span >During our discussions the subject of Wikipedia’s gradual transition to HTTPS was raised: we anticipated that this change would affect our data gathering.</span>

## <span >Changes</span>

<span >When you’re reading webpage and click on a link to another page, your web browser will usually tell the server of that second page the last page you were on. This forms the basis of trackers like Google Analytics.</span>

<span >In the days before HTTPS, the next site would know the full URL that you were previously on. With the change to HTTPS, this was reduced to just sending the domain name and not the full URL, or no data at all if you click from an HTTPS page to HTTP.</span>

<span >DOI hyperlinks are just like any other hyperlink, and are mostly HTTP not HTTPS.</span>

<span >Up until 2015, Wikipedia was served over HTTP, only switching to HTTPS when users were logged in or if they requested it. The Wikimedia Foundation started planning to move to HTTPS and we knew that if they did that, and continued to use HTTP DOIs then we would lose valuable research data.</span>

## <span >A Plan</span>

<span >We decided that the best course of action was to try and change the DOIs in Wikipedia to use HTTPS. Simple, right?</span>

<span >After some further research, Dario <a href="https://meta.wikimedia.org/wiki/Research:Wikimedia_referrer_policy">posted a proposal</a> on how to mitigate the impact of the HTTPS rollout, to make sure that Wikipedia can still signal its importance as a traffic source, while preserving the privacy of its users. <a href="https://meta.wikimedia.org/wiki/Research_talk:Wikimedia_referrer_policy">Discussion followed</a> and the conclusion was to change the format of every single DOI on Wikipedia, which fortunately could be done without having to edit millions of pages. You can read the full story in <a href="/blog/real-time-stream-of-dois-being-cited-in-wikipedia/">this post from a year ago</a>.</span>

<span >The result of this effort was that well in advance of the HTTPS switchover, the DOI links were ready to continue reporting referral data.</span>

## <span >The Switch</span>

<span >In June 2015 the Wikimedia foundation made the <a href="http://blog.wikimedia.org/2015/06/12/securing-wikimedia-sites-with-https/">announcement that they were finalising the switch</a>, and that within a few weeks all traffic would be HTTPS.</span>

<span >We held our breath. Would it work? Would we lose all referral data from Wikipedia sites? In February 2016 <a href="https://phabricator.wikimedia.org/T99174#2053812">the last piece of the puzzle fell into place</a> as Wikipedia gained a &#8216;meta referrer’ tag to explicitly specify how they would like referrers to be sent: a detailed report on the effect of this change is coming up on the Wikimedia Foundation’s blog.</span>

## <span >The results</span>

<span >As detailed in <a href="/blog/where-do-doi-clicks-come-from/">the last blog post</a> the traffic that we measured coming from Wikipedia doesn’t seem to have slowed down during 2015:</span>

<img src="/wp/blog/uploads/2016/05/month-top-10-filtered-domains-1.png" alt="month-top-10-filtered-domains" class="img-responsive" />

<span >I’d call that a success! Over the period covered in the graph, Wikipedia remained prominent as a non-publisher referral of traffic to DOIs.</span>

<span >Looking at the balance of HTTP vs HTTPS traffic coming from wikipedia.org, the switchover was dramatic:</span>

<img src="/wp/blog/uploads/2016/05/day-code-area.png" alt="day-code-area" class="img-responsive" />

<span >Thank you to Dario Taraborelli, Nemo (Federico Leva), Aaron Halfaker, Alex Stinson and everyone who put in this effort.</span>

<span >I’ll leave the last word to Dario:</span>

<span >It’s great to see this data. It shows that the switchover happened successfully, which better protects the privacy of our users whilst still reporting the fact that Wikipedia is a prominent source of traffic. This is important validation of the increasing role that Wikipedia plays in the education and scientific community.</span>
