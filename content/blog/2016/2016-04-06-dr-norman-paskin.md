---
title: Dr Norman Paskin
author: Ed Pentz
authors:
  - Ed Pentz
date: 2016-04-06

categories:
  - DOI Foundation
archives:
  - 2016

---
<figure id="attachment_1484"  class="wp-caption alignright"><a href="/wp/blog/uploads/2016/04/Norman.png" rel="attachment wp-att-1484"><img class="size-full wp-image-1484" src="/wp/blog/uploads/2016/04/Norman.png" alt="Dr Norman Paskin" width="197" height="197" srcset="/wp/blog/uploads/2016/04/Norman.png 197w, /wp/blog/uploads/2016/04/Norman-150x150.png 150w" sizes="(max-width: 197px) 85vw, 197px" /></a><figcaption class="wp-caption-text">Dr Norman Paskin</figcaption></figure>

<span >It was with great sadness and shock that I learned that Dr Norman Paskin had passed away unexpectedly on the 27th March. This is a big loss to the DOI, Crossref and digital information communities. Norman was the driving force behind the DOI System and was a key supporter and ally of Crossref from the start. Norman founded the International DOI Foundation in 1998 and ran it successfully until the end of 2015 when he moved to a strategic role as an Independent Board Member.</span>

<span >Norman was an early proponent of the value of persistent digital identifiers paired with standardised metadata and laid the groundwork for the system and infrastructure that has made Crossref and eight other Registration Agencies so successful. Norman was also a key adviser and participant in many standards organizations and initiatives where he regularly provided key intellectual input to help improve digital communications.</span>

<span >Personally, it was a great pleasure to work with Norman over the last twenty years and I greatly appreciated his intelligence, humour, advice, and particularly his help and generous support when I relocated to Oxford.</span>

<span >The International DOI Foundation has <a href="http://www.doi.org/index.html">posted a notice</a>, and has created <a href="mailto:condolences@doi.org">condolences@doi.org</a> for people to send messages. </span>
