---
title: A look back at LIVE16
author: April Ondis
authors:
  - April Ondis
date: 2016-11-24
categories:
  - Crossref LIVE
  - Community
archives:
  - 2016

---
Crossref LIVE16 opened with a Mashup Day on 1st November 2016 in London. Attendees from the scholarly communications world met to chat with Crossref team members in an open house atmosphere. The Crossref team put their latest projects on display and were met with questions, comments, and ideas from members and other metadata folks. Here’s what it looked like &#8212; you may recognize a few familiar faces. <!--more-->

### Crossref LIVE16 in London


|   |   |   |   |   |
|---|---|---|---|---|
|<img src="/images/blog/LIVE16-1-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-2-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-3-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-4-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-5-72-.jpg" alt="image of people at the event" width="175px"/>
|<img src="/images/blog/LIVE16-6-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-7-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-8-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-9-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-10-72-.jpg" alt="image of people at the event" width="175px"/>|
|<img src="/images/blog/LIVE16-11-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-12-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-13-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-14-72-.jpg" alt="image of people at the event" width="175px"/>|<img src="/images/blog/LIVE16-15-72-.jpg" alt="image of people at the event" width="175px"/>|

<br>
LIVE16 continued with the Conference Day on 2nd November, a plenary session with invited speakers and presentations by the Crossref team. Here are the presentations, in chronological order.

* Dario Taraborelli speaks on &#8220;Wikipedia’s role in the dissemination of scholarship&#8221; 

* Ian Calvert speaks on: &#8220;You don’t have metadata (and how to befriend a data scientist)&#8221; 

* Ed Pentz speaks on &#8220;Crossref’s outlook & key priorities&#8221;

* Ginny Hendricks speaks on &#8220;A vision for membership&#8221;

* Geoffrey Bilder speaks on &#8220;The case of the missing leg&#8221; 

* Lisa Hart Martin speaks on &#8220;The meaning of governance&#8221;

* Jennifer Lin speaks on &#8220;New territories in the Scholarly Research Map&#8221;

* Chuck Koscher speaks on &#8220;Relationships and other notable things&#8221;

* Carly Strasser speaks on &#8220;Funders and Publishers as Agents of Change&#8221; 

* April Hathcock speaks on &#8220;Opening Up the Margins&#8221;

**Your survey feedback**

We’re serious about making Crossref LIVE a useful and welcoming annual event for the Crossref membership as well as members of the wider scholarly communications community. That’s why we appreciate responses from the attendees who answered our survey. Here’s what we have learned from your feedback:

_Content_

  * You want speakers to tell you something new, even if you don’t agree with their points of view
  * Your favorite speakers were those who inspired you
  * You prefer an unscripted presentation style that makes complex topics accessible to all
  * You’re not as interested in the mechanics of Crossref’s annual election as we are

_Format_

  * <span >You enjoyed the diversity of presenters and would like even more external speakers </span>
  * <span >You want more opportunity to ask us technical questions on the Mashup Day  </span>
  * <span >You want to see panel discussions in addition to individual presentations on the Conference Day</span>
  * <span ><span >Those who attended the Conference Day only wished they had also attended the Mashup Day</span> </span>

_Atmosphere_

  * <span >You liked the casual atmosphere but wanted more seating and more dessert.  So noted!

LIVE17 will be held next November 14-15 in Asia. Until then, we hope you’ll have the chance to see us at the regional Crossref LIVE events we are planning around the world throughout the year. Our next local event is Crossref LIVE in Brazil, held 13 December in Campinas and 16 December in Sao Paulo. 
