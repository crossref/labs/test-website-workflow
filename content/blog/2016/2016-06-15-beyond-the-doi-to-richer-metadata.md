---
title: Beyond the DOI to richer metadata
author: April Ondis
authors:
  - April Ondis
date: 2016-06-15

categories:
  - DOIs
  - Metadata
archives:
  - 2016

---
<span ><span >The act of registering a DOI (Digital Object Identifier) for scholarly content is sometimes conflated with the notion of conferring a seal of approval or other mark of good quality upon an item of content.  </span><a href="/blog/dois-unambiguously-and-persistently-identify-published-trustworthy-citable-online-scholarly-literature-right/"><span >This is a fundamental misunderstanding</span></a><span >.</span></span>

<span ><b>A DOI is a tool, not a badge of honor.</b><span >  </span></span>

<span >The presence of a Crossref DOI on content sends a signal that:</span>

  1. <span >The owner of the content would like to be formally cited if the content is used in a scholarly context.</span>
  2. <span >The owner of the content considers that it is worthy of being made persistent.</span>

<span ><b>Beyond the DOI</b></span>

<span ><span ><span >For Crossref, a </span><a href="http://www.doi.org/factsheets/DOIKeyFacts.html"><span >DOI</span></a></span><span ><span > is just one of several types of metadata we register, albeit an important one.</span>  </span></span>   

<span >Metadata about scholarly works extends beyond the DOI.  In addition to bibliographic details, layers of information accompanying published works may now extend to data that describes the research, such as the source of research funding.  It may also include non-descriptive information that facilitates usage, such as copyright and access permissions.</span>

<span >In fact, this “richer” metadata can tell you more about the context of the content deposited for a published work than you might realize.  </span>

<span >For example:</span>

<li >
  <span ><b>Author data - </b><span >Crossref metadata may include information specifying the author’s unique </span><a href="https://web.archive.org/web/20160610063458/http://orcid.org/about/what-is-orcid/mission"><span >ORCID</span></a><span >, allowing you to find other works by the same person.  </span></span>
</li>

<li >
  <span ><b>Copyright and access indicators - </b><span >You can view the license terms under which the full content may be available, which is very helpful for scholars who want to access the full content for research and teaching or for text and data mining.  </span></span>
</li>

<li >
  <span ><b>Funding data - </b><span >Metadata may also include the </span><a href="/services/funder-registry/index.html"><span >identity of the grant-making institution</span></a><span > that funded the research, so that the funder and, in the case of publicly funded research, the general public and other researchers, have visibility on the resulting research outputs.  </span></span>
</li>

<li >
  <span ><b>Clinical Trials data - </b><span >Similarly, when research involves a clinical trial, (testing of medicines and treatments on human beings), Crossref metadata can enhance output visibility by displaying the </span><a href="/blog/linked-clinical-trials-are-here/"><span >clinical trial number and the related clinical trial registry</span></a><span >.     </span></span>
</li>

<span >Like the full content they describe, these metadata have become research resources in their own right.  Unfortunately, too much metadata is entered into Crossref with missing, incomplete, or duplicated fields.  This “bad” metadata slows the pace of discovery, confounding attempts to find and understand scholarly content and its context.  </span>

<span >As a community, we really need to do something about that.</span>

<span ><b>“The Map is not the Territory”</b></span>

<span ><span ><a href="/wp/blog/uploads/2016/06/scholarly-road-map.png"><img class="alignright wp-image-1751" src="/wp/blog/uploads/2016/06/scholarly-road-map-300x300.png" alt="scholarly-road-map" width="148" height="148" srcset="/wp/blog/uploads/2016/06/scholarly-road-map-300x300.png 300w, /wp/blog/uploads/2016/06/scholarly-road-map-150x150.png 150w, /wp/blog/uploads/2016/06/scholarly-road-map-768x768.png 768w, /wp/blog/uploads/2016/06/scholarly-road-map-1024x1024.png 1024w, /wp/blog/uploads/2016/06/scholarly-road-map.png 1200w" sizes="(max-width: 148px) 85vw, 148px" /></a>And the metadata is not the content.  In <a href="https://mitpress.mit.edu/books/metadata-0"><em>Metadata</em></a> (MIT Press), Jeffrey Pomerantz quotes Alfred Korzybski’s insight that a map is a simplified representation of a territory, a tool of abstraction that allows us to find our way.  Jennifer Lin contributed the concept of the scholarly road map as a useful metaphor for the way we use metadata about scholarly works to find our way between and among them in the digital world. </span></span>

<span >Metadata deposited with Crossref amounts to pieces of information-structured, descriptive, administrative, contextual-about published works that humans can read and machines can use to automate linking and retrieval.  The systematic development of such metadata allows us to make sense of such complex information by finding, linking, citing, and assessing scholarly content. </span>

<span ><span >If you want to understand how Crossref acts as a map of scholarly metadata, try searching for content on </span><span ><a href="https://web.archive.org/web/20131229210637/http://search.crossref.org//?q=zika+virus">search.crossref.org</a> (our human API interface)</span><span >.  Or simply talk with us <a href="https://twitter.com/CrossrefOrg"><strong>@CrossrefOrg</strong></a> and via  <a href="mailto:member@crossref.org">member@crossref.org</a>.  </span></span>
