---
title: '2019 election slate'
author: Lisa Hart Martin
draft: false
authors:
  - Lisa Hart Martin
date: 2019-08-23
categories:
  - Board
  - Member Briefing
  - Governance
  - Elections
  - Crossref Live
  - Annual Meeting
archives:
    - 2019
---

## 2019 Board Election

The annual board election is a very important event for Crossref and its members. The board of directors, comprising 16 member organizations, governs Crossref, sets its strategic direction and makes sure that we fulfill our mission. Our members elect the board - its "one member one vote" - and we like to see as many members as possible voting. We are very pleased to announce the 2019 election slate - we have a great set of candidates and an update to the ByLaws addressing the composition of the slate to ensure that the board continues to be representative of our membership.

## 2019 Election Slate

Crossref received 52 expressions of interest this year through the link that was sent out via our blog, and over 100 emails from members interested in serving on our Board. It is very exciting to see that our members want to be involved.

In March of this year, the Board made a motion per the recommendation of an adhoc Governance Committee. It was resolves to "provide the following guidance to the Nominating Committee: To achieve balance between revenue tiers by proposing a 2019 slate consisting of one Revenue Tier 1 seat and four Revenue tier 2 seats, and a 2020 slate consisting of four Revenue Tier 1 seats and two Revenue Tier 2 seats; thereby resulting in, as nearly as practicable, an equal balance between board members representing Revenue Tier 1 and Revenue Tier 2 (as those terms are defined in Crossref's ByLaws below)."

_Section 2._     Nominating Committee. The Board shall appoint a Nominating Committee of five (5) members, each of whom shall be either a Director or the designated representative of a member that is not represented on the Board, whose duty it shall be to nominate candidates for Directors to be elected at the next annual election. The Nominating Committee shall designate a slate of candidates for each election that is at least equal in number to the number of Directors to be elected at such election. Each such slate will be comprised such that, as nearly as practicable, one-half of the resulting Board shall be comprised of Directors designated by Members then representing Revenue Tier 1; and one-half of the resulting Board shall be comprised of Directors designated by Members then representing Revenue Tier 2.  "Revenue Tier 1" means all consecutive membership dues categories, starting with the lowest dues category, that, when taken together, aggregate, as nearly as possible, to fifty percent (50%) of Crossref's annual revenue. "Revenue Tier 2" means all membership dues categories above Revenue Tier 1. The Nominating Committee shall notify the Secretary in writing, at least twenty (20) days before the date of the annual meeting, of the names of such candidates, and the Secretary, except as herein otherwise provided, shall transmit a copy thereof to the last recorded address of each member of record simultaneously with the notice of the meeting.

The Committee and the Board has worked very hard to balance the Board, so you will see two categories on the ballot, large and small.

## The 2019 slate includes: seven candidates for five available seats

Candidate organizations, in alphabetical order, for the Small category (1 seat available):

* **eLife**, Melissa Harrison
* **The Royal Society**, Stuart Taylor

Candidate organizations, in alphabetical order, for the Large category (4 seats available):

* **Clarivate Analytics**, Nandita Quaderi
* **Elsevier**, Chris Shillum
* **IOP**, Graham McCann
* **Springer Nature**, Reshma Shaikh
* **Wiley**, Todd Toler

{{% divwrap blue-highlight %}}

### [Take a look at the candidates' organizational and personal statements](/board-and-governance/elections/2019-slate/)

{{% /divwrap %}}

## You can be part of this important process, by voting in the election

If your organization is a voting member in good standing of Crossref as of September 13, 2019, you are eligible to vote when voting opens on September 27, 2019.

## How can you vote?

On September 27, 2019, your organization's designated voting contact will receive an email with the Formal Notice of Meeting and Proxy Form with concise instructions on how to vote.  You will also receive a user name and password with a link to our voting platform.

The election results will be announced at [LIVE19 Amsterdam](/crossref-live-annual/) on November 13, 2019.  
