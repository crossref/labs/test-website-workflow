---
title: 'Express your interest in serving on the Crossref board'
author: Lisa Hart Martin
draft: false
authors:
  - Lisa Hart Martin
date: 2019-04-24
categories:
  - Board
  - Member Briefing
  - Governance
  - Election
archives:
    - 2019
---


The Crossref Nominating Committee is inviting expressions of interest to serve on the Board as it begins its consideration of a slate for the November 2019 election.

The board's purpose is to provide strategic and financial oversight and counsel to the Executive Director and the staff leadership team, with the key responsibilities being:

1.  Setting the strategic direction for the organization;
2.  Providing financial oversight; and
3.  Approving new policies and services.

The Board tends to review the strategic direction every few years, taking a landscape view of the scholarly communications community and trends that may affect Crossref's mission. In July 2017, the board and staff came up with four strategic themes and these have been developed into an [organization-wide roadmap](/strategy).

The board votes on any new policy or service that staff and committees propose if it is a departure from normal practice for Crossref.Some of the recent things the board has approved include:

* Approval of all the new terms of membership; broadening of the membership eligibility criteria to include non-publishers.

* Involvement in the ROR.org initiative including community outreach, technical prototyping, and helping to explore governance options.

* Approval of a proposal for funders to join at a reduced annual fee; the registration of DOIs for research grants.

* Allocating $50,000 USD of the operating budget to research the community's level of interest in a distributed usage service.

* Specifying the Board makeup to include equal numbers of small and large members; reframing the election processes.

### What is expected of a Crossref Board member?

Board members should be able to attend all board meetings, which occur three times a year in different parts of the world. If you are unable to attend in person you may send your named alternate as your proxy or be able to attend via telephone.

Board members must:

* be familiar with the three key responsibilities listed above;

* actively participate and contribute towards discussions; and

* read the board documents and materials provided, prior to attending meetings.

### How to submit an expression of interest to serve on the Board

We are seeking people who know about scholarly communications and would like to be part of our future. If you have experience on a governing board (as opposed to an operational board) and have a vision for the international Crossref community, we are interested in hearing from you.

If you are a Crossref member, are [eligible to vote](/blog/are-you-having-an-identity-crisis/), and would like to be considered, you can complete and submit the [expression of interest form](https://docs.google.com/forms/d/e/1FAIpQLSdwqraD2fjb3eqZgLpTQWsMYPQvvz4LARLq6k8H8mA7xGbZAw/viewform) with both your organization's statement and your personal statement before **21 May 2019**.

>It is important to note it is your organization who is the Crossref member---and therefore the seat will belong to your organization.

### About the election and our Board

We have a principle of ["one member, one vote"](/truths/); our board comprises a cross-section of members and it doesn't matter how big or small you are, every member gets a single vote. Board terms are three years, and one third of the Board is eligible for election every year. There are five seats up for election in 2019, 4 large and 1 small.

The board meets in a variety of international locations in March, July, and November each year. [View a list of the current Crossref Board members and a history of the decisions they've made (motions).](/board-and-governance/)

The slate will be decided by the Nominating Committee and interested parties will be informed if they have made the slate by July 15, 2019.

The election opens online in September 2019 and voting is done by proxy online, results will be announced at the annual business meeting during 'Crossref LIVE19' on 13th November 2019 in Amsterdam, Netherlands. Election materials and instructions for voting will be available online to all Crossref members in September 2019.

### The role of the Nominating Committee

The Nominating Committee meets to discuss change, process, criteria, and potential candidates, ensuring a fair representation of membership. The Nominating Committee is charged with selecting a slate of candidates for election from those who have expressed an interest.

The selection of the slate (which might exceed the number of open seats) is based on the quality of the expressions of interest and the nominating committee's review of the candidates in light of the board's directive of maintaining an appropriately balanced and representative board. The nominating committee will prioritize maintaining representation of members having both commercial and non-commercial business models, in addition to continuing to seek balance across factors such as gender, ethnic and racial background, geography, and sector.

The Board voted in March 2019 that balance according to size (based on revenue tier) will be achieved by a 2019 slate consisting of one revenue tier 1 seat (small) and 4 revenue 2 seats (large), and a 2020 slate consisting of 4 revenue tier 1 seats and 2 revenue tier 2 seats _(see [Crossref's amended Bylaws](/board-and-governance/bylaws) on the Crossref website)_.

The Committee is made up of three board members not up for election, and two non-board members. The current Nominating Committee members are:

* Jasper Simons, APA (Chair);

* Scott Delman ACM;

* Catherine Mitchell, CDL;

* Vincent Cassidy, The Institution of Engineering & Technology (IET); and

* Claire Moulton, The Company of Biologists.

Please [submit your expression of interest](https://docs.google.com/forms/d/e/1FAIpQLSdwqraD2fjb3eqZgLpTQWsMYPQvvz4LARLq6k8H8mA7xGbZAw/viewform) or reply to me with any questions at [lhart@crossref.org](mailto:lhart@crossref.org). This is your opportunity to help guide our wonderful organization!
