---
title: A Journey of a Crossref Ambassador in Latin America
author: Arley Soto
draft: false
authors:
  - Arley Soto
date: 2019-12-11
categories:
  - Members
  - Member Briefing
  - Community
archives:
  - 2019
---
_**English version** –– [Información en español](#spanishversion)_


In this post, Arley Soto shares some experiences about his work as a Crossref ambassador in Latin America.  

When I joined as a volunteer Crossref ambassador in 2018, I never imagined that in less than two years, I would have the opportunity to travel to three Latin American cities, visit Toronto, organize the first Crossref LIVE in Spanish and hold webinars in Spanish about Crossref's services. After almost two years of continuous learning, I think it is worth sharing my experience with the Crossref community for a better understanding of the ambassadors' role in Latin America and to inspire ambassadors from other parts of the world to write and post their experiences.

Before becoming a Crossref ambassador, I had already been working with Crossref since 2011, when we started to coordinate DOI registration for the Biomédica Journal of the National Health Institute, one of the first journals to implement the DOI in Colombia. During these first years of relations with Crossref, I acquired basic knowledge on membership and the technical aspects of the services the agency offers, including [Reference Linking](/services/reference-linking), [Content Registration](/services/content-registration) and [Crossmark](/services/crossmark). This close relationship with Crossref enabled us to hold the PKP-Crossref workshop in 2018 with Juan Pablo Alperín and Susan Collins at the [Third International Congress of Redalyc Editors at Universidad César Vallejo, city of Trujillo](https://web.archive.org/web/20200316022408/http://congreso.redalyc.org/ocs/public/congresoEditores/index.html).

In the same year, thanks to the invitation by the State University System (SUE, for the Spanish original) (Bogotá chapter), I had the opportunity to give a presentation on Crossref during the 2018 International Open Access Week held at Universidad Militar Nueva Granada. Around 50 people participated, including members and non-members of Crossref. There, I emphasized the nature of Crossref as a non-profit organization, based on affiliations and the importance of new members participating in the annual elections organized by Crossref and running to be representatives in the Crossref Board of Directors.

In November 2018, I had the pleasure of participating in the [Crossref Meeting in Toronto](https://www.youtube.com/playlist?list=PLe_-TawAqQj2QMxKbOmBs4WFHnIAK4iwn), thanks to an invitation from the organizers. There, I talked to the representatives of other organizations who are members of Crossref around the world and I also met some of the members of the Crossref team in person. This event was essential for me as an ambassador, because I learned about Crossref's vision and different projects firsthand, which increased my capacity to explain Crossref's scope and role in the area of scientific communications. I remember that the booth Crossref provided to answer technical questions was particularly useful. There, Isaac, Shayn and other members of the technical team were always available to resolve specific queries that I had not been able to resolve before myself.

In my second year as an ambassador, I represented Crossref at the Universidad Central del Ecuador (Quito, Ecuador), in a talk with an average of 40 people from different parts of Ecuador. There, I emphasized the technical aspects of the DOI and good practices for its use in academic publications. This talk was held on April 21, 2019, in collaboration with Crossref and [BITECA S.A.S](http://biteca.com/)., a [sponsoring member](/community/sponsors) of Crossref.  

{{% imagewrap center %}}
{{< figure src="/images/blog/2019/arley-biteca-blog.jpg" alt="images of Arley Soto presenting" width="70%" >}}
{{% /imagewrap %}}

In May 2019, with Susan Collins and Vanessa Fairhurst, we organized [Crossref LIVE Bogotá](/events), which was not only successful because of the number of attendees from different parts of Colombia and other countries in the region, but also due to the meeting of Latin American ambassadors, where we worked the full morning discussing the priorities and issues of the region with ambassadors from Brazil, Mexico, Chile and Peru. Apart from other issues, at this meeting, it became clear the need to have better resources and support in Spanish for Spanish-speaking members.

Additionally, we helped to review the Spanish translation of the "You are Crossref" booklet, which we printed and distributed at Crossref LIVE Bogotá.

During 2019, I participated in the [Introduction to Crossref and Content Registration](https://www.slideshare.net/CrossRef/introduction-to-crossref-and-content-registration-in-spanish) and [Introduction to Reference Linking and Cited-by webinar](https://www.slideshare.net/CrossRef/reference-linking-and-cited-by-in-spanish) webinars and held the first webinar in Spanish about the new [Metadata Manager](/education/member-setup/metadata-manager/) tool, always with the ongoing support and assistance of the Crossref team.

And to end the year with a bang, together with Rachael Lammey, we organized the presentation: Open infrastructure and open data for the global metrics community: what can you build? I presented this at the [2Latmetrics: Altmetrics and Open Science in Latin America colloquium on November 4 in the city of Cusco (Peru).](https://www.latmetrics.com/)

{{% imagewrap center %}}
{{< figure src="/images/blog/2019/arley-blog-3.jpg" alt="image of people on the panel" width="70%" >}}
{{% /imagewrap %}}

This account of activities is a demonstration of the commitment of Crossref's ambassadors to transmit the message of the importance of ethically and responsibly sharing, citing and making science visible on the web.

<a id="spanishversion"></a>
_**Spanish version**_

Cuando me vinculé como embajador voluntario de Crossref en 2018, no imaginaba que en menos de dos años tendría la oportunidad de viajar a 3 ciudades en Latinoamérica, conocer Toronto, organizar el primer Crossref LIVE en español y realizar webinars en español sobre los servicios de Crossref. Después de casi dos años de continuo aprendizaje, creo que vale la pena compartir mi experiencia a la comunidad de Crossref para entender mejor el rol de los embajadores en Latinoamérica y para inspirar embajadores de otras regiones del mundo a que escriban y publiquen sus experiencias.

Antes de convertirme en embajador de Crossref ya había trabajado con Crossref desde el año 2011, año en el que empezamos a gestionar DOI para la revista Biomédica del Instituto Nacional de Salud, una de las primeras revistas en implementar DOI en Colombia. Durante esos primeros años de relaciones con Crossref, adquirí un conocimiento básico sobre las membresías y los aspectos técnicos de los servicios que la agencia ofrece, incluyendo el [Reference Linking](/services/reference-linking), [Content Registration](/services/content-registration) y [Crossmark](/services/crossmark), entre otros. Esta relación estrecha con Crossref favoreció para que en 2018 realizáramos el taller de PKP - Crossref entre Juan Pablo Alperín y Susan Collins en el [3er Congreso Internacional de Editores Redalyc, en la Universidad César Vallejo, ciudad de Trujillo ](https://web.archive.org/web/20200316022408/http://congreso.redalyc.org/ocs/public/congresoEditores/index.html)

En ese mismo año, gracias a la invitación realizada por el Sistema Universitario Estatal, SUE (capítulo Bogotá) tuve la oportunidad de hacer una presentación de Crossref en la Semana Internacional de Acceso Abierto 2018, realizado en Universidad Militar Nueva Granada 2018, allí participaron alrededor de 50 personas entre miembros y no miembros de Crossref, aquí hice énfasis en la naturaleza de Crossref como organización sin ánimo de lucro, basada en afiliaciones y la importancia de que los nuevos miembros participen en las votaciones anuales que organiza Crossref y que se postulen para ser representantes en la junta directiva de Crossref.

En noviembre de 2018 tuve el placer de participar en el [Crossref Meeting en la ciudad de Toronto](https://www.youtube.com/playlist?list=PLe_-TawAqQj2QMxKbOmBs4WFHnIAK4iwn), gracias a una invitación de los organizadores. Allí conversé con representantes de  otras organizaciones afiliadas a Crossref alrededor del mundo y también conocí en persona a algunos de los integrantes del equipo de Crossref. Este evento fue de vital importancia para mí como embajador ya que conocí de primera mano la visión y los diferentes proyectos que realiza Crossref, lo que aumentó mi capacidad para explicar en mi contexto el alcance y el papel de Crossref en el entorno de la comunicación científica. Recuerdo que fue particularmente útil el kiosco que dispuso Crossref para atender inquietudes técnicas en donde Isaac, Shane y otros miembros del equipo técnico siempre estuvieron dispuestos a solucionar dudas específicas que no había podido resolver antes por mi mismo.

En el segundo año como embajador representé a Crossref en la Universidad Central del Ecuador (Quito, Ecuador), charla a la que asistieron en promedio 40 personas de diversos lugares del Ecuador, allí hice énfasis en los aspectos técnicos del DOI y buenas prácticas de su utilización en publicaciones académicas.. Esta charla tuvo lugar el 21 de abril de 2019 y la realizamos en colaboración con Crossref y [BITECA SAS](https://www.biteca.com/) miembro [patrocinador en Crossref](/community/sponsors).

En mayo de 2019 organizamos junto con Susan Collins y Vanessa Fairshuit el [Crossref LIVE Bogotá](/events), que no solamente fue exitoso por la cantidad de asistentes de diferentes partes de Colombia y de otros países de la región, sino por la reunión de embajadores de Latinoamérica, donde trabajamos una mañana completa para discutir acerca de las prioridades y temáticas propias de la región con embajadores de Brasil, México, Chile y Perú. Entre otros asuntos, en esta reunión se hizo evidente la necesidad de tener mayores recursos y soporte en Español para los miembros hispanohablantes.

Así mismo contribuimos con la revisión de la traducción al español de la cartilla "Usted es Crossref" que imprimimos y repartimos durante el Crossref LIVE Bogotá.

Durante 2019 participé en los webinars [Introduction to Crossref and Content Registration](https://www.slideshare.net/CrossRef/introduction-to-crossref-and-content-registration-in-spanish) y [Introduction to Reference Linking and Cited-by webinar](https://www.slideshare.net/CrossRef/reference-linking-and-cited-by-in-spanish) y llevé a cabo el primer Webinar en español sobre la nueva herramienta Metadata Manager, siempre con el acompañamiento y el soporte permanente del equipo de Crossref.

Y para terminar el año de la mejor manera, preparamos junto con Rachael Lammey la ponencia Open infrastructure and open data for the global metrics community: what can you build? Que presenté en el congreso [2Latmetrics: métricas alternativas y ciencia abierta en américa latina el 04 de noviembre en la ciudad de Cusco (Perú).](https://www.latmetrics.com/)

Este recuento de actividades es una muestra del compromiso de los embajadores de Crossref en transmitir el mensaje de la importancia de compartir, citar y hacer visible la ciencia en la web, de una manera ética y responsable.
