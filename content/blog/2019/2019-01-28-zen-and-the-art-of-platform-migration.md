---
title: 'Zen and the Art of Platform Migration'
author: Amanda Bartell
draft: false
authors:
  - Amanda Bartell
date: 2019-01-28
categories:
  - Collaboration
  - Persistence

archives:
    - 2019
---

Nowadays we’re all trying to eat healthier, get fitter, be more mindful and stay in the now. You think you’re doing a good job — perhaps you’ve started a yoga class or got a book on mindfulness. And then, wham! Someone in your organization casually mentions they’re planning a platform migration. I can sense the panic from here.


<!--more-->

While the [Holmes and Rahe Stress Scale] (https://www.stress.org/holmes-rahe-stress-inventory/) doesn’t include platform migration as one of the top ten most stressful life events, we hear from our members that it should probably be in there somewhere. There’s so much to think about and plan for - how do you know you’re choosing the right platform partners for the future? How can you be sure that your understanding of what they offer really matches what you need? Will it make it easier for your readers to access your content? What about delays? What if it all breaks on changeover day?

Gaaaaah!

With all that to think about, worrying about whether your DOIs will resolve and what the migration will mean for the quality of your Crossref metadata just seems like an unnecessary layer of stress. It is, however, very important to consider this - even before you start thinking about who your platform partners will be. The process of working through these things up front could help you make better decisions, and set you up for success with the project and into the future.  

So, to help you plan ahead, we’ve created a [platform migration guide] (/service-providers/migrating-platforms/) that offers guidance on things like:

- What to consider even before you start selecting a new service provider
- Planning the change over process
- The change over itself (and what that means for your URLs)
- What you should do after the migration is complete

The guide gives advice on how to plan for what you really need right now, and what you’re going to need in the future. For example, what metadata are you going to want to register with us and share with the thousands of industry organizations that make use of the data? What other Crossref services might benefit you in the future? What different content types are in your publishing plans?

The guide also has a [handy checklist] (/education/member-setup/working-with-a-service-provider/checklist-for-platform-migration/) which you can include in your Request For Proposal documentation, to ensure that you’re asking the right questions of potential suppliers.

Once you’ve read the [platform migration guide] (/service-providers/migrating-platforms/), [let us know] (mailto:feedback@crossref.org) if there’s anything else you think we should add to it - we’re sure many of you have platform migration stories, and it’s good to share!



___
