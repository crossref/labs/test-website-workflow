---
title: Crossref OpenURL resolver
author: Chuck Koscher
authors:
  - Chuck Koscher
date: 2009-07-07

categories:
  - OpenURL
archives:
  - 2009

---
A new version of our OpenURL resolver was deployed July 2 which should handle higher traffic (e.g. we have re-enable the LibX plug-in ) Unfortunately there were a few hick ups with the new version which I believe are now corrected (a character encoding bug and a XML structure translation problem).

Sorry for any inconvenience.
