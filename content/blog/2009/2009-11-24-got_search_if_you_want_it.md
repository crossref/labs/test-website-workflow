---
title: got SEARCH if you want it!
author: thammond
authors:
  - thammond
date: 2009-11-24

categories:
  - Search
archives:
  - 2009

---
[See this link if you’re short on time: [facets][1] search client. Only tested on Firefox at this point. **Caveat:** At time of writing the Crossref Metadata Search was being _very_ slow but was still functional. Previously it was just slow.]

Following on from Geoff’s [announcement][2] last month of a prototype Crossref Metadata OpenSearch on [labs.crossref.org][3], I wanted to show what typical OpenSearch responses might look like in a more mature implementation.

I have taken the liberty of modelling these on the response formats that we are already providing in our nature.com OpenSearch service which in turn are based on the draft syndication formats that I [blogged here][4] earlier.

I am therefore returning ATOM, JSON, JSONP and RSS responses from these four OpenSearch URL templates:

  * http://nurture.nature.com/cgi-bin/opensearch?db=crossref&#038;out=atom&#038;q={searchTerms}
      * http://nurture.nature.com/cgi-bin/opensearch?db=crossref&#038;out=json&#038;q={searchTerms}
          * http://nurture.nature.com/cgi-bin/opensearch?db=crossref&#038;out=jsonp&#038;q={searchTerms}
              * http://nurture.nature.com/cgi-bin/opensearch?db=crossref&#038;out=rss&#038;q={searchTerms}</ul>
                as this [OpenSearch description][5] file details. Note that the URL templates include no indexing or pagination parameters as the Crossref prototype does not currently support these features.

                An example query (&#8216;apple’) returning an ATOM feed from a Crossref Metadata OpenSearch would be the following:

                  * <http://nurture.nature.com/cgi-bin/opensearch?db=crossref&#038;out=atom&#038;q=apple></ul>
                    And the same query returning a JSON version of that ATOM feed would look as follows:

                      * <http://nurture.nature.com/cgi-bin/opensearch?db=crossref&#038;out=json&#038;q=apple></ul>
                        By the way, this is just for demonstration purposes and there are still issues to be resolved including character encoding.

                        This interface uses the existing Crossref OpenSearch response format and parses the [COinS][6] objects embedded in that response to provide a more standard OpenSearch syndication result set format. The prototype implemenatation also has some bugs which I needed to work around. (I will forward on details of these.) And there is also a more fundamental issue of response time from the experimental search server.

                        But still this should give some idea of what a Crossref Metadata OpenSearch service could look like.

                        To show this all in action I’ve worked up one of my [demo OpenSearch clients][1] for [nature.com OpenSearch][7] which displays a facetted search response for a Crossref search. For good measure this includes also an OpenSearch interface for PubMed and the search client allows for simple selection between three journals databases: nature.com, Crossref and PubMed.

                        Of course, with a reasonably uniform set of search result formats such as presented here it then becomes a simple exercise to reuse these search responses in additional search clients.

                        As can be anticipated it would be very straightforward to carry this over into a single metasearch service which could run across these multiple databases.

 [1]: http://nurture.nature.com/opensearch/apps/client-facets.html
 [2]: /blog/crossref-labs/
 [3]: http://labs.crossref.org/
 [4]: /blog/opensearch-formats-for-review/
 [5]: http://nurture.nature.com/opensearch/xml/opencrossref.xml
 [6]: https://web.archive.org/web/20090927174724/http://ocoins.info/
 [7]: http://www.nature.com/opensearch/
