---
title: CURIE Syntax 1.0
author: thammond
authors:
  - thammond
date: 2009-01-19

categories:
  - Identifiers
archives:
  - 2009

---
The W3C has recently (Jan. 16) released [CURIE Syntax 1.0][1] as a Candidate Recommendation and is inviting implementations.

(Note that I made a fuller post [here][2] on CURIEs and erroneously confused the [Editor’s Draft (Oct. 23, ’08)][3] as being a Candidate Recommendation. Well, at least it’s got there now.)

 [1]: http://www.w3.org/TR/2009/CR-curie-20090116/
 [2]: /blog/curies-a-cure-for-uris/  
 [3]: http://www.w3.org/MarkUp/2008/ED-curie-20081023/
