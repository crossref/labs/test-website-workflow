---
title: A Cheatsheet for nature.com OpenSearch
author: thammond
authors:
  - thammond
date: 2009-10-22

categories:
  - Search
archives:
  - 2009

---
<img alt="opensearch-cheatsheet-fragment.png" src="/wp/blog/images/opensearch-cheatsheet-fragment.png" width="328" height="267" />
  
Following on from my [recent post][1] about our shiny new [nature.com OpenSearch][2] service we just put up a cheatsheet for users. I’m posting about this here as this may also be of interest especially to those exploring how SRU and OpenSearch intersect.
  
The cheatsheet can be downloaded from our nature.com OpenSearch [test page][3] and is available in two forms:

  * [Cheatsheet (PDF, 65K)][4] 
      * [Cheatsheet (PNG, 141K)][5]</ul> 
        Naurally, all comments welcome.

 [1]: /blog/nature.com-opensearch-a-structured-search-service/
 [2]: http://www.nature.com/opensearch
 [3]: http://nurture.nature.com/opensearch
 [4]: http://nurture.nature.com/opensearch/docs/opensearch-cheatsheet.pdf
 [5]: http://nurture.nature.com/opensearch/docs/opensearch-cheatsheet.png