---
title: 'OCLC defines requirements for a &#8220;Cooperative Identities Hub&#8221;'
slug: 'oclc-defines-requirements-for-a-cooperative-identities-hub'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-05-01

categories:
  - ORCID
archives:
  - 2009

---
OCLC has [published a report][1] (PDF) identifying some requirements for what they call a &#8220;Cooperative Identities Hub&#8221;. A quick glance through it seems to show that the use cases focus on what we are calling the &#8220;Knowledge Discovery&#8221; use cases. As I mentioned in my [interview with Martin Fenner][2], there is also a category of &#8220;authentication&#8221; use cases that I think needs to be addressed by a contributor identifier system. Still, this is a good report that highlights many of the complexities that an identifier system needs to address.

 [1]: https://web.archive.org/web/20101210071719/http://www.oclc.org/research/publications/library/2009/2009-05.pdf
 [2]: https://web.archive.org/web/20091225201433/http://network.nature.com/people/mfenner/blog/2009/02/17/interview-with-geoffrey-bilder
