---
title: 'Crossref’s OpenURL query interface'
author: Chuck Koscher
authors:
  - Chuck Koscher
date: 2009-05-06

categories:
  - OpenURL
  - APIs
archives:
  - 2009

---
Over the past two weeks we’ve focused on our OpenURL query interface with the goal being to improve its reliability. I’d like to mention some things we’ve done.

1) We now require an OpenURL account to use this interface (see  [the registration page][1]) . This account is still free, there are no fixed usage limits, and the terms of use have been greatly simplified.

2) Resources have been re-arranged dedicating more horse-power to the OpenURL function.

3) The OpenURL function is now in our advanced monitoring function which means some lucky staff member will be getting phone calls at 3AM (me included!).

I should note that #1 has already reduced inappropriate usage. This also is not the end of planned changes. Crossref has undertaken a major rewrite of parts of our system and this will include the OpenURL interface.

Chuck

 [1]: https://apps.crossref.org/requestaccount/
