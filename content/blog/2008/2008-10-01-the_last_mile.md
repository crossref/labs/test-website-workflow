---
title: The Last Mile
author: thammond
authors:
  - thammond
date: 2008-10-01

categories:
  - Handle
archives:
  - 2008

---
[<img alt="last-mile.png" border="0" src="/wp/blog/images/last-mile.png" width="357" height="252" />][1]

The figure above (click to enlarge) is probably self-explanatory but a few words may be in order.

With no end-to-end delivery of data from the Handle System to the user’s application (browser or reader), getting data out of the Handle System has traditionally meant using the Web (ie. HTTP) as a courier - in effect, this is the &#8220;[last mile][2]&#8221; for Handle data. Typically an upstream (Handle) client provides services to the user. The most well known of these services is the URL redirect service which underpins the Crossref reference linking service. Another hosted service is the web form which displays data stored in the Handle records in a simple HTML table for user browsing. See panel a) in the figure above.

By contrast, the OpenHandle proposal aims to move data in the Handle record in structured form (JSON or XML) over the Web for downstream processing - either in the user’s browser or on the desktop. See panel b). Advantages are that the Handle data and data structures are moved closer to the user and the services provided are capable of being better targeted and made more relevant. Data mobility as a whole is much improved. The data are accessible using standard Web description and scripting languages. One might almost say (to paraphrase the well-known Java slogan &#8220;[write once, run anywhere][3]&#8220;) that this is a case of &#8220;read once, write anywhere&#8221;.

 [1]: /wp/blog/images/last-mile.png
 [2]: http://en.wikipedia.org/wiki/Last_mile
 [3]: http://en.wikipedia.org/wiki/Write_once,_run_anywhere
