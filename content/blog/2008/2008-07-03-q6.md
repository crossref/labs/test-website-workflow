---
title: Q6
author: thammond
authors:
  - thammond
date: 2008-07-03

categories:
  - Linking
archives:
  - 2008

---
For anybody interested in the why’s and wherefore’s of OpenURL, Jeff Young at OCLC has started posting over on his blog Q6: 6 Questions - A simpler way to understand OpenURL 1.0: Who, What, Where, When, Why, and How (note: no longer available online). He’s already amassing quite a collection of thought provoking posts. His latest is The Potential of OpenURL (note: no longer available online), from which:

*OpenURL has effectively cornered the niche market where Referrers need to be decoupled from Resolvers.*

Blog has UML diags, definitions, musings, etc. - something for everybody. Definitely worth checking out.
