---
title: 'BISG Paper on  Identifying Digital Book Content'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2008-01-14

categories:
  - Identifiers
archives:
  - 2008

---
BISG and BIC have published a discussion paper called &#8220;The identification of digital book content&#8221; - <https://web.archive.org/web/20090920075334/http://www.bisg.org/docs/DigitalIdentifiers_07Jan08.pdf>. The paper discusses ISBN, ISTC and DOI amongst other things and makes a series of recommendations which basically say to consider applying DOI, ISBN and ISTC to digital book content. The paper highlights in a positive way that DOI and ISBN are different but can work together (the idea of the &#8220;actionable ISBN&#8221; and aiding discovery of content). However, it doesn’t go into much depth on any of the issues or really explain how all these identifiers would work together and the critical role that metadata plays.
  
Nevertheless it’s great that the paper has been put forward as a discussion document - Crossref plans to respond and be part of the ongoing discussion in this area.