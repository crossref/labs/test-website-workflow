---
title: Xmas XMP
author: thammond
authors:
  - thammond
date: 2008-12-19

categories:
  - XMP
archives:
  - 2008

---
Well, as I [blogged][1] on our web publishing blog [Nascent][2] we just went live with XMP labelling on _Nature_ in yesterday’s double issue. We will be adding XMP to all new issues of _Nature_ as well as rolling out across all our other titles in the next few weeks and months.

The screenshots below from Acrobat (_File > Properties_, `CMD-D` / `CTL-D`) show what the user might see both with (bottom-left) and without (top-right) semantic markup.

<img alt="pdf_props.png" src="/wp/blog/images/pdf_props.png" width="399" height="377" />

As to the actual contents of the metadata record, see [this sample][3] I posted to the semantic web list.

 [1]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2008/12/xmp_labelling_for_nature.html
 [2]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/
 [3]: http://lists.w3.org/Archives/Public/semantic-web/2008Dec/0134.html
