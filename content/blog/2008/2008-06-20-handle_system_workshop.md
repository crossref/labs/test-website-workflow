---
title: Handle System Workshop
author: thammond
authors:
  - thammond
date: 2008-06-20

categories:
  - Meetings
archives:
  - 2008

---
<img alt="charlemagne.jpg" src="/wp/blog/images/charlemagne.jpg" width="275" height="134" />
  
I was invited to speak at the [Handle System Workshop][1] which was run back to back with an [IDF Open Meeting][2] earlier this week in Brussels and hosted at the Office for Official Publications of the European Union. (Location was in the Charlemagne Building, at left in image, within the rather impressive meeting room Jean Durieux, at right.)
  
My talk (&#8216;[A Distributed Metadata Architecture][3]&#8216;) was focussed on how [OpenHandle][4] and [XMP][5] could be leveraged to manage dispersed media assets. (The [OpenHandle][4] work makes the Handle and DOI systems more readily acessible to applications.)
  
Other speakers were Norman Paskin (IDF), Gordon Dunsire (Centre for Digital Library Research, University of Strathclyde), Brian Green (Editeur), Jill Cousins (European Digital Library Foundation), Jan Brase (TIB, Germany), Larry Lannom (CNRI), Ed Pentz (Crossref), Nigel Ward (Link Affiliates), and Dan Broeder (CLARIN/MPG).
  
The agendas for the two meetings are posted [here][2] (DOI) and [here][1] (Handle).

 [1]: http://www.handle.net/workshop_08/
 [2]: http://www.doi.org/doi_presentations/members_meeting_2008/
 [3]: http://www.handle.net/workshop_08/presentations/Hammond_Handle08.ppt
 [4]: http://code.google.com/p/openhandle/
 [5]: http://www.adobe.com/products/xmp/