---
title: 'Describing Resource Sets: ORE vs POWDER'
author: thammond
authors:
  - thammond
date: 2008-12-04

categories:
  - Linking
archives:
  - 2008

---

I’ve been reading up on [POWDER][1] recently (the W3C Protocol for Web Description Resources) which is currently in last call status (with comments due in tomorrow). This is an effort to describe groups of Web resources and as such has clear similarities to the Open Archives Initiative [ORE][2] data model, which has been blogged about here before.

In an attempt to better understand the similarities (and differences) between the two data models, I’ve put up the table which directly compares the two heavyweight contendors OAI-ORE and POWDER and also (unfairly) places them alongside the featherweight [Sitemaps Protocol][5] for reference.

This is very much a draft document and I will aim to update the table based on my own further reading and on any feedback that I may get (contributions gratefully received). I’m all too aware that my understanding of the respective data models is painfully limited and I, for one, hope to profit through this exercise. There will be certainly errors which I will aim to fix as soon as I get wind of them. 🙂

By the way, the ORE work especially is of interest to Crossref members and has obvious synergies with the multiple resolution potential that DOI has long promised but not quite delivered on.

 [1]: http://www.w3.org/2007/powder/
 [2]: http://www.openarchives.org/ore/
 [3]: https://www.crossref.org/blog
 [4]: https://web.archive.org/web/20100901074832/http://nurture.nature.com/tony/blogs/crosstech/ore-pwdr.html
 [5]: http://www.sitemaps.org/protocol.php
