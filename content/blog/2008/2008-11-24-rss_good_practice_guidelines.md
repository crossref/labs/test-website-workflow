---
title: RSS Good Practice Guidelines
author: thammond
authors:
  - thammond
date: 2008-11-24

categories:
  - RSS
archives:
  - 2008

---
I just wanted to flag up here Lisa Rogers’ recent review article on RSS in FUMSI (the online magazine for information professionals published by Free Pint Ltd)

[RSS and Scholarly Journal Tables of Contents: the ticTOCs Project, and Good Practice Guidelines for Publishers][1]

Especially of interest is the diagram in Fig. 2 which breaks out the metadata elements that might be encountered in a rich web feed. Worthwhile pointing out that this reflects current practice and that under the item elements one would soon hope to see publishers routinely adding in **prism:doi** (with the bare DOI as value) and **prism:url** (with DOI target URL as value) from the PRISM 2.0 vocabulary published earlier this year. Publishers should also be aware of the new PRISM Usage Rights vocabulary which is expected to be published some time in the new year.

 [1]: https://web.archive.org/web/20081102075322/http://web.fumsi.com/go/article/share/3356
