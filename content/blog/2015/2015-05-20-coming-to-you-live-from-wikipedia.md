---
title: Coming to you Live from Wikipedia
author: Joe Wass
authors:
  - Joe Wass
date: 2015-05-20

categories:
  - Wikipedia
archives:
  - 2015

---
We’ve been collecting [citation events from Wikipedia][1] for some time. We’re now pleased to announce a [**live stream of citations**][2], as they happen, when they happen. Project this on your wall and watch live DOI citations as people edit Wikipedia, round the world.

## [**View live stream »**][2]

In the hours since this feature launched, there are events from Indonesian, Portugese, Ukrainian, Serbian and English Wikipedias (in that order).<!--more-->

<img src="/wp/blog/uploads/2015/05/Screen-Shot-2015-05-20-at-16.30.00-1024x760.png" class="img-responsive" alt="Live event stream" >

The usual weasel words apply. This is a labs project and so may not be 100% stable. If you experience any problems please email labs@crossref.org .

 [1]: /blog/real-time-stream-of-dois-being-cited-in-wikipedia/
 [2]: https://web.archive.org/web/20150422055509/http://events.labs.crossref.org/events/types/WikipediaCitation
