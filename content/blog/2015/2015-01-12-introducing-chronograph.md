---
title: Introducing the Crossref Labs DOI Chronograph
author: Joe Wass
slug: "/introducing-chronograph"
authors:
  - Joe Wass
date: 2015-01-12

categories:
  - Citation
  - Crossref Labs
  - Event Data
  - Linking
  - Metadata
  - Wikipedia
tags:
  - chronograph
archives:
  - 2015

---
tl;dr <http://chronograph.labs.crossref.org>

At Crossref we mint DOIs for publications and send them out into the world, but we like to hear how they’re getting on out there. Obviously, DOIs are used heavily within the formal scholarly literature and for citations, but they’re increasingly being used outside of formal publications in places we didn’t expect. With our DOI Event Tracking / ALM pilot project we’re collecting information about how DOIs are mentioned on the open web to try and build a picture about new methods of citation.<!--more-->

As part of the [preparation for collaborating with Wikipedia][1], we looked at our statistics about when DOIs are clicked and discovered that Wikipedia was, over a two year period from 2012, the eighth largest referrer of DOIs. This means that not only does Wikipedia have a lot of DOIs, but people click them too. This bit of one-off data analysis (which surprised us) gave us enough of a prod to kickstart our collaboration with Wikipedia.

At the [ALM Workshop 2014 in San Francisco][2] we talked to some Wikipedians and bibliometricians and realised that we were sitting on a really interesting data-set and that it would be churlish not to share it. At the hackathon ([read the report here][3]) we started work on a service to gather information about DOIs and, a month later, we’re ready to unveil the DOI Chronograph.

**Show me the goods**

You can see:

Daily referrals (clicks) from top level domains, e.g. Wikipedia.org: <http://chronograph.labs.crossref.org/domain.html?domain=wikipedia.org>

<img src="/wp/blog/uploads/2015/01/wikipedia-referrals.png" alt="wikipedia-referrals" class="img-responsive" />

Daily referrals from specific subdomains, e.g. fr.wikipedia.org: <http://chronograph.labs.crossref.org/domain.html?domain=fr.wikipedia.org>

<img src="/wp/blog/uploads/2015/01/fr-wikipedia-referrals.png" class="img-responsive" />

  Daily resolutions per DOI: <http://chronograph.labs.crossref.org/doi.html?doi=10.1787%2F20752288>

<img src="/wp/blog/uploads/2015/01/doi-referrals.png" alt="doi-referrals" class="img-responsive"/>

<a name="ranking"></a>

And, the chart that kicked this all off: DOI referring domains league tables. This shows that Wikipedia is the 3rd or 4th non-traditional referrer of DOIs (i.e. excluding referrals from Publishers’ domains): <http://chronograph.labs.crossref.org/top.html>

<img src="/wp/blog/uploads/2015/01/top-domains.png" alt="top-domains" class="img-responsive" />

**Try it out**

Visit the Chronograph and give it a try [chronograph.labs.crossref.org][4] on your [favourite DOI][5] ([everyone][6] [has][7] [one][8]).

**More data**

Talking to a bibliometrician we also realised we can correlate other data for DOIs. We’re getting the issue date (approximately the publication date) from our own metadata, as well as the date that the Crossref metadata was updated. This gives interesting results, like [the resolutions for 10.1038/ncomms2953][9], which peak after publication and then tails off. We are attempting to collect the following information:

  * daily resolution counts
  * day on which resolution was first successful
  * day on which it’s possible to resolve the DOI (we’ve got a bot running for new publications)
  * day on which the publisher says the article was published
  * day on which the metadata was most recently deposited with us
  * day on which the metadata was first deposited with us

We’re not there yet, but we’ve made a start and we’ve already got some pretty interesting data!

**Weasel words**

It’s a labs project so the usual weasel words apply. Specifically, we currently have the logs for 2012 to 2014 (we’re working at digging out the rest), and the referral information for 50 million DOIs (out of 71 million). That number will be higher by the time you read this. If your page is slow to load, be patient, as it’s currently working hard crunching numbers.

This project is focused on exploring the use of DOIs outside of the formal literature. As such, we are only looking at referrals from domains that do not appear to belong to primary publishers (i.e. our members). If you try a domain and it doesn’t work, it could be that the domain belongs to one of our members. If you’ve notice any mistakes, please email us at labs@crossref.org .

Finally, these numbers contain all DOI resolutions. That’s human clicks but also content negotiation to retrieve metadata, robots etc. We might try to filter them in future, but for now be aware that not every visitor is a human.

I’ll detail some of the the technical stuff (it’s very interesting) and what happened next with Wikipedia in a future post. Watch this space.

 [1]: /blog/many-metrics-such-data-wow
 [2]: /
 [3]: http://dx.doi.org/10.6084/m9.figshare.1287503
 [4]: http://chronograph.labs.crossref.org
 [5]: http://chronograph.labs.crossref.org/doi.html?doi=10.1657%2F1938-4246-44.4.483
 [6]: http://chronograph.labs.crossref.org/doi.html?doi=10.1007%2Fs12110-002-1021-6
 [7]: http://chronograph.labs.crossref.org/doi.html?doi=10.1136%2Fbmj.327.7429.1459
 [8]: http://chronograph.labs.crossref.org/doi.html?doi=10.1016/j.imavis.2011.05.002
 [9]: http://chronograph.labs.crossref.org/doi.html?doi=10.1038%2Fncomms2953
