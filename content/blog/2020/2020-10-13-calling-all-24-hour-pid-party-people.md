---
title: 'Calling all 24-hour (PID) party people!'
author: Kathleen Luschek
draft: false
authors:
  - Kathleen Luschek
date: 2020-10-13
categories:
  - PIDapalooza
  - Persistence
  - Identifiers
  - Collaboration
  - Community
  - Meetings
Archives:
  - 2020
---

While we wish we could be together in person to celebrate the fifth PIDapalooza, there's an upside to [moving it online](https://pidapalooza.us18.list-manage.com/track/click?u=3be6c9ed55c4e452e710b2d41&id=e88a641bb4&e=8567777e89): now _everyone_ can participate in the universe's best PID party! With 24 hours of non-stop PID programming, you'll be able to come to the party no matter where you happen to be.  

{{% imagewrap center %}}{{< figure src="/images/blog/2020/pid-blog-dance-image.png" alt="Pidapalooza dancing graphic" width="70%" >}}{{% /imagewrap %}}

### Send us your ideas for #PIDapalooza21

Now is your chance to share your work in the #PIDapalooza21 spotlight! We're seeking proposals for short, interactive sessions about what you are doing––or want to do––with persistent identifiers and the communities that love and use them.   

[#PIDapalooza21](https://twitter.com/hashtag/PIDapalooza21) will feature sessions around the broad theme of PIDs and Open Research Infrastructure, focusing on the following areas:

### Theme 1. PIDs 101

For PID beginners! You've got just 30 minutes to get attendees up to speed on a PID or PIDs. Make it fast! Make it fact-filled! Make it fun!

### Theme 2. PID Communities International

Have you always wanted to host a Spanish-language PID session, or bring together PID people in the humanities? Tell us how you'd connect with PID peers around the world!

### Theme 3. PID Success Stories

There's nothing better than hearing about what's working in the PID world––and why! Share your success stories so we can all benefit from them.

### Theme 4. PID Party!

It wouldn't be PIDapalooza without the party sessions, so be creative! Help us make this the best PID party ever!

{{% divwrap blue-highlight %}} ### [Propose a session now!](https://docs.google.com/forms/d/e/1FAIpQLSflQyhg_FN6qU-20dZSnfGnmAZoKn5JsJaHcuDRYlpyvQTp-g/viewform){{% /divwrap %}}  
<br/>

The call for proposals will be open until October 30. Submit your PIDea now!  

_*Note: The PIDapalooza submission form uses Google. If you are unable to access Google Forms, [email your session idea](mailto:info@pidapalooza.org)._  

Get the full low-down on #PIDapalooza21 at the [PIDapalooza website](https://pidapalooza.us18.list-manage.com/track/click?u=3be6c9ed55c4e452e710b2d41&id=07e26525f0&e=8567777e89).
