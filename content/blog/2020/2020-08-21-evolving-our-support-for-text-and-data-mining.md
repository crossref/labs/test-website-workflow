---
title: 'Evolving our support for text-and-data mining'
author: Bryan Vickery
draft: false
authors:
  - Bryan Vickery
date: 2020-08-21
categories:
  - Text and data mining


archives:
    - 2020
---


Many researchers want to carry out analysis and extraction of information from large sets of data, such as journal articles and other scholarly content. Methods such as screen-scraping are error-prone, place too much strain on content sites and may be unrepeatable or break if site layouts change. Providing researchers with automated access to the full-text content via DOIs and Crossref metadata reduces these problems, allowing for easy deduplication and reproducibility. [Supporting text and data mining](/education/retrieve-metadata/rest-api/text-and-data-mining/) echoes our mission to make research outputs easy to find, cite, link, assess, and reuse.

<!--more-->

In 2013 Crossref embarked on a project to better support Crossref members and researchers with Text and Data Mining requests and access. There were two main parts to the project:  

1. To collect and make available full-text links and publisher TDM license links in the metadata.

2. To provide a service (TDM click-through service) for Crossref members to post their additional TDM terms and conditions and for researchers to access, review and accept these terms.

[The TDM click-through was launched in May 2014](/news/2014-05-29-crossref-text-and-data-mining-services-simplify-researcher-access).

To date, 37.5 million works registered with Crossref have both [full-text links and TDM license information](http://api.crossref.org/works?filter=has-license:true,has-full-text:true,license.version:tdm&facet=publisher-name:*&rows=0). We continue to encourage all members to include full-text links and license information in the metadata they register to assist researchers with TDM. You can see how each member is doing via its Participation Report (e.g. [Wiley's](https://www.crossref.org/members/prep/311)).

{{< figure src="/images/blog/2020/tdm_blog_prep.png" alt="participation report screenshot for Wiley" width="60%" >}}

Members are also making subscription content available for text mining (temporarily or otherwise) for specific purposes, such as to help the research community with its response to COVID-19. Back in April [we highlighted how this can be achieved](/blog/helping-researchers-identify-content-they-can-text-mine/) by including:  

1.  A "free to read" element in the access indicators section of publisher metadata indicating that the content is being made available free-of-charge (gratis)

2.  An assertion element indicating that the content being made available is available free-of-charge.

To access Crossref's **click-through** tool for text and data mining, users could log in via their ORCID iD. They could then review TDM license agreements posted by Crossref members and accept, reject or postpone their decisions until later. Having agreed to a publisher's terms and conditions this action was logged against the user's API token which they could use when requesting full-text from the publisher.

Since the pilot in 2014, only 2 publishers have continued with the tool and fewer than 300 API tokens have been issued.

Publishers have since developed their own mechanisms for managing TDM requests. The introduction of UK ([2014](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/375954/Research.pdf)) / EU ([2019](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2019.130.01.0092.01.ENG)) copyright exceptions for TDM has significantly reduced the number of requests and at the same time, more and more content is published under an open access license.

Given the low take-up of the click-through by both publishers and researchers, its goals are no longer being met. **Therefore we will retire the TDM click-through in December 2020.** Until that date, it will still operate for the two publishers and various researchers who use it while they finish implementing their alternative plans.

Crossref will continue to collect member-supplied TDM licensing information in metadata for individual works, and researchers can continue to find this via the Crossref APIs.
