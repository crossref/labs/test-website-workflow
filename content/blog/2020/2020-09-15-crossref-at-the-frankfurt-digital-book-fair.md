---
title: 'Crossref at the Frankfurt Digital Book Fair'
author: Rosa Clark
draft: false
authors:
  - Rosa Clark
date: 2020-09-17
categories:
categories:
  - Meetings
  - Community
  - Participation Reports

Archives:
  - 2020
---


Frankfurt Book Fair (#FBM20) will be online this year since people are really not traveling right now.  This special edition of #FBM20 will have an extensive digital program in which we will be participating.   So you can hang out with us from anywhere in the world! 
<!--more-->

{{% imagewrap center %}}{{< figure src="/images/blog/2020/FBF-stacked-combo-logo.png" alt="Crossref Frankfurt Digital Book Fair event logo" width="75%" >}}{{% /imagewrap %}}


Similar to the in-person event of years past, members of our technical support, membership, and outreach teams will be on hand at our online **Crossref Cafe**.     

Here are our **Crossref Cafe** hours: 

|   |Support|  Membership |  Community outreach | Product|
|---|---|---|---|---|
| [Wed 14 Oct 8:00 - 9:00  UTC](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0050/t/page/fm/0)  | Paul  |  Sally | Vanessa  |Bryan|
| [Wed 14 Oct 14:00 - 15:00 UTC](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0050/t/page/fm/0) |  Shayn |  Anna |Susan|Sara|
| [Thu 15 Oct 8:00 - 9:00  UTC](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0050/t/page/fm/0)  | Paul  | Laura  |  Vanessa |Martyn|
| [Thu 15 Oct 14:00 - 15:00 UTC](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0050/t/page/fm/0)  |  Isaac, Shayn | Anna, Kathleen  |  Susan |Kirsty|
| [Fri 16 Oct 8:00 - 9:00 UTC](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0050/t/page/fm/0)  | Paul  | Amanda  |  Vanessa, Rachael |Rakesh|
| [Fri 16 Oct 14:00 - 15:00 UTC](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0050/t/page/fm/0)  |  Isaac, Shayn |  Anna, Kathleen |  Susan |

**Who will be online:**

* [Susan](/people/susan-collins), [Vanessa](/people/vanessa-fairhurst/), and [Rachael](/people/rachael-lammey/) can talk to you about our upcoming [events](/events/).  
* [Kirsty](/people/kirsty-meddings/) can talk to you about [Crossmark](/services/crossmark/).
* [Kathleen](/people/kathleen-luschek/) can explain [Similarity Check](/services/similarity-check/).
* [Laura ](/people/laura-j-wilkinson) can show you how to use [Metadata Manager](https://www.crossref.org/metadatamanager/) for Content Registration.
* [Isaac](/people/isaac-farley), [Shayn](/people/shayn-smulyan/), and [Paul](/people/paul-davis) can help troubleshoot any metadata, DOI, or reporting needs. 
* [Sara](/people/sara-bowman/) can talk to you about [content registration](/services/content-registration/).
* [Anna](/people/anna-tolwinska) will give you a 'metadata health check' including a tour of your [Participation Report](https://www.crossref.org/members/prep/).
* [Rakesh](/people/rakesh-masih/) can talk to you about product design.
* [Sally](/people/sally-jennings/) and [Amanda](/people/amanda-bartell/) can answer your questions about [membership](/membership/).
* [Martyn](/people/martyn-rittman/) can talk to you about [Cited-by](/services/cited-by/).
* [Bryan](/people/bryan-vickery/) can talk to you about recent updates to our [products and services](/services/).


We are happy to [schedule one-on-one virtual meetings](mailto:feedback@crossref.org?Subject=Frankfurt%20meeting%20&Body=Hello%2C%20I%20would%20like%20to%20schedule%20a%20meeting%20to%20talk%20about%20...%20) as well. 

Please do drop-in to say _**"Guten Tag"**_.  We're looking forward to seeing you online!  
