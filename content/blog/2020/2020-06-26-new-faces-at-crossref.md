---
title: 'New faces at Crossref'
author: Crossref
draft: false
authors:
  - Crossref
date: 2020-06-30
categories:
  - Member Briefing
  - Community
  - Staff

archives:
    - 2020
---

Please help us welcome new faces at Crossref! Martyn, Sara, Laura, and Mark joined us very recently and we are happy they're with us.  Both Martyn and Sara have joined the Product team and this has given us the chance to reorganize the team into the following groups: content registration, scholarly stewardship, scholarly impact, metadata retrieval, and UX/UI leadership.  Laura joined the Finance and Operations team to help make the billing process simple for our members. Mark joins the Technology team and one of his projects will be improving the Event Data service.  

It is exciting to already see the impact of your contributions and look forward to what’s to come!   

<!--more-->


## And now a few words from each of them.

### Martyn Rittman  

{{< figure src="/images/blog/2020/martyn-rittman.jpg" alt="image of Martyn" width="300px" >}}

I am a former university researcher who worked on interdisciplinary projects around life sciences and analytical chemistry, with positions in the UK and Germany. I spent seven years at open access publisher MDPI doing everything from running journals to handling production, developing services for authors and publishers, and supporting preprints. I’m very excited to be joining Crossref as a Product Manager and developing some great products and services that focus on how Crossref-indexed research creates impact. This includes supporting the use of preprint metadata. I’m also looking forward to getting my teeth into event data, which looks at how those in the research community and beyond reference, use, and reuse research. If you are interested in making use of event data or have examples of event data applications, I would like to hear from you.  <br /><br />

### Sara Bowman

{{< figure src="/images/blog/2020/sara-slack.jpeg" alt="image of Sara" width="300px" >}}

I’m thrilled to have joined Crossref at this exciting time in the organization. As a member of the Product team, my primary area of focus is content registration, building, and improving tools for our members to deposit rich metadata. I’m particularly interested in how we can create a unified user experience for content registration while supporting the needs of our diverse membership. A scientist by training, I’ve spent the last 6 years working on open source technologies to support scholarly communication, most recently in the role of Product Manager at the Center for Open Science. I’m passionate about open tools and using data to drive product development, building innovative solutions to improve research and scholarly communication.  

### Laura Cuniff

{{< figure src="/images/blog/2020/laura_c.jpg" alt="image of Laura" width="300px" >}}

I joined Crossref two months ago as a part-time Billing Support Specialist on the Finance and Operations team. With the help of my supportive and knowledgeable colleagues, I took on learning the various systems. My goal is to make the billing process as simple as possible for our members by researching, retrieving, and relaying billing information.  This allows our members to focus on the reason for their engagement with Crossref. With several part-time jobs cobbled together at different times of the day, I have the flexibility to volunteer with a few organizations in my hometown of Ipswich, MA.  If you find yourself at the Ipswich Visitor Center, I may greet you, recommend the most beautiful spots in town, give you a tour of the Ipswich Museum, or send you off with a wonderful Ipswich Humane Group cat or dog! I’m very excited to be here!    


### Mark Woodhall  


{{< figure src="/images/staff/mark2-720px.jpg" alt="image of Mark" width="300px" >}}  

I am an open-source enthusiast who has worked in a range of technology roles at a variety of companies as a polyglot programmer with experience in Clojure(Script), Java, C#, and JavaScript.  It’s really exciting to be working at Crossref as a Senior Software Developer on the Technology team and I’m proud to be part of a team with open source at its heart. I’m really looking forward to getting more involved with event data and building a scalable solution to support its future uses.    

_Welcome to the Crossref community Martyn, Laura, Sara, and Mark._
