---
title: 'Discuss all things metadata in our new community forum'
author: Vanessa Fairhurst
draft: false
authors:
  - Vanessa Fairhurst
date: 2021-02-11
categories:
  - Collaboration
  - Community
Archives:
  - 2021
---

TL;DR:  We have a Community Forum (yay!), you can come and join it here: [community.crossref.org](https://community.crossref.org/).

Community is fundamental to us at Crossref, we wouldn’t be where we are or achieve the great things we do without the involvement of you, our diverse and engaged members and users. Crossref was founded as a collaboration of publishers with the shared goal of making links between research outputs easier, building a foundational infrastructure making research easier to find, cite, link, assess, and re-use. It is at the very core of what we do and who we are. Our global community now includes publishers, libraries, government agencies, funders, researchers, universities, ambassadors, and more from over 140 countries. We are also actively part of the larger scholarly research community, which includes other [open scholarly infrastructure](http://openscholarlyinfrastructure.org/) organizations, metadata users and aggregators, open science initiatives, and others with shared aims and values.

## What do we mean by 'community'?

‘Community’ is often one of those words which gets bandied around without much thought given to its meaning. At Crossref, we are aware that expertise lies within our broad, global community and we engage with them (you!) in a variety of ways to ensure that decisions we make are community-led and that what we do, as well as what we don’t do, are in line with the views of our members and developed with your insights and input. We do this via our working groups, committees, ambassador program, beta-testing groups, in-person and online events, webinars, and on-going dialogues and feedback via our support channels and even social media. We are also involved in a number of collaborative projects with other organizations such as [ROR](https://ror.org/), [Metadata 2020](http://www.metadata2020.org/), [Make Data Count](https://makedatacount.org/), [PIDapooloza](https://www.pidapalooza.org/), and the [FREYA](https://www.project-freya.eu/en/about/mission) project to name but a few.

Community is more than just signing up to be a Crossref member. It’s more than just attending an event or a webinar, or levelling up to include the use of a service like Crossmark or Similarity Check –– it’s really engaging with us and creating something together of shared value for the scholarly community. As an organization, we’ve been so thrilled that there is a new group dedicated to highlighting community managers and our work. We are working with –– and learning a lot from –– the [Centre for Scientific Collaboration & Community Engagement](https://www.cscce.org) to improve the way we interact and involve people in Crossref. The model below shows a trajectory towards true collaboration that we aim to follow in the coming months and years.

{{% imagewrap center %}}{{< figure src="/images/blog/2021/cscce-participation-model.png" caption="Cite as: [Center for Scientific Collaboration and Community Engagement. (2020) The CSCCE Community Participation Model – A framework for member engagement and information flow in STEM communities. Woodley and Pratt doi: 10.5281/zenodo.3997802](https://doi.org/10.5281/zenodo.3997802 )" width="80%" >}}{{% /imagewrap %}}   

In the current climate, there are additional challenges and limitations on how we interact with all the various communities that we as individuals are a part of, both professionally and personally. I wrote in my last blog about [how we have moved our events online](/blog/community-outreach-in-2020/) and thought about new ways to better connect and engage with our community virtually. One of those ways is our Community Forum.

## The purpose of our community forum

Hosted on the open-source discussion platform [Discourse](https://www.discourse.org/about), you can find our forum at [community.crossref.org](https://community.crossref.org/). The goal of the community forum is to create an inclusive, open space where Crossref members, ambassadors, sponsors, service providers, and others who share a passion for scholarly infrastructure, can connect. This enables collaborative problem-solving, the sharing of expertise and experiences across time zones and languages, and allows members to post questions to be answered by other community members or even our staff. Members of the community engage via creating posts, commenting on existing content in the forum, volunteering for working groups or beta-testing projects, helping to co-create materials that include translations and shared FAQs, giving feedback on new developments, and joining online events and webinars. Throughout these interactions, we expect that those who use the community forum will form relationships –– a collective working together to advance their work with Crossref and shape the future of scholarly infrastructure.

{{% imagewrap center %}}{{< figure src="/images/blog/2021/community-forum.png" width="80%" >}}{{% /imagewrap %}}          
<br/>

When I joined Crossref as Community Manager over three years ago, the idea of a forum had already begun to take shape, but it wasn’t quite there just yet. There was additional research and consultation with the community to be done to check this was the approach we wanted to take.

This involved speaking to others working in scholarly communications about forums they were involved in running or were an active participant of –– check out the [PKP forum](https://forum.pkp.sfu.ca/) for instance if you haven’t already –– and having numerous valuable conversations about successes, potential downfalls, and realistic expectations. The most important –– and commonly cited –– takeaway is that building an online community takes time. We are still at the start of this journey. It will only work if it is a place of value for all and a place where people feel a sense of belonging and co-ownership.

{{% imagewrap center %}}{{< figure src="/images/blog/2021/community-forum-post.png" width="80%" >}}{{% /imagewrap %}}

## Preparing to rollout the forum

We tested the platform with a small group of beta-testers and also sent out a survey to over 1,700 of our members, taking a sample with a geographical and organizational spread. The responses thankfully held no major surprises and reinforced our belief that this is something of use to people.

### Key research findings

-   77% of respondents had previously contacted our Support team for help resolving an issue.
-   90% stated either ‘yes’ or ‘maybe’ to whether they would use a community forum to post their questions, though over half have never used a forum before.
-   Most common reasons of importance for joining are 'Community support in solving issues or answering questions', 'To locate FAQs and quickly find answers to common issues', and 'To connect with others working in a similar role and/or with similar interests'
-   Most commonly-stated things that would discourage or limit member’s participation would be how time-consuming and complex the forum is to use, and any potential language barriers.

## Things you can do on the forum

We hope this will provide a much more open level of support for the community, enabling us to bring out all those great questions and thoughtful conversations we receive via our Support channels into the public sphere, where we can all benefit from these rich exchanges. Ultimately our goal for the future is that this space is owned by you, the Crossref community. This is a platform for you to connect and build relationships with others working in scholarly communications: metadata fanatics, identifier aficionados, developer gurus, and open research enthusiasts - we welcome you all!

-   Share what activities or projects you are working on and get input from others.
-   Share issues that you need some help resolving, post a question to the forum in your native language and get help from another community member.
-   Give us feedback on our plans and help us shape future developments at Crossref.
-   Test out new tools and services.
-   Find out about upcoming events and webinars, and share any you think are of interest to the community.
-   Help us identify better ways of working together through Crossref and co-create new materials and projects.

## How to get started

So, how do I sign up you ask? Simply head over to [community.crossref.org](https://community.crossref.org/) and set up an account. There's a useful How-To guide available on [our welcome post](https://community.crossref.org/t/about-the-welcome-to-the-crossref-community-forum-category/1026), as well as some Community Guidelines all our members should follow.

Do you have a question about registering or updating your metadata? Then head over to the [Content Registration category](https://community.crossref.org/c/content-registration/24) and post your query to the group. Want to find out about getting started with Similarity Check service? Then take a look at our [Similarity Check topic](https://community.crossref.org/c/crossref-services/similarity-check/22) in our services category. Or maybe you want to know more about upcoming multilingual webinars at Crossref, or perhaps you have one of your own you’d like to share? Then check out the [Community Calendar](https://community.crossref.org/c/crossref-calendar/10).

We’re also looking for talented linguists out there to help us translate our welcome email template into multiple languages so that anyone joining the community can get a welcome in their native language. To join in, visit [my post](https://community.crossref.org/t/help-us-translate-our-welcome-email/1527?u=vanessa) in our ‘Questions from Crossref’ category.

We look forward to seeing you in the community soon!
