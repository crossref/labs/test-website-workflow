---
title: 'An Advisory Group for Preprints'
author: Martyn Rittman
draft: false
authors:
  - Martyn Rittman
date: 2021-06-09
categories:
  - Preprints
  - Metadata
  - Community
  - Relationships
Archives:
  - 2021
---

We are delighted to announce the formation of a new [Advisory Group](https://www.crossref.org/working-groups/preprints) to support us in improving preprint metadata. Preprints have grown in popularity over the last few years, with increasing focus brought by the need to rapidly disseminate knowledge in the midst of a global pandemic. We have supported metadata deposits for preprints under the content type [‘posted content’](https://www.crossref.org/education/content-registration/content-types-intro/posted-content-includes-preprints/) since 2016, and members currently register a total of around 17,000 new preprints metadata records each month.

As preprints develop and different practices arise, we are keen to re-examine the metadata schema: to do this properly we need community input. We want to ensure that the schema is fit for purpose and supports the diversity of ways in which preprints are posted, linked with other objects, and used. Metadata schema need regular review, and this is just one example of a number of areas we are looking to update. Several topics we see as a high priority for preprints are better notification for when a preprint has been withdrawn or removed, accurate recording of versioning, and better indication of preprint server names.

We have invited a number of organizations we know to be active in this area, and are looking forward to some very positive discussions. Participants span five continents and include members who post preprints, indexing services, and others with significant experience in the area of preprints. The first meeting took place earlier this week and brought up a diverse range of themes that will be tackled in future meetings.
