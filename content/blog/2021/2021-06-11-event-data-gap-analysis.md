---
Archives:
- 2021
DOI: https://doi.org/10.5555/cgqns3z2hu
author: Martyn Rittman
authors:
- Martyn Rittman
categories:
- Event Data
- Request for proposal
date: 2021-06-11
draft: false
title: 'Event Data: Help us fill in the gaps'
x-version: 0.0.0
---

_UPDATE August 2, 2021: This work was awarded to Laura Paglione of the [Spherical Cow Group](https://sphericalcowgroup.com)._

To date, we have collected around [740 million](http://api.eventdata.crossref.org/v1/events?rows=0) events from 12 different source since we launched our [Event Data service](https://www.crossref.org/services/event-data/) service in 2017. Each event is an online mention of the research associated with a DOI, either via the DOI directly or using the associated URL. However, we know that there is much more out there. Because of this, we would like to explore where we could expand.

We invite proposals to conduct a gap analysis for Event Data sources, looking at what we currently collect and seeing what more could be added. For the most relevant new sources, we are seeking an estimate of the effort to include them, and establish whether it is possible: we know that there are sources that are paywalled or with restrictive licensing not compatible with Event Data.

The aim of the project is to identify a list of potential new sources. With community input, we will look to add a number of these to Event Data in the future based on needs and priorities.

For full details of the requirements and how to make a proposal, see [here](/pdfs/event-data-gap-analysis-rfi.pdf). The deadline for proposals is 11 July 2021 and we anticipate that the work will be completed by the end of October 2021.