---
title: 'Similarity Check news: iThenticate v2.0 ready for launch'
author: Fabienne Michaud
draft: false
authors:
  - Fabienne Michaud
date: 2021-09-20
categories:
  - Similarity Check
  - Member briefing
  - Enrich services
archives:
    - 2021
---

Crossref Similarity Check news: iThenticate v2.0 ready for launch

[Last year](/blog/similarity-check-news-introducing-the-next-generation-ithenticate), we announced the upcoming launch of a new version of iThenticate, the product from Turnitin that powers [Crossref Similarity Check](/services/similarity-check). We know some of you have been waiting a long time for this upgrade and we are very happy to share with you that we are now ready to release it.

We will be rolling out this new version in stages, so not everyone will be able to upgrade to the new version immediately. We'll start with new Crossref Similarity Check subscribers who use iThenticate in the browser, and one member who uses iThenticate via the eJournalPress API integration.

Next month, we will reach out to existing Crossref Similarity Check subscribers who use iThenticate in the browser (rather than through a manuscript tracking system), and further eJournalPress users.  From then on, we'll be contacting those of you who use Similarity Check through your manuscript tracking system, as and when your providers are ready to work with the new version.

## Crossref Similarity Check - first things first

Crossref Similarity Check is a content comparison tool, powered by iThenticate and produced by Turnitin, to check the originality of scholarly works and detect potential cases of plagiarism. Crossref members are eligible for this service, which offers them a reduced rate for document checking (plus enhanced functionality) in exchange for making their own published content available to be indexed into the iThenticate database.

[The Crossref Similarity Check service](/services/similarity-check) continues to grow in membership (1,531 members in 2020; 1,964 members in 2021, to date) and in the number of documents checked (1,922,621 manuscripts checked between January and July 2020 and 2,419,612 over the same period this year).

Just as with the current version of iThenticate, Crossref Similarity Check subscribers will be able to compare documents against a vast database of internet sources and over 78 million full-text documents contributed by the Crossref members that use the service:

-   _Crossref_ - research articles, books, and conference proceedings provided by publishers of scholarly content all over the world
-   _Crossref posted content_ - preprints, eprints, working papers, reports, dissertations, and many other types of content that has not been formally published but has been registered with Crossref
-   _Internet_ - a database of archived and live publicly-available web pages, including billions of pages of existing content, and with tens of thousands of new pages added each day
-   _Publications_ - third-party periodical, journal, and publication content including many major professional journals, periodicals, and business publications from sources other than Crossref Similarity Check members
-   _Your Indexed Documents_ - other documents you have uploaded for checking (within your Crossref Similarity Check user account only, and not added to iThenticate's main indexes)

## What's new

We are delighted to introduce the following new features and enhancements with iThenticate v2.0:

-   Increased document upload capacity
-   Suspicious and hidden character detection
-   Preprint exclusion filter
-   Refreshed and responsive interface
-   Similarity reports -  save and share
-   Annotations
-   Content portal
-   Improved API

### Increased document upload capacity

This new version of iThenticate has an increased document upload capacity of up to 800 pages/200 MB and a Google Drive document upload functionality. Please note that per-document [fees](/fees/#similarity-check-fees) allow for a maximum of 25,000 25,000 ~~characters~~ (EDIT 21/11/4: words), as one billable unit (25,001-50,000 25,000 ~~characters~~ (EDIT 21/11/4: words) is two billing units, and so on).

### Suspicious or hidden character detection

A new 'Red flag' feature, highlighted at the top right hand side of the Similarity report and with in-line markers, signals the detection of hidden text such as text/quotation marks in white font or suspicious character replacement  e.g., the substitution of a Latin e for a Cyrillic е or a Latin o for a Greek ο, which may have been deliberately added to avoid text-matching detection. 

{{< figure src="/images/blog/2021/red-flag.png" caption="Red flag feature: Hidden characters in the iThenticate v2.0 Similarity report" width="80%" >}}

### Preprint exclusion filter

Increasingly, authors are making available a preprint of their article, either before or at the same time as submitting it to a journal. With Turnitin, we have therefore developed a new exclusion filter for 'Preprint Sources', which can be applied directly from your Similarity report.

### Refreshed and responsive interface

The new iThenticate has a cleaner, more intuitive and accessible interface, with responsive design for ease of use on different screen sizes.  The Similarity report is no longer a static image but a text that can be searched, copied and pasted. The display of matches has been improved and simplified with two views only: 'Sources overview' and 'All sources'. 

{{< figure src="/images/blog/2021/similarity-report.png" caption="Similarity report in iThenticate v2.0" width="80%" >}}

### Similarity reports - save and share

You can now save Similarity reports as a PDF file and share them via email through the iThenticate interface with authors. Please note: this is still work in progress and enhancements to this feature will be released in the coming months. 

### Annotations

Annotations in Similarity reports is a brand new feature available in private mode only (in shared folders) in this initial release. Annotations will display the date, time and comments and can be edited or deleted as required. These private annotations will not be included in the 'save and share' features mentioned above. Public, shareable, annotations will be included in a future release.

{{< figure src="/images/blog/2021/annotations.png" caption="Private annotations in the new Similarity report" width="80%" >}}

### Content portal

The new 'Content portal' is a useful tool to check how much of your own published content has been successfully indexed into the iThenticate database and is now searchable. It will also help you self-diagnose and fix the content that has failed to be indexed.

### Improved API for subscribers who integrate Similarity Check with their manuscript tracking system

API users will benefit from a new integration with manuscript tracking systems which will allow the display of the largest matching word count and the top 5 source matches alongside the Similarity score.

## What's next 

We're expecting a number of new features and enhancements to iThenticate version 2.0 as well as further manuscript tracking system API integrations in the coming months:

-   User/usage reporting functionality
-   Editorial Manager API integration
-   Further enhancements to the Similarity report user interface
-   Parent/child account management reporting, to assist Crossref Sponsors
-   Public vs. private annotations
-   Document resubmission flow
-   Customisable welcome email

## We'll keep you posted

We will post updates here as soon as new features, enhancements and API integrations are  available and/or we are ready to upgrade the next group of members.

We'll be contacting subscribers in stages to upgrade you to the new version, so keep your eyes open for an email from us. As you know, you have to supply full-text Similarity Check URLs in your Crossref metadata for over 90% of your own published content in order to be eligible for the service. We'll be checking that anyone who wants to upgrade to v2.0 is still at 90% or above. You can check this yourself in advance on our [eligibility checker](/documentation/similarity-check/participate/eligibility/) - if you've fallen below 90%, the tool will give you instructions for adding your missing full-text Similarity Check URLs.

In the meantime, you will find the Similarity Check service documentation for the [current](/documentation/similarity-check) version of iThenticate on our website. The documentation for the new version can be found on the [Crossref Similarity Check site provided by Turnitin](https://help.turnitin.com/crossref-similarity-check.htm).

✏️  Do get in touch via [support@crossref.org](mailto:support@crossref.org) if you have any questions or suggestions  or start a discussion on [our Community Forum](https://community.crossref.org)
