---
title: '&#8220;Spinning Around&#8221;'
slug: 'spinning-around'
author: thammond
authors:
  - thammond
date: 2007-02-23

categories:
  - Standards
archives:
  - 2007

---
There’s a great exposition of [FRBR][1] (the Functional Requirements for Bibliographic Records model &#8220;_**work -> expression -> manifestation -> item**_&#8220;) in [this post][2] from [The FRBR Blog][3] on [De Revolutionibus][4] as described in _The Book Nobody Read: Chasing the Revolutions of Nicolaus Copernicus_ by Owen Gingerich. See post for the background and [here (103 KB PNG)][5] for a map of the FRBR relationships.

(Yes, and a twinkly star in the title too. ;~)

 [1]: http://www.ifla.org/VII/s13/frbr/frbr.htm
 [2]: https://web.archive.org/web/20071004111040/http://www.frbr.org/2007/02/22/de-revolutionibus
 [3]: http://www.frbr.org/
 [4]: http://en.wikipedia.org/wiki/De_revolutionibus_orbium_coelestium
 [5]: https://web.archive.org/web/20070705095508/http://www.frbr.org/files/frbrevolutionibus.png
