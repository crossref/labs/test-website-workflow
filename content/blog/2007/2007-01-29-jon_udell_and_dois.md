---
title: Jon Udell and DOIs
author: admin
authors:
  - admin
date: 2007-01-29

categories:
  - DOIs
archives:
  - 2007

---
Not to get too self-referential here, but it was very cool to see that Tony Hammond has managed to get [Not to get too self-referential here, but it was very cool to see that Tony Hammond has managed to get][1] This based on a [podcast][2] interview with Tony posted on January 26th.

 [1]: http://blog.jonudell.net/2007/01/29/the-persistent-blogosphere/
 [2]: http://blog.jonudell.net/2007/01/26/a-conversation-with-tony-hammond-about-digital-object-identifiers/
