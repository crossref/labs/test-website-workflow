---
title: 'XMP - Some Other Gripes'
author: thammond
authors:
  - thammond
date: 2007-09-10

categories:
  - XMP
archives:
  - 2007

---
Following on from the missing XMP Specification version number discussed in the [previous post][1] here below are listed some miscellaneous gripes I’ve got with XMP (on what otherwise is a very promising technology). I would be more than happy to be proved wrong on any of these points.

<!--more-->



_1. XMP version history and archive_

There doesn’t appear to be any XMP version history or archive hosted by Adobe as far as I can tell.

_2. Unpublished schemas_

Also there is nothing published - outside the XMP Spec itself - on the core schemas used by XMP. There’s nothing to be gleaned from the namespace URIs used. The Adobe namespaces, e.g.

> [<https://web.archive.org/web/20070929102516/http://www.adobe.com/products/xmp/> (listed in XMP Spec)](http://www.adobe.com/products/xmp/)

> &nbsp;

> [<https://web.archive.org/web/20070929102516/http://www.adobe.com/products/xmp/> (not listed in XMP Spec)](http://www.adobe.com/products/xmp/)

seem to all resolve to this page

> <http://www.adobe.com/products/xmp/>.

So, that can leave us with undocumented terms (e.g. &#8216;<tt>xmpMM:Manifest</tt>&#8216; used by Adobe InDesign CS2 4.0.5) from documented schemas and also undocumented schemas (e.g. &#8216;<tt>pdfx</tt>&#8216;).

_3. UUID_

Note also that many Adobe apps do not use the URN syntax for &#8216;<tt>uuid:</tt>&#8216;. The XMP Spec also has this to say:

> _&#8220;There is no formal standard for URIs that are based on an abstract UUID. The following proposal may be relevant:</p>
>
> <https://datatracker.ietf.org/doc/rfc4122/>;</i>
>
> (see: 3 XMP Storage Model / Serializing XMP / rdf:Description elements / rdf:about attribute)&#8221;</i></blockquote>
>
> I guess the XMP Spec (Sept. ’05) had just been bedded down more or less when the URN namespace for &#8216;<tt>uuid:</tt>&#8216; was published as [RFC 4122][2] in July ’05.

> _4. RDF/XML serialization_

> The biggie.

> XMP schemas specify fixed property value types in RDF/XML, i.e. they specify a fixed profile of RDF/XML instead of generic RDF/XML. This has been commented on recently by [myself][3] on the [semantic-web][4] list, and also [here][5] by Bruce D’Arcus speaking about OpenDocument, and [here][6] by Mike Linksvayer speaking for CC.

> This profiling of RDF/XML leads to real problems. For example, Adobe have defined a Dublin Core (DC) schema which lists the property value types that DC values can assume in an XMP serialization. Meantime, the [PRISM 2.0][7] draft spec defines an incompatible mapping of DC terms to XMP property values. Since both schemas would make use of the same DC namespace (though PRISM haven’t actually specified a DC namespace for use with XMP but do use elsewhere the regular DC namespace) this isn’t going to work. I did supply some feedback on this to the PRISM WG but have heard nothing back from them. So, PRISM XMP looks uncertain at this time. Which, for us, must be a shame.

 [1]: /blog/w5m0mpcehihzreszntczkc9d
 [2]: http://www.ietf.org/rfc/rfc4122.txt
 [3]: http://lists.w3.org/Archives/Public/semantic-web/2007Sep/0007.html
 [4]: http://lists.w3.org/Archives/Public/semantic-web/
 [5]: http://lists.w3.org/Archives/Public/semantic-web/2007Sep/0008.html
 [6]: http://lists.w3.org/Archives/Public/semantic-web/2007Sep/0027.html
 [7]: https://web.archive.org/web/20070910031332/http://www.prismstandard.org/
