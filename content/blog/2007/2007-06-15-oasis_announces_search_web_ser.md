---
title: OASIS Announces Search Web Services TC
author: thammond
authors:
  - thammond
date: 2007-06-15

categories:
  - Search
archives:
  - 2007

---
[OASIS][1] has just [announced][2] a technical committee for standardising search services. This from the Call for Participation:

> _
  
> b. Purpose
  
> To define Search and Retrieval Web Services, combining various current and
  
> ongoing web service activities.
  
> Within recent years there has been a growth in activity in the development of
  
> web service definitions for search and retrieval applications. These include
  
> SRU, a web service based in part on the NISO/ISO Search and Retrieval standards;
  
> the Amazon OpenSearch, which defines a means of describing and automating search
  
> web forms; as well as many proprietary definitions (e.g. the Google and MSN
  
> Search APIs). There are also a number of activities for defining abstract search
  
> APIs that can be mapped onto multiple implementations either within native code
  
> or onto remote procedural calls and web services, such as ZOOM (Z39.50 Object
  
> Oriented Model); SQI (Simple Query Interface), an IEEE standard developed for
  
> searching and retrieval in the IMS (Instructional Management Systems) space; and
  
> OSIDs (Open Service Interface Definitions from the Open Knowledge Initiative.
  
> While abstract APIs would be out of scope, these would inform the work to
  
> increase interoperability and compatibility.
  
>_

 [1]: http://www.oasis-open.org/home/index.php
 [2]: http://lists.oasis-open.org/archives/tc-announce/200706/msg00008.html