---
title: The Second Wave
author: thammond
authors:
  - thammond
date: 2007-09-11

categories:
  - Metadata
archives:
  - 2007

---
You might have been wondering why I’ve been banging on about [XMP][1] here. Why the emphasis on one vendor technology on a blog focussed on an industry linking solution? Well, this post is an attempt to answer that.

Four years ago we at [Nature Publishing Group][2], along with a select few early adopters, started up our RSS news feeds. We chose to use [RSS 1.0][3] as the platform of choice which allowed us to embed a rich metadata term set using multiple schemas - especially Dublin Core and PRISM. We evangelized this much at the time and published documents on [XML.com][4] (Jul. ’03) and in [D-Lib Magazine][5] (Dec. ’04) as well as speaking about this at various meetings and blogging about it. Since that time many more publishers have come on board and now provide RSS routinely, many of them choosing to enrich their feeds with metadata.

Well, RSS can be seen in hindsight as being the **_First Wave_** of projecting a web presence beyond the content platform using standard markup formats. With this embedded metadata a publisher can expand their web footprint and allow users to link back to their content server.

Now, XMP with its potential for embedding metadata in rich media can be seen as a **_Second Wave_**. Media assets distributed over the network can now carry along their own metadata and identity which can be leveraged by third-party applications to provide interesting new functionalities and link-back capability. Again a projection of web presence.

(Continues.)

<!--more-->



XMP has much in common with RSS 1.0. They are both profiles of RDF/XML. They are both flawed in certain respects because of self-imposed limitations. But they both build on a robust and open data model for the web (RDF) and are reasonably open, at least they are extensible. One (RSS 1.0) was defined in an open process by committee, the other is an open (i.e published) specification provided by a vendor.

From our point of view both specifications are sufficiently advanced to be immediately useful. I’m not sure how one could interact with the further development of either specification. RSS 1.0 is essentially frozen with Atom being posed as a successor technology, although Atom does not conform to the RDF model. (The upshot is that an RSS 1.0 feed can be consumed completely by an RDF-aware application, while an Atom feed would need to be pre-processed before any RDF &#8220;goodness&#8221; could be gleaned from it.) By contrast, XMP is a vendor-defined technology and alive, if not perhaps kicking. I am unaware of any process to formally contribute to the XMP development apart from shouting from the terraces. None the less, both technologies are usable as is.

It is curious that no consistent packaging (and delivery) of metadata has yet been achieved with HTML, the original web interface. The HTML <tt><title></tt> and <tt><meta></tt> elements are employed by publishers with various degrees of consistency. There are also RDF islands that can be embedded within HTML comments (as used e.g. by [CC licenses][6]). And then there are [COinS][7] objects. But it’s all a bit of a mish-mash to date. Certainly, I don’t recall seeing any guidelines from Crossref as to how machine readable metadata (even markup for the DOI itself) may be embedded within HTML pages, rather than on HTML pages for human readers.

This lack of uniform metadata deployment for HTML pages could be something to do with context. With RSS and XMP we are dealing with remote objects, whereas with HTML we are generally accessing this directly on the content server and so have a semantic context. It could be though that metadata delivery from HTML pages will finally be more uniformly available with the further development of standards such as [microformats][8] and especially [RDFa][9], [GRDDL][10], etc. It is also interesting to note that an XMP packet could just as easily be embedded within the HTML page, and if this technology were to be adopted more widely for embedding in other media assets then why not consider the same technology for ordinary web pages?

I can’t help feeling though that XMP has a lot of promise and is very timely. There are only three real obstacles: creating XMP packets, writing them and reading them. To my mind, once one has a good grasp of XMP then creating the packets can be done with common tools. The same, more or less, for reading the packets. I have shown earlier that this is readily achievable. The only major block is writing the packets into media files although there is support for create/write (if patchy) by open source libraries, as well as there being support (perhaps limited) from products for create/write. But, anyway, it’s certainly do-able.

 [1]: http://www.adobe.com/products/xmp/
 [2]: http://www.nature.com/
 [3]: http://web.resource.org/rss/1.0/
 [4]: http://www.xml.com/pub/a/2003/07/23/rssone.html
 [5]: http://www.dlib.org/dlib/december04/hammond/12hammond.html
 [6]: http://creativecommons.org/
 [7]: https://web.archive.org/web/20090927174724/http://ocoins.info/
 [8]: http://microformats.org/
 [9]: http://www.w3.org/2006/07/SWD/RDFa/syntax/
 [10]: http://www.w3.org/2004/01/rdxh/spec
