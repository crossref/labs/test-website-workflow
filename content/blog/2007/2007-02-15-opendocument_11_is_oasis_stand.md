---
title: OpenDocument 1.1 is OASIS Standard
author: thammond
authors:
  - thammond
date: 2007-02-15

categories:
  - Standards
archives:
  - 2007

---
From the OASIS [Press Release][1]:

> _&#8220;Boston, MA, USA; 13 February 2007 &#8212; OASIS, the international standards consortium, today announced that its members have approved version 1.1 of the Open Document Format for Office Applications (OpenDocument) as an OASIS Standard, a status that signifies the highest level of ratification.&#8221;_

 [1]: https://web.archive.org/web/20070219093602/https://www.oasis-open.org/news/oasis-news-2007-02-14.php
