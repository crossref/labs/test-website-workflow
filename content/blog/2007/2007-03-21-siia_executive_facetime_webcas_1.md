---
title: SIIA Executive FaceTime Webcast Series
author: Anna Tolwinska
authors:
  - Anna Tolwinska
date: 2007-03-21

categories:
  - Webinars
archives:
  - 2007

---
We thought that this program might interest our CrossTech bloggers.

Howard Ratner, Chief Technology Officer, Executive Vice-President at Nature Publishing Group is on the agenda.

More information is available at: <https://web.archive.org/web/20070322234448/http://www.siia.net/>.

<!--more-->



SIIA Executive FaceTime Webcast Series

Howard Ratner, EVP/CTO, Nature Publishing Group

Wednesday, March 28, 2007

12:00PM – 1:30PM EST

The SIIA is pleased to announce that Howard Ratner of Nature Publishing Group will be our guest for the upcoming Executive FaceTime. This live webcast series features one-on-one conversations between leading industry executives and host Hal Espo. Participation is encouraged, the web audience is invited to submit questions posed through the host. Past guests include Tad Smith, CEO of Reed Business Information and L. Gordon Crovitz, EVP of Dow Jones &#038; Company. Registration is free to SIIA members and non-members alike; to participate, you must register by the end of the day on Tuesday, March 27th.

Howard Ratner

Howard Ratner is Chief Technology Officer, Executive Vice-President, for the Nature Publishing Group. Based in New York, Howard is in charge of NY operations and has global responsibilities for Production and Manufacturing, Web Development, Web Services, Content Services, and Information Technology across all NPG products. Howard’s prior positions include Director, Electronic Publishing &#038; Production for Springer-Verlag New York, as well as the North American Manager for LINK, and a member of the production staff at John Wiley &#038; Sons. He also serves on the Crossref board, PubMed Central, CORDS and LOCKSS advisory committees, and is a former chair for both the AAP/PSP DOI subcommittee and the DOI-X project.

Hal Espo

Hal Espo is President of Contextual Connections, LLC, a NYC-based consultancy which focuses exclusively in the digital services arena, including digital content, distribution, and applications. Hal has more than 25 years experience as an operating executive as well as a business and product development professional in the electronic information industry. He served as Chief Operating Officer of Index Stock Imagery, Inc., a web-based commercial stock photography and illustration vendor, and previously was the Chief Operating Officer at CORSEARCH, Inc., a trademark research firm serving Fortune 500 companies and law firms.
