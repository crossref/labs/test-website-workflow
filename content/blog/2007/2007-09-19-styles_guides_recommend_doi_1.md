---
title: Style Guides Recommend DOI strings
author: Ed Pentz
authors:
  - Ed Pentz
date: 2007-09-19

categories:
  - Citation Formats
archives:
  - 2007

---
A couple of recent posts - from [A couple of recent posts - from][1] at Jefferson University and [IFST at Univ of Delaware][2]- note that the AMA and APA style guides now recommend using a DOI, if one is assigned, in a journal article citation.

A citation in the APA style with a DOI would be:

> Conley, D., Pfeiffera, K. M., &#038; Velez, M. (2007). Explaining sibling differences in achievement and behavioral outcomes: The importance of within- and between-family factors. Social Science Research36(3), 1087-1104. doi:10.1016/j.ssresearch.2006.09.002

In the AMA style a reference would be:

> Kitajima TS, Kawashima SA, Watanabe Y. The conserved kinetochore protein shugoshin protects centromeric cohesion during meiosis. Nature. 2004;427(6974):510-517. doi:10.1038/nature02312

This is great news. I haven’t looked at the full style guides but it’s not clear if information is given about linking DOIs via http://dx.doi.org/

<!--more-->



Information on the APA Style Guide is available - <http://apastyle.apa.org/> with [specific info on electronic references, URLs and DOIs][3] and here is the [AMA info][4].

This raises the existential question of a DOI as a URI. Is

> Conley, D., Pfeiffera, K. M., &#038; Velez, M. (2007). Explaining sibling differences in achievement and behavioral outcomes: The importance of within- and between-family factors. Social Science Research36(3), 1087-1104. doi:10.1016/j.ssresearch.2006.09.002 <http://dx.doi.org/10.1016/j.ssresearch.2006.09.002>

unnecessary or redundant?

 [1]: http://jeffline.jefferson.edu/SML/reference/reftips/?p=19
 [2]: https://web.archive.org/web/20080412044026/http://forfaculty.wordpress.com/2007/09/17/apas-new-recommendations-for-citing-e-journals/
 [3]: http://apastyle.apa.org/elecmedia.html
 [4]: http://www.amamanualofstyle.com/
