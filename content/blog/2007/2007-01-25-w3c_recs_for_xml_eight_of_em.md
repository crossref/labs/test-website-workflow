---
title: 'W3C Recs for XML - Eight of &#8216;Em!'
slug: 'w3c-recs-for-xml-eight-of-em'
author: thammond
authors:
  - thammond
date: 2007-01-25

categories:
  - XML
archives:
  - 2007

---
Although most folks will already know about this it still seems significant enough to blog the arrival of XQuery 1.0, XSLT 2.0, and XPath 2.0. See the [W3C Press Release][1].

 [1]: http://www.w3.org/2007/01/qt-pressrelease