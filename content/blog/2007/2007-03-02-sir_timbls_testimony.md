---
title: 'Sir TimBL’s Testimony'
author: thammond
authors:
  - thammond
date: 2007-03-02

categories:
  - Web
archives:
  - 2007

---
Just in case anybody may not have seen this, [here][1]&#8216;s the testimony of Sir Tim Berners-Lee yesterday before a House of Representatives Subcommittee on Telecommunications and the Internet. Required reading.

(Via this [post][2] yesterday in the [Save the Internet][3] blog.)

 [1]: http://dig.csail.mit.edu/2007/03/01-ushouse-future-of-the-web.html
 [2]:https://web.archive.org/web/20070307171557/http://www.savetheinternet.com/blog/2007/03/01/world-wide-web-inventor-says-net-neutrality-a-top-priority-for-congress/
 [3]: http://www.savetheinternet.com/blog/
