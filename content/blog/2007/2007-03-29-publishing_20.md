---
title: Publishing 2.0
author: admin
authors:
  - admin
date: 2007-03-29

categories:
  - Conference
archives:
  - 2007

---
XML:UK is holding a one-day conference entitled titled “[Publishing 2.0][1]” at [Bletchley Park][2] on Wednesday 25th April 2007. Bletchley Park was the location of the United Kingdom’s main codebreaking establishment during the Second World War and is now a museum (and has a train station!). The event will examine some of the more cutting-edge applications of XML technology to publishing. With keynotes by Sean McGrath and Kate Warlock and a series of must-see presentations, this will be the place to be on the last Wednesday in April.

 [1]: https://web.archive.org/web/20070630093957/http://www.xmluk.org/publishing20407.htm
 [2]: http://www.bletchleypark.org.uk/
