---
title: Indexing URLs
author: thammond
authors:
  - thammond
date: 2007-03-08

categories:
  - Linking
archives:
  - 2007

---
Leigh Dodds proposes [in this post][1] some solutions to persistent linking using web crawlers and social bookmarking.

> _&#8220;When I use del.icio.us, CiteULike, or Connotea or other social bookmarking service, I end up bookmarking the URL of the site I’m currently using. Its this specific URL that goes into their database and associated with user-assigned tags, etc._

> _&#8230;_

> _A more generally applicable approach to addressing this issue, one that is not specific to academic publishing, would be to include, in each article page, embedded metadata that indicates the preferred bookmark link. The DOI could again be pressed into service as the preferred bookmarking link.&#8221;_

He’s inviting feedback. I’d certainly like to hear what others may think of these suggestions.

 [1]: http://allmyeye.blogspot.com/2007/03/persistent-linking-web-crawlers-and.html