---
title: STM Innovations 2007
author: Ed Pentz
authors:
  - Ed Pentz
date: 2007-12-10

categories:
  - Conference
archives:
  - 2007

---
After a busy Online Information conference, Friday was the STM Innovations Meeting in London (presentations not online yet). There was a very nice selection of tea which helped get the morning off to a good start.

Patricia Seybold kicked off with a review of Web 2.0 that mentioned lots of sites and some good case studies:

Alexander Street Press (<https://alexanderstreet.com/>) - user tags combined with a taxonomy.

Slideshare (<http://www.slideshare.net>) - share presentations

Threadless (<http://www.threadless.com/>) - design and vote on t-shirts

The most interesting parts of the talk were the case studies of how National Instruments and Staples have built a vibrant community of customers. Staples invited top purchasers on the their site to create product categories and sales went up 30% and now they use the categorization in physical stores and customer reviews from the web are used in stores.

<!--more-->



NI has a whole suite of tools that allow customers to build products and get their jobs done (using NI products and services).

Five steps to Web 2.0 success –

1. Focus on findability

2. Solicit sutomers’ reviews, ratings and opinions

3. Empower users to classify and organize content

4. Nurture community, social networks, communities of practice

5. Get lead users to strut their stuff, using your IP to build their IP

The most useful part came in the questions when Geoffrey Bilder asked about [&#8220;astroturfing&#8221;][2] - this is a problem for Web 2.0. Interestingly, the NI and Staples examples are closed communities and other sites have to have moderators to try and track this stuff down. Often you don’t hear about these types of issues amid the web 2.0 boosterism.

Joris van Rossum gave an very good overview of Scirus’ wiki-based Topic Pages (<https://web.archive.org/web/20071231210906/http://topics.scirus.com/>). It’s interesting to see the creative way Elsevier is experimenting. Joris said that it is Elsevier’s vision that wiki forms a promising topic-centered platform for informal collaboration and the sharing of highly relevant info within STM in addition to the traditional peer-reviewed system. There is a critical issuem though - will researchers go to publishers for this type of thing or will they self-organize using inexpensive tools? The danger here is that publishers will do their own thing leading to a replay of the portal craze in the late 90s.

Geoffrey Bilder gave a very good talk entitled &#8220;Anonymous Bosh: Attribution in a Mashed-up World&#8221; about trust and CrossReg (contributor ID).

Simon Willison gave a very good explanation and update on OpenID. Some resources for more information - <http://openid.net>

[http://www.openidenabled.com](https://openid.net/what-is-openid/)

<https://web.archive.org/web/20070715235636/http://simonwillison.net/tags/openid/>

Mark Bide wrapped things up with an update on ACAP (<https://web.archive.org/web/20071019045302/http://www.the-acap.org/>)- &#8220;an evolving, open, royalty-free standard for expression of permissions in machine readable form&#8221; - that was launched in November. Will the search engines pay any attention?

Overall, the day was very thought provoking.

 [1]: http://www.stm-assoc.org/events/stm-innovations-seminar-2007/
 [2]: http://en.wikipedia.org/wiki/Astroturfing
