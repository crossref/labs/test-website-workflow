---
title: 'RSC’s Project Prospect v1.1'
author: rkidd
authors:
  - rkidd
date: 2007-05-31

categories:
  - Metadata
  - Publishing
  - RSS
  - XML
  - InChI
archives:
  - 2007

---
We updated our [Project Prospect][1] articles today to release v1.1, with a pile of look &#038; feel improvements to the HTML views and links. The most interesting technical addition is the launch of our enhanced RSS feeds, where we have updated our [existing feeds][2] for enhanced articles. These now include ontology terms and primary compounds both visually (as text terms and 2D images) and within the RDF - using the OBO in OWL representation and the info:inchi specification mentioned here by Tony only a few weeks ago.

The enhanced entries will soon become more common as we concentrate our enhancements on our Advance Articles, but the current example below from our [Photochemical and Photobiological Sciences feed][3] is lovely. RDF code after the jump - just as beautiful to the parents&#8230;

<img alt="ProspectRSS.jpg" src="/wp/blog/images/ProspectRSS.jpg" width="395" height="517" />

<!--more-->



So the RDF code for the OBO terms and InChIs looks like this:

```
<tt><br /> <rdf:li><br /> <content:item rdf:about="info:inchi/InChI=1/C20H28O/c1-16(8-6-9-17(2)13-15-21)11-12-19-18(3)10-7-14-20(19,4)5/h6,8-9,11-13,15H,7,10,14H2,1-5H3/b9-6-,12-11+,16-8+,17-13+"/><br /> </rdf:li><br /> <rdf:li><content:item rdf:about="http://purl.org/obo/owl/CL#CL:0000210"/><br /> </rdf:li><br /> </tt>
```

We now have over five hundred 2007 articles enhanced, so we’ve brought the majority back into controlled access. There are always [examples][4] from each journal freely available.

 [1]: https://web.archive.org/web/20070401173200/http://www.rsc.org/Publishing/Journals/ProjectProspect/index.asp
 [2]: https://pubs.rsc.org/en/EAlerts/RssFeed
 [3]: https://pubs.rsc.org/en/ealerts/rssfeed
 [4]: https://web.archive.org/web/20081004073354/http://www.rsc.org/Publishing/Journals/ProjectProspect/Examples.asp
