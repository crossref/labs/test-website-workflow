---
title: The Names Project
author: thammond
authors:
  - thammond
date: 2007-10-05

categories:
  - ORCID
archives:
  - 2007

---
Was reminded to blog about this after reading [Lorcan’s post][1] on the [Names Project][2] being run by JISC. From the blurb:

> _&#8220;The project is going to scope the requirements of UK institutional and subject repositories for a service that will reliably and uniquely identify names of individuals and institutions.

> &nbsp;

> It will then go on to develop a prototype service which will test the various processes involved. This will include determining the data format, setting up an appropriate database, mapping data from different sources, populating the database with records and testing the use of the data.&#8221;_

One immediate project tangible is the [landscape report][3] (&#8216;A review of the current landscape in relation to a proposed Name Authority Service for UK repositories of research outputs’) which summarizes some current initiatives in author identification from a UK perspective, including _inter alia_ Elsevier’s [Scopus Author Identifier][4].

 [1]: http://orweblog.oclc.org/archives/001445.html
 [2]: https://web.archive.org/web/20071013215645/http://names.mimas.ac.uk/
 [3]: https://web.archive.org/web/20120119133351/http://names.mimas.ac.uk/documents/Names_landscape_report_1Oct2007.pdf
 [4]: http://help.elsevier.com/app/answers/detail/a_id/2845/p/8150/c/8430
