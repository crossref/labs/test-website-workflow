---
title: password control
author: Ed Pentz
slug: password-control-1

authors:
  - Ed Pentz
date: 2006-09-11

categories:
  - Blog
archives:
  - 2006

---
We’ve taken the top level access control off the site. This means that anyone can read the blog but posting will be limited to those with an account (Crossref members and invited participants). This will make it possible to include the CrossTech feed in your regular RSS reader/aggregator. We’ll soon be posting some general terms and conditions for this blog and also sending a message to all Crossref members about joining so we should see membership (and activity) pick up.
