---
title: And Just Relax
author: thammond
authors:
  - thammond
date: 2006-11-28

categories:
  - XML
archives:
  - 2006

---
Nice piece of advocacy [here][1] by Tim Bray for [RELAX][2]. High time to see someone standing up for RELAX - a much friendlier XML schema language.

 [1]: http://www.tbray.org/ongoing/When/200x/2006/11/27/Choose-Relax
 [2]: http://relaxng.org/