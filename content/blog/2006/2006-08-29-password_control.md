---
title: password control
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-08-29

categories:
  - Blog
archives:
  - 2006

---
Hi,

At the moment a username and password is needed to read the CrossTech blog in addition to needing an account to post entries. However, it may be better to take off the access control to read the blog - this would mean that services like Technorati and Google could index the blog, which they can’t do at the moment and posting to the blog would be public.

As people come on to the list maybe the first thing to comment on is whether we should take off the access control to read the blog. What to people think?
