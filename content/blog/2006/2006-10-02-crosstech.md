---
title: CrossTech
author: thammond
authors:
  - thammond
date: 2006-10-02

categories:
  - Discussion
archives:
  - 2006

---
<span >Just a couple comments about CrossTech:</span>
  
 <span >1. Shouldn’t it (or couldn’t it) be linked to from the Crossref home page? (This is a public read list after all and so should be made more widely available.) Maybe at some point could be announced on some lists of interest.</span>
  
 <span >2. Would be very nice to (at least) have a count of membership. I would also like to canvas opinions about making names of the membership public. What do others think about this?</span>
  
 <span >At the end of the day though this facility needs to be driven, otherwise it will end up being just another pier over the water (i.e. a &#8216;disappointed bridge’ And sorry for cribbing again from JAJ).</span>