---
title: 'SEMANTIC WEB: GOOGLE HAS THE ANSWERS, BUT NOT THE QUESTIONS'
slug: "semantic-web-google-has-the-an-1"
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-08-22

categories:
  - Discussion
archives:
  - 2006

---
Posted by special permission from EPS <!--broken link www.epsltd.com.-->

EPS INSIGHTS :: 01/08/2006

SEMANTIC WEB: GOOGLE HAS THE ANSWERS, BUT NOT THE QUESTIONS

* The Google v. Semantic Web discussion at the AAAI (American Association for Artificial Intelligence) featured plenty of confrontation and even some rational argument, but it may chiefly be remembered as the day when Google responded to the challenge of semantic web thinking by saying that the semantic web movement did not matter - thereby demonstrating that it did.

by David Worlock, Chairman

<!--more-->



And we thought that the real battle this year was between net neutrality and the network owners. Or between those who think that click fraud crucially undermines Google, and those who think it doesn’t matter. We were wrong. July’s &#8220;Thrilla in Manila&#8221; was the discussion between Tim Berners-Lee and the Google Director of Search, Peter Norvig, at the Boston AAAI meeting. And it is an important moment because Berners-Lee’s assertion that the last semantic web building blocks are moving into place comes at exactly the time when Google seems anxious to diminish semantic web searching. It is a good guess that the latter results from a stimulus dictated by threat. A world where keyword searching was reduced to ground floor in a building of many storeys where it may even be an advantage to be a new market entrant with no history is a world where Google would have to progressively re-invent itself. And what is more difficult, in the recent history of these things, than a company created by a technology re-inventing itself in terms of a new technology?

So Google’s Boston blows were first of all aimed at the reality test. Like STM publishers pointing to the unlikelihood of academic researchers adding metadata to articles for repository filing, Google pointed to user and webmaster incompetence as the chief reason why semantic interoperability was doomed to a long, slow and painful generative process. If users cannot configure a server or write HTML, how can they understand all this stuff? And then suppliers would slow it down by trying to make it proprietary. And then, machine to machine interoperability would encourage deception (obviously the click fraud business is hurting). The answer to the Semantic Web, from a Google stance, thus appears to be: very interesting, but not very soon.

Dancing like a bee and stinging like a butterfly, Tim Berners-Lee clearly had the answers to this. The reason why the semantic web appears threatening to those who have entrenched tenancies in search is probably because it is going quicker than expected. His original &#8216;layer cake’ diagram, a feature on the conference circuit for five years, could now be completed at all levels. RDF as a data language is now well-established (think of RSS). Ontologies, mostly in narrow vertical domains, are moving into place, though there may be issues about relating them to each other. Query and rules languages now populate the other layers, with one of the former, SPARQL, emerging this year as a W3C candidate recommendation (6 April 2006). In a real sense this is the missing link which makes the Semantic Web a viable proposition, and at the same time joins it to the popular hubbub around Web 2.0. If part of the latter dream is data sourcing from a wide variety of service entities to create new web environments from composite content, then SPARQL sitting on top of RDF looks closest to realising that idea. In an important note in O’Reilly XML.com (SPARQL: Web 2.0 Meet the Semantic Web; 16 September 2005), Kendall Clark wrote &#8220;Imagine having one query language, and one client, that lets you arbitrarily slice the data of Flickr, del.icio.us, Google, and your three other favourite Web 2.0 sites, all FOAF files, all of the RSS 1.0 feeds (and, eventually, I suspect, all Atom 1.0 feeds) plus MusicBrainz etc&#8221;.

Imagining that might well impel you into the ring with Tim Berners-Lee. If Google has to be re-invented, the process of recognition of change has to be slowed. Denying the speedy reality of the semantic web becomes essential while furious R&#038;D takes place. And content and information service providers are not just spectators of this, but participants too.

© Electronic Publishing Services
<!-- links broken, not in wayback machine  
>From the EPS archive

&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;-

Topix.net: semantic web building block? EPS Insights, 31 March 2005 ::

<http://www.epsltd.com/accessArticles.asp?articleType=1&#038;updateNoteID=1557>

Spotlight on . RDF and semantic web, imi, June 2006 ::

http://www.epsltd.com/accessArticles.asp?articleType=2&#038;articleID=384&#038;imiID=8

1

Semantic Web: another milestone reached, EPS Insights, 27 February 2004 ::

http://www.epsltd.com/accessArticles.asp?articleType=1&#038;updateNoteID=1191 -->

Related links

&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;-

Google :: <http://www.google.com>

AAAI :: <http://www.aaai.org>

Kendall Clark - SPARQL: Web 2.0 Meet the Semantic Web ::

[http://www.oreillynet.com/xml/blog/2005/09/sparql\_web\_20\_meet\_the_semanti.ht

ml][1]

W3C :: <http://www.w3.org>

Flickr :: <http://www.flickr.com>

FOAF :: <http://www.foaf-project.org/>

MusicBrainz :: <http://musicbrainz.org/>

&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;-

 [1]: http://www.oreillynet.com/xml/blog/2005/09/sparql_web_20_meet_the_semanti.ht
