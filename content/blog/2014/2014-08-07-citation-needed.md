---
title: Citation needed
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2014-08-07

categories:
  - Citation
  - Crossref Labs
  - Wikipedia
archives:
  - 2014

---
Remember when [I said that the Wikipedia was the 8th largest referrer of DOI links to published research][1]? This _despite_ only a fraction of eligible references in the free encyclopaedia using DOIs.

We aim to fix that. Crossref and Wikimedia are launching a new initiative to better integrate scholarly literature in the world’s largest public knowledge space, Wikipedia.

This work will help promote standard links to scholarly references within Wikipedia, which persist over time by ensuring consistent use of DOIs and other citation identifiers in Wikipedia references. Crossref will support the development and maintenance of Wikipedia’s citation tools on Wikipedia. This work will include bug fixes and performance improvements for existing tools, extending the tools to enable Wikipedia contributors to more easily look up and insert DOIs, and providing a “linkback” mechanism that alerts relevant parties when a persistent identifier is used in a Wikipedia reference.

In addition, Crossref is creating the role of Wikimedia Ambassador (modeled after [Wikimedian-in-Residence][2]) to act as liaison with the Wikimedia community, promote use of scholarly references on Wikipedia, and educate about DOIs and other scholarly identifiers (ORCIDs, PubMed IDs, DataCite DOIs, etc) across Wikimedia projects.

Starting today, Crossref will be working with [Daniel Mietchen][1] to coordinate Crossref’s Wikimedia-related activities. Daniel’s team will be composed of [Max Klein][3] and [Matt Senate][4], who will work to enhance Wikimedia citation tools, and will share the role of Wikipedia ambassador with [Dorothy Howard][5].

Since the beginnings of Wikipedia, Daniel Mietchen has worked to integrate scholarly content into Wikimedia projects. He is part of an impressive community of active Wikipedians and developers who have worked extensively on linking Wikipedia articles to the formal literature and other scholarly resources. We’ve been talking to him about this project for nearly a year, and are happy to finally get it off the ground.

-G<figure id="attachment_367"  class="wp-caption alignnone">

<img src="/wp/blog/uploads/2014/08/IMG_0602-300x150.jpg" alt="Matt, Max and Daniel at #wikimania2014. Photo by Dorothy." width="300" height="150" class="size-medium wp-image-367" srcset="/wp/blog/uploads/2014/08/IMG_0602-300x150.jpg 300w, /wp/blog/uploads/2014/08/IMG_0602-1024x515.jpg 1024w, /wp/blog/uploads/2014/08/IMG_0602-624x314.jpg 624w" sizes="(max-width: 300px) 85vw, 300px" /><figcaption class="wp-caption-text">][7][4] Matt, Max and Daniel at #wikimania2014. Photo by Dorothy.</figcaption></figure>

# wikimania2014

 [1]: /blog/many-metrics-such-data-wow
 [2]: https://outreach.wikimedia.org/wiki/Wikipedian_in_Residence
 [3]: https://github.com/notconfusing
 [4]: https://github.com/wrought
 [5]: http://www.dorothyhoward.com/
