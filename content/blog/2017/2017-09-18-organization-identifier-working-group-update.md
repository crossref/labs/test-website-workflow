---
title: 'Organization Identifier Working Group Update'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2017-09-18
categories:
  - DataCite
  - ORCID
  - CDL
  - Organization Identifier
  - Request for Community Comment
  - Community
  - Infrastructure
  - Collaboration
archives:
  - 2017
---

About 1 year ago, Crossref, DataCite and ORCID [announced a joint initiative] (https://orcid.org/blog/2016/10/31/organization-identifier-project-way-forward) to launch and sustain an open, independent, non-profit organization identifier registry to facilitate the disambiguation of researcher affiliations. Today we publish governance recommendations and product principles and requirements for the creation of an open, independent organization identifier registry and invite community feedback.

<!--more-->

The [Organization Identifier (OrgID) Working Group](https://orcid.org/content/organization-identifier-working-group) was established as [a joint effort by Crossref, DataCite and ORCID](https://orcid.org/blog/2016/10/31/organization-identifier-project-way-forward) in January 2017. The members of the group bring a broad range of experience and perspectives, including expertise in research data discovery, data management, persistent identifiers, economics research, funding, archiving, non-profit membership organizations, academia, publishing, and metadata development.

The Working Group was charged with refining the structure, principles, and technology specifications for an open, independent, non-profit organization identifier registry to facilitate the disambiguation of researcher affiliations.

The group has been working in three interdependent areas: Governance, Registry Product Definition, and Business Model & Funding, and today releases for public comment its findings and recommendations for governance and product requirements.

* [Governance Recommendations](https://figshare.com/articles/ORG_ID_WG_Governance_Principles_and_Recommendations/5402002/1) - https://doi.org/10.23640/07243.5402002.v1<br>
* [Product Principles and Recommendations](https://figshare.com/articles/ORG_ID_WG_Product_Principles_and_Recommendations/5402047/1) - https://doi.org/10.23640/07243.5402047.v1

We invite your feedback!

Please [send comments](mailto:oi-project@orcid.org) by October 15th, 2017.
