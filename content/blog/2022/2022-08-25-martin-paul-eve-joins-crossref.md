---
DOI: https://doi.org/10.5555/pwxgmblvtp
archives:
- 2022
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Staff
- R&D
- Labs
date: 2022-08-26
draft: false
title: Martin Paul Eve is joining our R&D group as a Principal Developer
x-version: 0.0.0
---

I'm delighted to say that [Martin Paul Eve](https://en.wikipedia.org/wiki/Martin_Paul_Eve) will be joining Crossref as a Principal R&D Developer starting in January 2023.

As a Professor of Literature, Technology, and Publishing at Birkbeck, University of London- Martin has always worked on issues relating to metadata and scholarly infrastructure. In joining the Crossref R&D group, Martin can focus full-time on helping us design and build a new generation of services and tools to help the research community navigate and make sense of the scholarly record.

[Martin himself explains the logic of this move on his own blog](https://eve.gd/2022/08/26/moving-on-my-infrastructural-turn/), so I won't attempt to do the same here other than to say:

> praxis makes perfect.

_(mic drop)_

{{< figure src="/images/labs/bookwheel.png"  height="500" width="500" caption="[Created with DALL·E, an AI system by OpenAI](https://labs.openai.com/s/pAPWg9vK7kIn763OLho9QvZ1) with the the prompt: 'A bookwheel in the style of the 16th-century illustration by Agostino Ramelli and where the books are replaced by open laptops'" alt="Because it is a law that all blog posts having to do with anything related to the digital humanties are required to include a picture of a bookwheel, we present an image generated by DALL·E with the folloiwng prompt: 'A bookwheel in the style of the 16th-century illustration by Agostino Ramelli and where the books are replaced by open laptops'">}}