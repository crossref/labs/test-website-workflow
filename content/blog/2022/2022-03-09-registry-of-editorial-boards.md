---
title: 'A Registry of Editorial Boards - a new trust signal for scholarly communications?'
author: Fabienne Michaud
draft: false
authors:
  - Fabienne Michaud
date: 2022-03-09
categories:
  - Metadata
  - Community
  - Enrich services
archives:
    - 2022
---

## Background

Perhaps, like us, you've noticed that it is not always easy to find information on who is on a journal's editorial board and, when you do, it is often unclear when it was last updated. The editorial board details might be displayed in multiple places (such as the publisher's website and the platform where the content is hosted) which may or may not be in sync and retrieving this information for any kind of analysis always requires manually checking and exporting the data from a website (as illustrated by the [Open Editors research](https://osf.io/preprints/socarxiv/jvzq7) and its [dataset](https://openeditors.ooir.org)).

For well-established as well as early career researchers, membership of an editorial board demonstrates their contribution to their community, brings prestige, improves (or maintains) their professional profile and often increases their chances of being published.

Whilst most journal websites only give the names of the editors, others possibly add a country, some include affiliations, very few link to a professional profile, an ORCID ID. Even when it's clear when the editorial board details were updated, it's hardly ever possible to find past editorial boards information and almost none lists declarations of competing interest.

We hear of instances where a researcher's name has been listed on the board of a journal without their knowledge or agreement, potentially to deceive other researchers into submitting their manuscripts. Regular reports of [impersonation](https://www.nature.com/articles/d41586-021-03035-y), [nepotism](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3001133), [collusion](https://arxiv.org/pdf/2112.13322.pdf) and [conflicts of interest](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0127362) have become a cause for concern.

Similarly, recent studies on [gender representation](https://link.springer.com/article/10.1007%2Fs12630-019-01378-9) and [gender and geographical disparity](https://www.biorxiv.org/content/10.1101/2021.02.15.431321v1) on editorial boards have highlighted the need to do better in this area and provide trusted, reliable and coherent information on editorial board members in order to add transparency, prevent unethical behaviour, maintain trust, promote and support research integrity.

## Registry of Editorial Boards

We are proposing the creation of some form of Registry of Editorial Boards to encourage best practice around editorial boards' information and governance that can easily be accessed and used by the community.

### What we have in mind

A Registry of Editorial Boards could be a new trust-signal for Crossref members and details would be included on a member's Participation Report.

Crossref members would register and maintain this information for their journal titles in a similar way as they currently manage their metadata. Only the owner of the title, or their trusted service provider, would be able to update it.  Editors would be linked by ORCID iD and ROR and Crossref would use 'autoupdate' to push editorship information to ORCID profiles, saving researchers time. The information would be made available via Crossref's API.

This new service would introduce more transparency and automation to the editorial process and connect content platforms (i.e. peer review management systems, publishers' websites, ORCID and other author register systems, ROR, bibliographic databases, etc.) and make available current and historical information on editorial boards including metadata on the editorial boards' full affiliations.

### The benefits for the community

The benefits would be wide-ranging for the different stakeholders in the scholarly communications community, from publishers, researchers, institutions, funders, bibliometricians to librarians including:

-   providing those involved in the peer review process and research ethics a single, authoritative and up-to-date resource on editorial boards

-   reducing fraudulent claims to be or to have been on an editorial board of a publication in order to be published or publish others

-   connecting and automating editorship role updates with e.g. ORCID, ROR, etc.

-   generating a detailed analysis of the publication practices of editorial board members and their close contacts 

-   assessing any relationships between authors, reviewers and editorial board members for conflict of interest, etc.

-   supporting researchers responding to a request to join an editorial board, making proactive approaches to a journal or wanting to ensure that an editorial board is representative of its community and assess its levels of diversity and inclusivity

-   providing increased visibility to researchers, particularly to early career researchers

## Your feedback

Before we progress further, we would like to fully understand what the needs of the community are and whether members would be willing and have the capacity to participate and contribute regularly in registering and maintaining details of their editorial boards.  

✏️  Please let us know what your thoughts and experience are with editorial boards by completing this brief [survey](https://forms.gle/UQpbsTgjQnEY43FT6) by 31 March 2022.
