---
title: 'DOIs for PHD Comics’ Valentine’s Day Reading List'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2012-02-14

categories:
  - Comics
  - DOIs
archives:
  - 2012

---
<p class="p1">
  <a href="http://goo.gl/8OzY">PHD Comics</a> has posted its <a href="http://goo.gl/V5hhs">Valentine’s Day Reading</a> list.
</p>

<p class="p1">
</p>

<p class="p1">
  Without DOIs!&nbsp;
</p>

<p class="p1">
  &nbsp;
</p>

<p class="p1">
  So in order to preserve the scholarly citation record, we’ve resolved those that have DOIs&#8230;.
</p>

<p class="p1">
</p>

<p class="p1">
  Title:&nbsp; <i>The St. Valentine’s Day Frontal Passage</i>
</p>

<p class="p1">
  Citation:&nbsp; Sassen, K, 1980, &#8216;The St. Valentine’s Day Frontal Passage’, <i>Bulletin of the American Meteorological Society</i>, vol. 61, no. 2, p. 122.
</p>

<p class="p2">
  <span class="s1">Crossref DOI:&nbsp; <a href="http://dx.doi.org/10.1175/1520-0477(1980)061%3C0122:TSVDFP%3E2.0.CO;2"><span class="s2">http://dx.doi.org/10.1175/1520-0477(1980)061<0122:TSVDFP>2.0.CO;2</span></a></span>
</p>

<p class="p3">
</p>

<p class="p1">
  Title:&nbsp; <i>SUICIDE AND HOMICIDE ON ST. VALENTINE’S DAY</i>
</p>

<p class="p1">
  Citation:&nbsp; LESTER, D, 1990, &#8216;SUICIDE AND HOMICIDE ON ST. VALENTINE’S DAY’, <i>Perceptual and Motor Skills</i>, vol. 71, no. 7, p. 994.
</p>

<p class="p2">
  <span class="s1">Crossref DOI:&nbsp; <a href="http://dx.doi.org/10.2466/PMS.71.7.994-994"><span class="s2">http://dx.doi.org/10.2466/PMS.71.7.994-994</span></a></span>
</p>

<p class="p3">
</p>

<p class="p1">
  Title:&nbsp; <i>The St. Valentineʼs Day Massacre</i>
</p>

<p class="p1">
  Citation:&nbsp; Eckert, W, 1980, &#8216;The St. Valentineʼs Day Massacre’, <i>The American Journal of Forensic Medicine and Pathology</i>, vol. 1, no. 1, pp. 67-70.
</p>

<p class="p2">
  <span class="s1">Crossref DOI:&nbsp; <a href="http://dx.doi.org/10.1097/00000433-198003000-00011"><span class="s2">http://dx.doi.org/10.1097/00000433-198003000-00011</span></a></span>
</p>

<p class="p3">
</p>

<p class="p1">
  Title:&nbsp; <i>For Valentine’s Day</i>
</p>

<p class="p1">
  Citation:&nbsp; Kutzner, H, 2001, &#8216;For Valentine’s Day’, <i>Cancer</i>, vol. 91, no. 4, pp. 804-805.
</p>

<p class="p2">
  <span class="s1">Crossref DOI:&nbsp; <a href="http://dx.doi.org/10.1002/1097-0142(20010215)91:4%3C804::AID-CNCR1067%3E3.3.CO;2-K"><span class="s2">http://dx.doi.org/10.1002/1097-0142(20010215)91:4<804::AID-CNCR1067>3.3.CO;2-K</span></a></span>
</p>
