---
title: Crossref Metadata Search++
slug: crossref-metadata-search-plus-plus
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2012-10-11

categories:
  - Citation Formats
  - Crossref Labs
  - Search
archives:
  - 2012

---
We have just released a bunch of new functionality for [Crossref Metadata Search][1]. The tool now supports the following features:

<ul class="disc" >
  <li>
    A completely new UI
  </li>
  <li>
    <a href="http://en.wikipedia.org/wiki/Faceted_search" rel="external" target="_blank" >Faceted</a><span class="Apple-converted-space">&nbsp;</span>searches
  </li>
  <li>
    Copying of search results as formatted citations using<span class="Apple-converted-space">&nbsp;</span><a href="http://en.wikipedia.org/wiki/Citation_Style_Language" rel="external" target="_blank" >CSL</a>
  </li>
  <li>
    <a href="http://en.wikipedia.org/wiki/COinS" rel="external" target="_blank" >COinS</a>, so that you can easily import results into Zotero and other document management tools
  </li>
  <li>
    <a href="http://web.archive.org/web/20121014215757/http://search.labs.crossref.org/help/api" rel="external" target="_blank" >An API</a>, so that you can integrate Crossref Metadata Search into your own applications, plugins, etc.
  </li>
  <li>
    Basic<span class="Apple-converted-space">&nbsp;</span><a href="http://en.wikipedia.org/wiki/OpenSearch" rel="external" target="_blank" >OpenSearch</a><span class="Apple-converted-space">&nbsp;</span>support- so that you can integrate Crossref Metadata Search into your browser’s search bar.
  </li>
  <li>
    Searching for a particular Crossref DOI
  </li>
  <li>
    Searching for a particular Crossref<span class="Apple-converted-space">&nbsp;</span><a href="http://shortdoi.org/" rel="external" target="_blank" >ShortDOI</a>
  </li>
  <li>
    Searching for articles in a particular journal via the journal’s ISSN
  </li>
</ul>

At the moment, Crossref Metadata Search (CRMDS) is a [Crossref Labs project][2] and, as such, should be used with some trepidation. Our goal is to release CRMS as a production service ASAP, but we wanted to get public feedback on the service before making the move to a production system.

 [1]: https://web.archive.org/web/20131229210637/http://search.crossref.org//
 [2]: http://labs.crossref.org/
