+++
title = "Board election 2020 candidates"
date = "2020-09-01"
draft = false
author = "Lucy Ofiesh"
rank = 2
parent = "Elections"
weight = 6
type = 'dontindexcontent'
+++

If you are a voting member of Crossref your ‘voting contact’ will receive an email in late September, please follow the instructions in that email which includes links to the relevant election process and policy information.

From 72 applications, our [Nominating Committee](/committees/nominating/) proposed the following eight candidates to fill the six seats open for election to the Crossref Board of Directors.

### Tier 1, Small and mid-sized members
| Member organization | Candidate standing     | Country      |
|:--------------------|:-----------------------|:-------------|
|[Beilstein-Institut](#beilstein)|Wendy Patterson |Germany|  
|[Korean Council of Science Editors](#kcse)|Kihong Kim |South Korea  |  
|[OpenEdition](#openedition)|Marin Dacos|France |  
|[Scientific Electronic Library Online (SciELO)](#scielo)       |Abel Packer |Brazil |  
|[The University of Hong Kong](#uhk)|Jesse Xiao|Hong Kong |  

### Tier 2, Large members
| Member organization | Candidate standing     | Country      |
|:--------------------|:-----------------------|:-------------|
|[AIP Publishing](#aip)   |Jason Wilde   |USA    |  
|[Oxford University Press](#oxforduni)|James Phillpotts|UK |  
|[Taylor & Francis](#tf)|Liz Allen|UK    |  

---
<a id="aip"></a>
## Jason Wilde, AIP Publishing, USA<br>

<img src="/images/board-governance/jason-wilde.png" alt="Jason Wilde,  AIP Publishing" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
From being an instigator in the development of CHORUS to taking a lead role in enabling public access to research content AIP Publishing has played a pivotal role in shaping our industry. Through our role on the boards of key industry bodies, Crossref, International Association of Scientific, Technical and Medical Publishers (STM) and CHORUS, AIP Publishing continues to ensure that the voice of small, scholarly, publishers is clearly heard.  

At a time when a small number of large commercial publishers are taking dominant roles in scientific publishing, there is an ongoing need for strong representation of self-published not-for-profit societies, such as AIP Publishing, across our industry and, most importantly, on the Crossref board.    

### Personal statement <br>
As the Chief Publishing Officer (CPO) for AIP Publishing, Jason is responsible for the leadership, vision, and growth of the publishing program.  

Jason currently serves as a board member for Crossref (2014–2020) and previously from 2007–2013 when he was with Nature. Jason was a member of the Crossref Nominating Committee in 2011, 2012, and 2015; and between 2017 and 2020 he served on Crossref’s Executive Committee. Currently Jason is a member of CrossRef’s Audit Committee and the Membership and Fees Committee.  

With 20 years’ experience in both large for-profit and smaller not-for-profit organizations and significant involvement in the Crossref organization, Jason has a unique understanding of our industry and the vital role that Crossref plays. Crossref is not only the core infrastructure that links research and aids discoverability, but it provides the framework and data on which many other industry tools and services are built. As publishing continues to evolve, there is opportunity for Crossref to increasingly serve as a focal point for the industry, fostering collaboration among publishers, institutions, funders and governments to develop solutions that serve the needs of our customers.  

Before joining AIP Publishing, Jason was Business Development Director for Nature Publishing Group (NPG). During his twelve years with NPG, Jason oversaw the creation of the Nature-branded physical science research journals and NPG’s Open Access publications Nature Communications, Scientific Reports and Scientific Data. Prior to his career in publishing, Jason earned degrees from Dundee University (BEng in Electrical and Electronic Engineering), Cambridge University (PGCE in Balanced Science and Physics) and Durham University (PhD in Molecular Electronics).  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Depuis plus de 80 ans, AIP Publishing défend les besoins de la communauté mondiale de la recherche, en tant que filiale à part entière et à but non lucratif de l’American Institute of Physics (AIP). Nous avons pour mission de soutenir les objectifs caritatifs, scientifiques et éducatifs de l’AIP par l’intermédiaire d’activités scientifiques dans le domaine des sciences physiques.

D’abord instigatrice dans le développement de CHORUS, AIP occupe maintenant un rôle de chef de file pour permettre au public l’accès au contenu de la recherche et a joué un rôle a joué un rôle essentiel dans la formation de notre industrie. Notre rôle au sein des conseils d’administration des principaux organismes industriels permet à Crossref, à l’Association internationale des éditeurs scientifiques, techniques et médicaux (STM) et CHORUS, et à AIP Publishing de veiller encore à ce que les petits éditeurs spécialisés se fassent entendre.

Aujourd’hui, seul un nombre restreint de grands éditeurs commerciaux jouent un rôle dominant dans l’édition scientifique, il est donc nécessaire d’assurer une forte représentation des sociétés à but non lucratif autoéditées, telles que AIP Publishing, dans tout notre secteur et, surtout, au sein du conseil d’administration de Crossref.

### Déclaration personnelle  
Directeur général de l'édition (CPO) pour AIP Publishing, Jason est chargé du leadership, de la vision et du développement du programme de publication.  

Jason est actuellement membre du conseil d'administration de Crossref (2014–2020) et il l’a été précédemment de 2007 à 2013 lorsqu’il travaillait chez Nature. Jason a été membre du Comité de nomination de Crossref en 2011, 2012 et 2015 ; et de 2017 à 2020, il a siégé au Comité exécutif de Crossref. Actuellement, Jason est membre du Comité d’audit de CrossRef et du Comité des adhésions et des cotisations.  

Avec 20 ans d’expérience tant dans les grandes organisations à but lucratif que dans les petites organisations à but non lucratif et un important investissement au sein de l’organisation Crossref, Jason bénéficie d’une compréhension unique de notre secteur d’activité et du rôle vital joué par Crossref. Crossref n’est pas simplement l’infrastructure centrale qui relie la recherche et facilite l’accessibilité, mais il fournit le cadre et les données sur lesquels s’appuient de nombreux autres outils et services de l'industrie. Le secteur de l’édition poursuit son évolution, il y a donc là l’occasion pour Crossref de servir de plus en plus de point de convergence pour l’industrie, en encourageant la collaboration entre éditeurs, institutions, bailleurs de fonds et gouvernements afin de développer des solutions qui répondent aux besoins de nos clients.  

Avant de rejoindre AIP Publishing, Jason a été directeur du développement commercial pour Nature Publishing Group (NPG). Au cours de ces douze ans chez NPG, Jason a supervisé la création de revues de recherche en sciences physiques sous la marque Nature et des publications en libre accès de NPG, Nature Communications, Scientific Reports and Scientific Data. Avant cette carrière dans l’édition, Jason a décroché des diplômes à l’Université de Dundee University (BEng en génie électrique et électronique), à l’Université de Cambridge (PGCE en sciences et physique équilibrées) et à l’Université de Durham (doctorat en électronique moléculaire).  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
Por mais de 80 anos, a AIP Publishing tem patrocinado as necessidades da comunidade de pesquisas global como uma subsidiária integral, sem fins lucrativos, do American Institute of Physics (AIP). Nossa missão é apoiar objetivos de caridade, científicos e educacionais do AIP por meio de atividades de publicações acadêmicas em ciências físicas.

Trabalhando como incentivadora do desenvolvimento do CHORUS até desempenhar um papel de liderança na habilitação de acesso público ao conteúdo de pesquisas, a AIP Publishing tem representado um papel fundamental no desenvolvimento do setor. Por meio de nosso trabalho nos conselhos de organizações importantes do setor, como a Crossref, a International Association of Scientific, Technical and Medical Publishers (STM) e a CHORUS, a AIP Publishing continua garantindo que a voz de editoras acadêmicas pequenas seja ouvida claramente.

Em uma época onde um pequeno número de editoras comerciais grandes está assumindo papéis dominantes na publicação científica, há uma necessidade contínua para a representação firme de organizações de autopublicação sem fins lucrativos, tais como a AIP Publishing, em nosso setor e, o mais importante, no conselho da Crossref.

### Declaração pessoal
Como Diretor Executivo de Publicações para a AIP Publishing, Jason é responsável pela liderança, visão e crescimento do programa de publicações.

Jason no momento trabalha como membro do conselho para a Crossref (2014-2020) e anteriormente, no período de 2007a 2013, quando estava com a Nature. Jason foi membro do Comitê de Nomeações da Crossref em 2011, 2012 e 2015, e entre 2017 e 2020 atuou no Comitê Executivo da Crossref. Atualmente Jason é membro do Comitê de Auditoria da CrossRef e do Comitê de Membros e Taxas.

Com 20 anos de experiência em grandes organizações com fins lucrativos e em menores sem fins lucrativos, além de um envolvimento significativo na organização da Crossref, Jason possui um entendimento particular do nosso setor e do papel fundamental que a Crossref desempenha. A Crossref não é somente a infraestrutura central que vincula a pesquisa e ajuda na capacidade de descoberta, mas também fornece a estrutura e os dados nos quais muitas outras ferramentas e serviços do setor são desenvolvidos. Considerando que a publicação continua a evoluir, há oportunidades para que a Crossref aumente o seu trabalho como ponto focal para o setor, estimulando a colaboração entre as editoras, instituições, financiadores e governos para desenvolver soluções que atendam às necessidades de nossos clientes.

Antes de trabalhar na AIP Publishing, Jason foi diretor de desenvolvimento comercial para o Nature Publishing Group (NPG). Durante seus doze anos com o NPG, Jason supervisionou a criação de periódicos de pesquisa científica física com a marca Nature, além das publicações de acesso livre do NPG, da Nature Communications, da Scientific Reports e da Scientific Data. Antes da carreira em publicações, Jason graduou-se pela Universidade de Dundee (Bacharel em Engenharia Elétrica e Eletrônica), Universidade de Cambridge (Pós-graduação em Ciência do Equilíbrio e Física) e pela Universidade de Durham (PhD em Eletrônica Molecular).  
{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
Durante más de 80 años, AIP Publishing ha abogado por las necesidades de la comunidad de investigación global como una filial de propiedad absoluta y sin ánimo de lucro del American Institute of Physics (AIP). Nuestro objetivo es respaldar los propósitos caritativos, científicos y educativos del AIP mediante actividades editoriales académicas en todas las ciencias físicas.

Desde promover el desarrollo de CHORUS, hasta asumir el liderazgo para permitir el acceso público a trabajos de investigación, AIP Publishing ha jugado un papel esencial en la transformación de nuestra industria. Mediante nuestro papel en las juntas de organismos clave de la industria como Crossref, la Asociación Internacional de Editoriales Científicas, Técnicas y Médicas (STM) o CHORUS, AIP Publishing sigue asegurándose de que se escuchen claramente las voces de pequeñas editoriales académicas.

En tiempos donde un pequeño número de grandes editoriales comerciales está tomando el control de las publicaciones científicas, hay una necesidad imperiosa de reforzar la representación de sociedades autopublicadas sin ánimo de lucro, como AIP Publishing, en toda nuestra industria y, más importante, en la junta de Crossref.

### Declaración personal
Como director de publicaciones (CPO) de AIP Publishing, Jason es responsable del liderazgo, la visión y el crecimiento del programa editorial.

Jason es actualmente miembro de la junta de Crossref (2014-2020) y también lo fue anteriormente con Nature (2007-2013). Jason fue miembro del comité de nominación de Crossref en 2011, 2012 y 2015, y entre 2017 y 2020 fue parte del comité ejecutivo de Crossref. Actualmente, Jason es miembro del comité de auditoría de Crossref y del comité de miembros y tarifas.

Con 20 años de experiencia en organizaciones grandes y pequeñas, con y sin ánimo de lucro, y una participación importante en la organización Crossref, Jason tiene una visión única de la industria y del papel clave que tiene Crossref. Crossref no solo es la infraestructura central que enlaza investigaciones y ayuda a que se puedan recuperar, sino que además aporta el marco de trabajo y los datos sobre los que se crean muchas otras herramientas y servicios de la industria. Mientras el mundo de las publicaciones sigue evolucionando, Crossref tiene la oportunidad de servir cada vez más como punto focal para la industria, animando a la colaboración entre editoriales, instituciones, promotores y Gobiernos para desarrollar soluciones que ayuden a satisfacer las necesidades de nuestros clientes.

Antes de unirse a AIP Publishing, Jason fue director de desarrollo empresarial de Nature Publishing Group (NPG). Durante sus 12 años con NPG, Jason supervisó la creación de revistas de investigación de ciencias físicas de la marca Nature y las comunicaciones, informes científicos, datos científicos de las publicaciones de Nature de acceso abierto. Antes de su carrera editorial, Jason se graduó en la Universidad de Dundee (Licenciatura en Ingeniería Eléctrica y Electrónica), la Universidad de Cambridge (Posgrado en Ciencias y Física Equilibradas) y la Universidad de Durham (Doctorado en Electrónica Molecular).

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
80년 이상 동안, AIP Publishing은 AIP(American Institute of Physics)의 전액 출자된 비영리 자회사로서 글로벌 연구 커뮤니티의 요구에 부응해왔습니다. 저희의 미션은 물리과학 분야의 학술 출판 활동을 통해 AIP의 자선, 과학 및 교육적 목적을 지원하는 것입니다.

CHORUS 개발의 주역이 되는 것에서부터 연구 콘텐츠에 대한 대중적 접근을 가능케 하는 선구적 역할을 하는 것까지, AIP Publishing은 우리 업계에서 중심적 역할을 담당해 왔습니다. Crossref, International Association of Scientific, Technical and Medical Publishers (STM) 및 CHORUS 등 업계 핵심 단체 위원회에서의 역할을 통해, AIP Publishing은 소규모 학술 퍼블리셔의 목소리를 분명하게 대변할 수 있도록 계속 노력할 것입니다.

소수의 대형 상업 퍼블리셔가 과학 출판에서 지배적인 역할을 차지하고 있는 이 때, AIP Publishing과 같은 자비 출판의 비영리 단체가 우리 업계에서, 특히 Crossref 위원회에서 강력한 대변이 필요하다는요구가 계속해서 있어 왔습니다.

## 자기소개서
AIP Publishing의 최고발행인(Chief Publishing Officer, CPO)으로서, Jason은 출판 프로그램의 리더십, 비전 및 성장을 담당하고 있습니다.

Jason은 현재 Crossref (2014~2020년) 이사회 멤버로 재직하고 있으며 이전에 Nature에 있었던 2007~2013년에도 이사회 멤버였습니다. Jason은 2011년, 2012년 및 2015년에 Crossref Nominating Committee의 위원이었고, 2017~2020년에 Crossref의 집행위원회(Executive Committee)에서 활동했습니다. 현재 Jason은 Crossref의 감사위원회(Audit Committee) 및 멤버십요금위원회(Membership and Fees Committee) 위원입니다.

대형 영리 및 소규모 비영리 단체 모두에서와 Crossref 단체에서 상당한 참여를 해 온 20년간의 경험과 함께, Jason은 우리 업계에 대해 그리고 Crossref가 수행하는 중요한 역할에 대해 특히 잘 알고 있습니다. Crossref는 연구를 연계하고 발견가능성을 돕는 핵심 인프라일 뿐만 아니라, 기타 많은 업계 도구 및 서비스 구축의 기반이 되는 프레임워크 및 데이터를 제공합니다. 출판이 계속 진화함에 따라, 고객의 필요를 충족하는 솔루션을 개발하기 위해 퍼블리셔, 연구소, 재정 지원자 및 정부 간 협업을 촉진하는 등 Crossref는 업계에서 중심점 역할을 할 기회가 점차 많아지고 있습니다.

AIP Publishing 입사 전, Jason은 Nature Publishing Group(NPG)의 비즈니스 개발 디렉터였습니다. 12년간 NPG에서 근무하는 동안, Jason은 네이쳐 브랜드의 물리과학 연구 학술지와 NPG의 오픈 액세스 출판물인 Nature Communications, Scientific Reports 및 Scientific Data의 제작을 감독했습니다. 출판 커리어에 앞서, Jason은 던디대학교(Dundee University, 전기전자공학 학사), 케임브리지대학교(Cambridge University, 균형 과학 및 물리학 PGCE) 및 더럼대학교(Durham University, 분자 전자공학 PhD)에서 학위를 받았습니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="beilstein"></a>
## Wendy Patterson, Beilstein-Institut, Germany<br>
<img src="/images/board-governance/wendy-patterson.jpg" alt="Wendy Patterson, Beilstein-Institut" width="225px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
Background: The Beilstein-Institut is a non-profit foundation based in Frankfurt, Germany. The results of all our projects are freely available, and we do not charge any fees for our services. We support science and scientists worldwide with our platinum open access journals (no costs for authors or readers), data standardization projects and symposia. We have built our own submission, tracking and publishing system, in addition to a preprint server, and are thus aware of the challenges in implementing new procedures or workflows. We are actively involved in community initiatives including OASPA, Free Journal Network, DORA, RDA, CDIG, GO FAIR, and we are also participants in the i4OC open references project.  

Benefits to Crossref: We have a long and unique history in the German/European chemistry and open science community. Our diverse network, coming from the many conferences that we have organized and sponsored on the topics of open science and tools for scholarly communication, could be of benefit to Crossref. We have built and operate our own preprint server, giving us exceptional insight into the interplay between preprints and published articles, and we would like to assist in bringing next-generation metadata to preprints.  

How we represent the community and add diversity: Like us, approximately 14,000 of the publishers listed in the Directory of Open Access Journals (DOAJ) have two or fewer titles, and most Crossref content providers deliver <1,000 items per year and are in the “low fee tier” category. We, the (very) small, independent publisher, are clearly an important sector in publishing but are severely underrepresented in discussions and on boards where policy and decisions are made. Crossref aims to expand its outreach and interact further with the global publishing community, thus it is necessary to include these diverse voices and engage with these small publishers.  

Conclusion: We, like you, want to make scholarly communications better and provide a sustainable and shared infrastructure to support research communication. We feel that the vision and goals of our two non-profit organizations are very well aligned and that we are well-positioned to represent the much-needed voice of the very small publisher.  

### Personal statement<br>
What I can offer: As a former researcher turned non-profit publisher, I can provide a unique perspective with hands-on experience in scholarly publishing together with insight into the struggles and barriers that smaller publishers face. Among other roles at our non-profit institute, I manage and develop our platinum open access journals, I developed our journal-specific preprint server, and I am responsible for our journal and preprint metadata.  

I am also one of the founding board members of the Free Journal Network, a non-profit organization working to facilitate sustainable open access publishing. All scholarly journals in our coalition are run according to the Fair Open Access model (journals are led by the scholarly community and have no financial barriers to readers and authors).  

Knowledge of Crossref services: My involvement with the Crossref Metadata Interest Group has allowed me to appreciate even more Crossref’s true community-oriented approach in gaining feedback and involving all stakeholders, large and small. We are well aware of the services and benefits to the community that Crossref provides, and the importance of the role that Crossref plays in the development of new identifier and metadata initiatives cannot be overstated.  

Vision: My goal is to empower small, scholar-led publishers and to help identify ways to make scholarly communication more transparent and fair. If I were elected to the Crossref Board, I would directly support by additionally focusing on: preprint metadata, improving transparency in scholarly publishing, and supporting FAIR data.  

Conclusion: It would be a true honor to represent small publishers on the Crossref Board, and if elected my alternate would be Dr. Martin Hicks, who has led the Beilstein-Institut’s open science initiatives for over 15 years.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Historique : Le Beilstein-Institut est une fondation à but non lucratif basée à Francfort, en Allemagne. Tout le monde peut consulter gratuitement les résultats de l’ensemble de nos projets, et nous ne facturons aucuns frais pour nos services. Nous soutenons la science et les scientifiques dans le monde entier avec nos revues platinum en accès libre (aucuns frais pour les auteurs ou les lecteurs), projets de normalisation des données et de colloques. Nous avons élaboré notre propre système de dépôt, de suivi et de publication, en plus d’un serveur de pré-impression, et nous connaissons donc bien les difficultés posées par la mise en place de nouvelles procédures ou flux de travail. Nous nous impliquons activement dans des initiatives communautaires, notamment l’OASPA, le Free Journal Network, DORA, RDA, CDIG, GO FAIR et participons également au projet de références ouvertes i4OC.  

Avantages pour Crossref : Nous avons une longue et unique histoire dans la chimie germano/européenne et la communauté de la science ouverte. Notre réseau varié, né des nombreuses conférences que nous avons organisées et parrainées sur des thèmes de la science ouverte et des outils pour la communication universitaire pourrait être utile à Crossref. Nous avons conçu et élaboré notre propre serveur de pré-impression, ce qui nous donne un aperçu exceptionnel des interactions entre les pré-impressions et les articles publiés, et nous souhaitons contribuer à l’introduction de métadonnées de nouvelle génération dans les préimpressions.  

Comment nous représentons la communauté et ajoutons de la diversité : Tout comme nous, environ 14 000 des éditeurs répertoriés dans le Directory of Open Access Journals (DOAJ) comptent deux titres ou moins, et la plupart des fournisseurs de contenu Crossref livrent moins de 1 000 éléments par an et figurent à présent dans la catégorie « faible niveau d’honoraires ». Nous, le (très) petit éditeur indépendant, représentons clairement un secteur important dans l’édition, mais nous sommes largement sous-représentés dans les débats et les comités où se décident les politiques et où se prennent les décisions. Crossref a pour objectif d’élargir sa diffusion et d’interagir davantage avec la communauté internationale de l’édition, il est donc nécessaire d’intégrer ces voix diverses et de s’impliquer avec ces petits éditeurs.  

Conclusion : Tout comme vous, nous voulons améliorer les communications universitaires et fournir une infrastructure durable et partagée afin de soutenir la communication de la recherche. Nous avons le sentiment qu’il y a une belle harmonie entre la vision et les objectifs de nos deux organisations non lucratives, et que nous sommes bien positionnés pour représenter la voix indispensable des très petits éditeurs.  

### Déclaration personnelle
Mon offre : Ancien chercheur devenu éditeur à but non lucratif, je peux fournir une perspective unique avec une expérience concrète dans l’édition universitaire associée à un éclairage sur les luttes et les obstacles auxquels sont confrontés les plus petits éditeurs. Entre autres postes au sein de notre institut à but non lucratif, je gère et développe nos revues platine en libre accès, j’ai développé notre serveur de pré-impression spécifique à la revue, et je suis chargé de notre revue et de nos métadonnées de pré-impressions.  

Je suis également l’un des membres fondateurs du conseil d'administration Free Journal Network, organisation à but non lucratif s’efforçant de faciliter la publication durable en libre accès. L’ensemble des revues universitaires de notre coalition est géré selon le modèle de l’Accès libre et équitable (les revues sont dirigées par une communauté universitaire et n’ont pas d’obstacles financiers pour les lecteurs et les auteurs).  

Connaissance des services Crossref : Mon implication auprès du Groupe d’intérêt des métadonnées Crossref m’a permis d’apprécier encore plus la véritable approche orientée vers la communauté de Crossref pour obtenir un retour d'information et impliquer l’ensemble des acteurs, petits et grands. Nous connaissons bien les services et les avantages offerts par Crossref, et il est impossible d’exagérer le rôle que joue Crossref dans le développement de nouvelles initiatives en matière d’identificateurs et de métadonnées.  

Vision : Mon objectif est de responsabiliser les petites maisons d’édition dirigées par des universitaires et de les aider à identifier des façons de rendre la communication universitaire plus transparente et plus juste. Si je suis élu au Comité Crossref, je le soutiendrai directement en mettant l’accent sur la pré-impression des métadonnées, l’amélioration de la transparence dans l’édition universitaire et le soutien aux données FAIR.  

Conclusion : Ce serait un véritable honneur de représenter de petits éditeurs au Comité Crossref, et si je suis élu, mon suppléant sera le Dr Martin Hicks, qui a dirigé les initiatives de sciences ouvertes du Beilstein-Institut pendant plus de 15 ans.  
{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
Qualificação: O Beilstein-Institut é uma fundação sem fins lucrativos com sede em Frankfurt, Alemanha. Os resultados de todos os nossos projetos estão disponíveis gratuitamente e não cobramos nenhuma taxa pelos nossos serviços. Apoiamos a ciência e os cientistas em todo o mundo com nossos periódicos platinum em acesso livre (sem custos para autores ou leitores), projetos de padronização de dados e simpósios. Criamos nosso próprio sistema de envio, rastreamento e publicação, além de um servidor de pré-impressão, e estamos conscientes dos desafios na implementação de novos procedimentos ou fluxos de trabalho. Estamos ativamente envolvidos em iniciativas comunitárias, incluindo OASPA, Free Journal Network, DORA, RDA, CDIG, GO FAIR, e também participamos do projeto de referências abertas i4OC.

Benefícios para a Crossref: Temos um histórico longo e único na comunidade química e científica livre alemã/europeia. Nossa rede diversificada, proveniente das várias conferências que organizamos e patrocinamos sobre os tópicos de ciência livre e ferramentas para comunicação acadêmica, pode beneficiar a Crossref. Criamos e operamos nosso próprio servidor de pré-impressão, o que nos dá um insight excepcional da interação entre as pré-impressões e os artigos publicados, e gostaríamos de ajudar a trazer metadados de última geração para as pré-impressões.

Como representamos a comunidade e agregamos diversidade: Como nós, aproximadamente 14.000 das editoras listadas no Directory of Open Access Journals (DOAJ) têm dois ou menos títulos, e a maioria dos provedores de conteúdo da Crossref entregam <1.000 artigos por ano e estão na categoria de “nível baixo de taxa”. Nós, da (muito) pequena editora independente, somos claramente um setor editorial importante, mas estamos gravemente sub-representados nas discussões e nos conselhos onde as políticas e decisões são tomadas. A Crossref visa expandir seu alcance e interagir ainda mais com a comunidade editorial mundial, portanto, é necessário incluir essas diversas vozes e se envolver com essas pequenas editoras.

Conclusão: Nós, como você, queremos melhorar a comunicação e oferecer uma infraestrutura sustentável e compartilhada para respaldar a comunicação das pesquisas. Acreditamos que a visão e os objetivos de nossas duas organizações sem fins lucrativos estão muito bem alinhados e que estamos bem posicionados para representar a voz indispensável das editoras muito pequenas.

### Declaração pessoal
O que posso oferecer: Como uma ex-pesquisadora que se tornou uma editora sem fins lucrativos, posso oferecer uma perspectiva única com experiência prática em publicações acadêmicas, assim como um insight das lutas e barreiras que as editoras menores enfrentam. Dentre outras funções em nosso instituto sem fins lucrativos, gerencio e desenvolvo nossos periódicos platinum de acesso livre, desenvolvi nosso servidor de pré-impressão específico para periódicos e sou responsável por nossos metadados de periódicos e pré-impressão.

Sou também um dos membros fundadores da Free Journal Network, uma organização sem fins lucrativos que trabalha para facilitar a publicação sustentável em acesso livre. Todos os periódicos acadêmicos em nossa coalizão são administrados de acordo com o modelo Fair Open Access (os periódicos são dirigidos pela comunidade acadêmica e não têm barreiras financeiras para leitores e autores).

Conhecimento dos serviços da Crossref: Meu envolvimento no grupo de interesse dos metadados da Crossref me permitiu apreciar ainda mais a verdadeira abordagem orientada para a comunidade da Crossref em obter feedback e envolver todas as partes interessadas, grandes e pequenas. Estamos bem conscientes sobre os serviços e benefícios que a Crossref oferece para a comunidade, e a importância do papel que a Crossref desempenha no desenvolvimento de novos identificadores e as iniciativas de metadados não pode ser superestimada.

Visão: Meu objetivo é capacitar pequenas editoras dirigidas por acadêmicos e ajudar a identificar maneiras de tornar a comunicação científica mais transparente e justa. Se for eleito para o Conselho da Crossref, apoiarei diretamente, me concentrando ainda em: pré-impressão de metadados, melhoria da transparência na publicação acadêmica e suporte a dados FAIR.

Conclusão: Será uma verdadeira honra representar pequenas editoras no Conselho da Crossref e, se eleito, meu suplente será o Dr. Martin Hicks, que dirigiu as iniciativas de ciência livre do Beilstein-Institut durante mais de 15 anos.  
{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
Antecedentes: el Beilstein-Institut es una fundación sin ánimo de lucro con sede en Fráncfort del Meno, Alemania. Los resultados de todos nuestros proyectos están disponibles de manera gratuita, y no exigimos ninguna tarifa por el uso de nuestros servicios. Apoyamos a la ciencia y a los científicos de alrededor del mundo con nuestras revistas de acceso abierto platino (sin costes para autores ni lectores), proyectos de estandarización de datos y simposios. Hemos creado nuestro propio sistema de envío, seguimiento y publicación, además de un servidor de preimpresión, por lo que somos conscientes de los retos a la hora de implementar nuevos procedimientos o flujos de trabajo. Estamos involucrados de manera proactiva en iniciativas comunitarias, como la OASPA, Free Journal Network, DORA, RDA, CDIG, GO FAIR y también participamos en el proyecto de referencias abiertas i4OC.

Beneficios para Crossref: Contamos con un largo y singular historial en la comunidad de química y ciencia abierta alemana/europea. Nuestra red diversa, alimentada por la multitud de conferencias que hemos organizado y patrocinado sobre temas de ciencia abierta y herramientas de comunicación académica, puede resultar beneficiosa para Crossref. Hemos creado nuestro propio servidor de preimpresión, el cual gestionamos, que nos aporta una percepción excepcional sobre cómo las preimpresiones y los artículos publicados interactúan entre sí, y nos gustaría ayudar a traer los metadatos de nueva generación a las preimpresiones.

Cómo representamos a la comunidad y añadimos diversidad: al igual que nosotros, aproximadamente 14 000 de las editoriales que figuran en el Directorio de revistas de acceso abierto (DOAJ) cuentan con dos o menos títulos, y la mayoría de los proveedores de contenido de Crossref ofrecen menos de 1000 artículos al año y están en la categoría de «nivel de tarifa baja». Nosotros somos una editorial independiente y (muy) pequeña, y claramente somos un sector importante en el ámbito de las publicaciones, pero estamos enormemente infrarrepresentados en debates y juntas donde se toman decisiones y se crean políticas. Crossref desea expandir su alcance e interactuar en mayor medida con la comunidad de publicaciones internacional; por tanto, es necesario incluir estas voces diversas e interactuar con editoriales pequeñas.

Conclusión: nosotros, al igual ustedes, deseamos mejorar las comunicaciones académicas y proveer una infraestructura sostenible y compartida para apoyar la comunicación en la investigación. Creemos que la visión y las metas de nuestras dos organizaciones sin ánimo de lucro están muy bien alineadas y que estamos en la posición adecuada para representar a las editoriales más pequeñas que necesitan que se les dé voz.

### Declaración personal
Qué puedo ofrecer: como antigua investigadora convertida en editora sin ánimo de lucro, puedo aportar una perspectiva única basada en mi experiencia directa en el campo de las publicaciones académicas, además de aportar una visión de las dificultades y barreras a las que se enfrentan las editoriales pequeñas. Entre otras funciones, en nuestro instituto sin ánimo de lucro me encargo de gestionar y desarrollar nuestras revistas de acceso abierto platino, he desarrollado nuestro servidor de preimpresión específico para revistas, y soy responsable de los metadatos de nuestra revista y preimpresión.

También soy uno de los miembros fundadores de la junta de la Free Journal Network, una organización sin ánimo de lucro que trabaja para ofrecer publicaciones sostenibles de acceso abierto. Todas las revistas académicas de nuestra coalición se publican acorde al modelo Fair Open Access (las revistas están dirigidas por la comunidad académica y no se impone ninguna barrera económica para lectores y autores).

Conocimiento de servicios de Crossref: mi involucración con el grupo de interés de metadatos de Crossref me ha permitido apreciar aún más el verdadero enfoque de Crossref orientado a la comunidad para conseguir opiniones e involucrar a todas las partes interesadas, tanto las de gran tamaño como las de pequeño tamaño. Conocemos bien los servicios y beneficios para la comunidad que aporta Crossref, y no se puede pasar por alto la importancia que juega Crossref en el desarrollo de nuevas iniciativas de identificación y metadatos.

Visión: mi objetivo es dar poder a las editoriales académicas pequeñas y ayudar a identificar las maneras de hacer que la comunicación académica sea más transparente y justa. Si se me eligiese para la junta de Crossref, apoyaría de manera directa, y prestando una atención especial, los metadatos de preimpresiones, la mejora de la transparencia en las publicaciones académicas y el fomento de los datos de FAIR.

Conclusión: sería un gran honor para mí representar a las pequeñas editoriales en la junta de Crossref, y si se me eligiese, mi sustituto sería el Dr. Martin Hicks, líder de las iniciativas de ciencia abierta del Beilstein-Institut durante más de 15 años.
{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
배경: Beilstein-Institut는 독일 프랑크푸르트에 위치한 비영리 재단입니다. 우리 프로젝트의 모든 결과물은 무료로 제공되며, 우리의 서비스에 대해 어떠한 요금도 받지 않습니다. 우리는 우리의 플래티넘 오픈 액세스 학술지(저자나 독자에게 어떠한 비용도 없음), 데이터 표준화 프로젝트 및 심포지엄으로 과학 및 전 세계의 과학자들을 지원합니다. 우리는 프리프린트 서버 외에도 자체적으로 제출, 추적 및 출판 시스템을 마련했으며, 그렇기 때문에 새로운 절차 또는 워크플로우 이행의 어려움을 잘 알고 있습니다. 우리는 OASPA, Free Journal Network, DORA, RDA, CDIG, GO FAIR를 포함한 커뮤니티 이니셔티브에 적극적으로 관여하고 있고, 또한 i4OC 오픈 레퍼런스 프로젝트에도 참여하고 있습니다.

Crossref가 얻는 혜택: 우리는 독일/유럽 화학 및 오픈 사이언스 커뮤니티에 길고 특별한 이력을 갖고 있습니다. 오픈 사이언스 및 학술적 소통을 위한 도구를 주제로 우리가 조직하고 후원해 온 많은 컨퍼런스에 기반한 우리의 다양한 네트워크가 Crossref에 혜택이 될 수 있습니다. 우리에게 프리프린트와 출판된 논문 사이의 상호작용에 대한 뛰어난 통찰을 제공하는, 자체적 프리프린트 서버를 구축 및 운영해 왔으며, 프리프린트에 차세대 메타데이터를 구현하는 것을 돕고자 합니다.

우리가 커뮤니티를 대변하고 다양성을 추가하는 방법: 우리처럼, 오픈 액세스 학술지 명부(Directory of Open Access Journals, DOAJ)에 게재된 약 14,000개 퍼블리셔들은 둘 또는 그보다 적은 타이틀을 갖고 있으며, 대부분의 Crossref 콘텐츠 제공자들은 연간 1,000개 미만의 아이템을 제공하고 “저렴한 요금 티어” 카테고리에 속합니다. (매우) 작은 독립 퍼블리셔인 우리는 분명 출판에서 중요한 섹터이지만, 정책 및 결정이 정해지는 논의 및 위원회에서 그 대변은 심각하게 불충분합니다. Crossref는 아웃리치 확대와 글로벌 출판 커뮤니티와의 교류 증대를 목표로 하고 있으며, 그렇기 때문에 이러한 다양한 목소리를 포함시키고 이들 소규모 퍼블리셔들과 관계를 맺는 것이 필수적입니다.

결론: 여러분처럼 우리도 학술적 소통을 보다 잘 하고, 연구 소통을 지원하기 위해 지속가능하고 공유된 인프라를 제공하고자 합니다. 우리의 두 비영리 단체의 비전 및 목표가 소규모 퍼블리셔의 목소리를 대변하기에 매우 적절하고 잘 자리잡혀 있다고 우리는 생각합니다.


### 자기소개서
제가 제공할 수 있는 것: 전직 연구원에서 비영리 퍼블리셔로 전환한 저는 학술 출판에서의 실무 경험에서 우러나온 특별한 관점과 함께, 소규모 퍼블리셔가 직면하는 난관 및 장벽에 대한 통찰을 제공할 수 있습니다. 저희 비영리 단체에서 다른 역할 가운데서도 저는 플래티넘 오픈 액세스 학술지를 관리 및 개발하고 있으며, 학술지 전용 프리프린트 서버를 마련했으며, 학술지 및 프리프린트 메타데이터를 담당하고 있습니다.

저는 또한 지속가능한 오픈 액세스 출판을 촉진시키기 위해 노력하는 비영리 단체, Free Journal Network의 위원회 창립 위원 중 한 명이기도 합니다. 저희 연합의 모든 학술지는 Fair Open Access 모델에 따라 운영됩니다(학술지는 학술 커뮤니티에 의해 주도되며 독자 및 저자에게 어떠한 재정적 장벽도 없습니다).

Crossref 서비스의 지식: Crossref Metadata Interest Group과의 관계 덕분에 저는 Crossref의 피드백을 얻고 크고 작은 모든 이해관계자들을 개입시키는 데 있어 진정한 커뮤니티 지향적 접근에 대해 보다 더 진가를 알아볼 수 있었습니다. 저희는 Crossref가 제공하는 커뮤니티에 대한 서비스 및 혜택을 잘 알고 있으며, Crossref가 새로운 식별자 및 메타데이터 이니셔티브의 개발에서 수행하는 역할은 매우 중요합니다.

비전: 저의 목표는 소규모 학술 퍼블리셔에게 힘을 실어주고, 학술적 소통이 보다 투명하고 공정하게 이뤄지도록 할 방법을 식별할 수 있도록 하는 것입니다. 만약 제가 Crossref 위원회에 선출된다면, 다음 사항에 추가적으로 집중함으로써 직접적으로 지원할 것입니다: 프리프린트 메타데이터, 학술 출판의 투명성 향상 및 FAIR 데이터 지원.

결론: Crossref 위원회에서 소규모 퍼블리셔를 대변하는 것은 진정 영예로운 일일 것이며, 선출된다면 저의 대체자는 15년 이상 동안 Beilstein-Institut의 오픈 사이언스 이니셔티브를 이끌어 온 Dr. Martin Hicks가 될 것입니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="kcse"></a>
## Kihong Kim, Korean Council of Science Editors, South Korea<br>
<img src="/images/board-governance/kihong-kim.jpg" alt="Kihong Kim, Korean Council of Science Editors" width="225px" class="img-responsive" />   

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
The Korean Council of Science Editors (KCSE) was established in 2011 with the aims of promoting the quality of science journals published in Korea to satisfy international standards, through the exchange and discussion of the latest information and ideas on editing and publishing. Currently, 346 journals published in Korea covering all areas of science, engineering, and medicine are KCSE member journals.   

The KCSE has organized and led many activities including workshops and training programs for editors, manuscript editors, publishers, and researchers every year. It has also given great importance to international cooperation and played a pivotal role in the creation of the Council of Asian Science Editors (CASE) in 2014, in which editors from many Asian countries have participated.  

Since its inception, the KCSE has emphasized improving the style and format of Korean science journals to meet the international standards. In particular, the services and technologies developed by Crossref, such as CrossCheck and CrossMark, have been regarded as important topics in many KCSE programs. Currently, the number of papers deposited by the KCSE member journals annually to Crossref is estimated to be about 25,000 and is on the rise. In 2014, the KCSE launched its official journal, Science Editing. Until now, this journal has published more than 10 articles that introduce Crossref’s visions and services.  

The KCSE and Crossref share a common vision that scholarly information can be found and used much more easily by linking and networking them through the use of their DOIs and metadata. The KCSE is a representative organization in Korea that can introduce new advances and services of Crossref widely to Korean researchers, editors, and publishers. It can also spread Crossref's technology to many countries in Asia through international activities. These contributions can be a great help to Crossref in increasing diversity and getting new perspectives.  

### Personal statement<br>
I am a professor at Ajou University in Korea majoring in theoretical physics. Since 2012, I have been serving as the chair of the KCSE committee on information and publishing. I have also served as the editor-in-chief of Science Editing, the official journal of the KCSE, since its launching in 2014. This year I was elected as the vice-president of the KCSE and am going to be its next president. During the last nine years, I have actively participated in the activities of both KCSE and CASE. Through these activities, I have gained much experience in many aspects of journal editing and publishing. I remember attending the Crossref Annual Meeting in 2014 and being deeply impressed by the innovative ideas presented there. I am quite familiar with the services provided by Crossref and have tried to keep up with the new advances. As the editor-in-chief of Science Editing, I have been deeply involved in publishing over 10 articles introducing Crossref's activities.  

If I get elected to the board, I will be able to contribute to developing a stronger collaboration between the KCSE and Crossref. I can help the services and technologies of Crossref to be adopted more widely by Korean journals. I can also collect the specific needs and opinions of Korean editors, publishers, and researchers and provide a useful input for Crossref. With a close tie to the CASE, I can also help to spread Crossref’s technologies to other Asian countries.
The Internet has fundamentally changed the concepts of information sharing and academic research. Rapid changes are expected to occur in all areas of scholarly publishing and many new ideas will be continually developed. Crossref is one of the most important organizations in leading such developments and it will be an honor if I get an opportunity to participate in such activities.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Le Conseil coréen des rédacteurs scientifiques (KCSE) a été fondé en 2011 avec pour objectif la promotion de la qualité des revues scientifiques publiées en Corée pour satisfaire aux normes internationales, par l’intermédiaire d’échanges et de discussions portant sur les dernières informations et idées sur l’édition et la publication. Actuellement, 346 revues publiées en Corée couvrant tous les domaines de la science, de l'ingénierie et de la médecine sont des revues membres du KCSE.   

Le KCSE organise et dirige chaque année de nombreuses activités notamment des ateliers et des programmes de formations pour des rédacteurs, des éditeurs de manuscrits, des éditeurs et des chercheurs. Il donne également une grande importance à la coopération internationale et a joué un rôle clé dans la création du Conseil des rédacteurs scientifiques asiatiques (CASE) en 2014, auquel des éditeurs de nombreux pays asiatiques ont participé.  

Depuis ses débuts, le KCSE insiste sur l’amélioration du style et du format des revues scientifiques coréennes afin de s’adapter aux normes internationales. Les services et la technologie développés par Crossref, comme CrossCheck et CrossMark, notamment, ont été considérés comme des sujets d’importance dans de nombreux programmes KCSE. On estime actuellement le nombre de journaux déposés chaque année par les journaux membres du KCSE à Crossref à 25 000 et ce nombre est en augmentation. En 2014, le KCSE a lancé son journal officiel, Science Editing. Jusqu’à présent, cette revue a publié plus d’une dizaine d’articles présentant la vision et les services de Crossref.  

Le KCSE et Crossref partagent une vision selon laquelle les informations universitaires peuvent être trouvées et exploitées bien plus facilement en les reliant et en les mettant en réseau par l’utilisation de leurs DOI et de leurs métadonnées. Le KCSE est une organisation représentative en Corée qui peut présenter les nouvelles avancées et services de Crossref de façon large aux chercheurs, rédacteurs et éditeurs coréens. Il peut aussi diffuser la technologie de Crossref dans de nombreux pays asiatiques par l’intermédiaire d’activités internationales. Ces contributions peuvent constituer une aide précieuse à Crossref en augmentant la diversité et en obtenant de nouvelles perspectives.  

### Déclaration personnelle
Je suis professeur à l’Université d’Ajou en Corée, et je me spécialise en physique théorique. Depuis 2012, je préside le comité du KCSE sur les informations et l’édition. J’ai aussi exercé les fonctions de rédacteur en chef de Science Editing, la revue officielle du KCSE depuis son lancement en 2014. Cette année j’ai été élu vice-président du KCSE et j’en serai le prochain président. Ces neuf dernières années, j’ai pris activement part aux activités du KCSE et du CASE. Ces activités m’ont permis d’acquérir une grande expérience dans de nombreux aspects de l’édition et de la publication de revue. Je me souviens avoir été très impressionné par les idées innovantes présentées à la réunion annuelle de Crossref en 2014. Je connais assez bien les services proposés par Crossref et j’ai essayé de suivre les nouvelles avancées. En tant que rédacteur en chef de Science Editing, je me suis énormément impliqué dans la publication de plus de 10 articles présentant les activités de Crossref.  

Si je suis élu au comité, je serai en mesure de contribuer au développement d'une meilleure collaboration entre le KCSE et Crossref. Je peux aider à faire adopter plus largement les services et les technologies de Crossref par les revues coréennes. Je peux également recueillir les besoins et les opinions spécifiques des rédacteurs, éditeurs et chercheurs coréens et apporter une contribution utile à Crossref. En étroite relation avec le CASE, je peux aussi contribuer à la diffusion des technologies de Crossref dans d’autres pays d’Asie.  

Internet a changé fondamentalement les concepts de partage des informations et de recherche académique. Des changements rapides devraient intervenir dans tous les domaines de l’édition universitaires et beaucoup de nouvelles idées vont être en développement constant. Crossref est l’une des plus importantes organisations dans la conduite de ces développements et je serai honoré d’avoir la possibilité de prendre part à ces activités.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
O Conselho Coreano de Editores de Ciência (KCSE) foi estabelecido em 2011 com o objetivo de promover a qualidade dos periódicos científicos publicados na Coreia para atender aos padrões internacionais pela troca e discussão das informações e ideias mais recentes sobre edição e publicação. Atualmente, 346 periódicos publicados na Coreia, cobrindo todas as áreas da ciência, engenharia e medicina, são periódicos membros do KCSE.

O KCSE organizou e dirigiu várias atividades, incluindo workshops e programas de treinamento para editores, editores de manuscritos, editoras e pesquisadores todos os anos. Ele também deu grande importância à cooperação internacional e desempenhou um papel fundamental na criação do Conselho de Editores Científicos da Ásia (CASE) em 2014, do qual participaram editores de vários países asiáticos.

Desde a sua criação, o KCSE enfatiza a melhoria do estilo e do formato dos periódicos científicos coreanos para atender aos padrões internacionais. Especialmente, os serviços e tecnologias desenvolvidos pela Crossref, como CrossCheck e CrossMark, foram considerados tópicos importantes em vários programas do KCSE. Atualmente, o número de artigos depositados a cada ano pelos periódicos membros do KCSE na Crossref é estimado em cerca de 25.000 e está aumentando. Em 2014, o KCSE lançou seu periódico oficial, Science Editing. Até agora, este periódico publicou mais de 10 artigos que apresentam as visões e serviços da Crossref.

O KCSE e a Crossref compartilham uma visão comum segundo a qual as informações acadêmicas podem ser encontradas e usadas com muito mais facilidade, vinculando-as e conectando-as em rede graças à utilização de seus DOIs e metadados. O KCSE é uma organização representativa na Coreia que pode apresentar amplamente novos avanços e serviços da Crossref a pesquisadores, editores e editoras coreanas. Ele também pode divulgar a tecnologia da Crossref para vários países da Ásia através de atividades internacionais. Essas contribuições podem ser uma grande ajuda para a Crossref aumentar a diversidade e obter novas perspectivas.

### Declaração pessoal
Sou professor na Universidade Ajou, na Coreia, com especialização em teoria da física. Desde 2012, atuo como presidente do comitê de informação e de publicação do KCSE. Também fui editor-chefe da Science Editing, o periódico oficial do KCSE, desde o seu lançamento em 2014. Este ano fui eleito vice-presidente do KCSE e vou ser o seu próximo presidente. Durante os últimos nove anos, participei ativamente das atividades do KCSE e do CASE. Graças a essas atividades, adquiri muita experiência em vários aspectos da edição e publicação de periódicos. Lembro-me de participar da Crossref Annual Meeting em 2014 e de ficar profundamente impressionado com as ideias inovadoras que foram apresentadas. Estou bastante familiarizado com os serviços prestados pela Crossref e tenho tentado acompanhar os novos avanços. Como editor-chefe da Science Editing, estive profundamente envolvido na publicação de mais de 10 artigos apresentando as atividades da Crossref.

Se eu for eleito para o conselho, poderei contribuir para o desenvolvimento de uma colaboração mais sólida entre o KCSE e a Crossref. Posso ajudar no sentido de que os serviços e tecnologias da Crossref sejam adotados mais amplamente pelos periódicos coreanos. Também posso buscar as necessidades e opiniões específicas de editores, editoras e pesquisadores coreanos e contribuir de maneira útil para a Crossref. Com um vínculo estreito com o CASE, também posso ajudar a divulgar as tecnologias da Crossref para outros países asiáticos.  

A Internet mudou fundamentalmente os conceitos de compartilhamento de informações e de pesquisa acadêmica. É esperado que ocorram mudanças rápidas em todas as áreas da publicação acadêmica e muitas novas ideias sejam desenvolvidas continuamente. A Crossref é uma das organizações mais importantes na direção de tais desenvolvimentos e será uma honra se eu tiver a oportunidade de participar de tais atividades.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
El Consejo coreano de editores científicos (KCSE, por sus siglas en inglés) se estableció en 2011 con el propósito de promover la calidad de las revistas científicas publicadas en Corea para así satisfacer estándares internacionales mediante el intercambio y el debate de la última información e ideas sobre edición y publicación. Actualmente, hay 346 revistas publicadas en Corea que abarcan todas las áreas de la ciencia, la ingeniería y la medicina y que pertenecen al KCSE.

El KCSE ha organizado y llevado a cabo cada año muchas actividades, incluidos talleres y programas de formación para editores, editores de manuscritos, editoriales e investigadores. También ha prestado especial atención a la cooperación internacional, y ha jugado un papel clave en la creación del Consejo de editores científicos de Asia (CASE, por sus siglas en inglés) en 2014, en el que han participado editores de muchos países asiáticos.

Desde sus orígenes, el KCSE ha subrayado la importancia de mejorar el estilo y el formato de las revistas científicas coreanas para que sigan estándares internacionales. Los servicios y tecnologías desarrollados por Crossref, como CrossCheck o CrossMark, han sido temas de particular importancia en muchos programas del KCSE. En la actualidad, se estima que el número de documentos depositados en Crossref anualmente por las revistas miembros del KCSE ronda los 25 000 y subiendo. En 2014, el KCSE publicó su revista oficial, Science Editing. Hasta ahora, la revista ha publicado más de 10 artículos que introducen la visión y servicios de Crossref.

El KCSE y Crossref comparten una visión común sobre cómo la información académica puede encontrarse y usarse de una manera mucho más fácil si se vincula y se interconecta usando sus DOI y metadatos. El KCSE es una organización representante de Corea que introduce nuevos avances y servicios de Crossref de manera amplia entre los investigadores, editores y editoriales de Corea. Difunde también la tecnología de Crossref entre numerosos países de Asia mediante actividades internacionales. Estas contribuciones pueden ser de gran ayuda para Crossref en su aumento de la diversidad y la obtención de nuevas perspectivas.

### Declaración personal
Soy profesor en la Universidad de Ajou de Corea, especializado en física teórica. Llevo siendo presidente del comité del KCSE en los departamentos de información y publicación desde 2012. También soy editor jefe de Science Editing, la revista oficial del KCSE, desde su primera publicación en 2014. Fui elegido este año vicepresidente del KCSE y seré su siguiente presidente. He participado de manera proactiva en actividades del KCSE y el CASE durante los últimos nueve años. Estas actividades me han aportado mucha experiencia en diferentes aspectos relacionados con la edición y publicación de revistas. Recuerdo asistir al encuentro anual de Crossref de 2014 y maravillarme por las ideas tan innovadoras que se presentaron allí. Conozco bien los servicios proporcionados por Crossref e intento mantenerme al día con los nuevos avances. Como editor jefe de Science Editing, he estado profundamente involucrado en la publicación de más de 10 artículos sobre las actividades de Crossref.

Si soy elegido miembro de la junta, podré contribuir en el desarrollo de una colaboración más estrecha entre el KCSE y Crossref. Puedo ayudar a que cada vez más revistas coreanas adopten los servicios y tecnologías de Crossref. También puedo recopilar las opiniones y necesidades específicas de editores, editoriales e investigadores de Corea, y transformarlas en información útil para Crossref. Gracias mi vínculo cercano con el CASE, también puedo contribuir a la difusión de tecnologías de Crossref por otros países de Asia.
Internet ha cambiado esencialmente los conceptos de difusión de información e investigación académica. Se espera que se produzcan cambios inmediatos en todas las áreas de publicación académica y que se vayan desarrollando nuevas ideas continuamente. Crossref es una de las organizaciones más importantes que lideran este desarrollo y sería un honor para mí tener la oportunidad de participar en dichas actividades.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
한국과학학술지편집인협의회(Korean Council of Science Editors, KCSE)는 편집 및 출판에 대한 최신 정보 및 의견의 교환과 논의를 통해 한국에서 출판되는 과학 학술지의 질이 국제 표준을 충족할 수 있도록 촉진하려는 목표로 2011년 설립되었습니다. 현재 한국에 출판되는 과학, 공학 및 의약품의 모든 영역을 망라하는 346개 학술지가 KCSE 회원 학술지입니다.

KCSE는 매년 편집자, 원고 편집자, 퍼블리셔 및 연구원들을 위한 워크샵 및 교육 프로그램을 포함한 많은 활동을 조직하고 이끌어 왔습니다. 또한 국제 협력에도 노력을 기울여 2014년 아시아과학학술지협의회(Council of Asian Science Editors, CASE)의 탄생에 중대한 역할을 하였으며, 여기엔 많은 아시아 국가의 편집자들이 참여하고 있습니다.

설립 이래 KCSE는 한국 과학 학술지가 국제 표준을 충족할 수 있도록 그 스타일 및 포맷의 개선을 강조해 왔습니다. 특히 CrossCheck 및 CrossMark와 같은 Crossref에 의한 서비스 및 기술은 많은 KCSE 프로그램에서 중요한 주제로 다뤄져 왔습니다. 현재 KCSE 회원 학술지에 의해 Crossref에 연간 제출되는 논문의 수는 약 25,000개로 예상되며 증가 추세에 있습니다. 2014년에 KCSE는 공식 학술지인 Science Editing을 론칭했습니다. 지금까지 이 학술지는 Crossref의 비전 및 서비스를 소개하는 10개 이상의 논문을 게재했습니다.

KCSE와 Crossref는 학술 정보가 DOI 및 메타데이터의 사용을 통한 연계 및 네트워킹으로 훨씬 더 간편하게 발견 및 사용될 수 있다는 공통된 비전을 공유합니다. KCSE는 Crossref의 새로운 진전 및 서비스를 한국의 연구원, 편집자 및 퍼블리셔에게 폭넓게 소개할 수 있는 한국의 대표적 단체입니다. 협의회는 또한 국제 활동을 통해 Crossref의 기술을 아시아의 많은 국가에 전파할 수 있습니다. 이러한 공헌은 다양성을 향상시키고 새로운 관점을 확보하여 Crossref에 큰 도움이 될 수 있습니다.

### 자기소개서
저는 한국 아주대학교(Ajou University) 이론물리학 전공 교수입니다. 2012년 이래 저는 KCSE 정보 및 출판 위원회의 의장직을 수행해 왔습니다. 저는 또한 KCSE의 공식 학술지인 Science Editing의 편집장직을 2014년 론칭 이래 맡아 왔습니다. 올해 저는 KCSE의 부회장으로 선출되었고 차기 회장이 될 예정입니다. 지난 9년간, 저는 KCSE 및 CASE 모두의 활동에 왕성하게 참여했습니다. 이러한 활동을 통해 저는 학술지 편집 및 출판의 여러 측면에서 많은 경험을 얻었습니다. 저는 2014년 Crossref 연례회의에 참석해 그곳에서 발표되었던 혁신적인 아이디어에 대해 깊게 감명을 받았던 것이 기억납니다. 저는 Crossref에 의해 제공되는 서비스에 꽤 익숙하며 새로운 진전과 보조를 맞추기 위해 노력해 왔습니다. Science Editing의 편집장으로서 저는 Crossref의 활동을 소개하는 10개 이상의 논문을 게재하는 데 깊이 관여해 왔습니다.

만약 제가 위원회에 선출된다면, 저는 KCSE와 Crossref 간 보다 강력한 협력을 구축하는 데 기여할 수 있을 것입니다. 저는 Crossref의 서비스 및 기술이 한국의 학술지에 의해 보다 폭넓게 채용될 수 있도록 도울 수 있습니다. 저는 또한 한국 편집자, 퍼블리셔 및 연구원들의 특정 필요 및 의견을 수집할 수 있으며 Crossref를 위해 유용한 입력을 제공할 수 있습니다. CASE와의 밀접하게 연계되어 있는 저는 또한 Crossref의 기술을 타 아시아 국가들에 전파하는 데 도움을 줄 수 있습니다.

인터넷은 정보 공유와 학술 연구의 개념을 근본적으로 변화시켰습니다. 모든 학술 출판 분야에서 급속한 변화가 이뤄질 것으로 예상되며 새로운 많은 아이디어가 계속해서 개발될 것입니다. Crossref는 그러한 개발을 이끄는 데 가장 중요한 단체 중 하나이며 제가 만약 그러한 활동에 참여할 기회를 갖게 된다면 영광스러운 일일 것입니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="openedition"></a>
## Marin Dacos, OpenEdition, France<br>
<img src="/images/board-governance/marin-dacos.jpg" alt="Marin Dacos, OpenEdition" width="225px" class="img-responsive" />   

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
OpenEdition is a major European platform for open access to HSS in Europe, which provides publishing services for 500 journals, 100 university presses (10 000 books) and more than 3000 academic blogs. OpenEdition is a comprehensive digital publishing infrastructure at the service of Humanities and social Sciences. OpenEdition is thus a space dedicated to the promotion of research, publishing hundreds of thousands of scientific documents in open access, while respecting the economic equilibrium of publications. OpenEdition is based in Marseilles and Paris. OpenEdition is pursuing at european level with a dozen of countries (Italy, Germany, Netherlands, UK, Croatia, Poland, Greece, etc.) under the OPERAS umbrella.  

Recognized as national research infrastructure since 2016, OpenEdition is developed by the CNRS, Aix-Marseille University, the EHESS and the University of Avignon. It is a public non-profit initiative supported by major research institutions. OpenEdition also aligns its work with the framework of the National plan for Open Science.   

OpenEdition was awarded the “Investments for the Future” label and 7 years grant in 2012 by the Ministry of Research.   

### Personal statement <br>
With 20 years of experience as the founding director of OpenEdition and now as Head of research of OpenEdition, I am very familiar with the diversity of the HSS publishing ecosystem in Europe.   

I have developed tools and adapted standards in order to include as many publishing actors as possible and to drive them to the most open solutions. As DOIs and Crossref have become key services for open science, it is necessary to take into account small and medium sized publishers and platforms, in order to provide a comprehensive service for the whole ecosystem, and not only to major players and international initiatives. To do so, the diversity of languages and disciplines for future developments of Crossref services should be acknowledged.  

I consider Crossref as a major Open Science Infrastructure. I have served as a member of the Board of Directors of Crossref for three years now and have learned a lot. I have tried to contribute to a broader definition of the mission of Crossref, including open science as a core principle.  

I have also been involved in public policies, as Open Science Advisor for the Director General for Research and Innovation of the Ministry of Higher Education, Research and Innovation in France. I am the main author of the National Open Science Plan (2018). The plan has various Crossref implementations: focusing on links between digital research objects, beyond publications only, and implementing as far as possible norms like CRediT (Contributor Roles Taxonomy).  

My alternate is Marie Pellen, Director of OpenEdition. Marie Pellen joined OpenEdition in 2007. She created in 2010 the LusOpenEdition project, supported by the Calouste Gulbenkian foundation. Based in Lisbon, she learned Portuguese. Marie Pellen became Deputy Editorial Director of OpenEdition in 2017. She is now the director of OpenEdition, a centre composed of 55 people. She is also a member of the Open Scientific Publishing groups of the French Open Science Committee.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
OpenEdition est une importante plateforme européenne destinée à l’accès libre au HSS en Europe, qui fournit des services d’édition à plus de 500 revues, 100 presses universitaires (10 000 livres) et plus de 3 000 blogs académiques. OpenEdition est une infrastructure d’édition numérique globale au service des Humanités et des Sciences sociales. OpenEdition constitue donc un espace dédié à la promotion de la recherche, à la publication de centaines de milliers de documents scientifiques en accès libre, tout en respectant l’équilibre économique des publications. OpenEdition est implanté à Marseille et à Paris. OpenEdition se développe au niveau européen dans une dizaine de pays (Italie, Allemagne, Pays-Bas, Royaume-Uni, Croatie, Pologne, Grèce, etc.) sous l’égide d’OPERAS.

Reconnu infrastructure de recherche nationale depuis 2016, OpenEdition est développé par le CNRS, l’université d’Aix-Marseille, l’EHESS et l’Université d’Avignon. Il s’agit d’une initiative à but non lucratif soutenue par les principales institutions de recherche. OpenEdition harmonise également son travail avec le cadre du Plan national pour la Science ouverte.

En 2012, OpenEdition a obtenu du Ministère de la Recherche le label « Investissements pour le futur » et une subvention de 7 ans.

### Déclaration personnelle
J’ai 20 ans d’expérience en tant que directeur fondateur d’OpenEdition et maintenant comme Responsable de recherche chez OpenEdition, je connais donc très bien la diversité de l’écosystème de la publication HSS en Europe.

J’ai développé des outils et adapté des normes afin d’intégrer le plus d’acteurs de la publication possible et pour les amener aux solutions les plus ouvertes. Les DOI et Crossref sont devenus des services clés pour la science ouverte, il convient alors de prendre en compte les éditeurs et les plateformes de petite taille et de taille moyenne, afin de fournir un service complet à l’ensemble de l’écosystème, et pas uniquement les principaux acteurs et les initiatives internationales. À cette fin, la diversité des langues et des disciplines pour de futurs développements des services Crossref doit être reconnue.

Pour moi, Crossref est une infrastructure majeure de la Science ouverte. Je suis membre du comité de direction de Crossref depuis trois ans maintenant, et j’y ai beaucoup appris. J’ai essayé d’apporter ma contribution à une définition plus large de la mission de Crossref, en intégrant la science ouverte comme principe de base.

Je m’investis également dans les politiques publiques, en tant que Conseiller Science ouverte pour le Directeur général de la recherche et de l’innovation du Ministre de l’Enseignement supérieur, de la Recherche et de l’Innovation en France. Je suis le principal auteur du Plan national pour la science ouverte (2018). Le plan comporte différentes phases : mettre l’accent sur les liens entre les objets de recherche numériques, au-delà des publications seules, et mettre en œuvre le plus de normes possible telles que la taxonomie CRediT (Contributor Roles Taxonomy).

Ma suppléante est Marie Pellen, Directrice d’OpenEdition. Marie Pellen a rejoint OpenEdition en 2007. Elle a lancé en 2010 le projet LusOpenEdition, soutenu par la fondation Calouste Gulbenkian. Basée à Lisbonne, elle a appris le portugais. Marie Pellen est devenue Directrice de rédaction adjointe d’OpenEdition en 2017. Elle est à présent directrice d’OpenEdition, un centre composé de 55 personnes. Elle est également membre des groupes d'édition scientifique ouverts du Comité français de Science ouverte.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
A OpenEdition é uma plataforma europeia importante de acesso livre para HSS (Humanidades e Ciências Sociais), que fornece serviços editoriais para 500 periódicos, 100 editoras universitárias (10.000 livros) e mais de 3.000 blogues acadêmicos. A OpenEdition é uma infraestrutura editorial digital abrangente a serviço de Humanidades e Ciências Sociais. Assim, a OpenEdition é um espaço dedicado à promoção da pesquisa, publicando centenas de milhares de documentos científicos em acesso livre, respeitando o equilíbrio econômico das publicações. A OpenEdition está localizada em Marselha e Paris. A OpenEdition continua no nível europeu com uma dúzia de países (Itália, Alemanha, Holanda, Reino Unido, Croácia, Polônia, Grécia, etc.) sob a égide da OPERAS.

Reconhecida como infraestrutura de pesquisa nacional desde 2016, a OpenEdition é desenvolvida pelo CNRS (Centro Nacional de Pesquisa Científica), pela Universidade de Aix-Marseille, pela EHESS (Escola de Estudos Avançados em Ciências Sociais) e pela Universidade de Avignon. É uma iniciativa pública sem fins lucrativos apoiada por instituições de pesquisa importantes. A OpenEdition também alinha seu trabalho com a estrutura do Plano Nacional de Ciência Livre.

A OpenEdition foi premiada com o selo “Investimentos para o Futuro” e uma bolsa de 7 anos em 2012 pelo Ministério da Pesquisa.

### Declaração pessoal
Com 20 anos de experiência como diretor de financiamento da OpenEdition e agora como Diretor de Pesquisa, estou bastante familiarizado com a diversidade do ecossistema editorial de HSS na Europa.

Desenvolvi ferramentas e adaptei modelos para incluir tantos autores de publicações quanto possível, e orientá-los para soluções mais abertas. Como os DOIs e a Crossref se tornaram serviços importantes para a ciência livre, é necessário levar em consideração as editoras e plataformas de pequeno e médio porte para oferecer um serviço abrangente para todo o ecossistema, e não somente para integrantes de grande porte e iniciativas internacionais. Para fazer isso, a diversidade de idiomas e as disciplinas para os desenvolvimentos futuros dos serviços da Crossref devem ser considerados.

Considero a Crossref como uma importante infraestrutura de ciência livre. Há três anos sou membro do Conselho de Administração da Crossref e aprendi muito. Tentei contribuir para uma definição mais ampla da missão da Crossref, incluindo a ciência livre como princípio fundamental.

Também estive envolvido em políticas públicas, como Conselheiro de ciência livre do Diretor Geral de Pesquisa e Inovação do Ministério do Ensino Superior, Pesquisa e Inovação da França. Sou o principal autor do Plano Nacional de Ciência Livre (2018). O plano tem diversas implementações da Crossref: ele se concentra nas ligações entre os objetos de pesquisa digital, além de apenas publicações, e implementa, na medida do possível, normas como CRediT (Taxonomia de Funções do Colaborador).

Minha suplente é Marie Pellen, Diretora da OpenEdition. Marie Pellen entrou na OpenEdition em 2007. Em 2010, ela criou o projeto LusOpenEdition, apoiada pela Fundação Calouste Gulbenkian. Estabelecida em Lisboa, ela aprendeu português. Marie Pellen se tornou Diretora Editorial Adjunta da OpenEdition em 2017. Agora, ela é a diretora da OpenEdition, um centro composto por 55 pessoas. Ela também é membro dos grupos de Publicação Científica Livre do Comitê de Ciência Livre da França.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
OpenEdition es una gran plataforma europea de acceso abierto a recursos relacionados con las humanidades y las ciencias sociales, que proporciona servicios de publicación a 500 revistas, 100 editoriales universitarias (10 000 libros) y más de 3000 blogs académicos. OpenEdition es una completa infraestructura de publicación digital al servicio de las humanidades y las ciencias sociales. OpenEdition es un espacio dedicado a la promoción de la investigación que publica cientos de miles de documentos científicos de acceso abierto, a la vez que respeta el equilibrio económico de las publicaciones. OpenEdition tiene sede en Marsella y París. OpenEdition opera a nivel europeo en una docena de países (Italia, Alemania, Países Bajos, Reino Unido, Croacia, Polonia, Grecia, etc.) bajo el amparo de OPERAS.

Reconocida como infraestructura de investigación nacional desde 2016, OpenEdition está desarrollada por el Centro Nacional para la Investigación Científica de Francia (CNRS), la Universidad de Aix-Marsella, la Escuela de Estudios Superiores en Ciencias Sociales de París (EHESS) y la Universidad de Aviñón. Esta iniciativa pública y sin ánimo de lucro está respaldada por grandes instituciones investigadoras. OpenEdition está también sujeto al marco de trabajo del Plan nacional de ciencia abierta francés.

OpenEdition recibió la denominación de «Inversión de futuro» y una subvención de 7 años en 2012 del Ministerio de Investigación.

### Declaración personal
Con 20 años de experiencia como director y fundador de OpenEdition, y ahora como jefe de investigación, conozco bien la diversidad del ecosistema de publicaciones humanísticas y de ciencias sociales en Europa.

He desarrollado herramientas y he adaptado estándares para poder incluir tantas partes como sea posible y para incitarlas a usar soluciones más abiertas. A medida que Crossref y los DOI se convierten en servicios clave para la ciencia abierta, es necesario tener en cuenta a las editoriales y plataformas pequeñas y medianas para poder proveer un servicio completo a todo el ecosistema, no solo a iniciativas internacionales y participantes reconocidos. Para ello, se debe reconocer la diversidad lingüística y de disciplinas para el futuro desarrollo de servicios de Crossref.

Considero a Crossref una gran infraestructura de ciencia abierta. He sido miembro de la junta directiva de Crossref durante tres años y he aprendido mucho. He intentado contribuir en la ampliación de la definición de la misión de Crossref, así como en el establecimiento de la ciencia abierta como un principio fundamental.

También he intervenido en políticas públicas, como consejero sobre ciencia abierta del director general de investigación e innovación del Ministerio de Enseñanza Superior, Investigación e Innovación de Francia. Soy el principal autor del Plan nacional de ciencia abierta de 2018. El plan incluye diferentes implementaciones de Crossref: centrarse en los vínculos entre los objetos de investigación digital, más allá de las publicaciones solamente, e implementar en la medida de lo posible normas como la CRediT (Contributor Roles Taxonomy).

Mi sustituta es Marie Pellen, directora de OpenEdition. Marie Pellen se unió a OpenEdition en 2007. Creó en 2010 el proyecto LusOpenEdition, respaldado por la fundación Calouste-Gulbenkian. Residente en Lisboa, ha aprendido portugués. Marie Pellen se convirtió en vicedirectora editorial de OpenEdition en 2017. Ahora es la directora de OpenEdition, un centro que cuenta con 55 personas. También es miembro de grupos de publicación científica abierta del Comité de ciencia abierta de Francia.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
OpenEdition은 유럽 HSS로의 오픈 액세스를 위한 유럽의 대표적인 플랫폼으로, 500개의 학술지, 100개의 대학 출판(책 10,000권), 3,000개 이상의 학술 블로그에 출판 서비스를 제공하고 있습니다. OpenEdition은 인문학 및 사회과학 분야를 담당하는 포괄적 디지털 출판 인프라입니다. OpenEdition은 출판물의 경제적 형평성을 존중하는 한편, 오픈 액세스로 수십만 과학 문서의 연구, 출판의 촉진에 전념하는 공간입니다. OpenEdition은 마르세유와 파리에 자리하고 있습니다. OpenEdition은 OPERAS 엄브렐라에 따라 십여개국(이탈리아, 독일, 네덜란드, 영국, 크로아티아, 폴란드, 그리스 등)과 함께 유럽적 수준을 추구하고 있습니다.

2016년 이래 국립 연구 인프라로 인정받은 OpenEdition은 CNRS, 엑스-마르세유 대학교(Aix-Marseille University), EHESS 및 아비뇽 대학교(University of Avignon)에 의해 개발되었습니다. 이것은 대표적인 연구 기관이 지원하는 공공 비영리 이니셔티브입니다. OpenEdition은 또한  National plan for Open Science(오픈 사이언스를 위한 국가적 플랜) 프레임워크와 협력하고 있습니다.

OpenEdition은 2012년에 연구부(Ministry of Research)에 의해 “Investments for the Future(미래를 위한 투자)” 라벨 및 7년 보조금에 선정되었습니다.

### 자기소개서
OpenEdition의 창립 이사 및 현재 OpenEdition의 연구 헤드로서 20년의 경험을 지닌 저는 유럽의 HSS 출판 생태계의 다양성에 매우 익숙합니다.

저는 가능한 많은 출판 활동가들을 포함시키고 이들을 가장 개방된 솔루션으로 이끌기 위해 도구들을 개발하고 기준을 적용해 왔습니다. DOI 및 Crossref가 오픈 사이언스를 위한 핵심 서비스가 되어감에 따라 전체 생태계에 대한 포괄적인 서비스를 제공하기 위해 단지 대표 주자 및 국제 이니셔티브만이 아닌, 중소규모 퍼블리셔 및 플랫폼을 포함시키는 것이 중요합니다. 이를 위해, 앞으로의 Crossref 서비스 개발을 위한 언어 및 학문의 다양성을 고려해야 합니다.

저는 Crossref를 대표적인 오픈 사이언스 인프라로 여깁니다. 저는 3년간 이사위원회 위원직을 수행했으며 많은 것을 배웠습니다. 저는 핵심 원칙으로서의 오픈 사이언스를 포함해, Crossref의 미션에 대한 보다 넓은 정의에 기여하기 위해 노력해 왔습니다.

저는 또한 프랑스의 우수 교육, 연구 및 혁신부(Ministry of Higher Education, Research and Innovation)의 연구 및 혁신 사무총장을 위한 오픈 사이언스 고문으로 공공 정책에 관여해 왔습니다. 저는 National Open Science Plan (2018)의 주 저자입니다. 이 플랜은 Crossref의 다양한 적용을 담고 있습니다: 단순히 출판을 넘어 디지털 연구물 간 연계에 집중, CRediT(Contributor Roles Taxonomy, 기여자 역할 분류)과 같은 표준을 최대한 적용.

저의 대체자는 OpenEdition의 디렉터인 Marie Pellen입니다. Marie Pellen은 2007년에 OpenEdition에 합류했습니다. 그녀는 2010년에 Calouste Gulbenkian 재단의 지원을 받는 LusOpenEdition 프로젝트를 만들었습니다. 리스본에 위치해 그녀는 포르투갈어를 배웠습니다. Marie Pellen은 2017년에 OpenEdition의 편집국장보가 되었습니다. 그녀는 이제 55명으로 구성된 센터인 OpenEdition의 디렉터입니다. 그녀는 또한 프랑스 오픈 사이언스 위원회(French Open Science Committee)의 오픈 사이언스 편집 그룹(Open Scientific Publishing Groups) 위원입니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="oxforduni"></a>
## James Phillpotts, Oxford University Press, UK<br>
<img src="/images/board-governance/james-phillpotts.jpg" alt="James Phillpotts, Oxford University Press" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
Oxford University Press is a department of the University of Oxford, with a clear mission of excellence in research, scholarship, and education by publishing worldwide, which informs everything we do. We have an incredibly diverse publishing programme, with products covering a broad academic and educational spectrum, and we aim to ensure that this content is readily discoverable and available to our users, in support of the University’s aims of furthering education and disseminating knowledge. Frequently this is in partnership with academic societies (on behalf of which we publish the majority of our journals list), as well as working with other university presses to help make the best scholarly publishing from around the world easily accessible online.  

In working to fulfil our mission, we share Crossref’s aim of making scholarly communications better – and we are keen to build upon our existing connections within the scholarly communications community to explore how we may work together to further that goal. As the largest University Press, OUP is able to combine and address the perspectives of both a large publishing organization and the academic community. We believe that bringing this broad perspective to Crossref’s board would enable us to help the organization reflect those intersecting considerations and values.  

### Personal statement <br>
Having worked at OUP for over 10 years, I am currently Director of Digital Operations Innovation, with responsibility for digital content models and structures across the Press, including application of content metadata standards and interoperability. Prior to OUP, I was at Wiley and I started my career with Blackwell Publishing.   

I have a keen interest in collaborative projects and initiatives that aim to improve the discourse and sustainability of scholarly communications. As such, I understand the importance of building and sustaining the infrastructure necessary to connect researchers with research outputs, and I would like to offer my knowledge, experience, and enthusiasm for doing so.  

I currently represent OUP’s voting membership of NISO, I am member of the NASIG Digital Preservation Committee, and I serve on the board of the CLOCKSS digital archive. I was also co-Chair of the Transfer Standing Committee for four years, working to foster collaboration on consistent guidelines to ensure journal content remains easily accessible when there is a transfer between publishers.  

Shared standards and the services that Crossref provides are of critical value to the wider publishing community. While the fundamentals may be easy to take for granted, the central role Crossref plays – and the improvements to scholarly communication processes as a result – should not be under-estimated. Crossref not only provides vital metadata registration and dissemination for content providers, but also continues to play a significant role in the development of tools and standards which support innovative publishing products and services.  

Crossref remains one of the most successful examples of collaboration in our industry and has a lot of value to continue to add in an increasingly complex publishing ecosystem. It would be an honour to serve on the board.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Oxford University Press est un département de l’Université d’Oxford, avec une mission d’excellence dans la recherche, l’érudition et l’éducation en publiant dans le monde entier, qui informe de tout ce que nous faisons. Nous avons un programme de publication d’une incroyable diversité, avec des produits couvrant un large spectre académique et éducatif, et notre objectif est de veiller à ce que ce contenu soit facilement accessible et disponible pour nos utilisateurs, pour soutenir l’objectif de l’Université d’approfondir l’éducation et de diffuser le savoir. Cela se fait souvent en partenariat avec des sociétés universitaires (pour le compte desquelles nous publions la majorité de la liste de nos revues), mais aussi en travaillant avec d’autres presses universitaires pour aider à rendre les meilleures publications universitaires du monde entier facilement accessibles en ligne.

En travaillant pour accomplir notre mission, nous partageons l’objectif de Crossref d’améliorer les communications universitaires, et nous sommes désireux de renforcer nos liens existants au sein de la communauté de la communication universitaire afin d’explorer la façon dont nous pouvons travailler de concert pour approfondir cet objectif. En tant que plus grande presse universitaire, OUP est en mesure d’associer et d’aborder les perspectives à la fois d’une grande organisation d’édition et de la communauté universitaire. Nous pensons que le fait d’apporter cette large perspective au comité de Crossref nous permettrait d’aider l’organisation à concrétiser le recoupement entre ces considérations et ces valeurs.

### Déclaration personnelle
Travaillant chez OUP depuis de 10 ans, je suis actuellement Directeur de l’innovation des opérations numériques, responsable des modèles de contenu numériques et des structures de la Presse, notamment l’application de normes de métadonnées de contenu et l'interopérabilité. Avant OUP, je travaillais chez Wiley, et c’est chez Blackwell Publishing que j’ai commencé ma carrière.

Je porte un vif intérêt aux projets collaboratifs et aux initiatives dont l’objectif est d’améliorer le discours et la viabilité des communications universitaires. En tant que tel, je comprends l’importance de l’élaboration et de l’entretien de l’infrastructure nécessaire pour connecter les chercheurs aux résultats de la recherche et j’aimerais offrir mes connaissances, mon expérience et mon enthousiasme à la tâche.

Je représente actuellement les membres votants de l'OUP au sein de la NISO, je suis membre du Comité de préservation numérique de la NASIG, et je siège au Conseil des archives numériques du CLOCKSS. J’ai également exercé les fonctions de co-président de la commission permanente des transferts pendant 4 ans, et j’ai tout fait pour favoriser la collaboration sur des lignes directrices cohérentes pour garantir l’accessibilité du contenu de la revue en cas de transfert entre éditeurs.

Les normes partagées et les services que Crossref fournit sont d’une importance critique pour la communauté de l’édition au sens large. Bien qu’il puisse sembler facile de considérer ces principes fondamentaux comme acquis, il ne faut pas sous-estimer le rôle essentiel que joue Crossref ainsi que les améliorations apportées aux processus de communication académique qui en résultent. Crossref n’assure pas simplement l’enregistrement et la diffusion de métadonnées essentielles à destination des fournisseurs de contenu, mais joue aussi un rôle important dans le développement d’outils et de normes qui appuient les produits et services d’édition innovants.

Crossref reste l’un de nos exemples de collaboration parmi les plus réussies dans notre secteur d’activité et a énormément de valeur ajoutée à apporter dans l’écosystème de plus en plus complexe de l’édition. Je serais honoré de siéger au Conseil.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
A Oxford University Press é um departamento da University of Oxford, com uma missão clara de excelência em pesquisas, bolsa de estudos e educação com publicações em todo o mundo, informando tudo o que fazemos. Temos um programa de publicação incrivelmente diversificado, com produtos que cobrem um amplo espectro acadêmico e educacional, e visamos garantir que esse conteúdo esteja prontamente acessível e disponível para nossos usuários, respaldando os objetivos da Universidade de promover a educação e disseminar conhecimento. Frequentemente, isso é feito em parceria com sociedades acadêmicas (em nome das quais publicamos a maioria da nossa lista de periódicos), bem como trabalhando com outras editoras universitárias para ajudar que as melhores publicações acadêmicas do mundo estejam facilmente acessíveis on-line.

Ao trabalhar para cumprir nossa missão, compartilhamos o objetivo da Crossref de melhorar as comunicações acadêmicas - e estamos ansiosos para aproveitar nossas conexões existentes dentro da comunidade de comunicações acadêmicas para explorar como podemos trabalhar juntos para atingir este objetivo. Como a maior editora universitária, a OUP é capaz de combinar e abordar as perspectivas de uma grande organização editorial e da comunidade acadêmica. Acreditamos que trazer essa perspectiva ampla para o conselho da Crossref nos permitirá ajudar a organização a refletir sobre essas considerações e valores contraditórios.

### Declaração pessoal
Tendo trabalhado na OUP por mais de 10 anos, atualmente sou Diretor de Inovação de Operações Digitais, com responsabilidade por modelos e estruturas de conteúdo digital em toda a editora, incluindo aplicação de padrões de metadados de conteúdo e interoperabilidade. Antes da OUP, eu trabalhei na Wiley e comecei minha carreira na Blackwell Publishing.

Tenho um grande interesse em projetos e iniciativas colaborativas que visem melhorar o discurso e a sustentabilidade da comunicação acadêmica. Desta forma, entendo a importância de criar e manter a infraestrutura necessária para conectar pesquisadores com resultados de pesquisa e gostaria de oferecer meu conhecimento, experiência e entusiasmo para fazê-lo.

Atualmente represento os membros votantes da OUP da NISO, sou membro do Comitê de Preservação Digital da NASIG e atuo no conselho de arquivo digital da CLOCKSS. Também fui Copresidente do Transfer Standing Committee por quatro anos, trabalhando para estimular a colaboração com diretrizes consistentes para garantir que o conteúdo do periódico permaneça facilmente acessível quando houver uma transferência entre editoras.

Os padrões compartilhados e os serviços que a Crossref oferece são de valor crítico para a comunidade editorial em geral. Embora os fundamentos possam ser facilmente considerados óbvios, o papel central que a Crossref desempenha, e as melhorias nos processos de comunicação acadêmica como resultado, não devem ser subestimados. A Crossref não apenas fornece registro e disseminação de metadados vitais para os provedores de conteúdo, mas também continua desempenhando um papel significativo no desenvolvimento de ferramentas e padrões que respaldam produtos e serviços editoriais inovadores.

A Crossref continua sendo um dos exemplos de colaboração mais bem-sucedidos em nosso setor e tem muito valor a ser agregado em um ecossistema editorial cada vez mais complexo. Seria uma honra atuar no conselho.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
Oxford University Press es un departamento de la Universidad de Oxford con un claro objetivo de excelencia en investigación, erudición y educación mediante publicaciones mundiales que informan de todo lo que hacemos. Tenemos un programa de publicaciones increíblemente diverso, con productos que cubren un amplio espectro académico y educativo, y tratamos de asegurar que este contenido sea fácilmente reconocible y esté disponible para nuestros usuarios, en apoyo a los objetivos de la Universidad de fomentar la educación y difundir el conocimiento. Normalmente nos asociamos con sociedades académicas (en cuyo nombre publicamos la mayor parte de nuestra lista de revistas), además de trabajar con otras editoriales universitarias para conseguir que las mejores publicaciones académicas del mundo sean fácilmente accesibles en línea.

Mientras cumplimos nuestra misión, compartimos el objetivo de Crossref de mejorar las comunicaciones académicas y estamos dispuestos a trabajar con nuestras conexiones ya existentes en la comunidad de comunicaciones académicas para explorar cómo podemos trabajar juntos para fomentar esa meta. Como la mayor editorial universitaria, OUP es capaz de combinar y tratar las perspectivas de una gran organización editorial y de la comunidad académica. Creemos que aportar esta gran perspectiva a la junta de Crossref puede permitirnos ayudar a que la organización refleje aquellas consideraciones y valores que coincidan.

### Declaración personal
Con más de 10 años trabajando en OUP, actualmente ostento el puesto de director de innovación de operaciones digitales, y soy el responsable de modelos y estructuras de contenido digital de la editorial, incluidas la aplicación de estándares de metadatos de contenido y la interoperabilidad. Antes de estar en OUP, trabajaba en Wiley y empecé mi carrera con Blackwell Publishing.

Tengo un interés especial en proyectos e iniciativas colaborativas que pretendan mejorar el diálogo y la sostenibilidad de las comunicaciones académicas. Conozco también la importancia de construir y mantener la infraestructura necesaria para conectar a los investigadores con los resultados de otros trabajos, y me gustaría ofrecer mis conocimientos, experiencias y entusiasmo para cumplir con este objetivo.

Represento actualmente al comité de votación de OUP para la NISO, soy miembro del comité de conservación digital del NASIG y pertenezco a la junta del archivo digital de la CLOCKSS. También fui copresidente del comité permanente de Transfer durante cuatro años, donde trabajé para promover la colaboración mediante pautas que aseguraren un contenido de revistas fácilmente accesible al realizarse transferencias entre editoriales.

Los estándares y los servicios compartidos que ofrece Crossref son de vital importancia para toda la comunidad editorial. Puede que se den por hecho fácilmente los fundamentos, pero no se debería subestimar el papel central que desempeña Crossref, y las mejoras que ha traído a los procesos de comunicación académica. Crossref no solo permite el registro y la divulgación de metadatos vitales para proveedores de contenido, sino que también juega un papel significativo en el desarrollo de herramientas y estándares que respaldan productos y servicios de publicación innovadores.

Crossref sigue posicionándose como uno de los ejemplos de colaboración de nuestra industria más exitosos y tiene muchos valores que añadir a un ecosistema de publicaciones que es cada vez más complejo. Sería un honor participar en la junta.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
옥스포드 대학교 출판부(Oxford University Press)는 옥스포드 대학교에 속한 부서로서, 전 세계로의 출판을 통해 우수한 연구, 학문 및 교육이란 명백한 미션과 함께 우리가 하는 모든 것에 대한 정보를 제공합니다. 저희는 매우 다양한 출판 프로그램과 광범위한 학술 및 교육 분야를 망라하는 제품을 갖고 있으며, 교육을 지속하고 지식을 전파한다는 대학교의 목표를 도와 콘텐츠가 저희의 사용자들에게 발견 가능하고 제공될 수 있도록 하는 것을 목표로 하고 있습니다. 전 세계의 최상의 학술 출판이 온라인으로 간편히 접속될 수 있도록, 가끔씩 이는 (우리의 학술지 목록 대다수를 우리가 대신해 출판하는)학술협회와의 파트너십으로 이뤄지며, 타 대학 출판부와 함께 작업하기도 합니다.

미션 달성을 위한 노력에서, 저희는 학술 소통을 더 낫게 만드려는 Crossref의 목표를 공유하며, 저희는 이러한 목표를 실현하기 위해 협력할 수 있는 부분을 모색하며 열정적으로 학술 소통 커뮤니티 내의 기존 연결에 기초해 구축해 가고 있습니다. 가장 큰 규모의 대학 출판부인 OUP는 대형 출판사와 학술 커뮤니티의 관점 모두를 통합하고 그에 부응할 수 있습니다. 이렇게 광범위한 관점을 Crossref의 위원회에 제공함으로써 저희는 단체가 이렇게 교차되는 사고 및 가치를 반영할 수 있도록 할 것입니다.

### 자기소개서
OUP에서 10년 이상 일해 온 저는 현재 콘텐츠 메타데이터 표준 및 상호운용성의 적용을 포함해 출판부 내 디지털 콘텐츠 모델 및 체계에 대한 책임을 지닌 디지털 운영 혁신 디렉터를 맡고 있습니다. OUP에 앞서 저는 Wiley에 있었고 저의 커리어는 Blackwell Publishing과 함께 시작되었습니다.

저는 학술 소통의 담론과 지속가능성 향상을 목표로 하는 협업 프로젝트 및 이니셔티브에 열정을 갖고 있습니다. 이처럼 저는 연구원을 연구 결과와 연결하는 데 필수적인 인프라 구축 및 지속의 중요성을 이해하며, 이를 위해 저의 지식, 경험 및 열정을 제공하고자 합니다.

저는 현재 NISO에 대한 OUP의 표결 멤버십을 대표하며, NASIG Digital Preservation Committee의 위원이며, CLOCKSS 디지털 아카이브 위원회에서 활동하고 있습니다. 저는 또한 4년간 Transfer Standing Committee의 공동의장직을 맡아, 학술지 콘텐츠가 퍼블리셔 간 이전이 있을 경우 쉽게 접근 가능할 수 있도록 일관된 가이드라인에 대한 협력을 장려하기 위해 노력했습니다.

Crossref가 제공하는 공유된 표준 및 서비스는 보다 넓은 출판 커뮤니티에 중요한 가치를 지닙니다. 근본사항을 당연한 것으로 여길 수 있지만, Crossref가 수행하는 중심적 역할 및 그 결과인 학술 소통 절차에 대한 개선사항은 과소평가되어선 안 됩니다. Crossref는 콘텐츠 제공자를 위한 중요한 메타데이터 등록 및 보급을 제공할 뿐만 아니라, 혁신적 출판 제품 및 서비스를 지원하는 도구 및 표준의 개발에 중대한 역할의 수행을 계속합니다.

Crossref는 우리 업계에서 협업에 대한 가장 성공적인 사례 중 하나이며, 점점 더 복잡해지는 출판 생태계에서 계속해서 추가되어야 할 많은 가치를 지니고 있습니다. 제가 위원회에서 일하게 된다면 영광일 것입니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="scielo"></a>
## Abel Packer, Scientific Electronic Library Online (SciELO), Brazil<br>
<img src="/images/board-governance/abel-packer.png" alt="Abel Packer, Scientific Electronic Library Online (SciELO)" width="250px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
My organization is the SciELO Program oriented to the development of national research infrastructure. It is implemented through the 22-year-old SciELO Network composed by 17 national collections of open access peer reviewed journals from Latin American countries, Portugal, Spain and South Africa.   

SciELO is probably the most important international cooperation program on scientific communication among developing countries. SciELO is a web-based library with advanced indexing, publishing and interoperability capabilities aiming at improving quality, visibility and credibility of nationally independently published journals and the research they communicate. SciELO covers all knowledge areas and it is in essence a multilingual operation.   

Since 2018, SciELO has been promoting the alignment of the program and journals with the Open Science modus operandi. Currently, SciELO Network indexes and publishes over 1,200 journals, operates the SciELO Preprints server with a focus on COVID-19 research, is implementing the SciELO Data repository, and is promoting peer review transparency and openness.   

SciELO's presence in the Crossref board in the last three years favored Crossref policies and operation with a broader and inclusive vision of scholarly communication. This commitment towards a global inclusive scientific information flow is the contribution SciELO will bring to the future of Crossref.  

### Personal statement <br>

With more than 20 years of national and international experience on the development of scientific communication policies, systems and networks, my application to continue as a Crossref board member envisages to strengthen the globalization of Crossref policies, services and priorities, and their alignment with Open Science best practices and to position as a voice and demands of Latin American, the Caribbean and other developing regions.   

My alternate will be Luis Gustavo Gomes, SciELO Program leader on administrative, finance and sustainability lines of action.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Mon organisation, le Programme SciELO se consacre au développement de l’infrastructure de recherche nationale. Ce programme est mis en place par l’intermédiaire du réseau SciELO, datant de 22 ans constitué de 17 collections nationales de revues évaluées par des pairs en accès libre de pays d’Amérique latine, du Portugal, d’Espagne et d’Afrique du Sud.

SciELO est probablement le programme de coopération international le plus important sur la communication scientifique dans les pays en développement. SciELO est une bibliothèque en ligne avec des capacités avancées d’indexation, de publication et d’interopérabilité destinée à améliorer la qualité, la visibilité et la crédibilité de revues publiées de façon indépendante au niveau national et des recherches qu’elles communiquent. SciELO couvre tous les domaines de la connaissance, opération fondamentalement multilingue.

Depuis 2018, SciELO favorise l’harmonisation du programme et des revues avec le modus operandi de la Science ouverte. À l’heure actuelle, le réseau SciELO indexe et publie plus de 1 200 revues, exploite le serveur de Préimpression de SciELO en mettant l’accent sur la recherche sur la COVID-19, met en place le référentiel de données de SciELO, et promeut la transparence de la révision par les pairs et l’ouverture.

Ces trois dernières années, la présence de SciELO au comité de Crossref a favorisé les politiques et le fonctionnement de Crossref avec une vision plus large et plus inclusive de la communication universitaire. L’engagement en faveur d’un flux d’information scientifique global et inclusif sera la contribution de SciELO pour le futur de Crossref.

### Déclaration personnelle
Avec plus de 20 ans d’expérience nationale et internationale dans le domaine du développement des politiques de communication scientifique, des systèmes et des réseaux, j’envisage, si je suis élu membre du comité Crossref de renforcer la globalisation des politiques, des services et des priorités de Crossref et leur alignement avec les meilleures pratiques de la Science ouverte. Je souhaite, en outre, devenir le porte-parole des revendications de l'Amérique latine, des Caraïbes et d’autres régions en développement.

Luis Gustavo Gomes, responsable du programme SciELO en matière administrative, financière et de développement durable sera mon suppléant.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
Minha organização é o Programa SciELO orientado para o desenvolvimento da infraestrutura nacional de pesquisa. É implementada por meio da Rede SciELO, de 22 anos, composta por 17 coleções nacionais de periódicos revisados por pares em acesso livre de países da América Latina, Portugal, Espanha e África do Sul.

A SciELO é provavelmente o programa de cooperação internacional mais importante em comunicação científica entre os países em desenvolvimento. A SciELO é uma biblioteca baseada na Web dotada de recursos avançados de indexação, publicação e interoperabilidade visando melhorar a qualidade, a visibilidade e a credibilidade dos periódicos publicados nacionalmente de forma independente e das pesquisas que eles divulgam. O SciELO abrange todas as áreas do conhecimento e é, em essência, uma operação multilíngue.

Desde 2018, a SciELO promove o alinhamento do programa e dos periódicos ao modus operandi da ciência livre. Atualmente, a Rede SciELO indexa e publica mais de 1.200 periódicos, opera o servidor SciELO Preprints com foco na pesquisa COVID-19, está implementando o repositório SciELO Data e promovendo a transparência e a abertura da revisão por pares.

A presença da SciELO no conselho da Crossref nos últimos três anos favoreceu as políticas e o funcionamento da Crossref com uma visão mais ampla e inclusiva da comunicação acadêmica. Este compromisso com um fluxo global de informações científicas inclusivas é a contribuição que a SciELO trará para o futuro da Crossref.

### Declaração pessoal
Com mais de 20 anos de experiência nacional e internacional no desenvolvimento de políticas, sistemas e redes de comunicação científica, minha candidatura para continuar como membro do conselho da Crossref pretende fortalecer a globalização das políticas, serviços e prioridades da Crossref e seu alinhamento com as melhores práticas da ciência livre e sua posição como uma voz e demandas da América Latina, Caribe e outras regiões em desenvolvimento.   

Meu suplente será Luis Gustavo Gomes, diretor do Programa SciELO nas linhas de ação administrativa, financeira e sustentabilidade.  
{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
Mi organización es el programa SciELO, el cual está orientado al desarrollo de la infraestructura nacional de investigación. Se implementa mediante la red SciELO, la cual cuenta con 22 años de antigüedad y está compuesta por 17 colecciones nacionales de revistas arbitradas de acceso abierto de países de Latinoamérica, Portugal, España y Sudáfrica.

SciELO es probablemente el programa de cooperación internacional más importante en cuanto a comunicación científica entre países en desarrollo. SciELO es una biblioteca basada en web con capacidades de indexación, publicación e interoperabilidad avanzadas, pensadas para mejorar la calidad, visibilidad y credibilidad de las revistas publicadas de manera independiente a nivel nacional y de las investigaciones que comparten. SciELO abarca todas las áreas de conocimiento y es, en sí, una operación multilingüística.

SciELO lleva promoviendo desde 2018 el ajuste del programa y las revistas al modus operandi de ciencia abierta. Actualmente, la red SciELO indexa y publica más de 1200 revistas, opera el servidor de preimpresiones de SciELO con especial atención a la investigación sobre la COVID-19, está implementando el repositorio de datos de SciELO, y además está promoviendo la transparencia y apertura de las revisiones por homólogos.

La presencia de SciELO en la junta de Crossref durante los últimos tres años ha ayudado a que las políticas y operaciones de Crossref contaran con una visión más amplia e inclusiva de la comunicación académica. Este compromiso para lograr un flujo de información científica global e inclusivo es la contribución de SciELO al futuro de Crossref.

### Declaración personal
Con más de 20 años de experiencia nacional e internacional en el desarrollo de políticas, sistemas y redes de comunicación científica, mi postulación para continuar como miembro de la junta de Crossref pretende reforzar la globalización de las políticas, servicios y prioridades de Crossref, y su ajuste a las prácticas recomendadas de ciencia abierta. También me postulo como voz de las peticiones de Latinoamérica, el Caribe y otras regiones desarrolladoras.

Mi sustituto será Luis Gustavo Gomes, líder de administración, finanzas y líneas de acción sostenibles del programa SciELO.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
저의 단체는 국가 연구 인프라의 발전을 지향하는 SciELO 프로그램입니다. 프로그램은 남미 국가, 포르투갈, 스페인 및 남아프리카 공화국으로부터의 피어 리뷰를 거친 오픈 액세스 학술지의 17개국 컬렉션으로 구성된 22년차의 SciELO 네트워크를 통해 이행됩니다.

SciELO는 아마도 개발도상국 사이에서 과학적 소통에 대한 가장 중요한 국제 협력 프로그램일 것입니다. SciELO는 국가적이고 독립적으로 출판되는 학술지 및 이들이 소통하는 연구의 품질, 시각성 및 신뢰성 향상을 목표로 하는 진보된 색인, 출판 및 상호운용성 능력을 지닌 웹 기반 라이브러리입니다. SciELO는 모든 지식 분야를 다루며 본질적으로 다국어로 운영됩니다.

2018년 이래 SciELO는 프로그램 및 학술지와 오픈 사이언스 운영 절차의 일관성을 위해 노력해 왔습니다. 현재, SciELO 네트워크는 1,200개 이상의 학술지를 색인화 및 출판하며, COVID-19 연구 위주의 SciELO 프리프린트 서버를 운영하며, SciELO 데이터 저장소를 적용하며, 피어 리뷰 투명성 및 개방성을 촉진하고 있습니다.

지난 3년간 Crossref 위원회에서의 SciELO 참석은 학술적 소통에 대한 보다 폭넓은 포용적 비전과 함께 Crossref 정책 및 운영에 영향을 미쳤습니다. 글로벌 포용적 과학 정보 흐름을 향한 이러한 신념은 SciELO가 Crossref의 미래에 가져 올 공헌입니다.

자기소개서
20년 이상의 국가 및 국제적 과학 소통 정책, 시스템 및 네트워크 개발 경험을 갖춘 저의 Crossref 위원회 위원직을 이어가기 위한 신청은 Crossref 정책, 서비스 및 우선사항, 그리고 이것의 오픈 사이언스 모범절차 일관성에 대한 글로벌화를 강화하고 남미, 캐리비안 및 기타 개발도상 지역의 목소리와 요구로서 위치할 것으로 예상합니다.

저의 대체자는 SciELO 프로그램 행정, 재무 및 지속가능성 작용선 리더인 Luis Gustavo Gomes입니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="tf"></a>
## Liz Allen, F1000, Taylor & Francis, UK<br>
<img src="/images/board-governance/liz-allen.jpg" alt="Liz Allen, F1000, Taylor & Francis" width="250px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
These are exciting times for scholarly publishing. As technology presents new opportunities for researchers to share their research in more rapid and holistic ways, it becomes ever more important to make that research easily discoverable, robust and trustworthy. Crossref continues to play an essential role in this evolving scholarly landscape, by providing the foundations to ensure the discoverability of research for the long term and, through the services it provides, maximising the potential for access to, use and reuse of research.  

F1000 Research is considered somewhat of a trail-blazer among scholarly publishers, launching, in 2013, a publishing platform which offered a radically different way of publishing scholarly research, using a post-publication peer review model, and employing fully open and transparent peer review. While the F1000 Research model might offer a different route for authors to share and publish their work, it remains paramount that the work we publish is certified, validated and discoverable, facilitated by the common standards and consistent descriptors of content prescribed by Crossref and adopted by scholarly publishers big and small and from across the world.  

As innovation in scholarly publishing continues and new types of content providers enter the space, the stability and glue that Crossref provides in linking content and supporting use and research of research becomes ever more important. To support Crossref in its mission, it is also therefore key that its governing Board comprises a diverse mix of scholarly content publishers and providers. As it has over the last 3 years, F1000Research can bring a valuable perspective to the Board, representing publishers who offer non-traditional scholarly publishing solutions, and as a publisher who has developed innovative partnerships with funding agencies, research-performing institutions and research communities as we work to link scholarly publishing ever more closely into the research workflow.  

At the start of 2020, F1000 Research was acquired by the Taylor & Francis Group, part of Informa plc. And while F1000 Research retains its own unique identity, its links with a larger, more traditional publisher can also present some efficiencies in bringing the perspectives of a larger publisher together with those of a smaller, non-traditional publisher.  

### Personal statement <br>
I am seeking re-election for a second term as a Board Director of Crossref.
As Director of Strategic Initiatives at F1000Research I lead on shaping new initiatives and partnerships in scholarly publishing and around open research more broadly. I entered the publishing world in 2015 after over a decade working in research funding - latterly at Wellcome. I also hold a Visiting Senior Research Fellowship at the Policy Institute at King’s College London. As a result, I can bring multiple perspectives to discussions around the evolving scholarly publishing landscape, and the positioning and ongoing value of Crossref among this.  

I understand the vital importance that research infrastructure plays in ensuring the discoverability and democratisation of research and have enjoyed immensely working with Crossref, given its pivotal role in making research findable and useable. I have a track record in ‘all things research meta-data’, serving on the Board of [ORCID](https://orcid.org/) between 2010-2015, and co-founding the development of the Contributor Roles Taxonomy (CRediT). I also serve as an Advisory Board member of the Software Sustainability Institute.  

During my first term I have been an active member of the Crossref Board, being a proponent of its meta-data 2020 initiatives and update to its meta-data schema, participating in several community outreach and engagement events, and serve on the Crossref Audit Committee.  

If elected for a second term, my aim would be to consolidate what I have learned to support the staff and organisation at a time of transition in the senior leadership at Crossref. More strategically, I would help to guide Crossref as it navigates the transformations underway in scholarly communication, identifying opportunities for novel ways of working with its members, and seeking new strategic partners and potential collaborators - allowing Crossref to do more of what it does well while increasing its value and impact.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Les temps sont très intéressants pour l’édition universitaire. Étant donné que la technologie présente de nouvelles opportunités pour les chercheurs de partager leur recherche de façon plus rapide et plus holistique, il devient encore plus important de rendre cette recherche facilement accessible, robuste et fiable. Crossref continue à jouer un rôle essentiel dans ce paysage universitaire en pleine évolution, en fournissant les bases permettant d’assurer l’accessibilité de la recherche pour le long terme, et par l’intermédiaire des services offerts, en optimisant le potentiel pour l’accès, l’utilisation et la réutilisation de la recherche.

F1000 Research est un peu vu comme une sorte de pionnier parmi les éditeurs universitaires, en lançant en 2013 une plateforme de publication qui a offert une façon très différente de publier la recherche universitaire, à l’aide d’un modèle d’examen par les pairs après publication, et en utilisant un modèle d'examen par les pairs totalement ouvert et transparent. Bien que le modèle de recherche F1000 puisse offrir différentes voies aux auteurs pour qu’ils partagent et publient leur travail, il reste primordial que le travail que nous publions soit certifié, validé et accessible, facilité par les normes communes et les descripteurs cohérents de contenu prescrits par Crossref et adoptés par les éditeurs universitaires, grands et petits, et venant du monde entier.

Avec la poursuite de l’innovation dans l’édition universitaire et l’arrivée de nouveaux types de fournisseurs de contenu, la stabilité et le lien procurés par Crossref en reliant le contenu et en soutenant l'utilisation et la recherche gagne en importance. Pour soutenir Crossref dans sa mission, il est donc essentiel que son comité de direction comprenne un panel varié d'éditeurs et de fournisseurs de contenu scientifique. Comme cela a été le cas au cours des 3 années précédentes, F1000Research peut offrir un point de vue intéressant au Comité en représentant des éditeurs qui proposent des solutions d’édition universitaire non traditionnelles. Notre position d’éditeur qui a développé des partenariats innovants avec des organismes de financement, des institutions de recherche et des communautés de recherche nous permet aussi ce nouveau point de vue, car nous mettons tout en œuvre pour rapprocher encore plus l’édition de la recherche du flux du travail de recherche.

Au début de 2020, F1000Research a été acheté par le groupe Taylor & Francis, qui fait partie d’Informa plc. Et si F1000 Research préserve sa propre identité unique, son lien avec un éditeur plus important et plus traditionnel peut aussi comporter une certaine efficacité en mettant en commun les perspectives d’un plus grand éditeur et celle d’un éditeur plus petit et non traditionnel.

### Déclaration personnelle
Je suis candidat à un second mandat de Directeur du comité de Crossref.
À mon poste de directeur des initiatives stratégiques chez F1000Research, je dirige la mise en place de nouvelles initiatives et de nouveaux partenariats dans le domaine de l’édition scientifique et, plus généralement, de la recherche ouverte. J’ai débuté dans le domaine de l’édition en 2015 après plus d’une décennie consacrée au financement de la recherche, dernièrement chez Wellcome. Je suis également titulaire d'une bourse de recherche senior à l’Institut de politique au King’s Collège de Londres. De ce fait, je peux apporter de nombreuses perspectives aux discussions portant sur l’évolution du paysage de l’édition scientifique, et le positionnement et la valeur permanente de Crossref parmi ceux-ci.

Je comprends l’importance vitale que joue l’infrastructure de la recherche en assurant l’accessibilité et la démocratisation de la recherche et j’ai adoré travailler avec Crossref, étant donné son rôle essentiel pour rendre la recherche repérable et exploitable. J'ai fait mes preuves dans le domaine des « métadonnées de recherche », en tant que membre du conseil d’administration d’[ORCID](https://orcid.org/) de 2010 à 2015, et co-fondateur du développement de la taxonomie CRediT (Contributor Roles Taxonomy). Je suis également membre du conseil consultatif du Software Sustainability Institute.

Au cours de mon premier mandat, j’ai été membre actif du conseil d’administration de Crossref où j’ai fait la promotion de ses initiatives de métadonnées 2020 et de la mise à jour de son schéma de métadonnées, j’ai pris part à plusieurs événements de sensibilisation et d’engagement communautaire, et je siège au comité d’audit Crossref.

Si je suis élu pour un second mandat, mon objectif sera la consolidation de ce que j’ai appris pour soutenir le personnel et l’organisation en cette période de transition au sein de la haute direction à Crossref. De façon plus stratégique, je voudrais aider Crossref dans le cadre des transformations en cours dans la communication universitaire, en identifiant des opportunités pour de nouvelles façons de travailler avec ses membres, et en recherchant de nouveaux partenaires stratégiques et collaborateurs potentiels, ce qui permet à Crossref de faire davantage de ce qu’il fait bien tout en augmentant sa valeur et son incidence.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
Estes são momentos estimulantes para as publicações acadêmicas. À medida que a tecnologia apresenta novas oportunidades para os pesquisadores compartilharem suas pesquisas de maneiras mais rápidas e holísticas, é cada vez mais importante tornar essa pesquisa facilmente acessível, robusta e confiável. A Crossref continua a desempenhar um papel essencial neste panorama acadêmico em evolução, proporcionando as bases necessárias para garantir a acessibilidade da pesquisa em longo prazo e, graças aos serviços que ela oferece, maximizar a possibilidade de acesso, uso e reutilização da pesquisa.

A F1000Research é considerada uma pioneira entre as editoras acadêmicas, lançando, em 2013, uma plataforma de publicação que oferecia uma maneira radicalmente diferente de publicar pesquisas acadêmicas, utilizando um modelo de revisão por pares após a publicação e uma revisão por pares totalmente aberta e transparente. Embora o modelo da F1000Research possa oferecer uma via diferente para os autores compartilharem e publicarem seus trabalhos, continua sendo primordial que o trabalho que publicamos seja certificado, validado e acessível, facilitado pelos padrões comuns e descritores consistentes de conteúdo prescritos pela Crossref e adotados pelas editoras acadêmicas, grandes e pequenas, de todo o mundo.

À medida que a inovação na publicação acadêmica continuar e novos tipos de provedores de conteúdo entrarem no mercado, a estabilidade e a adesão que a Crossref proporciona ao vincular conteúdo e respaldar o uso e a pesquisa da pesquisa se tornam cada vez mais importantes. Portanto, para apoiar o Crossref em sua missão, é importante que seu Conselho de Administração contenha uma mistura diversificada de editoras e provedores de conteúdo acadêmico. Como tem feito nos últimos 3 anos, a F1000Research pode trazer uma perspectiva preciosa para o Conselho, representando editoras que propõem soluções não tradicionais de publicação acadêmica e como uma editora que desenvolveu parcerias inovadoras com agências de financiamento, instituições de execução de pesquisas e comunidades de pesquisa à medida que nos esforçamos para vincular a publicação acadêmica cada vez mais de perto com o fluxo de trabalho de pesquisa.

No início de 2020, a F1000Research foi adquirida pelo Taylor & Francis Group, parte da Informa plc. E embora a F1000Research conserve sua própria identidade, suas ligações com uma editora maior e mais tradicional também podem apresentar ganhos de eficiência ao reunir as perspectivas de uma editora maior com as de uma editora menor e não tradicional.

### Declaração pessoal
Estou buscando a reeleição para um segundo mandato como Diretora da Crossref.
Como Diretora de Iniciativas Estratégicas da F1000Research, sou responsável pela elaboração de novas iniciativas e parcerias na publicação acadêmica e em torno da pesquisa aberta de maneira mais ampla. Entrei no mundo editorial em 2015, depois de mais de uma década trabalhando com financiamento de pesquisas - mais tarde na Wellcome. Também sou bolsista convidada de pesquisa sênior no Policy Institute do King’s College London. Consequentemente, posso trazer várias perspectivas às discussões sobre a evolução do panorama das publicações acadêmicas e sobre o posicionamento e o valor contínuo da Crossref.

Entendo a importância vital que a infraestrutura de pesquisa desempenha para garantir a acessibilidade e a democratização da pesquisa e gostei muito de trabalhar com a Crossref, dado seu papel fundamental em tornar a pesquisa localizável e utilizável. Tenho um histórico em “todas as coisas relacionadas a metadados de pesquisas”, atuando no Conselho do [ORCID](https://orcid.org/) entre 2010-2015 e cofundadora do desenvolvimento da Taxonomia de Funções do Colaborador (CRediT). Também sou membro do Conselho Consultivo do Software Sustainability Institute.

Durante meu primeiro mandato, fui membro ativo do Conselho da Crossref, sendo uma proponente de suas iniciativas de metadados de 2020 e da atualização de seu esquema de metadados, participando de vários eventos de sensibilização e alcance da comunidade e trabalhei no Comitê de Auditoria da Crossref.

Se eleita para um segundo mandato, meu objetivo será consolidar o que aprendi para apoiar a equipe e a organização em um momento de transição na direção sênior da Crossref. Mais estrategicamente, ajudarei a guiar a Crossref enquanto ela navega pelas transformações em andamento na comunicação acadêmica, identificando oportunidades para novas formas de trabalhar com seus membros e buscando novos parceiros estratégicos e possíveis colaboradores - permitindo que a Crossref faça mais do que faz bem ao mesmo tempo em que aumenta seu valor e impacto.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
Son tiempos emocionantes para las publicaciones académicas. A medida que la tecnología trae nuevas oportunidades a los investigadores para compartir su trabajo de maneras más rápidas y holísticas, es cada vez más importante conseguir que ese trabajo sea fácilmente reconocible, sólido y fiable. Crossref sigue siendo esencial en este panorama académico en evolución mediante la provisión de los cimientos necesarios para asegurar que una investigación pueda ser encontrada a largo plazo y, mediante los servicios que ofrece, el máximo desarrollo del potencial de acceso, uso y reutilización de la investigación.

F1000Research está considerado en cierta manera como pionero entre las editoriales académicas, ya que lanzó en 2013 una plataforma de publicación que ofrecía una manera de publicar investigaciones académicas completamente diferente mediante un modelo de pospublicación revisado por homólogos, además de usar revisiones por homólogos completamente abiertas y transparentes. Si bien el modelo de F1000Research puede ofrecer una ruta diferente para que los autores compartan y publiquen su trabajo, sigue siendo primordial que el trabajo que publiquemos esté certificado, validado y sea reconocible según los estándares comunes y los descriptores consistentes de contenido prescritos por Crossref y adoptados por las editoriales académicas grandes y pequeñas de todo el mundo.

Mientras la innovación en publicaciones académicas sigue su curso y entran en juego nuevos tipos de proveedores de contenido, la estabilidad y seguridad que Crossref ofrece a la hora de enlazar contenidos y apoyar el uso y estudio de la investigación se hacen cada vez más importante. Para apoyar a Crossref en su misión, es también clave que su junta directiva se componga de un grupo diverso de editoriales y proveedores de contenido académico. Como lleva haciendo los últimos 3 años, F1000Research puede aportar una perspectiva valiosa a la junta al representar a editoriales que ofrecen soluciones de publicación alejadas de lo académicamente tradicional. Además, lo haremos como editorial que ha desarrollado colaboraciones innovadoras con agencias patrocinadoras, instituciones y comunidades de investigación, a la vez que sigue trabajando para vincular cada vez más las publicaciones académicas con el flujo de trabajo de investigación.

A principios de 2020, F1000Research fue adquirida por el grupo Taylor & Francis, parte de Informa plc. Aunque F1000Research sigue conservando su identidad propia, ahora está vinculada con una editorial más grande y tradicional, lo que también puede resultar eficiente a la hora de aunar las perspectivas de una editorial grande con las de una editorial pequeña y menos tradicional.

### Declaración personal
Busco mi reelección como directora de la junta de Crossref.
Como directora de iniciativas estratégicas de F1000Research, lidero la creación de nuevas iniciativas y colaboraciones de publicación académica y de investigación abierta en general. Entré en el mundo de las publicaciones en 2015, tras una década dedicada a la financiación de investigaciones, en los últimos años en Wellcome. También poseo una beca de investigación superior en calidad de visitante en el Instituto de Políticas del King's College de Londres. Debido a ello, soy capaz de aportar diferentes perspectivas a debates sobre la evolución del panorama de las publicaciones académicas, y sobre la posición y el valor continuo que tiene Crossref a este respecto.

Sé la vital importancia que tiene la infraestructura de investigación a la hora de garantizar que se recuperen y democraticen las investigaciones, y he disfrutado como nunca trabajando con Crossref debido a su papel clave para hacer que las investigaciones sean recuperables y utilizables. Cuento con experiencia en materia de «metadatos de investigación» como miembro de la junta de [ORCID](https://orcid.org/) entre 2010 y 2015, y soy cofundadora del desarrollo de la taxonomía CRediT (Contributor Roles Taxonomy). También ejerzo como miembro de la junta consultiva del Software Sustainability Institute.

Durante mi primer mandato, he sido un miembro activo de la junta de Crossref, proponiendo iniciativas de metadatos de 2020 y la actualización del esquema de metadatos, además de participar en multitud de eventos de difusión e interacción entre comunidades y servir en el comité de auditoría de Crossref.

Si se me elige para un segundo mandato, mi objetivo sería consolidar lo que he aprendido para así apoyar a la organización y a sus empleados en estos tiempos de transición de la alta dirección de Crossref. Y estratégicamente hablando, ayudaría a guiar a Crossref por las transformaciones en la comunicación académica mediante la identificación de oportunidades para trabajar de maneras novedosas con sus miembros y buscaría nuevos socios estratégicos y potenciales colaboradores, de manera que Crossref consiga hacer más de lo que actualmente hace a la vez que aumenta su valor e impacto.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
지금은 학술 출판이 기대를 모으고 있는 시기입니다. 기술이 연구원이 자신의 연구를 보다 빠르고 종합적인 방법으로 공유할 수 있는 새로운 기회를 제시함에 따라, 그러한 연구가 쉽게 발견될 수 있고, 탄탄하며, 신뢰가치를 지니도록 하는 것은 더욱 중요해지고 있습니다. Crossref는 이렇게 진화하는 학술적 지형에서 장기적으로 연구의 발견가능성을 확보할 토대를 제공하고, 그 서비스를 통해 연구의 사용 및 재사용 접근성에 대한 잠재성을 최대화함으로써 필수적인 역할을 지속해 가고 있습니다.

F1000 Research는 출판 후 피어 리뷰 모델을 사용하고 완전한 오픈형의 투명한 피어 리뷰를 채택해 급진적으로 다른 학술 연구 출판 방식을 제공했던 출판 플랫폼을 2013년에 론칭하며 학술 퍼블리셔 사이에서 다소간 선구자라 여겨집니다. F1000 Research 모델이 저자가 자신의 작업을 공유하고 출판하는 다른 경로를 제공할 수 있는 한편, 우리가 출판하는 작업이 인증을 받고, 검증을 거치며, 발견가능하도록 하는 것은 여전히 중요시하며, 이는 Crossref에 의해 기술되고 전 세계 크고 작은 학술 퍼블리셔들에 의해 채택되는 콘텐츠의 공통된 표준과 일관된 기술어를 통해 촉진됩니다.

학술 편집에서 혁신이 계속되고 새로운 유형의 콘텐츠 제공자가 진입함에 따라, 콘텐츠 연계, 연구의 사용 및 지원에서 Crossref가 제공하는 안정성 및 접착성은 점점 더 중요해지고 있습니다. Crossref의 미션을 지원하기 위해, 그러므로 그 운영위원회가 다양한 학술 콘텐츠 퍼블리셔 및 제공자로 혼합되어 구성되는 것 또한 핵심입니다. 지난 3년간 그래왔듯이, F1000 Research는 비전통적 학술 출판 솔루션을 제공하는 퍼블리셔들을 대표하고, 그 어느 때보다 밀접하게 연구 워크플로우와의 연계를 노력하면서 학술 출판 재정 지원 기관, 연구소 및 연구 커뮤니티와 혁신적인 파트너십을 구축해 온 퍼블리셔로서 위원회에 귀중한 관점을 제공할 수 있습니다.

2020년 초에 F1000 Research는 Informa PLC에 속한 Taylor & Francis Group에 합병되었습니다. F1000 Research가 자신만의 아이덴티티는 유지하는 반면, 보다 대형의 보다 전통적인 퍼블리셔는 보다 작고 비전통적 퍼블리셔의 관점과 함께 보다 대형의 퍼블리셔의 관점을 전달하는 데 얼마간 효율성을 나타낼 수 있습니다.

### 자기소개서
저는 Crossref의 위원회 이사로서 두번째 임기를 위해 재선출되고자 합니다.
F1000Research에서 전략적 이니셔티브 디렉터로서, 저는 학술 출판과 보다 넓게는 오픈 리서치의 새로운 이니셔티브 및 파트너십 형성을 이끌고 있습니다. 저는 Wellcome을 끝으로 10년 이상 리서치 펀딩 분야에서 일하다가 2015년에 출판업계에 진입했습니다. 저는 또한 킹스칼리지런던(King’s College London)의 정책연구원으로부터 Visiting Senior Research Fellowship을 보유하고 있습니다. 결과적으로 저는 진화하는 학술 출판 지형, 포지셔닝 및 그 가운데 Crossref의 지속적 가치에 대한 논의에 다중적 관점을 제공할 수 있습니다.

저는 연구의 발견가능성 및 민주화 확보에 연구 인프라가 차지하는 중요성을 잘 알고 있으며, 연구를 찾을 수 있고 사용할 수 있도록 함에 있어 그 중대한 역할을 고려할 때 Crossref와 폭넓게 협력해 온 것이 즐거웠습니다. 저는 2010~2015년 사이에 [ORCID](https://orcid.org/) 의 위원회 활동을 하며 ‘모든 문제 연구 메타데이터’의에 실적과 함께 기여자 역할 분류(Contributor Roles Taxonomy, CRediT)의 개발을 공동 설립하였습니다. 저는 또한 소프트웨어 지속가능성 연구소(Software Sustainability Institute)의 자문위원회(Advisory Board) 위원으로도 활동하고 있습니다.

저의 첫 임기 동안 저는 메타데이터 2020 이니셔티브 및 메타데이터 스키마 업데이트의 지지자로서 여러 커뮤니티 아웃리치 및 관계성 이벤트에 참여하며 Crossref 위원회의 활동적인 위원이었으며, Crossref 감사위원회(Audit Committee)에서 활동했습니다.

만약 두번째 임기에 선출된다면, 저의 목표는 직원과 조직을 지원하기 위해 Crossref의 상급 리더십 전환의 시기에 제가 배웠던 것을 적용하는 것일 것입니다. 보다 전략적 측면에서 저는 Crossref의 가치 및 영향력을 향상시키는 동시에 Crossref가 잘 하는 것을 더욱 많이 할 수 있도록, 구성원들과의 신선한 협력 방법을 찾기 위한 기회를 식별하고, 새로운 전략적 파트너 및 잠재적인 협업자를 찾으며 학술 소통에서 변화를 모색하고 있는 Crossref를 인도할 것입니다.

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="uhk"></a>
## Jesse Xiao, The University of Hong Kong Libraries, Hong Kong<br>
<img src="/images/board-governance/jesse-xiao.jpg" alt="Jesse Xiao, The University of Hong Kong" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement
The University of Hong Kong Libraries uses the Crossref DOI and Funder Registry services now. We have experience in the integration of the Crossref services into institutional repositories and current research information systems. We can share our experience and explore how the academic library can better use the Crossref services.  

### Personal statement <br>
I worked in the big data life science journal - Gigascience as the Data Architect position. In Gigascience, we used lots of CrossRef services including Metadata retrieval, Content Registration, Funder Registry etc. Last year, I started a new position in The University of Hong Kong Libraries as the Data and Scholarly Communication Librarian. I'm responsible for the scholarly communication and research data services in the HKU. I used some Crossref services including metadata, Cited-by, and Funder Registry.   

Rich metadata is the foundation for future scholarly communication. And Crossref services will play a very important role in the scholarly community to make the records more discoverable. My vision for Crossref is providing the linked data and the semantic web for the metadata to greater discoverability.  

From my background and knowledge in academic publishing, data science and scholarly communication, I think I can provide the business needs for what the academic researchers and libraries require from the Crossref and lead the Crossref to provide the better rich and linked metadata services in future.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation
Les bibliothèques de l’Université de Hong Kong utilisent désormais le DOI de Crossref et les services du Registre des financeurs. Nous avons de l’expérience dans l’intégration des services de Crossref dans les dépôts institutionnels et les systèmes d’information sur les recherches en cours. Nous pouvons partager notre expérience et découvrir comment la bibliothèque universitaire peut améliorer son utilisation des services Crossref.

### Déclaration personnelle
J’ai travaillé à la grande revue des sciences de la vie, Gigascience, en tant qu’architecte des données. Chez Gigascience, nous avons utilisé de nombreux services Crossref, notamment la récupération de métadonnées, l’enregistrement de contenu, le registre des bailleurs de fonds, etc. L’an dernier, j’ai commencé un nouvel emploi de Bibliothécaire des données et de la communication universitaire aux Bibliothèques de l’Université de Hong Kong. Je suis responsable de la communication universitaire et des services de données de recherche à la HKU. J’ai utilisé certains des services de Crossref, dont les métadonnées, Cited-by et le registre des bailleurs de fonds.

Les riches métadonnées constituent la base de la future communication universitaire. Et les services Crossref joueront un rôle très important dans la communauté universitaire pour simplifier l’accès aux dossiers. Pour Crossref, je souhaite fournir les données liées et le web sémantique des métadonnées afin de simplifier l’accessibilité.

Grâce à mon histoire et à mes antécédents dans la publication universitaire, la science des données et la communication universitaire, je pense pouvoir répondre aux besoins de l’entreprise en matière de ce que les chercheurs et les bibliothécaires attendent de Crossref et pouvoir diriger Crossref pour fournir à l’avenir des services de métadonnées plus riches et mieux connectés.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização
Agora as Bibliotecas da Universidade de Hong Kong usam os serviços de DOI e de registro de financiadores da Crossref. Temos experiência na integração dos serviços da Crossref em repositórios institucionais e sistemas de informação de pesquisa atuais. Podemos compartilhar nossa experiência e explorar como a biblioteca acadêmica pode usar melhor os serviços da Crossref.

### Declaração pessoal
Trabalhei no periódico de big data e ciências da vida, Gigascience como arquiteto de dados. Na Gigascience, usamos muitos serviços da CrossRef, incluindo recuperação de metadados, registro de conteúdo, registro de financiadores, etc. No ano passado, comecei um novo cargo nas Bibliotecas da Universidade de Hong Kong como Bibliotecário de dados e de comunicação acadêmica. Sou responsável pelos serviços de comunicação acadêmica e dados de pesquisa na HKU. Usei alguns serviços da Crossref, incluindo metadados, Cited-by e registro de financiadores.

Metadados ricos são a base da futura comunicação acadêmica. E os serviços da Crossref terão um papel muito importante na comunidade acadêmica para tornar os registros mais acessíveis. Minha visão para a Crossref é oferecer dados vinculados e web semântica para que os metadados tenham maior acessibilidade.

Com base na minha qualificação e conhecimentos em publicação acadêmica, ciência de dados e comunicação acadêmica, acredito que posso responder às necessidades comerciais que os pesquisadores acadêmicos e bibliotecas exigem da Crossref e dirigir a Crossref para fornecer os melhores serviços de metadados vinculados e ricos no futuro.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización
Las bibliotecas de la Universidad de Hong Kong usan actualmente los servicios de DOI y el Funder Registry (registro de patrocinadores) de Crossref. Tenemos experiencia en la integración de servicios de Crossref en repositorios institucionales y en sistemas actuales de información sobre investigaciones. Podemos compartir nuestra experiencia y estudiar cómo las bibliotecas académicas podrían hacer un mejor uso de los servicios de Crossref.

Declaración personal
He trabajado en la revista sobre ciencias de la vida y big data, Gigascience, en calidad de arquitecto de datos. En Gigascience usábamos muchos servicios de CrossRef, como Metadata Retrieval (recuperación de metadatos), Content Registration (registro de contenidos), Funder Registry (registro de patrocinadores), etc. El año pasado asumí un nuevo cargo como bibliotecario de datos y comunicación académica en las bibliotecas de la Universidad de Hong Kong. Soy responsable de la comunicación académica y de los servicios de datos de investigación en la HKU. He usado algunos servicios de Crossref, como Metadata (metadatos), Cited-by (citado por), and Funder Registry (registro de patrocinadores).

Los metadatos enriquecidos son la base de la comunicación académica del futuro. Y los servicios de Crossref jugarán un papel muy importante en la comunidad académica haciendo que los registros sean más reconocibles. Mi visión para Crossref consiste en facilitar los datos enlazados y la web semántica para hacer que los metadatos resulten más reconocibles.

Con mis antecedentes y mis conocimientos en publicaciones académicas, ciencia de los datos y comunicación académica, creo poder satisfacer las necesidades de los investigadores y bibliotecas académicas con respecto a Crossref, y ayudar a la organización a ofrecer los mejores servicios de metadatos enriquecidos y enlazados en el futuro.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서
The University of Hong Kong Libraries는 현재 Crossref DOI 및 Funder Registry 서비스를 사용하고 있습니다. 저희에겐 Crossref 서비스를 기관 리포지터리 및 현재의 연구 정보 시스템에 통합시킨 경험이 있습니다. 저희는 경험을 공유하고 대학 도서관이 어떻게 하면 Crossref 서비스를 보다 잘 사용할 수 있는지를 탐구할 수 있습니다.

### 자기소개서
저는 빅 데이터 생명과학 학술지 Gigascience에서 데이터 아키텍트로 일하고 있습니다. Gigascience에서 저희는 메타데이터 검색, 콘텐츠 등록, Funder Registry 등을 포함해 많은 CrossRef 서비스를 사용했습니다. 작년에 저는 홍콩대학교 도서관(The University of Hong Kong Libraries)에서 새롭게 데이터 및 학술 소통 사서직을 맡게 되었습니다. 저는 HKU에서 학술 소통 및 연구 데이터 서비스를 담당하고 있습니다. 저는 메타데이터, Cited-by 및 Funder Registry를 포함한 몇몇 Crossref 서비스를 사용했습니다.

풍부한 메타데이터는 미래의 학술 소통을 위한 토대입니다. 그리고 Crossref 서비스는 학술 커뮤니티에서 기록을 보다 쉽게 발견할 수 있도록 하는 매우 중요한 역할을 할 것입니다. Crossref에 대한 저의 비전은 메타데이터의 발견가능성 향상을 위한 연계된 데이터 및 시맨틱 웹을 제공하는 것입니다.

학술 출판, 데이터 과학 및 학술 소통에 대한 저의 배경 및 지식으로부터 저는 학술 연구원 및 도서관 이 Crossref에 요청하는 것에 대한 비즈니스 니즈를 제공하고 Crossref가 미래에 보다 풍부하고 연계된 메타데이터 서비스를 제공하도록 인도할 수 있다고 생각합니다.

{{% /accordion %}}
{{% /accordion-section %}}
