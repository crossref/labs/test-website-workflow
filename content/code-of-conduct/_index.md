+++
title = "Code of conduct"
author = "Ginny Hendricks"
draft = false
date = "2022-01-19"
[menu.main]
weight = 35
rank = 5
parent = "Get involved"
+++

At Crossref, we assume that most people are intelligent and well-intentioned, and we’re not inclined to tell people what to do. However, we want all engagement with Crossref to be safe and productive for everyone involved. To that end, this Code of Conduct frames our expectations for behavior when interacting with Crossref staff and our community—in person and online—including when enquiring about membership, at our events, during meetings and webinars, through social media, email, support tickets, and forum discussions.

Crossref is dedicated to providing a harassment-free experience for everyone. We welcome your questions, concerns, and feedback relating to any aspect of our work and offer many opportunities to do so.

## Expected behavior

While interacting within a Crossref environment in any way, we expect you to respect all staff and community members, regardless of any diversity characteristics which include but are not limited to:

<table>
  <tr>
   <td>
<ul>

<li>age

<li>citizenship status

<li>disability

<li>ethnicity

<li>family and other caring responsibilities

<li>gender
</li>
</ul>
   </td>
   <td>
<ul>

<li>geographic location

<li>military/veteran status

<li>national origin

<li>physical appearance

<li>political beliefs

<li>pregnancy/parental status
</li>
</ul>
   </td>
   <td>
<ul>

<li>professional career level

<li>race

<li>religion/value system

<li>sex

<li>sexual orientation

<li>socio-economic background/social class
</li>
</ul>
   </td>
  </tr>
</table>


Our global community consists of publishers, editors, funders, developers, librarians, researchers, and more, across a wide variety of disciplines. We value the participation of every member and want everyone to have a fulfilling and enjoyable experience in their interactions with Crossref and the wider community.

> In short: We expect all community members to abide by these guidelines in all their interactions in the Crossref community, both online and in-person. We do not accept harassment or offensive behavior anywhere. It’s counter to Crossref’s values and is counter to our values as human beings.

As such, we expect the following:

* All communication should be appropriate for a professional audience of people of many different backgrounds. Sexual language and imagery are never appropriate in such a context, nor is language referring to personal qualities or characteristics or group membership.
* Empathize and be respectful of others. Be agreeable even when you disagree. Be polite even if there is cause to complain. Do not use insulting language, harass anyone, impersonate people, or expose their private information.
* Be encouraging. Include others in the conversation where appropriate and value their contributions. Additionally, avoid jargon, slang, and cultural references that can exclude others from engaging.
* Participate constructively. Aim to improve the discussion and ensure that your contribution is on topic and helpful for others. Never post spam or attempt to mislead others.
* Pay attention to non-verbal communication. Ensure that your behavior is considerate and any physical contact is consensual.


## Reporting

If someone makes you or anyone else feel unsafe or uncomfortable or otherwise violates the Code of Conduct, please bring this to the attention of Crossref staff or email the report to [conduct@crossref.org](mailto:conduct@crossref.org). All reports will be seen by Lucy Ofiesh and Ginny Hendricks and will be treated privately.


### Steps Crossref will take

1. Participants will be asked to stop any harassing behavior and are expected to comply immediately.
2. Anyone violating this Code of Conduct will be blocked—without warning—from the space where the incident occurred, such as our social media channels, the Crossref community forum, or expulsion from the meeting/event.
3. They may also be banned from all interaction on all platforms for a period of time at our discretion.
4. Crossref may publish a statement publicly about the incident, but never without the permission of those who have been harmed by the incident, and always within the law.


### Reinstatement

Requests to be reinstated after being blocked or banned may be sent to [conduct@crossref.org](mailto:conduct@crossref.org) and will be considered.

## Reviewing this policy

The Crossref Code of Conduct is adapted from several others, including [O’Reilly Media Conferences](https://web.archive.org/web/20190124173334/https://www.oreilly.com/conferences/code-of-conduct.html) and [FORCE11](https://force11.org/info/code-of-conduct/). We review this Code of Conduct regularly and learn from other organizations. Contributions have been made by: Ginny Hendricks, Geoffrey Bilder, Shayn Smulyan, Laura J. Wilkinson, Vanessa Fairhurst, Rosa Morais Clark, Isaac Farley, Amanda Bartell, Rachael Lammey, Lucy Ofiesh.

Please contact [conduct@crossref.org](mailto:conduct@crossref.org) with any suggestions.

We thank our community for your help in keeping Crossref a welcoming, respectful, and friendly community for all participants. Testing other files
