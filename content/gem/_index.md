+++
title = "Global Equitable Membership (GEM) program"
date = "2022-12-08"
draft = false
image = "/images/banner-images/gem-header.png"
author = "Susan Collins"
rank = 3
aliases = [
"/fee-assistance",
"/fee-assistance/"
]
[menu.main]
parent = "Become a member"
weight = 6
+++

In order to meet [our mission](/community/about) of a truly global and connected research ecosystem, it is important to ensure that participation in Crossref and all our services and metadata is accessible to everyone involved in documenting scholarly progress.  

Crossref [membership](/membership) is open to all organizations that produce scholarly and professional materials and content. But cost and technical capabilities can be barriers to joining, and where that's the case, we aim to reduce these in a number of ways. These include: partnering with organizations such as the Public Knowledge Project to support [plugins for OJS users](https://docs.pkp.sfu.ca/crossref-ojs-manual/); by developing our [Sponsor](/membership/about-sponsors/) program where members are supported by an organization that aggregates our fees and provides local language technical support.

## Membership equitability and accessibility: introducing the GEM program

For many years we have also waived content registration fees via specific Sponsor agreements for members in some countries and accounted for that as "donated deposits" under a "fee assistance" program. Starting in January 2023, this was expanded to be consistent across the world and to encompass the annual membership fee.

The Global Equitable Membership (GEM) program offers relief from membership and content registration fees for members in the least economically-advantaged countries in the world. Members in GEM-eligible countries do not pay Crossref membership or content registration fees. As we move toward realizing the vision of a connected [Research Nexus](/documentation/research-nexus), building a network for the global community must include input from the global community.

### List of eligible countries (as of 2023)

The countries currently eligible under the Global Equitable Membership (GEM) program are:

<table>
    <tr>
        <td>Afghanistan</td>
        <td>Gambia</td>
        <td>Maldives</td>
        <td>Solomon Islands</td>
    </tr>
    <tr>
        <td>Bangladesh</td>
        <td>Ghana</td>
        <td>Mali</td>
        <td>Somalia</td>
    </tr>
    <tr>
        <td>Benin</td>
        <td>Guinea</td>
        <td>Marshall Islands</td>
        <td>South Sudan</td>
    </tr>
    <tr>
        <td>Bhutan</td>
        <td>Guinea-Bissau</td>
        <td>Mauritania</td>
        <td>Sri Lanka</td>
    </tr>
    <tr>
        <td>Burkina Faso</td>
        <td>Guyana</td>
        <td>Micronesia (Federated States of)</td>
        <td>Sudan</td>
    </tr>
    <tr>
        <td>Burundi</td>
        <td>Haiti</td>
        <td>Mozambique</td>
        <td>Tajikistan</td>
    </tr>
    <tr>
        <td>Cambodia</td>
        <td>Honduras</td>
        <td>Myanmar</td>
        <td>Tanzania</td>
    </tr>
    <tr>
        <td>Central African Republic</td>
        <td>Kiribati</td>
        <td>Nepal</td>
        <td>Togo</td>
    </tr>
    <tr>
        <td>Chad</td>
        <td>Kosovo</td>
        <td>Nicaragua</td>
        <td>Tonga</td>
    </tr>
    <tr>
        <td>Comoros</td>
        <td>Kyrgyz Republic</td>
        <td>Niger</td>
        <td>Tuvalu</td>
    </tr>
    <tr>
        <td>Democratic Republic of the Congo</td>
        <td>Lao PDR</td>
        <td>Rwanda</td>
        <td>Uganda</td>
    </tr>
    <tr>
        <td>Côte d&#39;Ivoire</td>
        <td>Lesotho</td>
        <td>Samoa</td>
        <td>Vanuatu</td>
    </tr>
    <tr>
        <td>Djibouti</td>
        <td>Liberia</td>
        <td>São Tomé and Principe</td>
        <td>Yemen</td>
    </tr>
    <tr>
        <td>Eritrea</td>
        <td>Madagascar</td>
        <td>Senegal</td>
        <td>Zambia</td>
    </tr>
    <tr>
        <td>Ethiopia</td>
        <td>Malawi</td>
        <td>Sierra Leone</td>
    </tr>
</table>

### Eligibility

Eligibility for the program is based on a member’s country. We have curated the list of eligible countries based on the [International Development Association](https://datahelpdesk.worldbank.org/knowledgebase/articles/906519-world-bank-country-and-lending-groups) list and excluded anywhere we are bound by international sanctions.

The IDA is part of the World Bank. Its criteria is more nuanced than ‘low income’ or ‘lower-middle income’ as it takes into account GNI per capita as well as creditworthiness, which is especially important in countries where the gap between rich and poor is very large. We are not including the ‘blended’ countries (that is, countries that are on their way economically and therefore need less financial support).  

#### Reviewing the eligibility criteria

We will review the eligibility criteria annually and note any changes here. The IDA reviews the list annually, and while it is not common to have lots of movement, we will notify members whose country moves on or off the IDA list of any upcoming fees (or the removal of them) so they can plan and budget.  

We ask for both mailing and billing addresses on our membership application form and both of these need to be in an eligible country (not necessarily the same one) in order to qualify.

### GEM program specifics

Here are the details of what is waived and what is not, along with some answers to some frequently answered questions.

#### Q: Which fees are waived and which are not?

The annual membership fee is waived (irrespective of the member’s organizational size or revenue; it’s the country that determines the eligibility). All content registration fees are also waived for all content types. Participation in other paid services, such as Similarity Check and Metadata Plus, will be charged at the usual fees. Check out our standard [fees](/fees) to learn how much eligible members are saving.

#### Q: Can GEM program members also work with a Sponsor?

The GEM program is open to independent members as well as members joining via a Sponsor. However, note that sponsors may only work with members who would normally be categorized in the lowest membership fee tier. Please also note that Sponsors may charge for their services (which include local language, technical, and other support), so it is important to discuss the terms with the Sponsor. Crossref will be actively seeking organizations to become Sponsors in GEM countries to build out support in these countries.

### Join as a member

<img src="/images/community-images/crossref-gem-icon.png" alt="GEM logo" width="10%" class="img-responsive" style="padding: 10px; float: left;" />   Find out more about your benefits and obligations as a member and apply today. If your mailing and billing adresses are in a GEM country you will automatically be exempt from membership and content registration fees.

## [Join today](/membership)

---
Contact our [member experience team](mailto:feedback@crossref.org) with any questions.
