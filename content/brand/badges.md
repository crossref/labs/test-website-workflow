+++
title = "Account badges"
date = "2018-11-23"
draft = false
author = "Rosa Clark"
parent = "About us"
rank = 5
weight = 10
aliases = [
    "/brand/member-badges"
]
+++


Do you know [which type of account you have with us?](/blog/are-you-having-an-identity-crisis/). If you'd like to share that on your website or in your communications, we've made some standard badges to do so. Note that we recommend using `.svg` versions online at 200px wide for the sharpest display.

When placing your badge on your website we ask that you **reference** (not download) them, using the snippets given below. Please copy the code exactly so that if we update our badges, you’ll automatically get the correct file.

## Referencing your badge

| Account badge | Use this code |
|---|---|
|<img class="center-block" src="https://assets.crossref.org/logo/member-badges/member-badge-member.svg" width="200" height="200" alt="Crossref Member Badge">|`<img src="https://assets.crossref.org/logo/member-badges/member-badge-member.svg" width="200" height="200" alt="Crossref Member Badge">`|
|<img class="center-block" src="https://assets.crossref.org/logo/member-badges/member-badge-metadata-user.svg" width="200" height="200" alt="Crossref Metadata User Badge">|`<img src="https://assets.crossref.org/logo/member-badges/member-badge-metadata-user.svg" width="200" height="200" alt="Crossref Metadata User Badge">`|
|<img class="center-block" src="https://assets.crossref.org/logo/member-badges/member-badge-service-provider.svg" width="200" height="200" alt="Crossref Service Provider Badge">|`<img src="https://assets.crossref.org/logo/member-badges/member-badge-service-provider.svg" width="200" height="200" alt="Crossref Service Provider Badge">`|
|<img class="center-block" src="https://assets.crossref.org/logo/member-badges/member-badge-sponsored-member.svg" width="200" height="200" alt="Crossref Sponsored Member Badge">|`<img src="https://assets.crossref.org/logo/member-badges/member-badge-sponsored-member.svg" width="200" height="200" alt="Crossref Sponsored Member Badge">`|
|<img class="center-block" src="https://assets.crossref.org/logo/member-badges/member-badge-sponsoring-organization.svg" width="200" height="200" alt="Crossref Sponsor Badge">|`<img src="https://assets.crossref.org/logo/member-badges/member-badge-sponsoring-organization.svg" width="200" height="200" alt="Crossref Sponsor Badge">`|
