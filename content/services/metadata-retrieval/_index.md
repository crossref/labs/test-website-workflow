+++
title = "Metadata Retrieval"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
rank = 5
aliases = [
    "/cms/cms_policies.html",
    "/cms/index.html",
    "/metadata_services.html",
    "/services/metadata-delivery",
    "/affiliates",
    "/affiliates/",
    "/services/metadata-delivery/"
]
[menu.main]
parent = "Find a service"
weight = 7
+++

{{< snippet "/_snippet-sources/metadata-retrieval-intro.md" >}}
{{< snippet "/_snippet-sources/metadata-retrieval-detail.md" >}}

## Getting started with metadata retrieval<a id='00673' href='#00673'><i class='fas fa-link'></i></a>

Learn more about [metadata retrieval in our documentation](/documentation/retrieve-metadata/rest-api/).

---
