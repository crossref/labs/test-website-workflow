+++
title = "Event Data"
date = "2020-09-08"
draft = false
author = "Martyn Rittman"
rank = 5
[menu.main]
parent = "Find a service"
weight = 9
+++

{{< snippet "/_snippet-sources/event-data.md" >}}

## Getting started with Event Data<a id='00672' href='#00672'><i class='fas fa-link'></i></a>

Learn more about Event Data in our [comprehensive documentation](https://www.eventdata.crossref.org/guide/).

---
