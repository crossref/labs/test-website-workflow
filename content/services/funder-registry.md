+++
title = "Funder Registry"
date = "2020-04-08"
draft = false
author = "Rachael Lammey"
rank = 5
aliases = [
    "/fundingdata/faqs.html",
    "/fundingdata/index.html",
    "/fundref/",
    "/fundref/fundref_registry.html",
    "/fundref/index.html",
    "/fundingdata/"
]
[menu.main]
parent = "Find a service"
weight = 5
+++

{{< snippet "/_snippet-sources/funder-registry.md" >}}

## Getting started with Funder Registry<a id='00668' href='#00668'><i class='fas fa-link'></i></a>

Learn more about the [Funder Registry in our documentation](/education/funder-registry/accessing-the-funder-registry/).

---
