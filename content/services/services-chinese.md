+++
title = "Crossref Services - Simplified Chinese"
date = "2021-02-26"
draft = false
parent = "Find a service"
rank = 3
weight = 5
+++


## Find a service


{{% divwrap blue-highlight %}} Content Registration {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/d8jc2239ut.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_d8jc2239ut popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

---

{{% divwrap blue-highlight %}} Crossref 参考文献链接 (Reference Linking) {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/7hw2lvn4oa.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_7hw2lvn4oa popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

---

{{% divwrap blue-highlight %}} Crossref 引用 (Cited-by) {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/slzod0pvje.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_slzod0pvje popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

---

{{% divwrap blue-highlight %}} Crossmark {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/82ugp0bnq8.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_82ugp0bnq8 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

---

{{% divwrap blue-highlight %}} Crossref 元数据搜索 (Metadata Search) {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/etdbn3at3j.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_etdbn3at3j popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>
---

{{% divwrap blue-highlight %}} Crossref 元数据 API (Metadata retrieval) {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/s6sjaqeswu.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_s6sjaqeswu popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

---

{{% divwrap blue-highlight %}} Crossref 相似性检查 (Similarity Check) {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/rm5o8f3fvd.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_rm5o8f3fvd popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

---
{{% divwrap blue-highlight %}} Funder Registry {{% /divwrap %}}

<script src="https://fast.wistia.com/embed/medias/bdf5a63fro.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_bdf5a63fro popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>
