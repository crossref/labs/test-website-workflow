+++
title = "Crossmark statistics is no longer available"
date = "2021-09-20"
draft = false
author = "Martyn Rittman"
Weight = 50
rank = 5
parent = "Find a service"
+++

## Crossmark statistics is changing...

We are re-evaluating Crossmark statistics and looking for your input.

We didn't include Crosmark statistics in our recent authentication rollout, and the process of adding it has led us to look again at the service. It has low usage, suggesting that it either isn't sufficiently visible or has low utility. However, we know that there may be a small number of members who find it very valuable and we would like to know more about how it is being used.

We are therefore seeking your feedback. To contribute, please complete [our user survey](https://forms.gle/Lvo36fak5EF34qZF6).

For further details about how we sunset services, see our [deprecation](/deprecated) page.
