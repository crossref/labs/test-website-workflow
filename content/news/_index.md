+++
title = "News"
date = "2018-10-06"
draft = false
author = "Rosa Clark"
parent = "About us"
weight = 12
Rank = 2
aliases = [
    "/01company/09news_releases.html",
    "/01company/09press_releases.html"
]
+++

A comprehensive archive of all our official news announcements since Crossref's formation in 1999. Please also check out our [blog](/blog) as that's where the news tends to be written about nowadays. Contact [Rosa Clark](mailto:feedback@crossref.org) with any questions.

### 2015-2018 news

* 05 June 2018, [Crossref facilitates the use of essential peer review information for scholarly communications](/news/2018-06-05-introducing-metadata-for-peer-review)
* 15 November 2017, [Crossref introduces new Metadata APIs ‘Plus’ service offering new features and richer metadata](/news/2017-11-15-new-metadata-plus-service-launching)
* 02 November 2016, [Crossref now accepts preprints](/news/2016-11-02-crossref-now-accepts-preprints)
* 17 May 2016, [Crossref Publishers deliver win for clinical trial openness](/news/2016-05-17-crossref-publishers-deliver-win-for-clinical-trial-openness)
* 05 May 2016, [Crossref to accept preprints in change to long-standing policy](/news/2016-05-05-crossref-to-accept-preprints-in-change-to-long-standing-policy)
* 29 October 2015, [ORCID launches Crossref and DataCite Auto-Update](/news/2015-10-26-orcid-launches-crossref-and-datacite-auto-update)
* 20 August 2015, [Crossref Strengthens Product Focus with Appointment of Jennifer Lin to its Management Team](http://www.nfais.org/crossref-strengthens-product-focus-with-appointment-of-jennifer-lin-to-its-management-team)
* 26 March 2015, [Crossref Extends Management Team, Appoints Ginny Hendricks To Focus on Member and Community Outreach](/news/2015-03-26-crossref-extends-management-team-appoints-ginny-hendricks-to-focus-on-member-and-community-outreach)


## News archive

{{% accordion %}}


{{% accordion-section "News from 2013 to 2014" %}}

### 2014

* 12 November 2014, [Inera and Crossref Win Top Prize at the NEPCo Awards](/news/2014-11-12-inera-and-crossref-win-top-prize-at-the-nepco-awards)
* 10 November 2014, [Crossref and DataCite announce new initiative to accelerate the adoption of DOIs for data publication and citation](/news/2014-11-10-crossref-and-datacite-announce-new-initiative-to-accelerate-the-adoption-of-dois-for-data-publication-and-citation)
* 2 October 2014, [The Public Knowledge Project and Crossref Collaborate to Improve Services for Publishers using Open Journal Systems](/news/2014-10-02-the-public-knowledge-project-and-crossref-collaborate-to-improve-services-for-publishers-using-open-journal-systems)
* 5 August 2014, [Crossref and California Digital Library Agreement Extends Discoverability of Scholarly Publications](/news/2014-08-05-crossref-and-california-digital-library-agreement-extends-discoverability-of-scholarly-publications)
* 29 May 2014, [Crossref Text and Data Mining Services Simplify Researcher Access](/news/2014-05-29-crossref-text-and-data-mining-services-simplify-researcher-access)
* 6 May 2014, [Atypon Integrates Crossref's Crossmark Service in Literatum 14.1](https://web.archive.org/web/20171213213422/https://www.atypon.com/wp-content/uploads/2014/05/06-Press-Release-Atypon-CrossMark-v3.pdf)
* 28 March 2014, [Document Delivery Performance Bolstered with Reprints Desk’s Completed Crossref Digital Object Identifier Project](http://info.reprintsdesk.com/about/newsroom/2014/document-delivery-performance-bolstered-with-reprints-desks-completed-crossref-d)
* 27 March 2014, [Research Solutions Licenses Data from Crossref.org, Bolsters Efficiency in Systems and Business Processes](http://researchsolutions.investorroom.com/2014-03-27-Research-Solutions-Licenses-Data-from-Crossref-org-Bolsters-Efficiency-in-Systems-and-Business-Processes)

### 2013

* 13 November 2013, [Crossref Members add over a quarter million Crossmark records; Researchers click on Crossmark buttons 50K times per month](/news/crossref-members-add-over-a-quarter-million-crossmark-records)
* 8 October 2013, [Crossref discloses FundRef participation statistics; announces eJournalPress integration](/news/2013-10-08-crossref-discloses-fundref-participation-statistics-announces-ejournalpress-integration)
* 18 July 2013, [FundRef funder identification service from Crossref to help connect funding and publications to improve public access](/news/2013-07-18-fundref-funder-identification-service-from-crossref-to-help-connect-funding-and-publications-to-improve-public-access)
* 28 May 2013, [Crossref’s FundRef launches, Publishers and funders track scholarly output](/news/2013-05-28-crossrefs-fundref-launches-publishers-and-funders-track-scholarly-output)
* 10 February 2013, [NISO Launches New Initiative to Develop Standard for Open Access Metadata and Indicators](http://www.niso.org/news/pr/view?item_key=d2e5f409bc6af6b7f504a10edf0329203ffec6f9)

{{% /accordion-section %}}


{{% accordion-section "News from 2010 to 2012" %}}

### 2012

* 14 November 2012, [ORCID and Crossref Collaborate to Accurately Attribute Authorship of Scholarly Content](/news/2012-11-14-orcid-and-crossref-collaborate-to-accurately-attribute-authorship-of-scholarly-content)
* 14 September 2012, [ALPSP announces Winners of the 2012 Awards](/pdfs/ALPSP_Awards_2012_Winners.pdf)
* 10 August 2012, [Crossref Joins STM-DataCite Statement](/news/2012-08-10-crossref-joins-stm-datacite-statement)
* 2 May 2012, [Crossref Announces FundRef Pilot to Standardize Funding Source Information for Scholarly Publications](/news/2012-05-02-crossref-announces-fundref-pilot-to-standardize-funding-source-information-for-scholarly-publications)
* 27 April 2012, [Crossmark Update Identification Service Launches to Alert Readers to Changes in Scholarly Content](/news/2012-04-27-crossmark-update-identification-service-launches-to-alert-readers-to-changes-in-scholarly-content)
* 23 February 2012, [Ammons Scientific LTD Uses iThenticate to Check all Articles for Originality Prior to Peer Review](/news/2012-02-23-ammons-scientific-ltd-uses-ithenticate-to-check-all-articles-for-originality-prior-to-peer-review)

### 2011

* 15 November 2011, [Crossref Assigns 50 Millionth DOI](/news/2011-11-15-crossref-assigns-50-millionth-doi)
* 2 August 2011, [Crossref Revises DOI Display Guidelines](/news/2011-08-02-crossref-revises-doi-display-guidelines)
* 20 April 2011, [Crossref and International DOI Foundation Collaborate on Linked-Data-Friendly DOIs](/news/2011-04-20-crossref-and-international-doi-foundation-collaborate-on-linked-data-friendly-dois)

### 2010

* 15 December 2010, [Crossref Reaches 1000 Member Milestone](/news/2010-12-15-crossref-reaches-1000-member-milestone)
* 6 December 2010, [KISTI Registers 44000 Crossref DOIs for Korean Scholarly Papers](/news/2010-12-06-kisti-registers-44000-crossref-dois-for-korean-scholarly-papers)
* 16 November 2010, [Elsevier Joins Crossref Cited-by Linking](/news/2010-11-16-elsevier-joins-crossref-cited-by-linking)
* 16 November 2010, [Portico To Preserve Crossref Metadata](/news/2010-11-16-portico-to-preserve-crossref-metadata)
* 5 February 2010, [40 Million Crossref DOIs Preserve the Record of Scholarship](/news/2010-02-05-40-million-crossref-dois-preserve-the-record-of-scholarship)


{{% /accordion-section %}}

{{% accordion-section "News from 2008 to 2009" %}}

### 2009

* 9 November 2009, [Crossref Renews Relationship with Atypon for the Development of a New Query System](/news/2009-11-09-crossref-renews-relationship-with-atypon-for-the-development-a-new-query-system)
* 11 September 2009, [Crossref Collaborates with SAGE, OUP, CLOCKSS and Portico to Light Up Archive for Discontinued Journal Articles](/news/2009-09-11-crossref-collaborates-with-sage-oup-clockss-and-portico-to-light-up-archive-for-discontinued-journal-articles)
* 29 July 2009, [Crossref Hits the Books—Deposits Grow, Guidelines Released](/news/2009-07-29-crossref-hits-the-books-deposits-grow-guidelines-released)
* 13 May 2009, [Council of Science Editors Honors Crossref](/news/2009-05-13-council-of-science-editors-honors-crossref)
* 28 April 2009, [CrossCheck Plagiarism Screening Service Adds 50th Publisher](/news/2009-04-28-crosscheck-plagiarism-screening-service-adds-50th-publisher)

### 2008

* 15 September 2008, [ALPSP Announces winners of 2008 awards](/news/2008-09-15-alpsp-announces-winners-of-2008-awards)
* 4 August 2008, [Crossref Announces Improved Multiple Resolution Service](/news/2008-08-04-crossref-announces-improved-multiple-resolution-service)
* 19 June 2008, [CrossCheck Plagiarism Screening Service Launches Today](/news/2008-06-19-crosscheck-plagiarism-screening-service-launches-today)
* 15 April 2008, [Crossref Announces CrossCheck Plagiarism Detection Service](/news/2008-04-15-crossref-announces-crosscheck-plagiarism-detection-service)
* 12 March 2008, [Crossref Integrates with Papers to Help Scientists Manage Personal Libraries](/news/2008-03-12-crossref-integrates-with-papers-to-help-scientists-manage-personal-libraries)
* 7 March 2008, [Sermo Partners with Crossref to use DOIs as Linking Standard for Physician Discussion of Medical Research](/news/2008-03-07-sermo-partners-with-crossref-to-use-dois-as-linking-standard-for-physician-discussion-of-medical-research)
* 12 February 2008, [Crossref Launches Free Citation Look-up Tool For Bloggers](/news/2008-02-12-crossref-launches-free-citation-look-up-tool-for-bloggers)
* 2 January 2008, [Crossref Surpasses 30 Million DOI Mark](/news/2008-01-02-crossref-surpasses-30-million-doi-mark)

{{% /accordion-section %}}

{{% accordion-section "News from 2005 to 2007" %}}

### 2007

* 9 October 2007, [BSI British Standards Joins Crossref](/news/2007-10-09-bsi-british-standards-joins-crossref)
* 25 September 2007, [Crossref Grants The Smithsonian/NASA Astrophysics Data System Free Access To Crossref Metadata](/news/2007-09-25-crossref-grants-the-smithsonian-nasa-astrophysics-data-system-free-access-to-crossref-metadata)
* 9 August 2007, [Crossref Lowers Back-File Deposit Fee to 12 Cents, Creating Incentive for Registration of Older Publications](/news/2007-08-09-crossref-lowers-back-file-deposit-fee-to-12-cents-creating-incentive-for-registration-of-older-publications)
* 1 August 2007, [Crossref Pilots Cross-Publisher Plagiarism Detection Service](/news/2007-08-01-crossref-pilots-cross-publisher-plagiarism-detection-service)
* 4 June 2007, [Department of Energy grey Literature Now Registered with Crossref](/news/2007-06-04-department-of-energy-gray-literature-now-registered-with-crossref/)
* 13 February 2007, [Crossref Surpasses 25 Million DOI Mark](/news/2007-02-13-crossref-surpasses-25-million-doi-mark)
* 22 January 2007, [The Royal Society Digital Archive Now Registered in Crossref](/news/2007-01-22-the-royal-society-digital-archive-now-registered-in-crossref)

### 2006

* 5 December 2006, [Geoffrey Bilder To Join Crossref](/news/2006-12-05-geoffrey-bilder-to-join-crossref)
* 5 December 2006, [Crossref To Add Hundreds Of Journals From Africa and Asia](/news/2006-12-05-crossref-to-add-hundreds-of-journals-from-africa-and-asia)
* 8 November 2006, [Crossref Extends Web Services - New OAI-PMH Interface, and Partnerships with Scirus and EMBL-EBI](/news/2006-11-08-crossref-extends-web-services-new-oai-pmh-interface-and-partnerships-with-scirus-and-embl-ebi)
* 30 October 2006, [Crossref Simple-Text Query Service Officially Launches](/news/2006-10-30-crossref-simple-text-query-service-officially-launches)
* 12 July 2006, [Crossref Announces Web Services Agreement With Microsoft](/news/2006-07-12-crossref-announces-web-services-agreement-with-microsoft)
* 1 June 2006, [Over Half a Million Book DOIs Now Registered In Crossref](/news/2006-06-01-over-half-a-million-book-dois-now-registered-in-crossref)
* 26 April 2006, [Crossref Surpasses 20 Million DOI Mark](/news/2006-04-26-crossref-surpasses-20-million-doi-mark)
* 14 February 2006, [Crossref Launches Free-Text Query Trial Service](/news/2006-02-14-crossref-launches-free-text-query-trial-service)
* 14 February 2006, [Crossref Web Services and Search Partner Program Enhance Web Searching for Scholarly Content](/news/2006-02-14-crossref-web-services-and-search-partner-program-enhance-web-searching-for-scholarly-content)

### 2005

* 3 February 2005, [DOE/OSTI Joins Crossref to Assign DOIs to Technical Reports](/news/2005-02-03-doe-osti-joins-crossref-to-assign-dois-to-technical-reports)

{{% /accordion-section %}}

{{% accordion-section "News from 2002 to 2004" %}}

### 2004

* 21 September 2004, [Crossref Search Pilot Now Includes 29 Publishers, 3.4 Million Research Articles](/news/2004-09-21-crossref-search-pilot-now-includes-29-publishers-3/)
* 8 July 2004, [Crossref Search Pilot Adds 16 New Publishers](/news/2004-07-08-crossref-search-pilot-adds-16-new-publishers)
* 8 June 2004, [Crossref and Atypon Announce Forward Linking Service](/news/2004-06-08-crossref-and-atypon-announce-forward-linking-service)
* 28 April 2004, [Crossref Launches Pilot Program of Crossref Search, Powered By Google](/news/2004-04-28-crossref-launches-pilot-program-of-crossref-search-powered-by-google)
* 8 April 2004, [Crossref Reaches 300-Publisher Mark](/news/2004-04-08-crossref-reaches-300-publisher-mark)
* 19 January 2004, [Crossref Surpasses 10 Million DOI Mark](/news/2004-01-19-crossref-surpasses-10-million-doi-mark)

### 2003

* 15 November 2003, [Crossref Drops DOI Retrieval Fees As Steep Growth In Adoption Continues](/news/2003-11-13-crossref-drops-doi-retrieval-fees-as-steep-growth-in-adoption-continues)
* 7 November 2003, [Serials Solutions' Article Linker Expands Linking Options With Crossref DOIs](2003-11-07-serials-solutions-article-linker-expands-linking-options-with-crossref-dois)
* 25 September 2003, [Crossref Holds Annual Meeting - New Chairman of the Board and Treasurer Elected](/news/2003-09-25-crossref-holds-annual-meeting-new-chairman-of-the-board-and-treasurer-elected)
* 5 August 2003, [Innovative Interfaces Joins Crossref as Affiliate Member](/news/2003-08-05-innovative-interfaces-joins-crossref-as-affiliate-member)
* 21 May 2003, [No Charge to Libraries for DOI and Metadata Lookup](/news/2003-05-21-no-charge-to-libraries-for-doi-and-metadata-lookup)
* 8 April 2003, [Crossref Growing Fast as it Signs More Than 200 Members](/news/2003-04-08-crossref-growing-fast-as-it-signs-more-200-members)
* 30 January 2003, [Crossref Now Registering Books and Conference Proceedings](/news/2003-01-30-crossref-now-registering-books-and-conference-proceedings)

### 2002

* 23 September 2002, [Crossref Launches Version 2.0 of its Reference Linking System and Expands Membership Base](/news/2002-09-23-crossref-launches-version-2/)
* 8 February 2002, [Crossref Surpasses 100 members & 4 Million Journal Articles, And Expands Strategic Affiliate Program](/news/2002-02-08-crossref-surpasses-100-publisher-members-and-4-million-journal-articles-and-expands-strategic-affiliate-program)

{{% /accordion-section %}}

{{% accordion-section "News from 1999 to 2001" %}}

### 2001

* 10 October 2001, [Crossref Steps Up Pace of Linking Developments](/news/2001-10-10-crossref-steps-pace-of-linking-developments)
* 7 September 2001, [Crossref Selects Atypon As Technology Partner To Build The Next Release Of Its System](/news/2001-09-07-crossref-selects-atypon-as-technology-partner-to-build-the-next-release-of-its-system)
* 12 June 2000, [Revised Fees Released](/news/2000-06-12-revised-fees-released)
* 5 June 2000, [Crossref Goes Live, Journal Reference Linking Service Membership Roster Grows to 33 Publishers](/news/2000-06-05-crossref-goes-live-journal-reference-linking-service)

### 2000

*  2 June 2000, [Gallery of Reference Pages](/news/2000-06-02-gallery-of-reference-pages)
* 3 May 2000, [Crossref Fee Schedule for 2000](/news/2000-05-03-crossref-fee-schedule-for-2000)
* 2 February 2000, [Journal Reference Linking Service Names Executive Director, Board of Directors, New Members, and a "Go Live" Timetable](/news/2000-02-02-journal-reference-linking-service-names-executive-director-board-of-directors-new-members-and-a-go-live-timetable)

### 1999

* 9 December 1999, [Reference Linking Service Announces Name - More Scientific and Scholarly Publishers Join Crossref](/news/1999-12-09-reference-linking-service-announces-name-more-scientific-and-scholarly-publishers-join-crossref)
* 16 November 1999, [Reference Linking Service to Aid Scientists Conducting Online Research, Scientific and Scholarly Publishers Collaborate to Offer Ground-Breaking Initiative Publishers Join Crossref](/news/1999-11-16-reference-linking-service-to-aid-scientists-conducting-online-research)

{{% /accordion-section %}}

{{% /accordion %}}
