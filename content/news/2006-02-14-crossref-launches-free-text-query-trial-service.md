+++
title = "Crossref Launches Free-Text Query Trial Service"
date = "2006-02-14"
parent = "News"
weight = 1
+++

*14 February 2006*

**LYNNFIELD, MA – February 14, 2006** -  Crossref is very pleased to announce a new trial service for its member publishers. In partnership with Inera, Crossref has deployed a custom version of Inera’s eXtyles® refXpress that parses unstructured, free-text references into granular and valid XML and returns any matching DOIs for those references.

The Free-Text Query form is [now available to members on a trial basis](https://apps.crossref.org/simpleTextQuery)
cut-and-paste form accepts references formatted in common bibliographic styles and returns the DOI for the article if one is found in Crossref.  One or more references may be pasted into the form on this page; each reference must begin on a new line.

Crossref invites member input on their experiences using this pilot service (a link to a feedback form is provided from the FTQ site). Our hope is that some smaller publishers will experience this interface as a viable alternative to Crossref's batch query interface and that, as a result, more publishers will be able to implement reference linking using Crossref. According to Crossref member Ian Michael of IM Publications, "This is absolutely fantastic. We have been looking for a way to incorporate DOIs cost-effectively into our references and have failed, until now."

Crossref serves a very broad range of publishers, including many members with limited resources and technical expertise. Some Crossref members have been slow to begin reference linking due to technical limitations, underscoring the need for a free-text query method of retrieving DOIs in order to facilitate compliance with Crossref reference-linking policies.

**About Inera Inc.** - Since 1992, Inera (http://www.inera.com) has focused on supplying sophisticated editorial and production solutions to the publishing industry. Inera's eXtylesR product family provides integrated tools for Microsoft Word and content management systems that automate editorial, reference linking, and XML production processes. eXtyles products are used in the production of over five hundred prestigious journals worldwide.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
