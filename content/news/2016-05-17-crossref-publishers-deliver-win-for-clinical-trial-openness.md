+++
title = "Crossref Publishers deliver win for clinical trial openness"
date = "2015-05-17"
parent = "News"
weight = 5
+++

*17 May 2015*
_Clinical trials data now linkable with publications_

A group of publishers today announced a development to uniquely identify each clinical trial and easily link to related publications for the first time. This will make it easier for health professionals to obtain all the necessary information relating to a clinical trial for their patients, and to ensure transparency for researchers.

The resulting initiative is the culmination of three years' collaboration between publishers, led by the open access publisher BioMed Central and Crossref, the not-for-profit membership organization who runs a shared scholarly infrastructure.

Daniel Shanahan, Associate Publisher at BioMed Central and Chair of the Working Group, said: "Clinical trials can result in a large number of separate publications, from study protocols through to the results articles and secondary analyses, published in many different journals, sometimes years apart.

"Researchers need access to all of these articles if they are to reliably evaluate bias or selective reporting in the study. Identifying and centrally linking all articles related to each individual clinical trial is imperative, and publishers have an important part to play in ensuring researchers can find the information they need easily."

This development means that clinical trial numbers (CTNs) can now be linked up with Digital Object Identifiers (DOIs). The information is available through Crossref's open API and delivered through their Crossmark service, which also allows publishers to highlight key information, such as funding sources and retractions. members deposit metadata with Crossref, who accepts CTNTRNs from the sixteen trial registries endorsed by the World Health Organization (WHO).

"Crossref's mission is to solve precisely this kind of problem, we aim to improve research communications by connecting multiple parties and their metadata" explained Kirsty Meddings, the Product Manager responsible for delivering this. "It's exciting to see clinical trial info starting to come in. We know from experience that new metadata requests have to start somewhere but we will quickly see a snowball effect as hundreds of other biomedical publishers make the necessary upgrades to their workflows."

Stephanie Mathisen, Campaigns and Policy Officer at Sense about Science, co-founders of the AllTrials campaign: "AllTrials calls for all clinical trials to be registered and their results reported; anything that improves access to results is a welcome development. We need to be able to find results from all trials on the medicines we use today. This new system will make it easier to find those results that are published in journals and we look forward to seeing it being used widely. More organisations need to join the discussion about missing clinical trial results and, like Crossref and BioMed Central, be part of the solutions."

Quick Facts:

* Three-year effort led by BioMedCentral and Crossref bridges the gap between the initial publication of clinical trial registration to the later publications and secondary analyses.
* Health professionals and researchers will now have easy, open access to all the published content related to a clinical trial via the Crossref Crossmark dialogue box.

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.
