+++
title = "Crossref To Add Hundreds Of Journals From Africa and Asia"
date = "2006-12-05"
parent = "News"
weight = 1
+++

*05 December 2006*

**Lynnfield, MA. December 5, 2006** -- Crossref, the multi-publisher linking association, announced today that it had
reached agreement with three new partners to include journals from Africa and Asia in its linking network.

The three international organizations collaborating with Crossref are the International Network for the Availability of Scientific Publications (INASP) (http://www.inasp.info), the National Inquiry Services Centre (NISC) http://www.nisc.co.za and African Journals OnLine (AJOL),  http://www.nisc.co.za.

INASP, an international NGO working to promote scientific publishing in developing countries, will initially register journals from Nepal and Vietnam, while NISC will register its entire list of South African-based academic journals and bibliographic databases. AJOL, a fast-growing, independent journal aggregator, currently represents over 260 multi-disciplinary journals from 21 African countries. Crossref Executive Director, Ed Pentz commented, “For some time Crossref has been exploring how it could partner with publishers and journal publishing initiatives in regions of the world where scholarship and publication output are less accessible. We believe offering Crossref’s services on an affordable basis to qualified publishers in these regions links them with the global research literature and raises global visibility of and access to these journals.”

According to Pippa Smart, Head of Publishing Initiatives at INASP, which is a registered educational charity in the UK, “For journals that are largely invisible to most of the scientific community, the importance of linking cannot be overstressed. We at INASP are therefore delighted to be working with Crossref to promote discovery of journals published in the less developed countries. We believe that an integrated discovery mechanism which includes journals from all parts of the world is vital to global research - benefiting not only the editors and publishers in Africa, Southeast Asia and Latin America with whom we work, but countless other citizens in those countries as well.”
---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
