+++
title = "New Metadata Plus service launching"
draft = false
date = "2017-11-14"
parent = "News"
weight = 3
+++

_15 November 2017_
### Crossref introduces new Metadata APIs ‘Plus’ service offering new features and richer metadata
_Machine access to millions of records about research outputs is now even easier_

During its Annual Meeting, LIVE17, in Singapore today, Crossref announced a new paid-for service from January 2018 for machine access to metadata along with guaranteed uptime and priority support, with notifications of new matching records and ‘snapshot’ downloads to be added in the first quarter.

Publishers register content with Crossref by depositing metadata so that they don’t have to make bilateral agreements with third parties, or each other. Crossref makes this metadata openly available via APIs so that people and machines around the world can incorporate it into their tools and services to aid research. This metadata can be retrieved via a choice of APIs to find, cite, link, and assess research outputs. It means that publishers only have to send the metadata to one place to have their content discovered and used by the thousands of tools and services that rely on Crossref as a source of information about research outputs.

Ginny Hendricks, Director of Member and Community Outreach at Crossref, comments “We receive over 600 million queries each month through our various human and machine interfaces and this is only rising. We’re a sustainable community-governed not-for-profit, so this widespread use of our APIs helps the entire scholarly community rely on a single shared source of metadata, and create robust, persistent and accurate citations to and between our members’ content. The new Plus service marks a significant milestone for Crossref as we place strategic emphasis on serving the users of our metadata.”

Jennifer Kemp, Head of Partnerships at Crossref, explains “Crossref’s 92 million (and counting) metadata records provide information about about journal articles and preprints, books and book chapters, conference proceedings, standards, datasets, and reviews. And it’s not just bibliographic metadata—references, Cited-by counts, funding data, license information, full-text links, ORCID iDs, abstracts, and retractions and corrections—are all available, if included in publisher metadata. This metadata remains free and open through Crossref’s public APIs. Starting January 2018, anyone—from publishers, funders, research institutions, and others—can choose to pay an annual fee for guaranteed uptime, dedicated support, and soon after, volume downloads and notifications.”

Kudos is the first organization to sign up. David Sommer, Co-Founder and Product Director, says “Our service has always ingested Crossref bibliographic metadata to reduce manual data entry for our users. We’re excited to now be adding Crossref citations to the range of metrics we compile for users, and we’re also pleased that Crossref is strengthening the services associated with its API.”

Other early adopters in the first couple of weeks include: Reprints Desk; Science-Metrix; ScienceOpen; and Technical Information Center of Denmark (DTU Library).

<img src="/images/news-images/map-search-users.png" alt="World map of current metadata search users" width="80%">

### Further Information

Please contact Jennifer Kemp via [plus@crossref.org](mailto:plus@crossref.org).

- [About the Plus service](/services/metadata-retrieval/metadata-plus/)
- [Plus subscriber agreement](/services/metadata-retrieval/metadata-plus/terms/)
- [Plus service fees](/fees/#metadata-plus-subscriber-fees)

---

Please contact our [communications team](mailto:news@crossref.org) with any questions.
