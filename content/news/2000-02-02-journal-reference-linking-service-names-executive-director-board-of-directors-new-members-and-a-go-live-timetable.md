+++
title = "Crossref Update Journal Reference Linking Service Names Executive Director, Board of Directors, New Members, and a “Go Live” Timetable"
date = "2000-02-02"
parent = "News"
weight = 1
+++

*02 February 2000*

New York, N.Y. February 2, 2000. A group of leading scientific and scholarly publishers today announced the appointment of Ed Pentz as Executive Director of Crossref, the collaborative reference-linking service announced on November 16, 1999. Mr. Pentz, who will be based in Burlington, Massachusetts, assumed his new position on February 1.

“The reference links enabled by Crossref will be of great benefit to scientists. I am looking forward to moving ahead quickly to implement the service and to signing up more publishers so that Crossref can be a comprehensive source for linking journal articles,” said Mr. Pentz, who was previously Electronic Business Development Manager at Academic Press.

Mr. Pentz explained that Crossref will be operated under the aegis of a not-for-profit organization jointly formed by the member publishers and recently incorporated as Publishers International Linking Association, Inc. (PILA). He also reported on the election of its Board of Directors, which includes Eric A. Swanson, John Wiley & Sons, Inc., Chairman; Pieter Bolman, Academic Press, Treasurer; Michael Spinella, AAAS; Marc Brodsky, American Institute of Physics; John R. White, Association for Computing Machinery; John Strange, Blackwell Science; John Regazzi, Elsevier Science; Anthony Durniak, IEEE; Jeffrey K. Smith, Kluwer Academic Publishers; Stefan von Holtzbrinck, Nature Publishing Group; Martin Richardson, Oxford University Press; and Ruediger Gebauer, Springer-Verlag.

According to Mr. Pentz, Cambridge University Press, Marcel Dekker Inc., the Royal Society of Chemistry, Portland Press, the American Mathematical Society, and the American Psychological Association have recently joined this innovative initiative, bringing the current total to 22 member publishers. Active discussions are underway with many more scientific and scholarly primary journal publishers to further broaden this industry-wide initiative.

Crossref is scheduled to launch during the first quarter of this year. Mr. Pentz said that the multi-step process will begin next week when member publishers begin to submit information about their journal articles to the Crossref metadata database. In the weeks and months that follow, live reference links will begin to appear gradually, as member publishers use Crossref to enable them to add reference links in their journal articles to other publishers’ content.

Crossref will operate behind-the-scenes by enabling member publishers to add reference links to their online journals. Users of the online journals will see, click on, and follow the links directly to the content; there will be no visible Crossref interface. Publishers will control access to their content. Crossref will be run from a central facility operated by PILA, and will utilize the Digital Object Identifier (DOI) to ensure permanent links.

Once the service is fully launched, more than three million articles across thousands of journals will be linked through Crossref, and more than half a million more articles will be linked each year thereafter. Such linking will enhance the efficiency of browsing and reading the primary scientific and scholarly literature. It will enable readers to gain access to logically related articles with one or two clicks – an objective widely accepted among researchers as a natural and necessary part of scientific and scholarly publishing in the digital age. For more information about Crossref, visit http://www.crossref.org.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
