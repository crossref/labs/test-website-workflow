+++
title = "Crossref Joins STM-DataCite Statement"
date = "2012-11-14"
parent = "News"
weight = 1
+++

*10 August 2012*

**10 August 2012, Lynnfield, MA USA**—In June 2012, DataCite and the International Association of STM Publishers (STM) issued a joint statement on the Linkability and Citability of Research Data (http://www.stm-assoc.org/2012_06_14_STM_DataCite_Joint_Statement.pdf). Crossref is pleased to join and support this statement and the best practices for data it recommends.

Crossref, a not-for-profit association of representing 4,000 scholarly publisher with 55 million content items (journal and conference proceeding articles and books and book chapters), is committed to the interoperability of Crossref and DataCite’s services which are based on the Digital Object Identifier (DOI) System, recently approved as an ISO Standard (ISO 26324:2012, Information and documentation -- Digital object identifier system).

Specifically, Crossref encourages publishers to use DataCite DOIs to link to data sets referenced in the published literature, and encourages authors of research papers to use Crossref DOIs to link from data deposited in DataCite repositories to the published articles that draw on that data. Crossref and DataCite are also collaborating on joint services, such as DOI Content Negotiation (https://crosscite.org), to enable publishers and data repositories to automatically interlink their content.  

**ABOUT CROSSREF**

Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 2000, Crossref has provided reference linking services for over 53 million content items, including journal articles, books and book chapters, conference proceedings, reference entries, technical reports, standards, and data sets. Crossref also provides additional collaborative services designed to improve trust in the scholarly communications process, including Cited-By Linking, CrossCheck plagiarism screening, and Crossmark update identification.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
