+++
title = "Crossref Growing Fast as it Signs More Than 200 Members"
date = "2003-04-08"
parent = "News"
weight = 1
+++

*08 April 2003*

**LYNNFIELD, MA, April 8, 2003**  -- Crossref, a publisher collaborative that enables researchers to navigate online content via DOI-based citation links, announced today that it has surpassed the 200-member mark -- up by 100 publishers in just over a year. In addition, several hundred thousand non-journal DOIs, including books and conference proceedings, have been added to its extensive linking network. With these additions, the Crossref system now covers over 7,600 journals and books, and 7.5 million individual content items.

Crossref’s membership base is highly international and multi-disciplinary. According to Executive Director Ed Pentz, “Reaching 200 members is an important milestone for Crossref. Our rapid growth in just three years includes content from all areas of scholarly and professional publishing and member publishers from around the world.”

Among Crossref’s newest members are: the American Medical Association; Prous Science; Science in China Press; ASM International; Johns Hopkins University Press; and Dartmouth College Library, which joined as a journal publisher. Among the new Library Affiliates are Harvard University Library and Sandia National Laboratories.

Project Muse, which previously announced plans to participate with Crossref in 2003, has become active as a Sponsoring Agent, and SISIS Elektra of Germany has just joined as an Agent as well. Apex ePublishing, Inera Inc., and Parity Computing have all recently signed on as Allied Vendors, with a license to promote Crossref-related services to their clients. A number of other e-publishing service firms and consultants are expected to join in this capacity.

Unlike URLs, which become obsolete when an online entity changes location, the DOI provides a permanent name associated with an object’s location in a readily updateable directory. Most libraries today are implementing local link servers and recognize that DOIs enhance their linking capabilities.

Alluding to the significance of this trend, Pentz adds, “Crossref is very pleased that OpenURL-- an important developing NISO standard-- and DOI-- an existing NISO standard-- work together so well to provide effective linking solutions. With 7.5 million DOIs assigned, the DOI has become the standard for identifying and linking peer-reviewed scholarly literature.”



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
