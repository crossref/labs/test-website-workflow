+++
title = "Crossref Announces Web Services Agreement With Microsoft"
date = "2006-10-30"
parent = "News"
weight = 1
+++

*17 July 2006*

**Lynnfield MA, July 12, 2006** -- Crossref, the cross-publisher linking network with over 1,600 participating publishers, announced today that it had reached an agreement with Microsoft Corp. who will become the first official Crossref Web Services Search Partner. Crossref will provide Microsoft with a bulk feed of metadata from hundreds of participating Crossref member publishers.

Crossref Web Services offers an easy-to-use tool for authorized search partners to collect metadata on a cross-publisher basis to streamline web crawling. The program also provides standard terms and conditions for search engines using the cross-publisher metadata available through Crossref. Just as the core Crossref linking service removes the need for bilateral linking agreements among publishers, the Search Partner Program removes the need for bilateral agreements covering use of metadata between publishers and search providers. Participation in Crossref Web Services is optional for Crossref member-publishers.

“We are very happy to have Microsoft as the first Crossref Web Services Search Partner and we look forward to adding other partners in future,” said Ed Pentz, Executive Director of Crossref. “Crossref’s mission is to enable researchers and scholars to get to authoritative primary content and Crossref Web Services will enable this to happen by streamlining the delivery of Digital Object Identifiers (DOIs) for persistent linking and authoritative metadata.”

“Microsoft is dedicated to working cooperatively with industry associations such as Crossref, who have made it their mission to help students and researchers quickly find authoritative academic research,” said Danielle Tiedt, General Manager, Windows Live Premium Search at Microsoft Corp. “We are excited to support Crossref’s Web Services as we both strive to streamline and enhance the online research experience.”

Microsoft is using Crossref Web Services to assist in the indexing of scholarly content for Windows Live Academic Search, a beta of which was launched in April 2006. This new search service is designed to help students, researchers and university faculty conduct research across a spectrum of academic journals. The program is a cooperative effort between Windows Live Search, Crossref, and several leading publishers. Windows Live Academic Search can be found at http://academic.live.com.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
