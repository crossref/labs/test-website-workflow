+++
title = "Crossref Surpasses 30 Million DOI Mark"
date = "2008-01-02"
parent = "News"
weight = 1
+++

*02 January 2008*

**LYNNFIELD MA, January 2, 2008** -- Crossref, the well known linking service, announced today that it had recently registered its 30 millionth DOI. While the majority of Crossref’s Digital Object Identifiers® (DOIs)® are assigned to online journal articles, there are now over 2.5 million DOI names assigned to other types of publications, including conference proceedings, dissertations, books, datasets, and technical reports. Crossref’s dues-paying membership exceeds 500, with over 2,400 publishers and societies participating in Crossref linking.

The 30 millionth DOI, doi:10.1103/PhysRevE.76.055201 was registered by The American Physical Society for their journal Physical Review E.

Crossref adds an average of 550,000 new items every month to its DOI registry and linking service. Of the over 6.5 million DOIs created and assigned during the past year, a large number are associated with archival, or back-file, journal articles, as several large publishers have recently undertaken extensive retro-digitization projects.  These include the Royal Society, Elsevier, Springer, Sage, Kluwer, Wiley, Blackwell, and the American Association for the Advancement of Science. In 2007 alone, the scholarly journal archive JSTOR added a total of 325,251 DOIs to Crossref.

Other noteworthy deposit events during the past year:

-The Office of Scientific and Technical Information (OSTI) of the U.S. Department of Energy deposited more than 86,000 DOE technical reports with Crossref, the earliest report dating from 1933.  

-SciELO – The Scientific Electronic Library Online based in Brazil – added over 26,000 DOIs.  

-The Persee Program, established by the Ministry of State Education, Higher Education and Research in France, deposited over 30,000 DOIs, of which over 14,000 were for legacy content.



_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_

_Crossref® and Crossref.org® are registered trademarks and the Crossref logo is a trademark of PILA (Publishers International Linking Association)_






---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
