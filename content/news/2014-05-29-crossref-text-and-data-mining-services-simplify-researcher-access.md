+++
title = "Crossref Text and Data Mining Services Simplify Researcher Access"
date = "2015-10-26"
parent = "News"
weight = 1
+++

*29 May 2014*

**29 May 2014, Boston, MA** – Crossref Text and Data Mining services, allowing publishers to provide information that will simplify access arrangements for researchers who desire to mine and analyze scholarly publisher sites, is now available to Crossref Members.  Crossref, a not-for-profit association of worldwide scholarly publishers, made the announcement at the Society for Scholarly Publishing Annual Meeting here today.

Publishers participating in Crossref Text and Data Mining services may now deposit full-text links in the metadata for their DOIs, as well as license URIs by which researchers can determine whether they have permission to mine a particular content item. Through Crossref’s Application Programming Interface (API), researchers will then to be able to access the full-text, Crossref DOI–identified content across participating publishers’ sites, regardless of their access models.

For all publishers, whether using open access or subscription business models, Crossref Text and Data Mining services easily direct researchers to the appropriate location of the full text content and licenses for that content. In addition, publishers can add download rate limits to the information they provide to minimize any impact of text and data mining activities on web site performance.

“Crossref Text and Data Mining services extend the infrastructure for publications data to make the process of locating content for text and data mining easier and more consistent across publishers,” commented Crossref Product Manager Rachael Lammey “This common system for text and data mining benefits publishers and researchers alike, enhancing collaboration in scholarly communications.”

The launch of Crossref Text and Data Mining services follows a successful pilot. Charter participants include the following organizations:

- American Institute of Physics  
- Hindawi   
- Taylor & Francis  
- American Physical Society (APS)  
- Institute of Physics (IOP)    
- Elsevier  
- PLOS   
- Wiley    
- HighWire Press   
- Springer  

An optional part of Crossref Text and Data Mining services will allow a publisher to provide a click-through agreement if that publisher requires the researcher to agree to supplementary license terms before accessing content for text and data mining. For publishers whose general licenses already include permission for text and data mining, the click-through service will not be necessary. Crossref Text and Data Mining Services are provided at no cost to researchers, and at no cost to publishers during the first year of operation.

More information on Crossref Text and Data Mining services is available at [http://www.crossref.org/tdm/index.html](http://www.crossref.org/tdm/index.html).

**About Crossref**  
Crossref [(www.crossref.org)](https://www.crossref.org) serves as a digital hub for the scholarly communications community. A not-for profit membership organization of global scholarly publishers, Crossref's innovations shape the future of scholarly communications through cross-publisher collaboration. Crossref provides a wide spectrum of services for identifying, locating, linking to and assessing the reliability and provenance of scholarly content.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
