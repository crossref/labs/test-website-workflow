+++
title = "DOE/OSTI Joins Crossref to Assign DOIs to Technical Reports"
date = "2005-02-03"
parent = "News"
weight = 1
+++

*03 February 2005*

**Lynnfield, MA, February 3, 2005** -- Crossref, the reference-linking service for scholarly and professional content, is pleased to announce that the Department of Energy Office of Scientific and Technical Information (OSTI) has joined Crossref. OSTI plans to assign Digital Object Identifiers (DOIs) to its Information Bridge platform, which currently contains 94,000 scientific and technical reports.

Information Bridge serves as an open source to full-text and bibliographic records of DOE research in physics, chemistry, materials, biology, environmental sciences, energy technologies, engineering, computer and information science, renewable energy, and other topics. It consists of full-text documents produced and made available by the Department of Energy National Laboratories and grantees from 1995 forward. Additional legacy documents are also included as they become available in electronic format.

“We are pleased to join Crossref in pioneering this first-of-a-kind government-private partnership,” said Dr. Walter Warnick, director of OSTI. “We believe that coupling the vast resources available at Information Bridge with the reference-linking capabilities of Crossref will advance OSTI’s mission of making DOE research results more accessible.”

OSTI is the largest provider of so-called “grey literature” to join Crossref since Crossref moved late last year to include publishers of working papers and technical reports among its members. Other new Crossref members who are registering working papers or reports include the Social Science Research Network and Woods Hole Oceanographic Institute.

According to Crossref Executive Director Ed Pentz, this is all part of Crossref’s mission “to provide the most robust citation linking network possible, cutting across publishers, business models, and content types.” The 14.3 million DOIs registered in Crossref to date point mainly to journal articles, books, chapters, and conference proceedings. Among its 350 members, Crossref already includes several open-access publishers. Within the coming year, Crossref plans to extend its content coverage to standards and datasets as well.

OSTI's mission is to advance science and sustain technological creativity by making R&D findings available and useful to DOE researchers and the American people. OSTI has been delivering science information since 1947. With its suite of Web tools, OSTI has proved to be a leader in government science search.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
