+++
title = "Crossref Surpasses 25 Million DOI Mark"
date = "2007-02-13"
parent = "News"
weight = 1
+++

*13 February 2007*

**LYNNFIELD MA, February 13, 2007** -- Crossref, the citation linking service, announced today that over 25 million content items had been registered in the Crossref system since its inception in early 2000. Although the majority of these Digital Object Identifiers® (DOIs)® are assigned to online journal articles, there are over 2 million DOI strings assigned to conference proceedings, components and books, at the chapter as well as title level. Crossref has also been supporting assignment of DOIs to technical reports, working papers, dissertations, standards, and data elements.

The 25 millionth DOI, [10.1093/fh/6.1.106](http://dx.doi.org/10.1093/fh/6.1.106), was registered by Oxford Journals, Oxford University Press, for their journal French History.

Crossref hit the 10 million DOI mark back in January of 2004, after roughly four years in operation. Since then, the rate of growth in DOI creation across the scholarly publishing community has accelerated considerably, with the next 10 million DOIs being created and registered in just over two years.  Last April Crossref registered the 20 millionth DOI string and less than a year later there are over 25 million DOIs in the Crossref system.

Of the five million DOIs created and assigned during the past year, a large number are associated with archival journal articles. The Royal Society, for instance, recently registered its complete journal back-file. In so doing, it joins several Crossref member publishers who have recently completed, or are in the midst of, vast retro-digitization initiatives, including Elsevier, Springer, Sage, Kluwer, Wiley, Blackwell, the American Association for the Advancement of Science, and JSTOR, among others.


_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
