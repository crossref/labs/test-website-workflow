+++
title = "Crossref Selects Atypon As Technology Partner To Build The Next Release Of Its System"
date = "2001-09-07"
parent = "News"
weight = 1
+++

*07 September 2001*

**SANTA CLARA, CA -- (BusinessWire) –- September 7, 2001** -- Atypon Systems and Publishers International Linking Association, Inc. (PILA), which operates Crossref, announced today that Atypon will build the technology powering the enhanced Crossref reference linking service that interconnects bibliographical references between publishers. Atypon Systems provides custom software and business service solutions for electronic publishing.  The time-tested technology of Atypon’s Literatum platform, which is currently behind Academic Press’ highly successful IDEAL system, will be tailored to fit the infrastructure required by Crossref.  Components of Atypon’s fine-tuned access control system will be modified and custom development will be added to form the collaborative reference linking service that interconnects the content of Crossref’s 81 member publishers. The partnership extends into cooperation on future technologies and R&D efforts. Atypon has clearly demonstrated its ability to serve the publishing industry.  Their superior technology is what convinced us that they were the best choice,” said Crossref Executive Director, Ed Pentz.   “We feel very confident working with a company that comes recommended by so many of our member publishers.” “Our commitment to the technology of scholarly publishing uniquely positions us to build Crossref’s new system,” stated Atypon CEO, Georgios Papadopoulos.  “We have enormous respect for what PILA represents and are excited about the partnership, particularly in working closely with them on future technologies that will benefit the publishing industry as a whole.” The Crossref organization is dedicated to expanding its citation linking coverage for libraries and researchers.   With Atypon’s technology, Crossref will be able to add linking between major reference works and conference proceedings to their current electronic journal offering.  The system is designed to be flexible and expandable for additional services in the future.  Atypon will work with Crossref to further enhance interconnectivity and seamless access to scholarly content. “PILA and Atypon are creating a truly synergistic relationship at the intersection of publishing and technology,” said Ravi Singh, Atypon VP of Corporate Development.   “Atypon is committed to deliver a cost-effective system that will scale to meet Crossref’s current and future needs.  And working with PILA’s members enables us to continue to learn how best to serve their needs.”

**About Atypon**
Atypon is a leading software and business services provider (BSP) for the Science, Technology and Medicine information industry. It provides business-to-business solutions that are designed to help biotechnology; pharmaceutical and publishing companies leverage the Internet. Atypon's customers range from non-profit organizations to the world’s leading Publishers. The company is headquartered in Santa Clara, California and maintains a European office in Athens, Greece. For more information on Atypon Systems, please visit http://www.atypon.com.

Contact:
Kristen Fisher
Director of Product Marketing
Atypon Systems
Telephone (408) 988-1240 ext. 315
kristen@atypon.com



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
