+++
title = "Crossref Steps Up Pace of Linking Developments"
date = "2001-10-10"
parent = "News"
weight = 1
+++

*10 October 2001*

**CROSSREF WELCOMES FIVE NEW PUBLISHERS AND FOUR NEW AFFILIATES DURING JULY**

**BURLINGTON, MA, October 10, 2001** -- Crossref, a publisher collaborative reference linking service that enables scholarly researchers and students to navigate move from one online journals citation to another seamlesslyvia DOI-based citation links, is stepping up its cross-publisher linking initiatives.

According to Amy Brand, Director of Business Development, "Crossref is committed to providing the scholarly community with an increasing number of linked journals, a wider selection of connected information sources beyond journals and STM content, and a vehicle for greater collaboration amongst publishers, librarians, and vendors." Brand has been instrumental in Crossref's membership growth since joining in April 2001.
Embracing internal system improvements as well as member and affiliate member outreach, Crossref has implemented the following developments:

* A prototype solution to the "appropriate copy" problem combining OpenURL, Crossref, and Ex Libris' SFX localized linking technologies to redirect DOI links to a library's local holdings.
* Partnering with Atypon Systems -- a Santa Clara, CA-based software and business services company for the STM information arena -- in a comprehensive upgrade to Crossref's linking capabilities.
* Extending its services to books and conference proceedings, using an XML XSDschema developed for Crossref by Inera, an SGML and XML consulting and software services company specializing in STM publishing, based in Newton, MA., and improve on the existing journal linking capabilityAtypon Systems, a Santa Clara, CA-based software and business services company for the STM (Science, Technology, and Medicine) information arena will implement the capabilities. Crossref expects to begin registering DOIs for conference proceedings and book citation linking in early 2002.

Crossref now has 83 members and 23 affiliates from the library and information communities. Among recent publishers to join are the Massachusetts Medical Society, which publishes The New England Journal of Medicine (NEJM), one of the most highly cited journals in the STM publishing industry; BioMed Central, an independent publisher providing biomedical researchers with free access to more than 50 peer-reviewed online journals; S. Karger AG, medical and scientific publishers since 1890 and Sage Publications, extending Crossref's journal coverage well into the humanities and social sciences. This brings the total number of Crossref- enabled journals to 5,100, encompassing over 3.5 million article records.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
