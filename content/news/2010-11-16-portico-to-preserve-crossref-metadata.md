+++
title = "Portico to preserve Crossref metadata"
date = "2010-11-16"
parent = "News"
weight = 1
+++

*16 November 2010*

**16 November, London, UK**—Crossref and Portico announced today an agreement in principle  under which Portico will preserve Crossref’s bibliographic and reference metadata in the Portico archive. This agreement would act as an extra level of insurance to the scholarly community, which relies on reference linking services from Crossref.

Ed Pentz, Crossref’s Executive Director, noted, “This agreement with Portico represents the most recent in a series of steps Crossref has taken to implement our plan for long-term sustainability. For Crossref DOIs to be persistent links Crossref itself must be persistent as an organization. We updated our membership terms to encourage member publishers to enter into archiving agreements for their own content and to ensure Crossref can keep the bibliographic metadata even if publishers stop assigning DOIs or cease operations. We have implemented multiple resolution of Crossref DOIs to enable links to several archive locations for “triggered” content that is no longer available from the publisher. And now, we will be able to take advantage of Portico’s quality digital preservation services to archive our own data in the unlikely event that Crossref itself should cease to function without a successor organization in place.”

“Crossref and Portico share a commitment to preserving the scholarly record,” said Eileen Fenton, Portico’s Managing Director. “Crossref joins other forward-looking organizations who have committed to the long-term preservation of their content by participation in the Portico preservation service.”

Portico is a community-supported digital preservation service provided by [ITHAKA](http://www.ithaka.org/), a not-for-profit organization with a mission to help the academic community use digital technologies to preserve the scholarly record and to advance research and teaching in sustainable ways. Portico preserves 12,000 e-journals, 66,000 e-book titles, and 39 digitized historical collections from 118 publishers on behalf of over 2,000 societies and associations, and with the support of 710 libraries worldwide. The agreement with Crossref would add bibliographic data with Crossref Digital Object Identifiers (DOIs) for more than 44 million records from more than 3200 publishers. For a complete list of Portico-related facts and figures, please visit Portico’s Archive [Facts & Figures](http://www.portico.org/digital-preservation/the-archive-content-access/archive-facts-figures/). The complete list of titles and participating publishers is available at www.portico.org/digital-preservation/.

**About Crossref**
Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 1999, Crossref has provided reference linking services for 44 million content items, including journal articles, conference proceedings, books, book chapters, reference entries, technical reports, standards, and data sets. Crossref currently has 980 voting members, representing 3237 publishers. Almost 60% of Crossref publishers are not-for-profit organizations. Crossref also serves more than 1600 libraries and provides citation look-up services to a number of affiliated organizations.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
