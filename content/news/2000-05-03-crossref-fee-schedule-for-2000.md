+++
title = "Schedule of Fees for 2000"
date = "2000-05-03"
parent = "News"
weight = 1
+++

*03 May 2000*


|Primary Publisher Annual Member Fee|  |
| :------| :-----------:|
|1 title, max 500 articles per year|$200|
|2-5 titles, max 2,500 articles per year|$500|
|6-20 titles, max 10,000 articles per year|$750|
|21-100 titles, max 50,000 articles per year|$1,000|
|>100 titles or >50,000 articles per year|$2,000|

|Per Article Deposit Fee for Primary Article Metadata|  |
| :------| :-----------:|
|Current|$0.60|
|Back File<sup>1</sup> (deposit after December 31, 2000)|$0.10|
|Back File (deposit prior to December 31, 2000)|$0.05|

|Member Retrieval Fee for Primary Content Use (for successful match, i.e. per DOI returned.)|  |
| :------| :-----------:|
|2000|$0.05 (Waived in 2000)|
|2001|$0.10|
<sup>1</sup> <sub>"Back File" means content published prior to the current year</sub>

|Non-Member Annual Administrative Fee/Member Secondary Use Fee|  |
| :------| :-----------:|
|Secondary, database <100,000 records/yr|$2,000|
|Secondary, database >100,000 records/yr|$5,000|
|Agents <sub>(sharing and caching for multiple members)</sub><sup>2</sup>|$5,000|
|Agents <sub>(acting individually for one or more members)<sub>|$1,000|
|Libraries|$500|

|Non-Member Retrieval Fee/Member Secondary Use Retrieval Fee (for successful match, i.e. per DOI returned)|  |
| :------| :-----------:|
|Secondaries, Agents, Others -2000|$0.05|
|Secondaries, Agents, Others -2001|$0.10|
|Libraries (Libraries only pay the Annual Admin Fee)|$0.00|

<sup>2</sup><sub>"Agents" are organizations hosting journals for members or other service agencies acting on behalf of members.  An Agent acting for multiple members can look up a DOI once, store it in a local system and share it among different member publishers - this is referred to as "sharing or "caching".  An Agent looking up DOIs individually for each member (i.e. not sharing or caching) pays the lower fee.</sub>

**Background on Fees**

Crossref is a service run by Publishers International Linking Association, Inc (PILA), a not-for-profit membership organization comprised of publishers of original scholarly material.

* Fees are subject to change without notice
* Primary Publisher Annual Member Fee is based on the number of journals available online.  Publishers must deposit metadata from all their online journals in the Crossref system.
* Before retrieving DOIs for references in articles, Members must deposit the article metadata in the Crossref system.  In other words, in order to link **from** an article, the metadata for that article must be deposited in the Crossref system so that others can link **to** the article.
* Organizations with both primary and secondary divisions must pay an annual member fee plus an annual administrative fee if DOIs are used in both primary and secondary content
* Secondary divisions affiliated with primary-division publishers will not be allowed to retrieve DOIs unless the primary-division publisher deposits metadata for its journals
* Members and non-members may "cache" retrieved DOIs (i.e. store them in their local systems).
* Members may appoint Agents to act on their behalf with Crossref (registering metadata, looking up DOIs). An Agent can represent any number of members

© 2000 PILA, Inc.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
