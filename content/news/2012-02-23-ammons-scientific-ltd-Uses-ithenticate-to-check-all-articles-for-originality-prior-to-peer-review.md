+++
title = "Ammons Scientific LTD Uses iThenticate to Check all Articles for Originality Prior to Peer Review"
date = "2012-02-23"
parent = "News"
weight = 1
+++

*23 February 2012*

_iThenticate helps leading psychology publisher educate individuals from other cultures on the issues of plagiarism and authentic writing_

**OAKLAND, Calif., February 23, 2012** – Turnitin, creators of iThenticate and the leader in plagiarism prevention, is helping Ammons Scientific (AmSci), a leading publisher of psychology journals, ensure research is original before publication. The majority of manuscripts submitted to AmSci are from authors and researchers in countries where English is not the primary language. Cultural differences in writing methodologies often collide with those of peer-reviewed publication.  Using iThenticate, AmSci is able to verify the originality of all work and have a way to educate individuals on permissible writing practices in the field of scholarly research.

“Around 60 percent of the articles submitted to us are coming from individuals for whom English is a second language. In some cases, there is a tendency to want to say something the way someone else said it and, in some cultures, it is considered respectful to copy someone else’s work,” said Dr. Stephanie A. Isbell, managing editor at Ammons Scientific LTD. “iThenticate has helped us develop our service to ensure quality and intervene with writers as needed to ensure that their work is original and robust.”

The pressure to publish, combined with the growing population of scholarly researchers, has made it increasingly difficult for publishers crosscheck logoand institutions to prevent cases of plagiarism and self-plagiarism. AmSci has united with more than 280 top-tier publishers in an initiative called CrossCheck, which is a partnership between Crossref and iThenticate that provides scholarly publishers with iThenticate’s anti-plagiarism services in order to prevent duplication and other academic misconduct issues. CrossCheck compares millions of documents annually against iThenticate’s database of more than 116 million scholarly content items, including 30 million articles, books and conference proceedings from over 70,000 publications, and 20 billion web pages.  

“Plagiarism checking has become an essential tool for journal editors to ensure the originality of written work in scholarly research and published material,” said Chris Cross, general manager of iThenticate. “Ammons Scientific’s adoption of iThenticate is a strong example of steps publishers are taking to ensure originality in research and avoid costly misconduct cases.”  

For more information on how Ammons Scientific is using iThenticate, [read the case study](https://web.archive.org/web/20140307133929/http://www.ithenticate.com/customers/success-stories/ammons-scientific). For more information, please visit [www.ithenticate.com](https://www.ithenticate.com/).

**About Turnitin and iThenticate**
Turnitin is the world's leading provider of web-based solutions for plagiarism prevention. The company's products include Turnitin, used by educators worldwide to check students' papers for originality, to enable web-based peer review and for digital grading of student work. Turnitin’s iThenticate solution enables publishers, research facilities, government agencies, financial institutions, legal firms and now authors and researchers to reliably check submitted materials for originality before publication. The company's solutions check millions of documents each month and are used in over 100 countries. Turnitin is headquartered in Oakland, CA with an international office located in Newcastle, United Kingdom. Turnitin is backed by Warburg Pincus. http://www.iparadigms.com and http://www.ithenticate.com.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
