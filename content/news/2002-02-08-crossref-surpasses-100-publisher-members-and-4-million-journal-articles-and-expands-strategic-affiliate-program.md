+++
title = "Crossref Surpasses 100 members & 4 Million Journal Articles, And Expands Strategic Affiliate Program"
date = "2002-02-08"
parent = "News"
weight = 1
+++

*08 February 2002*

**CROSSREF WELCOMES FIVE NEW PUBLISHERS AND FOUR NEW AFFILIATES DURING JULY**

Five new publishers joined Crossref in the month of July, bringing the total membership to 77 publishers. The new members are the American Meteorological Society with 10 journals, American Psychiatric Publishing, Inc. with eight journals, the American Society for Microbiology with 12 journals, Maney Publishing with six journals, and Sage Publications with over 250 journals.

This will bring the total number of Crossref-enabled journals to well over 5,000. These new members bring excellent content in the behavioral and natural sciences for linking, and the inclusion of Sage's journal titles extends Crossref's coverage firmly into the social sciences and humanities.

We are extremely pleased that four new affiliates also joined Crossref last month. They are: Maruzen, Openly Informatics, Toshiba Corporate Library, and the National Center for PTSD.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
