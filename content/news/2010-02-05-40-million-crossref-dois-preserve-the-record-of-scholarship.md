+++
title = "40 Million Crossref DOIs preserve the record of scholarship"
date = "2010-02-05"
parent = "News"
weight = 1
+++

*05 February 2010*

**5 February 2010, Lynnfield, MA USA**—Crossref, which provides reference linking services for scholarly publications, has surpassed 40 million metadata records for scholarly content. Each of these records includes a Crossref Digital Object Identifier (DOI), which allows the content to be accessed by a permanent link on the Internet.

Of these 40 million items, 87 percent are from journals. Content from scholarly books and reference works makes up more than 5 percent and another 5 percent is from conference proceedings.

The oldest Crossref DOIs represent articles from 1665 with issues of the Royal Society’s Philosophical Transactions. In fact, Crossref contains more than 650,000 records from the 17th, 18th, and 19th centuries as well as the tens of millions from the 19th and 20th centuries.

Crossref includes metadata from more than 2900 publishers, 20,000 journal titles, and 100,000 book titles, which represent Crossref’s fastest growing content type. Content comes from 6 continents, including publishers from low-income countries through arrangements with organizations like the Information Network for the Availability of Scientific Publications (INASP).

“Passing the 40 million mark underscores how ubiquitous reference linking has become in the scholarly communications process,” said Ed Pentz, Executive Director of Crossref. “Crossref has enabled reference linking and persistent identification for the vast majority of electronic scholarly content. Nearly a million times every day, Crossref DOI links resolve to the appropriate article or other content. We are committed to supporting scholarship for the long term--and our definition of ‘long term’ is very long indeed. We look forward to continuing our strong collaborations with publishers, with libraries, with our affiliates including vendors, and ultimately with researchers to promote good research, building on the foundations of the past.”

“Crossref’s contribution to scholarly publishing has been remarkable in the relatively short ten years of its existence,” added Bob Campbell, President of Wiley-Blackwell and Chair of the Crossref Board of Directors. “The substantial number of reference links enabled by those millions of Crossref DOIs means that all over the world scholars can easily locate information to help them in their research, Crossref continues to add innovative services to promote collaboration, trustworthy scholarly content, and the infrastructure to benefit the many parties involved in scholarly communication.”
Crossref currently has 804 voting members, representing 2950 publishers. Almost 60% of Crossref publishers are not-for-profit organizations. Crossref also serves more than 1500 libraries.

**About Crossref**

Crossref (http://www.crossref.org) is a not-for-profit membership association founded and directed by publishers. Its mission is to enable easy identification and use of trustworthy electronic content by promoting the cooperative development and application of a sustainable infrastructure. Since its founding in 1999, Crossref has provided reference linking services for more than 40 million content items, including journal articles, conference proceedings, books, book chapters, reference entries, technical reports, standards, and data sets.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
