+++
title = "Linked clinical trials working group"
date = "2021-02-26"
draft = false
author = "Martyn Rittman"
parent = "Working groups"
weight = 80
+++

The purpose of this advisory group is to advise on linked clinical trials and support staff.  The group comprises Crossref members and non-members, and is lead by a Chair and Crossref staff facilitator.

## Group members

This group is not currently active but was Chaired by Daniel Shanahan, then at BioMed Central, and by Crossref's Kirsty Meddings.

---
## How the group works (and the guidelines)

Members commit to attend all meetings by conference call, and may choose to send a named proxy if they are not available. Meeting notes will be circulated to all by the facilitator. The schedule of meetings is at the discretion of the chair and facilitator and may vary depending on whether there are relevant topics for discussion, but will not be more than one per quarter.

With the exception of Crossref staff, the group will be limited to one representative from each participating organization, unless particular agenda items or topics call for domain expertise from specific colleagues or departments. Advisory group members are, however, free to discuss the information shared during meetings with colleagues or any external party.

---
Please contact [Bryan Vickery](mailto:crossmark@crossref.org) with any questions or to apply to join the advisory group.
