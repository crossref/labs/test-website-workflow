+++
title = "Preprint AG meeting notes"
date = "2023-03-14"
draft = false
author = "Martyn Rittman"
rank = 1
parent = "Preprint advisory group"
weight = 10
+++

# Preprint Advisory Group meeting notes

On this page you can find a summary of meetings held by the preprint advisory group. 

## AG meeting, 23 February 2023

### Agenda

 - Welcome to new members
 - Update on schema changes: upcoming survey of Crossref members to prioritize planned changes.
 - Introduction to the new eLife editorial workflow and how metadata is handled.
 - Introduction to Docmaps.

## AG meeting, 27 October 2022

### Agenda

 - Upcoming changes to preprint/journal-article notification emails.
 - Discussion of feedback on preprint metadata recommendations:
    - Origin of withdrawals.
    - Representation of editorial progress in preprint metadata.
    - Work of related groups, including NISO and ASAPbio.
    - Interoperability of Crossref metadata with other platforms.

### Actions

 - No suggested changes to recommendations as a result of feedback.
 - Review forthcoming report on AG discussions. 

##  AG meeting, 25 August 2022

### Agenda

 - Adding summary of AG meetings to the Crossref website.
 - Update on feedback recommendations.
 - Forthcoming COAR/ASPbio recommendations.
 - Crossmark documentation to support registration of DOIs for preprints.

### Actions

 - Take feedback on documentation to colleagues looking at website design.
 - Revise the subject line of preprint/journal-article notification emails.

 ## AG meeting, 23 June 2022

 ### Agenda

 - Publication of AG recommendations.
 - Discussion of withdrawal processes.
 - Notify Project from COAR.

 ### Actions

 - Public posting of AG recommendation document.

 ## AG meeting, 26 April 2022

 ### Agenda

 - Multilingualism in preprints

 ### Actions

  - Improve Crossref documentation to explain which fields can have multilingual content and how to deposit the metadata.
  - Clearer guidance from Crossref about how to register DOIs for lay summaries in different languages.

## AG meeting, 17 March 2022

### Agenda 

 - Feedback from recent Crossref Board meeting
 - Discussion on preprint recommendations:
    - Relationships
    - Versioning
 - Process retrospective

### Actions

 - Crossref can consider matching preprints to articles and adding them directly to metadata provided the false-positive rate can be quantified and is very low.
 - Crossref should provide more guidance on when to use types of relationships.
 - Recommendation discussion around a 'citable DOI' removed from recommendations document.
 - Seek public feedback on recommendations.

 ## AG meeting, 16 February 2022

 ### Agenda

 - Updates to preprint/journal-article notification emails
 - Working group summaries.
 - Metadata recommendation discussions:
    - Withdrawals and removals.
    - Preprints as an article type.
    - Preprint relationships.

 ### Actions

 - Discuss Crossref's fee structure in a future meeting.
 - Keep the term 'preprint' despite limitations if it's taken too literally.
 - Crossref should consider notifying interested parties when a preprint is withdrawn.

## Asynchronous discussion, October 2022

### Agenda

 - The balance between best practice and capturing current practice.
 - Followup topics from previous prioritization exercise.

### Actions

 - Make recommendations optional, do not force specific practices.
 - Come back to language metadata in a future meeting.

 ## AG meeting, 13 September 2021

 ### Agenda

 - Initial discussion outcomes of prioritization exercise:
   - Version number.
   - Withdrawal/removal and Crossmark metadata.
   - Establish questions to research, look at current practice.

 ### Actions

 - Condense discussions into a list of recommendations.

 ## AG meeting, 26 July 2021

 ### Agenda

 - Outcomes of prioritzation exercise.
   - Preprints as an article type.
   - Relationships to/from preprints.

 ### Actions

 - Continue discussion on other selected topics in next meeting.

 ## AG meeting, 7 June 2021

 ### Agenda

- Scope of the group.
- Review of current metadata practice for preprints.

 ### Actions

 - Prioritize topics around preprint metadata.
 - Appoint chair.