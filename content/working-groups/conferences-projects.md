+++
title = "PIDs for Conferences & Projects"
date = "2019-08-21"
draft = false
author = "Patricia Feeney"
rank = 1
[menu.main]
parent = "Working groups"
weight = 15
+++

This group aims to establish a persistent identifier (PID) system and registry for scholarly conferences. PIDS enable creation of a persistent metadata record about a conference and, when applied to published proceedings, enables more efficient decision making for researchers, libraries, publishers, funding and evaluation bodies. Longer term, it also helps to identify fraudulent and/or low-quality conferences. This group initially intended to research PIDs for Conferences and projects, but has limited the scope to Conferences for the first phase.

Metadata specifications were [circulated for comment](/blog/pids-for-conferences-your-comments-are-welcome/) in Spring 2018, and a set of metadata to define conferences has been decided. The working group held a kick-off meeting at CERN in February 2019. Both [DataCite](https://datacite.org/) and Crossref will be implementing this metadata set to allow conferences to be registered as DOIs.

Currently the group involves a good number of representative publishers and has regular calls every 1-2 months. You can follow the group activity on twitter using [#confpid](https://twitter.com/hashtag/confpid) tag or via the [DataCite blog](https://blog.datacite.org) and [our own](/blog).

Please see blog posts [Taking the con out of conferences](/blog/taking-the-con-out-of-conferences) and [Towards persistent identification for conferences](https://www.project-freya.eu/en/blogs/blogs/towards-persistent-identification-of-conferences) for background information.

## Group Members

Chair: Aliaksandr Birukou, Springer Nature<br>
Facilitator: Patricia Feeney, Crossref

{{% row %}}
{{% column %}}
* Marcel R. Ackermann, DBLP / Schloss Dagstuhl
* Nhora Cortes-Comerer, ASME
* Philip DiVietro, ASME
* Martin Fenner, DataCite
* Gerry Grenier, IEEE
* Christina Hoppermann, Springer Nature
* Bethan Keall, Elsevier
* Anna Lacson, ACM
{{% /column %}}
{{% column %}}
* Andres Mori, Digital Science
* Lisa Nienhaus, Springer Nature
* Craig Rodkin, ACM
* Sweitze Roffel, Elsevier
* Judy Salk, Elsevier
* Kruna Vukmirovic, The IET
* Alexander Wagner, DESY
{{% /column %}}
{{% /row %}}

---
Please contact [Patricia Feeney](mailto:pfeeney@crossref.org) with any questions or to apply to join the working group.
