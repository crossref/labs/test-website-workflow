+++
title = "Committees"
date = "2020-04-10"
draft = false
author = "Lucy Ofiesh"
rank = 4
[menu.main]
parent = "Board & governance"
weight = 2
+++

We have a number of committees that provide oversight of different aspects of our activities. They ensure that Crossref is governed and run efficiently and fulfills its mission. Committees are established in our [by-laws](/board-and-governance/bylaws) or they can be established by our board for a specific purpose.

Our board appoints the Chair of each committee each year at its November meeting. The Crossref [guiding principles](/truths) highlight that effective and representative governance is important to persistence and enabling us to achieve our mission.




## Committees

Committee  | Staff facilitator | Chair
--- | --- | ---
[Audit committee](/committees/audit) | Lucy Ofiesh | Penelope Lewis, AIP  
[Executive committee](/committees/executive) | Ed Pentz | Lisa Schiff, California Digital Library
[Membership and Fees committee](/committees/membership-and-fees) | Amanda Bartell |
[Nominating committee](/committees/nominating) | Lucy Ofiesh | Aaron Wood, APA
