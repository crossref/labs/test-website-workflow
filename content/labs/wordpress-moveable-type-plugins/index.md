---
title: 'WordPress & Moveable Type Plugins'
author: admin
date: 2013-01-22

---

{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}
&nbsp;

We have created [WordPress][1] & [Moveable Type][2] plugins that allow you to search Crossref metadata using citations or partial citations. When you find the reference that you want, insert the formatted and DOI-linked citation into your blog posting along with supporting [COinS][3] metadata. The plugin supports both a long citation format and a short (op. cit.) format. The plugins can be found on [SourceForge][4].

Please note the following about the plugins:

  * To install these, you will need "shell access" to the machine running your WordPress or Moveable Type instance. If you don’t know what this means, you should probably talk to your system administrator.

  * We are releasing these as a test. The back-end is running on R&D equipment in a non-production environment and so it may disappear without warning or perform erratically. If it isn&#8217;t working for some reason, come back later and try again. If it seems to be broken for a prolonged period of time, then please report the problem to us via SourceForge.

  * There is currently a 20 item limit on the number of hits returned per query. This might seem arbitrary and stingy, but please remember- we are not trying to create a fully blown search engine- we&#8217;re just trying to create a citation lookup service. Of course, if, after looking at how the service is used, it looks like we need to up this limit, we will.

  * If you look in the plugin options (or at the code), you will see that the system includes an API key. At the moment we have no restrictions on use of this service, but have included this in case we need to protect the system from abuse. We recommend that you set this API key to the email address of the administrator of the blog.

  * The bulk of the functionality we have developed is actually at the back-end. The plugins are just  lightweight interfaces to that back-end. You can examine the guts of the plugins in order to easily figure out how to create similar functionality for your favourite blog platform, wiki, etc. If you do create something, please let us know. We&#8217;d love to see what people are building.

  * We are continuing to experiment with the metadata search function in order to increase its accuracy and flexibility. Again, this might result in seemingly inconsistent behaviour. Did we mention that this is a test?

  * Please note that this API is not meant for bulk harvesting of Crossref metadata. If you need such facilities, then please look at our web site for information about our metadata services.

We welcome your ideas for tools that we can provide to help researchers. Please, please, please send comments, requests, queries and ideas to us at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />


 [1]: http://wordpress.org/
 [2]: http://www.movabletype.org/
 [3]: https://web.archive.org/web/20090927174724/http://ocoins.info/
 [4]: https://sourceforge.net/projects/crossref-cite/

{{% /labsection %}}
