---
title: Funding data widget
author: Crossref
date: 2013-07-31

---

{{% labsection %}}

A number of Crossref members have told us that they are struggling with implementing a usable [funding data](/services/funder-registry/)  collection user interface in their content management systems. To that end, we have worked with some user interface consultants to come up with a "[funding data widget](/wp/labs/examples/funders-registry/widget/)" that illustrates some of the considerations one has to take into account when collecting funding data data.

Specifically the [funding data widget](/wp/labs/examples/funders-registry/widget/) illustrates how to:

  1. Autocomplete on [Funder Registry](/services/funder-registry/) names
  2. Handle multiple funders
  3. Handle multiple award numbers
  4. Handle sub-organizations
  5. Handle funding agencies that have the same name, but are in different locations (e.g. "Academy of Sciences")
  6. Handle sub-organizations that have the same name, but different parent organizations (e.g. "Division of Physics")
  7. Query the funding data API to make sure your Funder Registry list is always current
  8. Allow users to manually enter a funding agency if it is not already included in the Funder Registry.

[The funding data widget source is posted on Github](https://github.com/crossref/fundref-widget) and is released under an open source license (MIT). We encourage anybody implementing funding data to either use the Widget as-is or base their own UX on the widget.

As usual, we welcome feedback and snark at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />

&nbsp;

&nbsp;

 [1]: http://labs.crossref.org/wp-/uploads/2013/01/labs_email.png

{{% /labsection %}}
