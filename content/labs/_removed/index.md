---
title: Removed
author: Crossref
date: 2017-01-31

---

{{% labsection %}}

This was a Labs experiment that has now either graduated or been deprecated. Check out the [other stuff we're working on](/labs) too.

{{% /labsection %}}
