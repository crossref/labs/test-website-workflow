+++
title = "Get involved"
date = "2018-05-23"
draft = false
image = "/images/banner-images/village.jpg"
maskcolor = "crossref-darkgrey"
rank = 4
aliases = [
    "/01company/06publishers.html",
    "/01company/17crossref_members.html"
]
[menu.main]
weight = 1
author = "Ginny Hendricks"
+++

We’re only as strong as our links with the community we work with. There are lots of opportunities to participate in the Crossref community, to get together, and to contribute.

## Participate

If you publish scholarly content that is available online and/or you represent organizations who publish, you can become a member to register content and deposit metadata with us. That’s a good starting point, but we’re also keen for members to involve themselves in our advisory and working groups who help to support and give input on our different services and initiatives.

Want to use our metadata rather than deposit it? If so, you can look into becoming an affiliate. This is a global group including – but not limited to – hosting platforms, submission systems, libraries, publishing services companies, metrics and analytics companies, and many others who employ our metadata within their own tools and services to make sure the community can discover and cite content persistently.

## Get together

We run free Crossref LIVE events for our members and the wider community. This incorporates a two-day event each year featuring a mashup day and a conference day, coupled with other LIVE meetings around the globe. Through these we aim to inform and educate members and talk about what we’re working on, but we also want your feedback - what’s happening with you and what do you need from us?

Aside from our LIVE events, we’re also involved in lots of other industry events, so keep an eye out for our team at conferences. We’re happy to speak, present or meet with members.

We want to encourage you to come and join us at our our annual LIVE meeting and other free LIVE events, chat with us on [Twitter](https://twitter.com/CrossrefOrg), comment on [our blog](/blog) (or [offer to write a guest post](mailto:feedback@crossref.org)), meet up with us at [conferences](/events), speak with us on panels and community [webinars](/webinars), or code alongside us at hackathons.

We’d love to hear your thoughts, grumbles, ideas and your gossip, so find the medium or model that suits you best and get involved!
