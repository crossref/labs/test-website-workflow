+++
title = "Community forum"
date = "2021-01-20"
draft = false
author = "Vanessa Fairhurst"
image = "/images/banner-images/forum-starlings.jpg"
parent = "Get involved"
rank = 3
weight = 25
+++

---



Community is at the very core of what we do and who we are. At Crossref we work with a diverse, global community of publishers, libraries, government agencies, funders, researchers, universities, ambassadors, and more from over 140 countries. We are also actively part of the larger scholarly research community, which includes other persistent identifier organizations, metadata users and aggregators, open science initiatives, and others with shared aims and values.  


{{% imagewrap right %}} <a href="https://community.crossref.org/">
   <img src="/images/community-images/Forum-logo.png" width="200px" height="200px" alt="Crossref community forum logo" class="img-responsive">
</a>{{% /imagewrap %}}

We strive to work more openly and collaboratively with you, our community, and so we have established the Crossref Community Forum - [community.crossref.org](https://community.crossref.org/) - using open source discussion platform [Discourse](https://www.discourse.org/about). The forum compliments our existing support process by enabling collaborative problem solving, you can post questions to be answered by Crossref staff or other community members, and share your expertise and experiences across various time zones and languages. Our goal is that you, the Crossref community, will own this space. This is a platform for you to connect and build relationships with others working in scholarly communications, to advance your work with us and shape the future of scholarly infrastructure.


## Why join the Crossref Community Forum?

Becoming a member of the Crossref Community Forum allows you to connect and interact with our vast network of members from across the globe.

- Share issues that you need some help resolving, post a question to the forum in your native language and get help from another community member.
- Easily navigate to FAQs and related support documentation to answer your own questions.
- Share what activities or projects you are working on and get input from others.
- Give us feedback on our plans and help us shape future developments at Crossref
- Test out new tools and services.
- Find out about upcoming events and webinars, and share any you think are of interest to the community.
- Help us identify better ways of working together through Crossref and co-create new materials and projects.
- Make connections with other members, learn what others are working on, and identify opportunities for collaboration.
- Feel more actively involved in the Crossref community.


## How to get involved

Simply head over to [community.crossref.org](https://community.crossref.org/) to set up an account. There’s a useful [How-To guide](https://community.crossref.org/t/about-the-welcome-to-the-crossref-community-forum-category/1026) available in our welcome post, as well as some [Community Guidelines](https://community.crossref.org/t/about-the-welcome-to-the-crossref-community-forum-category/1026) all our members should follow.    

Some general tips are:
- Take a couple of minutes to personalise your profile - add a picture and a bit of a text about yourself - as well as making sure your preferences such as your email address, language, and notification settings are correct.
- ‘Track’ or ‘watch’ any specific categories you are interested in so you don’t miss out on new posts.
- Before creating a new post first look and see if anyone has already raised a similar topic, the answer you are looking for could already be there, or you could add to an existing conversation.
- When making a new post, make sure it’s in the relevant category, and provide as much information as you can so that other members of the community are able to fully understand and help you.
- And of course treat the forum and its members with respect. Don’t post spam or other non-relevant content and make sure that any language, links and images you post are professional. If you see a problem, flag it and we can take the appropriate action.

For more information on how to use the Crossref Community, please refer to our [Code of Conduct](/code-of-conduct/) and our [Terms of Service](https://community.crossref.org/tos). You can also give us feedback or ask us any questions via the forum itself or by emailing us at [feedback@crossref.org](mailto:feedback@crossref.org).
