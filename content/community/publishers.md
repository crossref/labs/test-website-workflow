+++
title = "For publishers"
date = "2020-11-02"
draft = false
parent = "Get involved"
author = "Rachael Lammey"
rank = 4
weight = 2
+++

We work with thousands of publishers from all over the world. No matter what your size, subject area or business model, it doesn’t limit your ability to connect your content with the global network of online scholarly research. Each Crossref member gets to cast their vote to create a board that represents all types of organizations, and members can also stand for election to the board. We have elections each year and have designated seats for different sizes of members.

If you publish one journal or thousands, you’re welcome to join our growing community.

## Apply

Find out more about what [becoming a member involves](/membership/). You may want to join Crossref directly, or you can also consider joining via one of our [Sponsors](/membership/about-sponsors/), who can provide technical, billing, language and administrative support to members (some may charge extra for this).  

Our [membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) can also help with any questions you may have about joining.  

## Participate

### Register your content
Our members join us to [register their content with us](/services/content-registration/) via human or machine interfaces. The metadata we collect supports [a variety of content types](/services/content-registration/#00535), to effectively support the different scholarly content members want to register. By sending us metadata and identifiers related to your publications, you’re making it available to numerous systems and organizations that together help credit and cite the work, report impact of funding, track outcomes and activity, and more.

Because of this, providing robust, accurate metadata helps make your content more discoverable. You can easily track what metadata you have registered by visiting our [Participation Reports](https://www.crossref.org/members/prep/) and entering your organization name. These reports give a clear picture of the metadata registered by a member and are open to all.

### Link references
Crossref is all about rallying the scholarly community to work together. Because of this, reference linking is an obligation for all Crossref members and for all current journal content. [Reference linking](/services/reference-linking/) means hyperlinking to Crossref DOIs when you create your citation list. This makes it possible for readers to follow a DOI link from the reference list of a published work to the location of the full-text document on a member’s publishing platform, building a network infrastructure that enhances scholarly communications on the web.

### Other services
From helping members check content for originality, finding out who has cited the work they have published, to providing a consistent way to show readers the latest status of an article (or any other research object), we’ve developed a growing [range of services](/services/) that support and enhance the specific needs of our members and how they work with their content.

## How your metadata is used

All of the metadata that Crossref collects helps our members’ content be more discoverable. We [make it available in a variety of formats](/services/metadata-retrieval/) so that anyone can come to one place to get information from our thousands of diverse members. Information about your publications is being shared by and used in search engines, collaborative editing and authoring tools, discovery platforms, library databases, by publishers themselves and many, [many other places](/services/metadata-retrieval/user-stories/).

---
You can contact our [membership specialist](mailto:member@crossref.org) with any questions or to get set up, or you can get in touch with our [technical support specialists](mailto:support@crossref.org) for any technical or troubleshooting questions.
