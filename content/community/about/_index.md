+++
title = "About us"
date = "2021-02-26"
draft = false
image = "/images/banner-images/bridge-light-trails.jpg"
aliases = [
    "/01company/02history.html",
    "/01company/16fastfacts.html",
    "/02publishers/14demo.html",
    "/01company/jobs.html",
    "/03libraries/16fastfacts.html"
]
[menu.main]
rank = 4
weight = 4
+++

> We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

We've been progressing towards this vision for two decades so far. And "we" means 17,000+ members from 146 countries, 130+ million records, 600+ million monthly metadata queries from thousands of tools across the research ecosystem. An ecosystem that includes several other foundational infrastructure organisations we collaborate with.

Take a look at our [strategic agenda](/strategy) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability) area aims to make transparent all the processes and procedures we follow to run the operation for the long term, including our financials and our ongoing committment to the Principles of Open Scholarly Infrastructure [POSI](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance) area describes our board and its role in community oversight.

It also takes a strong team. We are a distributed group of 45 dedicated [people](/people) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. Our fascination with expired mints has been described as obsessive by some but we prefer to think of it as a passionate hobby. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide---which we take a bit more seriously than ourselves.

Take a look at our [organization chart](/people/org-chart).


## Our mission

Crossref makes research objects easy to find, cite, link, assess, and reuse.

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

It’s as simple—and as complicated—as that.  

{{% keyword "rally" %}}{{% keyword "tag" %}}{{% keyword "run" %}}{{% keyword "play" %}}{{% keyword "make" %}}

---

<a name="truths"></a>
## How do we work?
These are the Crossref 'truths', the principles that guide everything we do. Read our [truths](/truths) page, with full descriptions for each.

1. Come one, come all
2. Smart alone, brilliant together
3. One member, one vote
4. Love metadata, love technology
5. What you see, what you get
6. Here today, here tomorrow

---

<a name="history"></a>
## How we started

We started in January 2000 with one employee, [Ed Pentz](/people/ed-pentz), as Executive Director. Read about [our history](/community/about/history/) in Ed's words. Since we started we've grown to (as of 2022) [forty five](/people) and over 17,000 members coming from 146 countries.

Read our background story, with an overview of our current services, in this document:

<div data-configid="20568292/63654323" style="width:100%; height:266px;" class="issuuembed"></div>

<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>

Read or download this background story as a PDF in [English](/pdfs/crossref-brochure.pdf), [Spanish](/pdfs/crossref-brochure-eps.pdf) or [Brazilian Portuguese](/pdfs/crossref-brochure-bzpt.pdf).    


---
Please contact our [outreach team](mailto:feedback@crossref.org) with any questions.
