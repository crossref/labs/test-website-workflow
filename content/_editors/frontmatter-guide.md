+++
title = "Frontmatter Primer"
name = "Frontmatter Primer"
date = "2016-08-10T12:00:00-00:00"
draft = false
author = "Zach Anthony"

+++

**This is version 4 of the primer, accurate as of 5 May 2020**

*NB: The guidelines below are only current as of the date above! This is a development site so things are changing constantly.*

## What is "frontmatter"?
Frontmatter is a [Hugo](gohugo.io) term for a set of information at the top of a content file that allows content authors to specify certain characteristics of a page in the site. Typical settings you might find in the frontmatter include the page's title, whether it is published (visible on the live site) or draft (hidden) or a weighting that increases its ranking in search results.

There are three quick rules for frontmatter:

1. Frontmatter should always be the first thing in a file
2. **The order of items in the frontmatter is important!** Placing items in the wrong order can cause them to be ignored, or may even in some cases break the entire site.
3. Proper formatting is very important. Textual information and the `date` should be wrapped in double quotes. Failing to do so could break the entire website.

Here is some sample frontmatter:

```
+++
title = "Our Truths"
date = "2016-05-20T08:40:00-05:00"
draft = false
[menu.main]
name = "Our Truths"
parent = "About Crossref"
weight = 1
+++
```

As you can probably see, frontmatter is fairly self-explanatory. However its not always obvious the effect frontmatter may have on functionality. For example `parent` can be used to configure a different parent page elsewhere in the site (the parent page where a page appears in the main and sidebar navigation, among other things). A [table](#frontmatter-definitions) appears at the end of this page for reference.

## The order of frontmatter is key
The frontmatter example above is actually divided into two sections. The first few items above `[menu.main]` are more general settings and can be in any order, so long as they appear *before* [menu.main].

From `[menu.main]` down everything relates to how the page appears in the navigation. Some items, such as `name`, will simply be ignored if they appear before `[menu.main]`. This is why order is important.

Frontmatter is a bit like actual program code - it must be formatted correctly or it may be misinterpreted. **Any frontmatter that can take free text must be encapsulated by double quotes.** Free text is something like a page title, because it can be set to anything you want. Frontmatter that takes numbers or only specific text does not need to be encapsulated by double quotes, for example `weight` or `draft` (because draft can only = true or false).

## Frontmatter Definitions

### Page related
The items below should **always** appear **above** `[menu.main]` or they are likely to be ignored. For example if `rank` appears under `[menu.main]` it will not be picked up by Algolia, and the rank for the page in question will be defaulted to 1.

Frontmatter term | Required? | Definition/Functionality
--- | --- | ---
`aliases` | Optional | Use to create alternate URL(s) for a page. `aliases` creates a redirect rather than a rewritten URL like `slug` and `url`, so when a user visits the URL configured in `aliases` they still end up on the page desired page at its original URL (usually based on its filename). **This is the preferred method for creating shorter URLs for pages.** See [below](#slug-url-and-aliases) for more info.
`author` | Optional | Use to name the author of the page. If present this will be shown at the bottom of the page. Must appear **above** `[menu.main]`.
`date` | Optional | A date time stamp for when the page was created. This is currently pulled to display as the "last updated on" date so please update when you significantly update a page's content
`description` | Optional | Used to set a unique `meta description` tag for the page. If used will override the default meta description for all pages in the site, which is set in config.toml
`doi` | Optional | Specify the DOI for a page in the format `https://doi.org/10.xxxx/xxxxx`
`draft` | Optional | `draft = false` means a page will be visible to visitors, `draft = true` means it won't. If not present 'true' is assumed.
`identifier` | Optional | Specify a unique identifier for a page IF another page has the same name/title.
`image` | Optional | Specify a banner image to be used at the top of the page, for example `image = "/images/header-keys.jpg"`. Images must already exist in the //images folder. Must be encapsulated.
`mask-color` | Optional | Use to enable a semi-transparent diagonal colored mask that sits over the banner image on the page. Valid options are `crossref-red`, `crossref-blue`, `crossref-yellow` and `crossref-darkgrey`.
`publishdate` | Optional | Use to specify a date and time for a page to be published in the future. A page will remain hidden until this date and time. If present this will be shown at the bottom of the page.
`lastmod` | Optional | Use to specify the date and time a page was last edited. If present this will be shown at the bottom of the page.
`rank` | Optional | Use to force higher or lower rankings in Algolia search results for the specific page. Use a scale of 1 to 5, where 1 is least important and 5 is most important (so the higher the number, the higher the page will appear in Algolia search results).
`slug` | Optional | Use to change the part of the url that is based on the filename. Like `url` it can be used to control the url of a page, but it is always recommended that instead of relying on `url` and `slug` to manipulate the full URL of a page, simply ensure that the actual directory name and file name are in line with what you want the URL to be - or use `aliases` if you just want a shorter URL to publicise for a page.  **In most cases you should use `aliases` instead of `url`.** See [below](#slug-url-and-aliases) for more info.  
`title` | **Yes** | **Used for the page's title as it is shown at the top of the browser window when a user views the page. Must be encapsulated.**
`toc` | Optional | Used to control whether a table of contents is added to the top of the page. This is created automatically and inserted just above the page content. To use add `toc = true` to the frontmatter. Must appear **above** `[menu.main]`.
`url` | Optional | Use to control the full URL of a page rather than just the final segment as with `slug`, but as with `slug` it is always recommended that instead of relying on `url` to manipulate the full URL of a page, simply ensure that the actual directory name and file name are in line with what you want the URL to be - or use `aliases` if you just want a shorter URL to publicise for a page. **In most cases you should use `aliases` instead of `url`.** See [below](#slug-url-and-aliases) for more info.


### Navigation related

Frontmatter term | Required? | Definition/Functionality
--- | --- | ---
`[menu.main]` | Yes | Use `[menu.main]` to ensure a page appears in the main nav.
`name` | Yes | Use to determine the page's title in the nav only. **Name MUST appear AFTER [menu.main] or it will be ignored.** If not present `title` will be used. Must be encapsulated.
`parent` | Optional	| By default a page will always have a "parent page" that that sits just above it in the sitemap; usually this is the `_index.md` file in the same folder. However you can use the `parent` frontmatter to give a page a different parent anywhere else in the site. Use the `title` of the page you want to be the new parent and remember to encapsulate it. Giving a page a different parent will cause it to appear in the rights sidebar nav on that parent page (if present). Be careful of the effects of using `parent` in conjunction with `[menu.main]` as it may cause your page to suddenly appear in the site's main menu.
`weight` | Optional	| Weight determines the order of the page in nav. Lower numbers appear first (so a page with `weight = 1000` will appear above one with `weight = 2000`). Note that numbering can start with 0. For more about how `weight` can be used in practice, see [How to control the ordering of pages in navigation](/_editors/docs-how-to#how-to-control-the-ordering-of-pages-in-navigation).


### Labs pages only

Frontmatter term | Required? | Definition/Functionality
--- | --- | ---
`warningheader` | Optional | Use to populate the header of the Warning box that can appear at the top of Labs page.
`warningtext` | Optional | Use to populate the text of the Warning box that can appear at the top of Labs page.


### Documentation pages only

More information about managing the Documentation section can be found on [this page](/_editors/docs-how-to).

Frontmatter term | Required? | Definition/Functionality
--- | --- | ---
`type` | **Yes** | Used in conjunction with `layout` to control the template (page layout). Should always be set to `documentation`.
`layout` | **Yes** | Used in conjunction with `type` to control the template (page layout). Should always be set to `documentation_single`. (May be expanded in future.)
`documentation_section` | Optional | Used to group Documentation section pages. Not currently used.
`[menu.documentation]` | Optional | Used to control whether a page appears in the Documentation column of the main menu. For more about this see [Show or hide a page from the documentation side menu](/_editors/docs-how-to#show-or-hide-a-page-from-the-documentation-side-menu).
`nomenu` | Optional | If present, set to `true`. Used to control whether a page appears in the Documentation section submenu.


### Blog related

Frontmatter term | Required? | Definition/Functionality
--- | --- | ---
`categories` | Optional | Help to group posts together
`archives` | Optional | Hugo does not automatically create archives by  post year/month means we must create another taxonomy
`authors` | Optional | To generate an 'all posts for this author' page, the authors taxonomy is used

Sample frontmatter
```
---
title: Getting Started with Crossref DOIs, courtesy of Scholastica
author: Anna Tolwinska
date: 2016-04-25
categories:
  - DOIs
  - Identifiers
  - Linking
  - Metadata
  - Persistence
archives:
  - April 2016
authors:
  - Anna Tolwinska
---
```

### Examples and step-by-step process

1) Add a new page called "All about Boaty Mc Boatface" to the top level of the nav. But use just "About Boaty" for its link
on the nav.

```
+++
title = "All about Boaty Mc Boatface"
draft = false
[menu.main]
name = "About Boaty"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

2) Add another new page that is just about Boaty's motor, and make it a subpage of the main page.

```
+++
title = "Boaty's Motor"
draft = false
[menu.main]
name = "Boaty's Motor"
parent = "All about Boaty Mc Boatface"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

3) Specify a banner image

IMPORTANT: upload the banner image to //images folder before specifying in the frontmatter.

```
+++
title = "All about Boaty Mc Boatface"
image = "/images/header-keys.jpg"
draft = false
[menu.main]
name = "About Boaty"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

4) Go back to the All about Boaty page and give it the highest page ranking.
```
+++
title = "All about Boaty Mc Boatface"
draft = false
rank = 4
[menu.main]
name = "About Boaty"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

### `slug`, `url` and `aliases`

#### `slug`
`slug` only works on the last segment of the full URL, which would normally be based on the page’s filename. For example in the URL https://www.crossref.org/news/crossref-members-add-over-a-quarter-million-crossmark-records/ you could change `crossref-members-add-over-a-quarter-million-crossmark-records` to simply `crossref-members-add` if you wished. `slug` **does not work** on pages with a filename of _index.md because they are available at an URL of simply .../ (in other words, just the final slash). `slug` *rewrites* the URL, meaning the page will *only* be available at this new URL based on the `slug`. Sometimes, this isn't what you want. For example, it will "break" the Documentation section left-side nav, which is dependent on the full, original URL of a page to properly place things and highlight the parent page(s).

#### `url`
`url` replaces the entire URL, not just the final segment as `slug` does. For example you could change the URL of https://staging.crossref.org/news/crossref-members-add-over-a-quarter-million-crossmark-records/ to simply https://www.crossref.org/crossref-members-add.

Like `slug`, `url` *rewrites* a page's URL so that it will accessible at the new URL based on `url`. As with `slug` that means that it will "break" the documentation section left-side nav. Again, this is typically not what you really want.

#### `aliases`
`aliases` works much like `url` but it actually sets up a *redirect* rather than a *rewrite*, which preserves the original URL of the page and for example ensures that the left-side navigation in Education continues to work. **This is the recommended route for creating a shorter URL for any page in the site, within or outside of the Documentation section.**

As you can probably tell from the name `aliases` supports multiple aliases for any page.

Configure aliases like this if you have just one alias:

```
+++
aliases = “/suffix-generator”
+++
```

or if you want to setup multiple aliases:

```
+++
aliases = [
		“/suffix-generator”,
		"/suffix-thingamie",
		"/suffix-anotheralias"
	]
+++
```

Note the leading slashes in for example `/suffix-generator`. This sets up an alias from the root of the site, in other words https://www.crossref.org/suffix-generator. You can alternately include a longer path in an alias, like https://www.crossref.org/education/suffix-generator/ if you’d like.
