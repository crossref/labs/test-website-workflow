+++
title = "Managing the Documentation Section"
name = "Managing the Documentation Section"
date = "2022-11-16"
draft = false
author = "Zach Anthony"
toc = true

+++

## Introduction

This document contains information specific to the [Documentation section](/documentation) section of the site.

## How to use frontmatter in the Documentation section

### Set the page template

The `layout` and `type` frontmatter are very important to the Documentation section and must always be set as shown below:

<pre>
type = "documentation"
layout = "documentation_single"
</pre>

These tell Hugo what page layout template to use, and where to find it.

### Group pages into sections (future)

`documentation_section = [xxx]` is not used at present but provides a taxonomy for future reference. In other words it makes it possible to automatically build a menu of all the pages in a 'section' by referencing this. It is technically optional.


## How to control the ordering of pages in navigation
The  section contains a hierarchical subnav at the left side of every page. The order of items in this subnav can be managed using the `weight` frontmatter (this is also how the ordering of pages is controlled in the main nav, and in the right side subnav that you'll see on some pages).

**Simply put, a page with `weight = 1` will come before a page with `weight = 2` in the nav.** Also note that that `weight` is relative to the directory that contains a file.

To make it easier to add pages or change ordering later, you should generally try to use larger numbers however. For example if you have a page with `weight = 01000` and another page with `weight = 02000` you'll see its much easier to add another page between these in the nav by just giving it `weight = 01500`. If the pages had weights of 1 and 2, then to add a new page you would need to change the weight of the second page. Where you have a large numbers of pages this becomes a lot of work, and invites errors.



<pre>
\root directory
	_index.md (`weight = 1000`))
	page-A.md (`weight = 2000`)
	page-B.md (`weight = 4000`)
	page-C.md (`weight = 3000`)
	\subdirectory
		_index.md (`weight = 1000`)
		page-D.md (`weight = 3000`)
		page-E.md (`weight = 2000`)
	page-F.md (`weight = 6000`)
	page-G.md (`weight = 5000`)  
</pre>

When the site is built, the navigation menu linking to these pages will look like this because of each page's `weight`:

<pre>
	- Index
	- Page A
	- Page C
	- Page B
		- Index
		- Page E
		- Page D
	- Page G
	- Page F
</pre>

Note that /subdirectory/page-D.md has a `weight` of 03000, but this is only relative to the other pages within /subdirectory.


## How to control where a documentation page appears in menus

Top-level pages within the documentation section can appear within several menus:

* The site wide **main menu** (via the hamburger)
* The **in-page** menu on the [ Homepage](/documentation)
* The **documentation side menu** embedded within each content page of the documentation section [for example](/documentation/membership/benefits)

It is possible to control which menus a page will appear in using the methods outlined below.

### Show or hide a page from the main menu

The presence of `[menu.main]` **and its related elements** in the frontmatter control whether any page appears in the main site menu. More info on this can be found in the [Frontmatter Guide](/_editors/frontmatter-guide#navigation-related).

If `[menu.main]` and/or any of its related elements, usually some combination of `name`, `parent`, `weight` or `identifier`, appear in the frontmatter then a page will appear in the main menu. Note that even if `[menu.main]` *does not* appear but any of the related elements *do*, the page will still appear in the main menu.

### Show or hide a page from the in-page menu on the main  page

A similar method to the one above can be used to control whether a page appears in the in-page menu on the main  page, using `[menu.documentation]`. If `[menu.documentation]` and any of its related elements appear in the frontmatter then the page will appear in the in-page menu.

Note that unlike `[menu.main]` if the related elements like `identifier` appear in the frontmatter but `[menu.documentation]` does not, this will not cause the page to appear in the in-page menu. This is because these elements will always be assumed to be related to `[menu.main]`.

If any of these elements appear more than once without being related to separate `[menu.main]` and `[menu.documentation]` sections, it may cause unexpected results. Note too that order is important: the related elements **must** appear under the related `[menu.xxxx]` frontmatter.

### Show or hide a page from the documentation side menu

To ensure a page does not appear in the documentation side menu (at the left of every page in the documentation section) simply add `nomenu = true` to the frontmatter for that page.

### Samples

Hide a page from the main menu but show it everywhere else: the lack of `[menu.main]` and its related elements means the page **will not** appear in the main site menu, and the lack of `nomenu = true` means it **will** show in the side menu.

<pre>
+++
title = "The importance of metadata"
date = "2020-04-08"
draft = false
author = "Author B. Author"
type = "documentation"
layout = "documentation_single"
documentation_section = ["metadata"]
[menu.documentation]
parent = ""
weight = 10000
rank = 4
+++
</pre>

Show a page *only* in the main site menu: the lack of `[menu.documentation]` means the page **will not** appear in the in-page menu on the main site menu, and the presence of `nomenu = true` means it **will not** show in the side menu.

<pre>
+++
title = "Crossref documentation for Funders"
date = "2020-04-08"
draft = false
author = "Author B. Author
type = "documentation"
layout = "documentation_single"
documentation_section = ["documentation"]
nomenu = true
[menu.main]
parent = ""
rank = 4
weight = 0
+++
</pre>

Show a page in the main site menu and the in-page menu on the main  page, but not in the  side menus: the presence of `nomenu = true` means it **will not** show in the side menu.

<pre>
+++
title = "Crossref documentation for Funders"
date = "2020-04-08"
draft = false
author = "Author B. Author"
type = "documentation"
layout = "documentation_single"
documentation_section = ["documentation"]
nomenu = true
[menu.main]
parent = ""
rank = 4
weight = 0
[menu.documentation]
parent = ""
weight = 10000
rank = 4
+++
</pre>


## The "snippets" shortcode

The snippet shortcode was developed for the  section to provide editors with an easy way to show a single block of common content across multiple pages. We've called these blocks of common content "snippets". Because of the way this works, making a change to the snippet then changes it everywhere it is shown across the site.

### How to place a snippet in a page

Snippets are Hugo shortcodes, so they work much like other [shortcodes](/_editors/shortcodes-reference). Simple drop the below into any part of the body of a page anywhere on the site:

<pre>
&#123;&#123;< snippet "/[directory]/[filename]" >&#125;&#125;
</pre>

Replace `[directory]/[filename]` with the location of the snippet file itself, for example `/content/snippet-sources/cited-by.md`. Note that as of the writing of this documentation, all snippets are kept in the `/content/_snippet-sources` directory.

The contents of the `/_snippet-sources` directory are hidden from Google and Algolia (the site search engine) because of the leading underscore in the directory name. Both Google and Algolia will index the pages that pull in the snippet content, and so we want to prevent them from indexing *both* the original snippet and all its occurrences on other pages.

When you place a snippet shortcode in a page and commit this to the git repository, the site will rebuild, and you will see that the page now includes the text of the snippet.


### Creating snippet files

Snippets are just like any other markdown file in the site **except that they should not have any frontmatter**.


## Configuring anchor tags (including how to number them)

Typically each subheading on a page in the Docs section should have an anchor tag. These provide an easy way for users to bookmark or share links to specific content on a page, rather than just the page itself. 

**Because this is a static site, the anchor tags must be added manually.**

A fully formed subheading with an anchor tag might look something like this:

```
#### Setting up the plugin and credentials <a id=‘00800’ href=‘#00800’><i class=‘fas fa-link’></i></a>
```

The `####` is Markdown; here it designates this as a 4th level subhead (an `<h4>` in HTML). Any level of heading can have an anchor tag. 

The `<a>` tag itself contains both an `id` and `href`; the repetition is for better compatibility across browsers and devices. The `<i>` tag is what produces the link icon that is the actual clickable target of the `<a>` tag. 

With regard to the numbering itself, its best to follow the system: a 5 digit number, with leading zeros if required, that is unique. Multiple anchors on the same page should be numbered in sequence if possible. However, anything is technically valid.

**Remember to never change an `id/href`, as doing so could break user's bookmarks.**