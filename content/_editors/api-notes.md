+++
title = "Crossref REST API Notes"
draft = false
author = "Bruce Murray"
date = "2019-03-15"
tags = ["help"]
+++

## Interactive API calls

Similarity Check does has serveral API calls that happen live on the site

- 2 calls to get the counts to create a percentage from
- 1 to get the actual report DOI and corresponding simcheck link
- 1 to lookup the member

The reports are generated from multiple 'paged' calls fetching blocks of data at a time as per the spec.

The shortcode is simcheckjs.html

## API calls at build time

### Members reports
The 2 big members with open/closed references reports fetch data from the prefetched BIG api call run on every build.
The shortcode is members-list-refs.html. The script to run the big data dump is /scripts/list-members
_[EDIT 6th June 2022 - all references are now open by default with the March 2022 board vote to remove any restrictions on reference distribution]._

### Dashboard
The dashboard does run a query for each field but only on every build.
Shortcode is dasboard.html and then the data file that drives it is /data/dashboard/dasboard-fields.json

### Total Results

Total Results shortcode total-results.html

Runs only at site build and calls the api based on a passed parameter.

Called 29 times on the following pages

1. ./about/_index.md
1. ./about/history.md
1. ./get-started/metadata-plus.md
1. ./services/metadata-retrieval/rest-api-documentation.md
1. ./committees/membership-and-fees.md
1. ./jobs/_index.md
1. ./_editors/shortcodes-reference.md
1. ./_editors/markdown-examples.md
1. ./_editors/test-reports.md
1. ./help/metadata-snapshots.md
1. ./reporting/members-with-closed-references.md
1. ./reporting/members-with-open-references.md
1. ./fees/_index.md
1. ./blog/2016/2016-03-04-python-and-ruby-libraries-for-accessing-the-crossref-api.md
1. ./blog/2018/2018-12-13-phew-its-been-quite-a-year.md
1. ./blog/2018/2018-05-31-preprints-update.md
1. ./blog/2018/2018-04-26-how-good-is-your-metadata.md
1. ./community/funders.md
1. ./community/working-for-you.md
1. ./community/librarians.md
1. ./community/developers.md
1. ./membership/_index.md

## Config file settings

### Offline Mode

There is the option to use a config parameter OfflineMode. If this is set to false or not present then the build time API calls will still all take place. If it is set to true then only the big data dump build takes place. All of the other calles will result in 'OFFLINE MODE ACTIVE' text replacing the numbers.

```
OfflineMode = true
```

### API Url and polite query email

```
apiURL = "https://api.crossref.org/v1"
apiUser = "&mailto=ginny@crossref.org"
```
