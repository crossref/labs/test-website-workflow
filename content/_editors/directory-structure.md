+++
date = "2019-03-15"
draft = false
title = "Directory structure"
author = "Zach Anthony"

+++

## Site hierarchy and file naming conventions

**Note: this file was updated on 2019-03-15 by Zach Anthony (Cazinc). References to multilingual setup were removed as they are no longer relevant. Other amends were made to reflect the actual nature of the live site.**

Below is the site hierarchy for everything that lives below `cr-site/content`.

Note that the directory structure does not necessarily reflect menu and navigation labels of the site itself. This is because the URIs of our pages should from useful resource identifiers, whereas the labels used to navigate our site should reflect activities and tasks that a site user wants to accomplish. This also allows us to keep URIs relatively short while site navigation text can continue to be longer and more expressive as needed. **Refer to the `parent` tag in a page's frontmatter to determine where it will exist in the menu, not where it sits in the directory structure.

So for example the Annual Reports page  can be found in directory `cr-site/content/annual-report`, but it in the menu that site visitors see it is a *child* of the About Us page (About Us > Annual Reports). Among other things this means that the page is available at the shorter URI of [/annual-report/](/annual-report/) instead of `/about/annual-report/`.

TK - the above is no longer a good example (GH March 2022)

### General rules


- filenames must be all lowercase
- markdown files must always end in in the extension `.md`
- files that only end in the suffix `.md` will be assumed to be in US English.
- words in filenames must be divided by a dash. For example: `crossmark-best-practices.md`
- the top level `content` directory must only contain 1 file- `_index.md`. Everything else immediately under `content` must be a subdirectory
