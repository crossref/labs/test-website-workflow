+++
title = "Name of the Service"
draft = false
author = "Ginny Hendricks"
date = "2016-11-25"
tags = ["help", "template"]
+++

*No heading required as the page has a title*

Go straight into starting with a "tagline" as the first sentence (not a heading). It doesn't necessarily have to be an official one but a catchy sentence or two that covers the main *purpose* of the service. It's sometimes good to start a statement of purpose with an active positive word like "**Get** (e.g. get all your metadata from one place)", or **Connect** (e.g. connect  your content with the rest of the world's), etc.

*[a short animation about the service will be embedded here on the right]*

## Benefits
I.e. how does it help? List a sentence per key audience with which tasks it will assist with and how (benefit)

## How it works
This could be a more technical description of what we actually do. If relevant, include a sub-heading for...

### Common tasks

## How to participate
How to join, how to sign up, what they need on their end. Should link to get-started guide when available.

## Obligations & fees
This is what was previously called "requirements" or "policies" or "terms & conditions". Be friendly. There are no requirements or policies anymore, just "obligations". Also include any limitations (like e.g. preprints are not included in Cited-by)

## Best practice
This can be a link to the get started guide in folder /best-practice, OR you can summarize here and then link through.

## FAQs
If there is a full section in {FAQs}(/faqs) then link to that section anchor but otherwise list them here, e.g.:
* How do I report errors in the metadata (linked to a page on that)

---

Put the three underscores above to make a linebreak (so no heading, go straight into the Please contact our [membership specialist](mailto:member@crossref.org) with any questions or to get set up, or consult other users on our forum [community.crossref.org](https://community.crossref.org) or open a ticket with our [technical support specialists](mailto:support@crossref.org) for any technical or troubleshooting questions.
