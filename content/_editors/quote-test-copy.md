+++
title = "Quote test 2"
date = "2019-02-24"
draft = false
author = "Ginny Hendricks"
+++

Just testing embeds of XML files hosted on gitlab.

[Instant xml file download](https://gitlab.com/crossref/schema/raw/master/examples/grantid_full_example.xml?inline=false)

Or in another world, this may magically work for us (GitLab snippet embeds)?:

<script src="https://gitlab.com/akaemmerle/example-project/snippets/1718255.js"></script>




## We use Crossref metadata to…

New line entered after and before shortcodes:

{{% quotecite "Gavin Reddick, Researchfish" %}}
Add funding info from publications to researchers’ portfolios, and report the publications as arising from the grant; to validate the data provided by universities; and we use the license and embargo period information to help understand the open access status of publications.
{{% /quotecite %}}


No new line, but 2 paragraphs:

{{% quotecite "Polina Grinbaum, PLOS" %}}Link references on our journal platforms, pull citations statistics for our Article-Level Metrics and ensure we are publishing unique science. Crossref metadata is vital to our everyday operations and the discovery of the research we publish.

Link references on our journal platforms, pull citations statistics for our Article-Level Metrics and ensure we are publishing unique science. Crossref metadata is vital to our everyday operations and the discovery of the research we publish.{{% /quotecite %}}

Standard, i.e. no new lines, only 1 paragraph:

{{% quotecite "Ulf Kronman, National Library of Sweden" %}}
Enhance and correct the metadata delivered to us, just with a correct DOI.
{{% /quotecite %}}

---

You don't have to sign up to anything in order to use our REST API. That means we don't necessarily know who is using it, although we see millions of hits every day. If you are using it in your projects and would like to share, please [let us know](mailto:feedback@crossref.org) and we'll feature you on this page.
