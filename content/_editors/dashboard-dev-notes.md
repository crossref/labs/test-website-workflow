+++
title = "Dashboard Development Notes"
draft = false
author = "Bruce Murray"
date = "2019-05-10"
tags = ["help"]
+++

## Dashboard Development Notes

2 parts
part 1 - get the data
/content/json/data/dashboard/contenttypes.md
contains a shortcode -

&#123;&#123;< dashboard/data/content-types-data >&#125;&#125;
that gets the data from an api call. The resulting output ends up in the built website here
/json/data/dashboard/contenttypes/index.json
end or part 1

part 2 - visualising the data
/dashboard-new
contains lots of shortcodes, one of which is
&#123;&#123;< dashboard/content-types >&#125;&#125;
this does the d3 magic with the data file from part 1

This keeps the data out of the shortcodes and keeps the building of the data separate.