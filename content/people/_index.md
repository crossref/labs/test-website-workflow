+++
title = "Our people"
date = "2021-04-20"
draft = false
type = 'Staff'
author = "Rosa Clark"
aliases = [
    "/01company/04staff.html"
]
[menu.main]
parent = "About us"
rank = 3
weight = 20
+++

We are a distributed team of dedicated people who mostly like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. Our fascination with expired mints has been described as obsessive by some but we prefer to think of it as a passionate hobby. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide, which we take a bit more seriously than ourselves.

Click through to read more about each of us (most of us hate having photos taken so please do be kind). And take a look at our [organization chart](/people/org-chart) too, if you like; you can tell we are very hierarchical.
