+++
title = "Product Manager"
date = 2023-03-11
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Michelle Cancel"
caption = "“Flower Garden”, Euskirchen, Germany, by Mina-Marie Michell via Pexels"
[menu.main]
weight = 3
parent = "Jobs"
+++

## Product Manager

**Location:** Remote and global (with minimum overlap with the US Eastern time zone)  
**Remuneration:** €80,000 – €105,000 or local equivalent, depending on experience  
**Reports to:** Director of Product  
**Application timeline:** We will receive applications until March 31st.


Crossref is a not-for-profit membership organization that exists to make scholarly communications better by providing metadata and services that make research objects easy to find, cite, link, assess, and reuse. We’re passionate about providing open foundational infrastructure for the scholarly communications ecosystem - and we’re continuously evolving our tools and services in response to emerging needs. 

We are a [small team](/people/) with a big impact, and we’re looking for a creative, technically-oriented Product Manager to join us in improving scholarly communications. Reporting to the [Director of Product](/people/org-chart/),  Product Managers at Crossref are expected to work across multiple products and services based on organisational goals (/strategy) and manage projects across the different teams in the organization and with community partners like the [Public Knowledge Project](https://pkp.sfu.ca). 

Unlike traditional SaaS Product Management, being community-led means the Product Manager is not always the expert in what they are developing, so the ability to plan, research, convene, listen, and facilitate consensus-based decisions, is a key factor to being successful in this role at Crossref.

Some examples of the diverse set of tools, services, features and functionality we support include ORCID auto-update, simple API-driven tools to help members register records with us, API endpoints and interfaces to help the community retrieve and use the metadata we store, Crossmark and the Funder Registry, and we have more in the pipeline supported by our R&D, engineering, and infrastructure services teams. Does this sound like your sort of thing? 

### Key responsibilities  

- Manage all aspects of the life cycle for one or more key areas within the Crossref ecosystem
- Coordinate work across teams at Crossref by communicating ideas and writing project plans, gathering and assessing feedback and using that to drive decision-making
- Seek, absorb and articulate community input through advisory groups, convening meetings, and reviewing data.
- Integrate usability studies, user research, system investigations, and ongoing community feedback into requirements
- Influence the product development strategy by communicating priorities based on organisational needs and community feedback
- Define goals, methods and metrics for the adoption of features and functionality to track success
- Coordinate and direct working groups made up of community members and users
Promote adoption directly with the community as well as develop relationships with key community influencers & strategic partners
Working with R&D to test concepts internally and with the community to inform and support Crossref’s service(s)
Evangelize areas of focus to rally people and resources behind ideas and ambitions critical to success

### About you  

- You think in terms of the big picture, but can work closely with others to explain context and deliver on the details
- You can turn a range of inputs into solid action plans and achievable chunks of work
- You have an understanding and experience of complex workflow systems, writing clear specifications and working with APIs
- You care about open infrastructure and want to make scholarly communications better
- You do whatever it takes to make your product and community successful and love to problem solve, whether that means writing a QA plan, tracking down the root cause of a user’s frustration or working with our R&D team to spin up and test a POC in response to an idea
- You are passionate about understanding community needs, working transparently, eliciting advice and feedback openly and advising on community calls
- You communicate with empathy and exceptional precision
- You can convey and encapsulate strategic (and technical) concepts in presentations verbally, visually, and textually. 
- You have experience working with developers. You are technical enough to discuss with engineers critical questions about architecture and product choices 
- You are motivated to continually improve products based on community feedback
- You are self-motivated with a collaborative and can-do attitude and enjoy working with a small team across multiple time zones 
- You’re comfortable facilitating and chairing meetings and working groups
- You maintain order in a dynamic environment, managing multiple priorities 
- You have 5+ years of product management experience with internet technologies and/or equivalent experience in the research publishing arena

### What it’s like working at Crossref

We’re about [45 staff](/people/) and now ‘remote-first’ although we have optional offices in Oxford, UK, and Boston, USA. This means that we support our teams working asynchronously and to flexible hours. Some international travel will likely be appropriate when it’s safe to do so, for example to in-person meetings with colleagues and members, but in line with our [travel policy](/blog/rethinking-staff-travel-meetings-and-events/). We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out the [organisation chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

### Thinking of applying?

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

[Click here](https://100hires.com/j/F3ThF3Q) to apply. Please submit your CV/Resume and Cover Letter. We require both documents in order to be considered for the role.

Please strive to submit your application by March 31st, 2023.

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

Thanks for your interest in joining Crossref. We are excited to hear from you!
