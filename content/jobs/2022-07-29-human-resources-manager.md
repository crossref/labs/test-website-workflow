
+++
title = "Human Resources Manager"
date = 2022-07-29
draft = false
image = "/images/banner-images/hr-job-wildflower-meadow.jpg"
author = "Lucy Ofiesh"
caption = "“Flower Garden”, Euskirchen, Germany, by Mina-Marie Michell via Pexels"
weight = 1
parent = "Jobs"
+++


{{% divwrap red-highlight %}} Applications for this position closed 2022-08-23. {{% /divwrap %}}


## Join Crossref as our **Human Resources Manager**. It'll be fun. No, really!

- **Location:** Remote
- **Salary:** Between 75K-90K USD (or currency equivalent) depending on experience and location. Benchmarked every two years. Excellent benefits.
- **Reports to:** Director of Finance & Operations
- **Application timeline:**  We will begin to review applications the week of August 22nd


## About the position

Human resources is often seen solely as an operational necessity, and it is, but at Crossref it is also pivotal to our culture and our journey toward a remote-first, global, and transparent community-led organization. Sometimes it is even fun! We like to run staff activities online and occasionally in person, and we try to live by our commitments to diversity, equity, and inclusion.

We are looking for a Human Resources Manager with a collaborative style who will support and evolve the working experience for everyone at Crossref. Reporting to the Director of Finance and Operations, the Human Resources Manager is a central role within the organization.

This position helps to recruit, retain, and support our 40+ staff across the world. As a remote-first organization, this position helps to develop and implement practices that provide a consistent, equitable employment experience for our team. The ideal candidate should have experience working with staff in multiple countries.

In the day to day, this position helps recruit new staff, administers payroll and benefits, and troubleshoots employment issues for staff.

We are a small team and this position is a good fit for someone looking to work across a multinational organization. This position sits on the finance & operations team, but works closely with every part of the organization.  

## Key responsibilities

### Recruiting new staff
- Support the new hire process, posting the job description, tracking and reviewing job applicants. Ensure a robust, inclusive recruitment process
- Support the interview process as needed
- Onboard all new hires

### Administering the employment experience
- Administer payroll (biweekly for US staff, monthly for EU, Indonesia, Ireland, Kenya, UK staff)
- Keep up to date with applicable employment law in countries and regions where we employ staff. As a US legal entity, we comply with US Federal employment laws. We comply with state laws where we have staff (currently 9 states in the US). We comply with regional or country level employment laws in France, Germany, Guernsey, Indonesia, Ireland, Kenya, and the UK. This list changes as staff are hired in additional locations. This position will work with additional contracted HR expertise in the UK, EU, and through the PEOs we work with in Indonesia and Kenya.
- Select and manage PEO contracts
- Administer renewal and open enrollment for US employee benefits including health, dental, FSA, HRA and vision; review communication materials; audit employee elections and respond to employee issues
- Administer pension and 401(k) programs, including non-discrimination testing and 5500 filings for US 401(k)
- Support annual financial and security audits

### Helping shape employment policies and supporting a healthy working culture
- Ensure that the policies on diversity, equity and inclusion are put into action
- Support the performance evaluation process by consulting with management, tracking information and updating documentation as needed
- Benchmark positions using compensation software and give advice to managers to ensure consistency in pay practices
- Maintain employee handbooks and processes identifying areas for enhancement and making recommendations for change
- Identify and administer required and optional training opportunities  
- Contribute ideas to Crossref leadership to help us progress further on our journey to a global and remote-first organization
- Lead the charge with our commitment to open and transparent operations, creating and updating content on our website regarding HR practices

## About you

This role is for a hands-on manager who wants to help set direction and is comfortable handling day-to-day administration. You might not have all the skills we list below, but we encourage you to apply if this sounds like you:

- Has experience, roughly 5-7+ years, working in an HR role.
- Is experienced in and knowledgeable about contemporary best practices for diversity, equity, and inclusion
- Has experience working with remote-first teams across various countries/regions and international payroll and benefit programs
- Has managed payroll and benefits
- Experience working in the scholarly communications space is nice to have but not required
- Some experience managing a budget or providing a perspective on the financial impact of a decision
- Enjoy helping staff find solutions to problems they may have with their employment experience. For example, this could be helping to navigate issues with benefits, or developing policies that support working from home.
- Attention to detail and an organized approach
​​- Problem solving ability
- Excellent communicator
- Comfortable with digital tools and technology


## Location & travel requirements

This is a remote position. The Finance & Operations team currently has members working on the east coast of the US, primarily in the Boston-area. As a remote-first organization, we are not bound to a specific location.

In general, Crossref is committed to lowering its environmental impact by reducing unnecessary travel. We also recognize that some people may be unable to travel and the ability to travel is not a requirement for this position. That being said, if you are able to travel, it would be for no more than 5-10 days a year (possibly international).


## What it’s like working at Crossref
We’re a little more than 40 staff and now ‘remote-first’ although we have optional offices in Oxford, UK, and Boston, USA. We are dedicated to an open and fair research ecosystem, and that’s reflected in our ethos and staff culture. We like to work hard, but we have fun too! We take a creative, iterative approach to our projects and believe that all team members can enrich the culture and performance of our whole organization.

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation, and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

## About Crossref
Crossref makes research outputs easy to find, cite, link, and assess. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put scholarly content in context. It’s as simple—and as complicated—as that.

Since January 2000 we have grown from strength to strength and now have over 17,000 members across 146 countries, and thousands of tools and services relying on our metadata.

## To apply

Please send a cover letter and a CV via email by August 19th, 2022 to Lucy Ofiesh via jobs@crossref.org.


## Equal opportunities commitment

Crossref is an equal opportunity employer. We believe that diversity and inclusion among our staff is critical to our success as a global organization, and we seek to recruit, develop and retain the most talented people from a diverse candidate pool.

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.
