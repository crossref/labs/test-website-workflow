
+++
title = "Principal R&D Developer"
date = 2022-06-17
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Geoffrey Bilder"
weight = 1
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed 2022-07-05. {{% /divwrap %}}

## Come work at Crossref as a **Principal R&D Developer**. It'll be fun!

Help us research, prototype, and build new services for our members and the community.

- **Location:** Remote. But we are looking for somebody in +/- 2 UTC Time zones (e.g. Brazil, Ireland, UK, Scandinavia, Central Europe, West/Central Africa)
- **Salary:** Between  80K-124K EUR (or equivalent) depending on experience and location. Benchmarked every two years.
- **Benefits:** Competitive.
- **Reports to:** Director of Technology and Research.
- **Closing date:**  July 5, 2022

## About the position

We are hiring a Principal R&D Developer to help prototype and develop new web-based tools and services.

Crossref operates a service that connects thousands of scholarly publishers, millions of research articles, and research content and serves an increasingly diverse set of communities within scholarly publishing, research, and beyond.

The Crossref R&D team focuses on the kinds of research projects that have allowed Crossref to make transformational technology changes, launch innovative new services, and engage with entirely new constituencies. Some Illustrious projects that had their origins in the R&D group include:

{{% imagewrap right %}}{{< figure src="/images/blog/2021/labs-logo-ribbon.svg" align="right" width="100%" >}}{{% /imagewrap %}}

- DOI Content Negotiation
- Similarity Check (originally CrossCheck)
- ORCID (originally Author DOIs)
- Crossmark
- The Open Funder Registry
- The Crossref REST API
- Linked Clinical Trials
- Event Data
- Grant registration
- ROR

We're looking for a developer who will thrive taking messy, vague, and often contradictory requirements and working with the community to refine those requirements into a practical implementation plan. This process involves a lot of listening, writing, and prototyping. And it also requires a lot of iteration.

You will report to the Director of Technology and Research and work on a team that includes the Head of Strategic Initiatives and another Principal R&D developer.

## About you

There are a lot of skills that we are looking for, but we don't expect to find a [purple unicorn](https://www.urbandictionary.com/define.php?term=Purple%20Unicorn). Instead, our primary criterion is that you have a track record of working with communities to deliver innovative projects using a variety of tools, languages, frameworks, and development paradigms. We are looking for someone who:

- Is an expert in one or more programming languages (e.g. Python, Kotlin, Java, Clojure).
- Wants to learn new skills and work with a variety of technologies.
- Relishes working with metadata.
- Has experience delivering web-based applications using agile methodologies.
- Groks mixed-content model XML.
- Groks RDF.
- Groks REST.
- Understands relational databases (MySQL, Oracle).
- Is self-directed, a good manager of their own time, with the ability to focus.
- Enjoys working with a small, geographically dispersed team.
- Can see a solo project through or collaborate on a larger team.
- Has deployed and maintained Linux-based systems.

### Bonus points for:

- Experience with data science techniques and tools
- Experience with machine learning, deep learning, natural language processing
- Experience building tools for online scholarly communication.
- Experience with a variety of programming language paradigms (OO, Functional, Declarative).
- Experience contributing to open-source projects that are not their own.
- Experience with ElasticSearch, Solr, or Lucene.
- Experience with front-end development (HTML, CSS, React, Angular or similar).
- Has worked on standards bodies.
- Experience with public speaking or willingness to build this skill.

### Responsibilities

{{% imagewrap left %}}{{< figure src="/images/blog/2021/creature1.svg" align="right" width="100%" >}}{{% /imagewrap %}}


The Principal R&D Developer will report to the Director of Technology & Research. They will be responsible for prototyping and developing new Crossref initiatives and applying new Internet technologies to further Crossref’s mission to make research outputs easy to find, cite, link, assess, and reuse.

The post will work with the Head of Strategic Initiatives, Head of Development, Head of Infrastructure, and Director of Product to develop and implement new services – taking ideas from concept to prototype and, where appropriate, create and deploy production services.

The Principal R&D Developer may represent Crossref at conferences and in industry activities and projects. They will also play an active role in developing industry and community technical standards and help develop technical guidelines for Crossref members.

Working with the Head of Development and the Head of Infrastructure, the Principal R&D Developer will ensure that new services are designed with a robust and sustainable architecture.  The Principal R&D Developer will actively engage with technical representatives from Crossref’s membership, the library community, scholarly researchers, and broader Internet initiatives.

And please note that this is not a back-office position. On the contrary, we believe that it is vital that the entire technical team develops an understanding of our members, the broader community, and their needs. Without this kind of empathy, we cannot add value to our services. As such, you will also find yourself working closely with the product and outreach teams.

## What it's like working at Crossref

We're about [40 staff](/people) and now 'remote-first' although we have optional offices in Oxford, UK, and Boston, USA. We are dedicated to an open and fair research ecosystem, and that's reflected in our ethos and staff culture. We like to work hard, but we have fun too! We take a creative, iterative approach to our projects and believe that all team members can enrich the culture and performance of our whole organization.

{{% imagewrap right %}}{{< figure src="/images/blog/2021/creature2.svg" align="right" width="100%" >}}{{% /imagewrap %}}

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation, and we only continue to grow. While we won't have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

## Location & travel requirements

This is a remote position. The R&D team currently has members working in the US (Brooklyn), Ireland (Dublin), and France (Nîmes). As a remote-first organization, we are not bound to a specific location. Ideally, for this role, we are looking for someone based in time zones +/- 2 UTC.

In general, Crossref is committed to lowering its environmental impact by reducing unnecessary travel.

We also recognize that some people may be unable to travel and the ability to travel is not a requirement for this position.

That being said, if you are able to travel, it would be for no more than 7-14 days a year (possibly international).

## Salary

Between 80K-124K EUR (or equivalent) depending on experience and location. Benchmarked every two years. Excellent benefits.

## To apply

Send a cover letter and a CV via email by July 5th, 2022 to:

Lindsay Russell

jobs@crossref.org

## Equal opportunities commitment

{{% imagewrap left %}}{{< figure src="/images/blog/2021/creature3.svg" align="right" width="80%" >}}{{% /imagewrap %}}

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.
