+++
title = "Product Manager - APIs"
date = 2017-12-20
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Jennifer Lin"
weight = 3
rank = 4
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed as of 2018-05-04 {{% /divwrap %}}

## Come work with us as a **Product Manager for APIs**. It'll be fun!
<br>
- **Location:** Oxford, UK; Lynnfield, MA; or remote<br>
- **Reports to:** Director of Product Management<br>
- **Benefits:** Competitive<br>


### Summary

Crossref makes research objects easy to find, cite, link, assess, and reuse.

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

It’s as simple—and as complicated—as that.

We are a small team that makes a big impact, and we’re looking for a creative, technically-oriented Product Manager to join us in improving scholarly communications, building “roads” to make the next big research discovery possible.

### Key responsibilities

* Manage all aspects of the product life cycle for Crossref’s suite of APIs through product requirements, feature prioritization, implementation, and measurement
* Articulate and influence product strategy focusing on business objectives and user experience
* Represent the voice of the Crossref API users in the continued improvement of these APIs and bring back the insight to our engineering and outreach teams
* Identify, grow, and manage our developer relations program for the community of Crossref API users

### About you

* You can think in terms of the big picture, but deliver on the details
* You have a nose for great products, and advocate for new features with qualitative and quantitative reasoning
* You do whatever it takes to make your product and team successful, whether that means writing a QA plan or hunting down the root cause of a user’s frustration
* You can break down large projects into granular milestones and track progress on them
* You can turn incomplete, conflicting, or ambiguous inputs into solid action plans
* You communicate with empathy and exceptional precision
* You’re comfortable working with developers. You are technical enough to ask engineers good questions about architecture and product decisions alike
* Beyond just shipping new products, you obsess about continuous product improvement
* You can maintain order in a dynamic environment, independently managing multiple priorities
* You are self-driven with a collaborative and can-do attitude and enjoy working with a small team across multiple time zones
* You champion agile development best practices and are able to teach, mentor, and coach teams in adopting this methodology.
* You have experience with JSON and can develop a simple application in any modern language such as Python, Clojure, etc.
* Bonus: you have experience developing APIs and/or web applications that use APIs

**Salary:** Competitive depending on skills and expertise. Excellent benefits.

### To apply

To apply, please send your cover letter, resume, and at least one sample of an effective product specification or epic/story development to [Jennifer Lin](mailto:jlin@crossref.org).


### About Crossref

Crossref is a not-for-profit membership organization representing members of all stripes (commercial publishers, scientific societies, university presses, open access, etc.). Crossref currently has just over 30 staff in the US and UK and France, yet it combines the small, intimate atmosphere of a technical startup, with the financial stability and strong international presence of a major commercial organization. We do important stuff, but we have a lot of fun doing it.

---
Please contact [Jennifer Lin](mailto:jlin@crossref.org) with any questions.
