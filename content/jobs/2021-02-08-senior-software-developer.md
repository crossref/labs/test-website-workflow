+++
title = "Senior Software Developer"
date = 2021-02-08
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Joe Wass"
weight = 3
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed. {{% /divwrap %}}

## Senior Software Developer

**Location:** Remote <br>
**Closing date:** Friday 5th March 2021 <br>
**Salary:** Competitive


Crossref makes research objects easy to find, cite, link, assess, and reuse. As an open infrastructure organization, we ingest and distribute metadata from our 15,000+ member organizations worldwide, ensuring community collaboration in everything that we do. Our work helps achieve open research and open metadata goals, for the benefit of society. This is a time of considerable change for the Crossref community, and you can help shape our future.

The software development team is responsible for maintaining, operating and building the services that enable the workflows of our members and the wider research community. We have a deep understanding not only of technology, but also the needs of our diverse community.

You will contribute primarily to our **Java** and **Kotlin** codebases, with the option of also contributing to our Clojure codebases. We don't expect you to be an expert in all of these, but you should have in-depth knowledge of at least one compiled, JVM, or Functional language (Java, Clojure, Kotlin, Scala, C#, Go etc). You will build primarily back-end services and APIs.

You will specify, design and implement improvements, features and services. You will have a key voice in discussions about technical approaches and architecture. You will always keep an eye on software quality and ensure that the code you and your colleagues produce is maintainable, well tested, and of high quality.

## Key responsibilities

 - Understand Crossref’s mission and how we support it with our services. Collaborate with external stakeholders when needed.
 - Pursue continuous improvement across legacy and green-field codebases.
 - Work flexibly in multi-functional project teams, especially in partnership with the Product team, to design and develop services. Ensure that solutions are reliable, responsive, and efficient.
 - Produce well-scoped, testable, software specifications.
 - Implement and test solutions using Kotlin, Java and other relevant technologies.
 - Work closely with the Head of Software Development to solve problems, maintain and improve our services and execute technology changes.
 - Provide code reviews and guidance to other developers regarding development practices and help maintain and improve our development environment.
 - Identify vulnerabilities and inefficiencies in our system architecture and processes, particularly regarding cloud operations, metrics and testing.
 - Communicate proactively with membership and technical support colleagues ensuring they have all the information and tools required to serve our users.
 - Openly document and share development plans and workflow changes.
 - Be an escalation point for technical support; investigate and respond to occasional but complex user issues; help minimize support demands related to our systems; be part of our on-call team responding to service outages.

## About you

We don't expect a successful candidate to tick all of these boxes right away!

 - An expert senior developer with experience in Java, Kotlin, Clojure or related languages. Experience in Spring or similar frameworks.
 - Have a proven track record of picking up new technologies.
 - Experienced with continuous integration, testing and delivery frameworks, and cloud operations concepts and techniques.
 - Familiar with AWS, containerization and infrastructure management using tools like Terraform.
 - Some experience with Python, JavaScript or similar scripting languages.
 - Experience working on open source projects.
 - Able to quickly understand, refactor and improve legacy code and fix defects.
 - Experience with, or a working understanding of, XML and document-oriented systems such as Elastic Search.
 - Experience building tools for online scholarly communication or related fields such as Library and information science, etc.  
 - Comfortable collaborating with colleagues or stakeholders in the community.
 - Ability to create and maintain a project plan.
 - Self-directed, a good manager of your own time, with the ability to focus. Comfortable being part of a distributed team.
 - Curious and tenacious at learning new things and getting to the bottom of problems.
 - Strong at written and verbal communication skills, able to communicate clearly, simply, and effectively.
 - Outstanding at interpersonal relations and relationship management. Comfortable collaborating with colleagues across the organisation.
 - Assuming that international travel ever becomes possible again, the applicant should expect they will need to travel internationally to work with colleagues for about 5-10 days a year.

## About the team

The software development team is distributed. Most issue tracking and all new code is open source. We strongly believe in open scholarly infrastructure and openness at all stages of the software development lifecycle. As a membership organization we keep closely in touch with our users, and encourage our developers to be familiar with our community. The Development, Product and Infrastructure teams are tightly knit and we work in 2 week sprints.

## To apply

This position is full time and, as for all Crossref employees, location is flexible. The Crossref team is geographically distributed in Europe and North America and we fully support working from home. It would be good to have a minimum 3-hour overlap with the UTC-0 time zone.

To apply, please send your cover letter and resume to Lindsay Russell at jobs@crossref.org. Even if you don’t think you have all of the right experience, we’re really excited to hear from you.

Crossref is an equal opportunity employer. We believe that diversity and inclusion among our staff is critical to our success as a global organization, and we seek to recruit, develop and retain the most talented people from a diverse candidate pool.  

Crossref is committed to a policy of non-discrimination and equal opportunity for all  employees and qualified applicants for employment without regard to  race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law.  Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.
