+++
title = "Technical Support Contractor"
date = 2023-03-01
draft = false
image = "/images/banner-images/hr-job-wildflower-meadow.jpg"
author = "Isaac Farley"
caption = "“Flower Garden”, Euskirchen, Germany, by Mina-Marie Michell via Pexels"
[menu.main]
weight = 1
parent = "Jobs"
+++

## Request for services: contract technical support

Come and work with us as an independent **Technical Support Contractor**. It’ll be fun!

**Location:** Remote  

## About the contractor role
The Technical Support Contractor will work closely with our Member Experience team, part of Crossref’s Outreach team, an eighteen-strong distributed team with members across Africa, Asia, Europe, and the US. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re aiming for a more open approach to having conversations with people all around the world - including within our growing [community forum](https://community.crossref.org/), which the right candidate will help us expand, in multiple languages. We’re looking for a Technical Support Contractor to provide front-line help to our international community of publishers, librarians, funders, researchers and developers on a range of services that help them deposit, find, link, cite, and assess scholarly content.

We’re looking for an independent contractor able to work remotely. There is no set schedule and contractors bill hours monthly.

## Scope of work
- Replying to and solving community queries using the Zendesk support system.
- Using our various tools and APIs to find the answers to these queries, or pointing users to support materials that will help them.
- Working with colleagues on particularly tricky tickets, escalating as necessary.
- Working efficiently but also kindly and with empathy with our very diverse, global community.

## About the team
You’ll be working closely with nine other technical and membership support colleagues to provide support and guidance for people with a wide range of technical experience. You’ll help our community create and retrieve metadata records with tools ranging from simple user interfaces to robust APIs.

## About Crossref
Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

We’re a small but mighty group working with over 17,000 members from 146 countries, and we have thousands of tools and services relying on our metadata. We take our work seriously but usually not ourselves.

---

## How to respond  

**We're currently looking for contract help, so responses are accepted on a rolling basis**:

A statement of interest that includes:
- Examples of similar work (and/or your CV)
- References from previous work
- Hourly rate

Please send your response, statement of interest, and resume to: [jobs@crossref.org](mailto:jobs@crossref.org).
