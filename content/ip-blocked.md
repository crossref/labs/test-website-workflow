+++
title = "Blocked IP"
date = "2019-02-14"
author = "Geoffrey Bilder"
Weight = 15
rank = 2
+++

Hello. We're delighted that you're using our REST API and we hope you find it useful.

Unfortunately, your usage also seems to be causing some problems with the API, which, in turn, is preventing other users from accessing it as well.

As such, we have temporarily blocked your access. Do contact us at [support@crossref.org](mailto:support@crossref.org). We’ll be happy to help you optimize your system to make the best, unobtrusive use of our API.

And do take a look at the etiquette section of our API documentation for more tips on using the system "politely".
