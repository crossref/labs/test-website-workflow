+++
title = "Similarity Check 2019 transition"
date = "2019-06-05"
draft = false
author = "Amanda Bartell"
rank = 1
parent = "FAQs"
weight = 100
+++

Here are the answers to some common questions about transitioning Similarity Check from Turnitin to be serviced directly by Crossref. These FAQs apply to Similarity Check participants who signed up prior to June 2019 (about 1500 members).


## Q: Why is the transition happening?

**A:** We know that Similarity Check is a critical service for our members, and we wanted to improve people’s experience of using it. So, in consultation with members, we’ve strengthened the service by updating our relationship with Turnitin to consolidate all the components of the service under our care and stewardship.

The new arrangement is a very good thing for our members. It puts us in a strong position to improve support and drive future improvements of the system. Representing our collective membership, we’ve agreed [better terms](/services/similarity-check/terms) than what people have today and what members would get acting individually.


## Q: What specifically will change for me?

**A:** The transition is only a change for Similarity Check participants who signed up prior to June 2019. For these members (about 1500 of you), there are five specific changes:

1. Members’ Similarity Check service agreement will be with us and not Turnitin.
2. Per-document checking fees will be invoiced by us, and not Turnitin. They’ll be included in members’ regular invoices, reducing international transfer fees for many.
3. The first 100 documents checked each year will be free of charge.
4. Turnitin will operate as a vendor for Crossref. We’ve already agreed a range of additions to their technology roadmap. Turnitin will remain responsible for fixing any bugs or technical issues with the system, but we’re in a stronger position to ensure these are fixed quickly.
5. Users will get training and on-boarding support from Crossref. This will cover both how to use the interface and how to interpret the results.


## Q: When I fill out the form and accept the Crossref agreement, how long will it be before I receive my final invoice from Turnitin?

**A:** You’ll officially move off the Turnitin agreement and over to the Crossref agreement on the 25th of the month that you filled out the transition form and clicked-through to accept the new terms. You’ll receive your final bill from Turnitin at the end of that month too.


<table>
  <tr>
   <td>Date you fill out the transition form
   </td>
   <td>Date your Turnitin agreement finishes and your Crossref agreement starts
   </td>
   <td>Date you’ll receive final invoice from Turnitin
   </td>
   <td>Date of next invoice from Crossref
   </td>
  </tr>
  <tr>
   <td>1st June - 24th June 2019
   </td>
   <td>25th June 2019
   </td>
   <td>By end of June 2019 (for per-document checking fees up to 25th June 2019)
   </td>
   <td>January 2020 (for service subscription renewal and per-document checking fees from June 26th to December 31st.)
   </td>
  </tr>
  <tr>
   <td>25th June - 24th July 2019
   </td>
   <td>25th July 2019
   </td>
   <td>By end of July 2019 (for per-document checking fees up to 25th July 2019)
   </td>
   <td>January 2020 (for service subscription renewal and per-document checking fees from July 26th to December 31st.)
   </td>
  </tr>
  <tr>
   <td>25th July - 24th August 2019
   </td>
   <td>25th August 2019
   </td>
   <td>By end of August 2019 ((for per-document checking fees up to 25th August 2019)
   </td>
   <td>January 2020 (for service subscription renewal and per-document checking fees from August 26th to December 31st.)
   </td>
  </tr>
  <tr>
   <td>25th Aug - 31st Aug 2019
   </td>
   <td>25th September 2019
   </td>
   <td>By end of September 2019 (for per-document checking fees up to 25th September 2019)
   </td>
   <td>January 2020 (for service subscription renewal and per-document checking fees from September 26th to December 31st.)
   </td>
  </tr>
</table>


## Q: When will I be billed for Similarity Check?

**A:** You’ll continue to receive your annual subscription for Similarity Check in your annual membership subscription invoice in January. You’ll also receive your annual per document checking fees invoice at the same time.


## Q: I’ve prepaid Turnitin for 2019 - what happens with my pre-payment?

**A:** Turnitin will refund you the remaining amount.


## Q: You said that under the new agreement, I’d be able to check my first 100 documents for free. When does this start?

**A:** Your 100 free documents will start when you’ve officially moved onto the new terms with Crossref. This will happen on the 25th of the month that you accepted the new terms - so if you filled out the transition form between 1-24th June, you’ll be on the new agreement from June 25th, and your 100 free documents will start then. Your free documents will reset each calendar year after this so you can enjoy 100 free document checks every calendar year following.


## Q: You said that Crossref will provide help in using Similarity Check under the new agreement. How do I get help?

**A:** After the 31st August, when the transition period ends, Crossref will be responsible for helping you understand how to use Similarity Check and interpret the results. We’ll be providing updated support materials, and our [technical support specialists](mailto:support@crossref.org) will be able to answer your questions. Extra staff have been recruited and trained to help you with your Similarity Check questions.

If there are any technical problems with the tool itself (for example, a bug), Turnitin will continue to be responsible for this, but we’ll be working closely with them to ensure that any technical issues that our users experience will be sorted out as quickly as possible.


## Q: What if I don’t want to move onto the Crossref terms?

**A:** Turnitin will cease all existing Similarity Check agreements on the 31st August. If you’ve transitioned over to the Crossref terms before then, you’ll seamlessly move over to us. If you’d like to negotiate a new agreement directly with Turnitin and outside of Crossref, you are of course free to do this, but you’ll need to have completed this before 31st August if you don’t want to lose access.


## Q: What is the relationship between Crossref and Turnitin?

**A:** Under the former arrangement (from 2008 to June 2019), Crossref acted as a ‘broker’ having agreed reduced-rate access to Turnitin’s iThenticate software for our members. Under the current arrangement (from June 2019), we are directly responsible for supporting and administering the Similarity Check including helping editors interpret the results, and we formally contract with Turnitin as a technology provider.


## Q: Who has access to our full-text content?

**A:** Nothing has changed in respect of access to your full-text content. The new terms mean that we set you up, ensure you have the correct metadata to be eligible, we invoice you and support you. But, as before, Crossref does not have access to any member’s full-text content. This remains the sole purview of the service provider, Turnitin and is covered in Schedule 1 of the terms.


## Q: Did Crossref get feedback from members in changing the new arrangement?

**A:** Yes, the desire for Crossref to have more control over the service came directly from members. Over the last year we have worked closely with the Similarity Check Advisory Group, the Crossref Board, and a group of ‘super users’. Within these groups, a mix of members’ business, legal, compliance, research integrity, and editorial staff were involved. The feedback rounds were numerous and all the feedback has been incorporated into standard terms that are suitable for all members.


## Q: How do I request an amendment to the terms?

**A:** The new terms have been extensively reviewed and revised by members and are standard terms suitable for all. We are not able to negotiate special terms for any member as it’s important to our transparency principle that we maintain a level-playing-field so that no member has better or worse arrangement than any other. This goes for all Crossref services. We are of course open to finessing the terms if needed—-for everyone—so definitely welcome your feedback and ideas but we won’t be able to adjust the terms before late 2020.

---
More questions? Please consult other users on our forum [community.crossref.org](https://community.crossref.org) or open a ticket with our [technical support specialists](mailto:support@crossref.org?subject=Question about the Similarity Check transition).
