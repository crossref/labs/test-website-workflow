## Test workflow for Static Site PID Generator using a version of the Crossref Website

This is a repository that contains a test workflow for the Crossref website using the [Static Site PID Generator](https://gitlab.com/crossref/labs/static-page-id-generator/) in CI. 


**FYI**: The doi prefix is set in [config.yml](https://gitlab.com/crossref/labs/test-website-workflow/-/blob/main/config.yml?ref_type=heads) and is set to `10.5555`

## A few things to consider:
* The scripts to create a DOI currently only run on when files are pushed to main. Check the `gen-pid` step in [.gitlab-ci.yml](https://gitlab.com/crossref/labs/test-website-workflow/-/blob/main/.gitlab-ci.yml?ref_type=heads#L13) for this.
* The script adds a DOI to the modified files that it is tracking and also creates a xml file that is submitted to Crossref. The xml file is not committed to the repository. It is added to the [artifacts](https://gitlab.com/crossref/labs/test-website-workflow/-/artifacts) of the job. If there are any problems with submission, please check the xml file for any issues of the latest `gen-pid` job.

## Add a DOI to a new page
* In the staging branch, add a page. The script tracks everything under the repository
* In the frontmatter, 
  * if it is a **toml** file, i.e., the frontmatter looks something like this:
    ```
    +++
    draft = false
    title = "Title"
    date = "Date"
    author = "Author"
    +++
    ```
    Add `x-version = "0.0.0"` to the frontmatter, so it looks like this:
    ```
    +++
    draft = false
    title = "Title"
    date = "Date"
    author = "Author"
    x-version = "0.0.0"
    +++
    ```
  * if it is a yaml file, i.e., the frontmatter looks like this:
    ```
    author: "Author"
    date: 2023-10-29
    draft: false
    title: "title"
    ```
    Add `x-version: 0.0.0` to the frontmatter, so it looks like this:
     ```
    author: "Author"
    date: 2023-10-29
    draft: false
    title: "title"
    x-version: 0.0.0
    ```
* Commit the file
* Merge files to main. Once the files are merged, i.e. pushed to main, the job will start the script which will:
   * Create unique IDs for tracked files
   * Generate urls for these files
   * Generate xml deposit files
   * Deposit them
   * Register the DOIs
   * Add the DOIs back to the file
* Once that's done, the following jobs will run:
  * The CI will generate the website
  * Push the website to Gitlab pages and eventually, you will see the DOI on the page
* To check:
  * In the main branch, go to the page that had been added. Copy and paste the DOI to the browser, it should take you to the page on the website. The DOI can sometimes take a few minutes longer to appear on the page. Occasionally, refreshing it helps. The update does take a little time...about 5 - 10 minutes.