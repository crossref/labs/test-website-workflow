## {{ sitename }}

This is a link report generated on {{ report_date }} for the site {{ sitename }}

## Site Summary

### Page summary
**Total number of pages:** {{ all_pages|length }}  
**Number of pages with general URI errors:** {{ page_error_counter|length }}  
**Number of pages with broken links:** {{ page_link_error_counter|length }}  

### URL summary

All URLs found on site.

**Number of URLs found:** {{ all_urls|length }}  
**Number of unique URLs found:** {{ unique_urls_counter|length }}  
**Number of broken URLs found:** {{ broken_urls|length }}  
**Number of unique broken URLs found:** {{ unique_broken_urls_counter|length }}  

### Link summary

These are the things that users are likely to click on.

**Number of links found:** {{ all_links|length }}  
**Number of unique links found:** {{ unique_links_counter|length }}  
**Number of broken links found:** {{ broken_links|length }}  
**Number of unique broken links found:** {{ unique_broken_links_counter|length }}

### Nonlink summary

These are the things that also use URLs

**Number of nonlinks found:** {{ all_nonlinks|length }}  
**Number of unique nonlinks found:** {{ unique_nonlinks_counter|length }}  
**Number of broken nonlinks found:** {{ broken_nonlinks|length }}  
**Number of unique broken nonlinks found:** {{ unique_broken_nonlinks_counter|length }}


## Broken template links

These broken links occur so frequently that they are likely to be defined in templates. They should be easy to fix and will make a big difference to this report.

{% for outlier_id in outlier_broken_links %}  
**Number of times the error occurs:** {{ outlier_broken_links[outlier_id]|length }}  
**Link text:** [{{ outlier_broken_links[outlier_id][0].anchor_text }}]({{ outlier_broken_links[outlier_id][0].href }})  
**Link url:** `{{ outlier_broken_links[outlier_id][0].href }}`  
**Example page:** `{{ outlier_broken_links[outlier_id][0].page }}`  
**Error ID:** `{{ outlier_id }}`  
{% endfor %}



## Broken regular links

These appear to be one-off broken links. The are grouped by the page they occur on, Pages with the most errors are listed first.




## Inconsistently named links

These links point to the same url, but their anchor text is different. This might be OK, but it also could be a sign of inconsistent link naming.


