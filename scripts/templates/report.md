## {{ sitename }}

This is a link report generated on {{ report_date }} for the site `{{ sitename }}`.

Links listed in red are internal links. They should be fixed first.

See the documentation at the end of this report for further explanations of the elements of the report.

## Site Summary

> {{ site_summary_section }}

## Frequently broken links

These broken links occur frequently and appear on multiple pages. They are likely to be defined in templates or occur in text that gets displayed on multiple pages (e.g. blog posts, category pages, etc.) They should be easy to fix and will make a big difference to this report.   

---

{{ template_link_report_section }}


## Broken links by page

These broken links are grouped by the page they occur on. Pages with the most errors are listed first.

---

{{ page_report_section }}


## Inconsistently named links

These links point to the same url, but their link text is different. This might be OK, but it also could be a sign of inconsistent link naming.

---

{{ link_consistency_section }}

## Other errors by page

---

> coming soon

## Suppressed pages/errors

The following rules were used to whitelist pages.


{% for i in whitelist %}
- `{{ i }}`      
{%- endfor %}



This resulted in the following pages or error_ids being supressed. 

{% for rule, count in ignored_errors.items() %}
- `{{ rule }}` ({{ count }})
{%- endfor %}

The whitelist can be modified by editing this page:

`{{ whitelist_url }}`

## Documentation

### General

Internal links are listed in red.

Excessive "noise" in an error report can be counterproductive and cause people to start ignoring the report. Sometimes an error simply cannot be fixed (perhaps it is a false positive, an external link that cannot be repaired or an example designed to illustrate an issue). This report supports a whitelist that allows you to suppress errors. You can whitelist entire pages or even individual errors by listing either the page's URL or the error's `Error ID` in the site's whitelist, located here:   

`{{whitelist_url}}`.   

Be careful with this feature. It can make you lazy.

### Site summary

There can be many kinds of URL-based errors in a web site. Some are immediately visible to the user and are therefor of high priority to fix. In this report these are called "broken links". They focus exclusively on URLs that are broken (return a 404 error) in anchor tags.

But there are other URL errors that occur as well. For instance a link in an anchor might time-out because the site is slow. Or it might return a 403 (permission denied)  because one needs authorisation to access the page. Or a URL used for DNS-caching might be broken. These errors are generally less visible and are segregated to the "other errors" sections of the report. 

Note that the same error may occur in multiple locations. This is why we include both the counts of "total" and "unique" errors of each type.   



