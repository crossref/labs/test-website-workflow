**Number of pages:** {{ total_pages }}    
**Number of pages with errors:** {{ total_pages_with_errors }}    
**Number errors:** {{ errors }}   
**Number unique errors:** {{ unique_errors }}    
**Number of broken links:** {{ broken_links }}   
**Number of unique broken links:** {{ unique_broken_links }}   
**Number of other errors:** {{ other_errors }}    
**Number of unique other errors:** {{ unique_other_errors }}    

