### URL summary

All URLs found on site.

**Number of URLs found:** {{ all_urls }}  
**Number of unique URLs found:** {{ unique_urls }}  
**Number of broken URLs found:** {{ broken_urls }}  
**Number of unique broken URLs found:** {{ unique_broken_urls }}  

