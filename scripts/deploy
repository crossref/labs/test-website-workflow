#!/usr/bin/env bash
# bail with error if anything goes wrong
set -e
# Want to make an atomic update with mv, but without getting rid of our rsync directory
#
# Define steps
CR_REMOTE_TEMP_DIR="$(ssh cr-site@$CR_WEB_HOST 'mktemp -d')"
STAGE_NEW_CONTENT="cp -a $CR_RSYNC_PATH/. $CR_REMOTE_TEMP_DIR"
BACKUP_CURRENT_CONTENT="mv $CR_SITE_PATH /tmp/site_backup"
INSTALL_NEW_CONTENT="mv $CR_REMOTE_TEMP_DIR $CR_SITE_PATH"
CLEANUP="rm -rf /tmp/site_backup && rm -rf $CR_REMOTE_TEMP_DIR"
BACKOUT="mv /tmp/site_backup $CR_SITE_PATH"
INSTALL_SCHEMAS="ln -s \$(realpath $CR_SCHEMAS_PATH) \$(realpath ~/$CR_SITE_PATH/public/schemas)"
INSTALL_STATUS="ln -s \$(realpath $CR_STATUS_PATH) \$(realpath ~/$CR_SITE_PATH/public/status)"

echo "Swapping old content for new content." | ./scripts/slacktee -p
rsync -r --delete-after --quiet $TRAVIS_BUILD_DIR/public cr-site@$CR_WEB_HOST:$CR_RSYNC_PATH

# Now we start executing stuff on $CR_WEB_HOST

# copy rsynched data to a temporary directory so that we can preserve rsynced data and make
# future copies to server more efficient
ssh cr-site@$CR_WEB_HOST "$STAGE_NEW_CONTENT"
# Swap staged content into site and cleanup. Restore original content if anything fails.
ssh cr-site@$CR_WEB_HOST "$BACKUP_CURRENT_CONTENT && $INSTALL_NEW_CONTENT && $CLEANUP || $BACKOUT "
# create soft link to huge schemas directory
ssh cr-site@$CR_WEB_HOST "$INSTALL_SCHEMAS"
if [ "$TRAVIS_BRANCH" == "development" ]; then
	# create soft link to dynamic status directory on testweb
	ssh cr-site@$CR_WEB_HOST "$INSTALL_STATUS"
fi
echo "Finished swapping old content for new content." | ./scripts/slacktee -p
