#!/usr/bin/env python
import sys
import os

from habanero import Crossref
import requests
import json
import codecs
from retrying import retry

cr = Crossref(mailto="ginny@crossref.org")


def all_items(fun, filter, offset=0, limit=500):
    res = fun(filter=filter, offset=offset, limit=limit)
    if len(res["message"]["items"]) < limit:
        return res["message"]["items"]
    return res["message"]["items"] + all_items(
        fun, filter=filter, offset=(offset + limit), limit=limit
    )


@retry(stop_max_attempt_number=5)
def cr_results(uri):
    try:
        res = requests.get(uri, header=HEADER)
        json_results = res.json()
    except Exception as e:
        print(e)


def summary(members):
    results = []
    for member in members:
        try:
            if member["flags"]:
                member["flags"]["deposits-some-public-references"] = False
                if member[
                    "prefix"
                ]:  # a bug in API wehere prefix is sometime set to null https://github.com/CrossRef/rest-api-doc/issues/291
                    for prefix in member["prefix"]:
                        if prefix.get("public-references", False):
                            member["flags"]["deposits-some-public-references"] = True
                            break

                member["flags"]["deposits-some-closed-visible-references"] = False
                if member[
                    "prefix"
                ]:  # a bug in API wehere prefix is sometime set to null https://github.com/CrossRef/rest-api-doc/issues/291
                    for prefix in member["prefix"]:
                        if prefix.get("reference-visibility", False) == "closed":
                            member["flags"][
                                "deposits-some-closed-visible-references"
                            ] = True
                            break

                member["flags"]["deposits-some-open-visible-references"] = False
                if member[
                    "prefix"
                ]:  # a bug in API wehere prefix is sometime set to null https://github.com/CrossRef/rest-api-doc/issues/291
                    for prefix in member["prefix"]:
                        if prefix.get("reference-visibility", False) == "open":
                            member["flags"][
                                "deposits-some-open-visible-references"
                            ] = True
                            break

                results.append(member)
            else:
                print(json.dumps(member, indent=4))
        except Exception as e:
            print(json.dumps(member, indent=4))
            sys.exit()

    return results


DASHLOC = "data/dashboard"
report_name = "member-list"

site = sys.argv[1]

members_with_some_public_references = all_items(cr.members, filter={})
print(len(members_with_some_public_references))

report = summary(members_with_some_public_references)

fp = os.path.join(site, DASHLOC, report_name + ".json")
with codecs.open(fp, "w", encoding="utf-8") as outfile:
    json.dump(report, outfile, sort_keys=True, indent=4, ensure_ascii=False)
